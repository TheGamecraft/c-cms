@extends('layouts.public.main')

@section('content')
    <div class="page-header header-filter clear-filter" data-parallax="true" style="background-image: url({{'"'.$page->banner.'"'}});">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <div class="brand ck-content">
                        {!! $page->header !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            <div class="section ck-content">
                {!! $page->body !!}
            </div>
        </div>
    </div>
@endsection
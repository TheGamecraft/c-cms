<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Schedule;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        clogNav('consulte l\'inventaire');
        return view('admin.inventory.index',[ 'items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function management()
    {
        clogNav('consulte la gestion de l\'inventaire');
        return view('admin.inventory.management');
    }
    
    public function booking()
    {
        $items = Item::all();
        $schedules = Schedule::all()->where('type','regular')->sortBy('date');

        $filtered = collect();

        foreach ($schedules as $day) {
            if ($day->date >= date('Y-m-d')) {
                $filtered->push($day);
            }
        }
        
        return view('admin.inventory.booking',[ 'items' => $items, 'schedules' => $filtered]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,$periode,$niveau)
    {
        $schedule = Schedule::find($id);
        $id_to_modify = "passet";
        $qt_to_add = 0;
        $periode_item = 'n'.$niveau.'_p'.$periode.'_item';

        if (isset($schedule->$periode_item)) {
            $array_items = explode("-",$schedule->$periode_item);
            for ($i=0; $i < count($array_items); $i++) { 
                $array_item_id = explode(":",$array_items[$i]);
                
                if ($array_item_id[0] == request('add')) {
                    $id_to_modify = $i;
                    $qt_to_add = $array_item_id[1];
                }
            }
            if ($id_to_modify === "passet") {
                array_push($array_items,request('add').":".request('qt'));
            } else {
                $toadd = $qt_to_add+request('qt');
                $array_items[$id_to_modify] = request('add').":".$toadd;
            }
        } else {
            $array_items = [];
            array_push($array_items,request('add').":".request('qt'));
        }

        $final_items = implode("-",$array_items);

        $schedule->$periode_item = $final_items;

        $schedule->save();

        return redirect('/admin/inventory/'.$id.'/'.$periode.'/'.$niveau);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$periode,$niveau)
    {
        $schedule = Schedule::find($id);

        $periode_item = 'n'.$niveau.'_p'.$periode.'_item';
        $items = collect();
        if (isset($schedule->$periode_item)) {
            $items_array = explode("-",$schedule->$periode_item);

            foreach ($items_array as $item_array) {
                if ($item_array != "") {
                    $item_array_ex = explode(":",$item_array);
                    $this_item = Item::find($item_array_ex[0]);
                    $this_item->quantity = $item_array_ex[1];
                    $items->push($this_item);
                }
            }
        }

        return view('admin.inventory.show',['schedule' => $schedule, 'periode' => $periode, 'niveau' => $niveau, 'items' => $items, 'dispo_item' => $schedule->getInventory($periode)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!isset($schedule->data[$periode])) {
            dd('Null');
        } else {
            dd($schedule->data[$periode]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$periode,$niveau)
    {
        $schedule = Schedule::find($id);
        $remove = [request('remove')];
        $periode_item = 'n'.$niveau.'_p'.$periode.'_item';

        if (isset($schedule->$periode_item)) {
            $array_items = explode("-",$schedule->$periode_item);
            $array_items = array_diff($array_items,$remove);
        } else {
            $array_items = [];
            $array_items = array_diff($array_items,$remove);
        }

        $final_items = implode("-",$array_items);
        $schedule->$periode_item = $final_items;
        $schedule->save();
        
        return redirect('/admin/inventory/'.$id.'/'.$periode.'/'.$niveau);
    }
}

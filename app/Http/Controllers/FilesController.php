<?php

namespace App\Http\Controllers;

use App\GoogleDriveFile;
use Illuminate\Http\Request;
use Redirect;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.files.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function guide()
    {
        // if (\App\GoogleDriveFile::checkConfig())
        // {
        //     $dirID = \App\GoogleDriveFile::findByPath('.Privé/.Staff/.Guide');
        //     $dir = collect(\Storage::cloud()->listContents($dirID->id,false))->sortBy('name');
        //     return view('admin.files.guide',['dir' => $dir]);
        // }
        // return redirect('/admin')->with('error','Google Drive n\'est pas configuré');
        return Redirect::away('https://drive.google.com/drive/folders/1iL_mMKMMAzyR203ni3HCPrU-ohCAel5S');
    }

    public function instruction()
    {
        if (\App\GoogleDriveFile::checkConfig())
        {
            return view('admin.files.Google Drive.index',['folder' => \App\GoogleDriveFile::where('path','=','.Privé/.Staff/.Instruction')->first()->id, 'mode' => 'folder']);
        }
        return redirect('/admin')->with('error','Google Drive n\'est pas configuré');

    }

    public function cadet()
    {
        if (!\App\GoogleDriveFile::checkConfig())
        {
            return redirect('/admin')->with('error','Google Drive n\'est pas configuré');
        }
        return view('admin.files.Google Drive.index',['folder' => \App\GoogleDriveFile::where('path','=','.Privé/.Cadet')->first()->id, 'mode' => 'folder']);
    }

    public function staff()
    {
        if (!\App\GoogleDriveFile::checkConfig())
        {
            return redirect('/admin')->with('error','Google Drive n\'est pas configuré');
        }
        return view('admin.files.Google Drive.index',['folder' => \App\GoogleDriveFile::where('path','=','.Privé/.Staff')->first()->id, 'mode' => 'folder']);
    }

    public function etamas()
    {
        if (!\App\GoogleDriveFile::checkConfig())
        {
            return redirect('/admin')->with('error','Google Drive n\'est pas configuré');
        }
        return view('admin.files.Google Drive.index',['folder' => \App\GoogleDriveFile::where('path','=','.Privé/.ETAMAS')->first()->id, 'mode' => 'folder']);
    }

    public function officier()
    {
        if (!\App\GoogleDriveFile::checkConfig())
        {
            return redirect('/admin')->with('error','Google Drive n\'est pas configuré');
        }
        return view('admin.files.Google Drive.index',['folder' => \App\GoogleDriveFile::where('path','=','.Privé/.Officier')->first()->id, 'mode' => 'folder']);
    }

    public function publique()
    {
        if (!\App\GoogleDriveFile::checkConfig())
        {
            return redirect('/admin')->with('error','Google Drive n\'est pas configuré');
        }
        return view('admin.files.Google Drive.index',['folder' => \App\GoogleDriveFile::where('path','=','.Publique')->first()->id, 'mode' => 'folder']);
    }
}

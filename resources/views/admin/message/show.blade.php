@extends('layouts.admin.main')

@section('content')
    <div class="col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-header">
            <strong>{{ $message->title}} par {{ \App\User::find($message->user_id)->fullname() }} publié le {{$message->updated_at}}</strong>
            </div>
            <div class="card-body card-block">
                {!! $message->body !!}
            </div>
          </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Afficher un message</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Message/Afficher</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
   <script>
    tinymce.init({
      selector: '#msg_body',
      branding: false,
      menubar: 'edit view format'
    });
    </script>
   <div class="log"></div>
@endsection
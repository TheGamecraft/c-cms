<div class="row w-100">
    <div class="col-7">
        <nav class="float-left">
            <ul>
                <li>
                @if(App\Config::getData('media_twitter'))
                    <li><a href="{{ App\Config::getData('media_twitter')}}" class="icon"><i class="fab fa-twitter mr-2"></i><span class="label">Twitter</span></a>@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/media_twitter"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</li>
                @endif
                @if(App\Config::getData('media_facebook'))
                    <li><a href="{{ App\Config::getData('media_facebook')}}" class="icon"><i class="fab fa-facebook mr-2"></i><span class="label">Facebook</span></a>@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/media_facebook"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</li>
                @endif
                @if(App\Config::getData('media_instagram'))
                    <li><a href="{{ App\Config::getData('media_instagram')}}" class="icon"><i class="fab fa-instagram mr-2"></i><span class="label">Instagram</span></a>@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/media_instagram"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</li>
                @endif
                @if(App\Config::getData('media_email'))
                    <li><a href="mailto:{{ App\Config::getData('media_email')}}" class="icon"><i class="fas fa-envelope mr-2"></i><span class="label">Email</span></a>@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/media_email"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</li>
                    @endif
                    </li>
            </ul>
        </nav>
    </div>
    <div class="col-2 my-auto text-right">
        <small class="text-gray">C-CMS @version('compact')</small>
    </div>
    <div class="col-3">
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            C-CMS.
        </div>
    </div>
</div>
<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        clogNav('consulte les messages');
        return view('admin.message.index', ['messages' => \App\Message::all()]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.message.add', ['messages' => \App\Message::all()]);           
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = new Message;

        $msg->title = request('msg_title');
        $msg->body = request('msg_body');

        $msg->user_id = \Auth::user()->id;
        
        /** Basic Shit to change */

        $msg->data = [
            'as_seen' => "",
            'parameter' => ""
        ];

        $msg->publish = true;
        $msg->private = true;

        $msg->save();

        clog('add','success','a ajouté un message',null,'App\Message',$msg->id);
        return redirect('/admin/message');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        \Auth::User()->seenMessage($id);
        clog('see','success','a consulté un message',null,'App\Message',$id);
        return view('admin.message.show', ['message' => Message::find($id)]);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id = request('id');

        $msg = Message::find($id);

        $msg->delete();
        clog('delete','success','a supprimé un message',null,'App\Message',$id);
    }
}

@extends('layouts.modal.schedule.show')

@section('content')

    <div class="row">
        <div class="col-md-6">
            Du <strong>{{$event->date_begin}}</strong> au <strong>{{$event->date_end}}</strong><br>
            <small>{{$event->location}}  </small>
        </div>
        <div class="col-md-6 text-right">
            @if ($event->is_mandatory)
                <span class="badge badge-pill badge-warning">Obligatoire</span>
            @else
                <span class="badge badge-pill badge-info">Optionnel</span>
            @endif
            @if($event->type() != null)
                <span class="badge badge-pill" style="background-color: {{$event->type()->calendar_color}}">{{$event->type()->name}}</span>
            @endif
        </div>
    </div>
    <hr>
    <div class="row mt-4">
        <div class="col-md-12">
            {!!$event->desc!!}
        </div>
    </div>
    @if ($event->use_schedule)
        <hr>
        <div class="row mt-4">
            <div class="col-md-12">
                <h4 class="text-center thead-dark m-0 p-2"><b>Horaire</b></h4>
                <div class="overflow-auto">
                    <div class="schedule-container">
                        <div class="row d-none d-md-flex thead-dark">
                            <div class="col-md-2"></div>
                            @foreach ($event->schedule["periodes"] as $periode)
                                <div class="col-md">
                                    <h4>{{$periode["name"]}}</h4> <small>{{ $periode["begin_time"] }}
                                        à {{ $periode["end_time"] }} </small>
                                </div>
                            @endforeach
                        </div>
                        <div class="schedule-body">
                            @foreach($event->schedule["niveaux"] as $niveauIndex => $niveau)
                                <div class="row">
                                    <div class="col-md-2 schedule-level-header px-3 pb-3">
                                        <h4 class="title pl-3">{{ $niveau["name"] }}</h4>
                                    </div>
                                    @foreach ($event->schedule["periodes"] as $periodeIndex => $periode)
                                        @php
                                            $course = $event->course($periodeIndex+1,$niveauIndex+1);
                                        @endphp
                                        <div class="col-md px-3 pb-3 schedule-course-container">
                                            <div class="row">
                                                <div class="col-12 p-1 bg-dark">
                                                    <div class="row">
                                                        <div class="col-5 m-auto">
                                                            <a class="text-white pl-3" href="/admin/course/{{$course->id}}">
                                                                <b>Cours #{{$course->id}}</b>
                                                            </a>
                                                        </div>
                                                        <div class="col-5 m-auto text-white">
                                                            @if(\App\OCOM::findByOCOM($course->ocom) != null) {{\App\OCOM::findByOCOM($course->ocom)->getDurationInMin()}} min @endif
                                                        </div>
                                                        <div class="col m-auto text-right">
                                                            @if($course->lessonPlan)
                                                                @if($course->lessonPlan->approved == 1)
                                                                    <i class="fas fa-check-circle text-success fa-2x" data-toggle="tooltip" data-placement="top" title="Plan de cours remis et vérifié"></i>
                                                                @else
                                                                    <i class="fas fa-exclamation-circle text-warning fa-2x" data-toggle="tooltip" data-placement="top" title="Plan de cours remis mais non vérifié"></i>
                                                                @endif
                                                            @else
                                                                <i class="fas fa-times-circle text-danger fa-2x" data-toggle="tooltip" data-placement="top" title="Plan de cours non remis"></i>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($course->desc != null)
                                                    <div class="col-12 pb-4 pt-3">{{ $course->desc }}</div>
                                                @else
                                                    <div class="col-12 pb-4 pt-3">
                                                        <a class="text-dark" @if(\App\OCOM::findByOCOM($course->ocom) != null) href="/admin/ocom/{{\App\OCOM::findByOCOM($course->ocom)->id}}" @endif>
                                                            <b>{{ $course->ocom }}</b> - {{ $course->name }}
                                                        </a>
                                                    </div>
                                                @endif
                                                <div class="col-6">{{ $course->instructor() }}</div>
                                                <div class="col-6">{{ $course->location }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
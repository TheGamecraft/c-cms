<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUrl()
    {
        $appURL = "dev.c-cms.cf";

        $urls = [
            'admin',
            'admin/schedule',
            'admin/message',
            'admin/message/add',
            'admin/inventory',
            'admin/item/add',
            'admin/inventory/booking',
            'admin/stats/log',
            'admin/user/add',
            'admin/user',
            'admin/config',
            'admin/update'
        ];

        echo  PHP_EOL;

        foreach ($urls as $url) {
            $response = $this->get($url);
            if((int)$response->status() === 404){
                echo  $appURL . $url . ' (FAILED) return a 404.';
                $this->assertTrue(false);
            } else {
                echo $appURL . $url . ' (SUCCESS)';
                $this->assertTrue(true);
            }
            echo  PHP_EOL;
        }

        foreach ($urls as $url) {
            $response = $this->get($url);
            if((int)$response->status() === 500){
                echo  $appURL . $url . ' (FAILED) return a 500.';
                $this->assertTrue(false);
            } else {
                echo $appURL . $url . ' (SUCCESS)';
                $this->assertTrue(true);
            }
            echo  PHP_EOL;
        }
    }
}

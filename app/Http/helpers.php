<?php

use App\Log;

/**
 * @return bool
 */
function getStatus()
{
    $client = new GuzzleHttp\Client();
    $incidents = $client->get('https://status.exvps.ca/api/v1/incidents');

    $isBroken = false;

    $incidents_decode = json_decode($incidents->getBody(),true);
    
    foreach ($incidents_decode['data'] as $incident) {
        if (!$incident['is_resolved']) {
            $isBroken = true;
        }
    }

    return $isBroken;
}

/**
 * @param string $type
 * @param string $result
 * @param string $event
 * @param int $user_id
 * @param null $obj_type
 * @param null $obj_id
 */
function clog(string $type,string $result,string $event,$user_id = null,$obj_type = null,$obj_id = null)
{
    $log = new Log;

    $log->result = $result;
    $log->event = $event;
    $log->type = $type;

    if ($user_id != null)
    {
        $log->user_id = $user_id;
    }
    else
    {
        $log->user_id = \Auth::User()->id;
    }


    if ($obj_type != null)
    {
        $log->logable_type = $obj_type;
    }
    else
    {
        $log->logable_type = '';
    }

    if ($obj_id != null)
    {
        $log->logable_id = $obj_id;
    }
    else
    {
        $log->logable_id = 0;
    }

    $log->save();
}

function clogNav($event)
{
    clog('navigate','success',$event);
}

function GetSizeName($octet)
{
    // Array contenant les differents unités
    $unite = array('octet','ko','mo','go');

    if ($octet < 1000) // octet
    {
        return $octet.' '.$unite[0];
    }
    else
    {
        if ($octet < 1000000) // ko
        {
            $ko = round($octet/1024,2);
            return $ko.' '.$unite[1];
        }
        else // Mo ou Go
        {
            if ($octet < 1000000000) // Mo
            {
                $mo = round($octet/(1024*1024),2);
                return $mo.' '.$unite[2];
            }
            else // Go
            {
                $go = round($octet/(1024*1024*1024),2);
                return $go.' '.$unite[3];
            }
        }
    }
}

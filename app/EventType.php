<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EventType
 *
 * @property int $id
 * @property string $name
 * @property string $admin_desc
 * @property string $calendar_color
 * @property string $calendar_icon
 * @property string $begin_time
 * @property string $end_time
 * @property string $location
 * @property int $is_mandatory
 * @property int $use_weekly_msg
 * @property string $weekly_msg_publication_time
 * @property int $use_schedule
 * @property array $schedule_model
 * @property int $is_promoted
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereAdminDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereBeginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereCalendarColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereCalendarIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereIsMandatory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereIsPromoted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereScheduleModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereUseSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereUseWeeklyMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereWeeklyMsgPublicationTime($value)
 * @mixin \Eloquent
 * @property int $hidden
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EventType whereHidden($value)
 */
class EventType extends Model
{
    protected $casts = [
        'schedule_model' => 'array',
    ];

    public function getScheduleModelData($level,$periode)
    {
        if (isset($this->schedule_model['default_value']))
        {
            dd($this->schedule_model ['default_value']);
            return $this->schedule_model['default_value'][$periode-1][$level-1];
        }
        else
        {
            return [
                'ocom' => 'M103.03',
                'name' => 'Super cours',
                'location' => 'Bic',
                'instructor' => 'Math',
                'desc' => ''
            ];
        }
    }

    public function nbLevel()
    {
        return count($this->schedule_model['niveaux']);
    }

    public function nbPeriode()
    {
        return count($this->schedule_model['periodes']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.item.add',['categories' => \App\ItemCategory::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $item = new Item;

        $item->name = request('name');
        $item->desc = request('desc');
        $item->quantity = request('quantity');
        $item->official_number = request('official_number');
        $item->metadata = ['size' => request('metadata-size')];
        $item->category_id = request('category_id');

        $item->save();
        clog('add','success',"a ajouté un item à l'inventaire",null,"App\Item",$item->id);
        return redirect('/admin/inventory')->with('success','Item sauvegardé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);

        return view('admin.item.edit',['item' => $item,'categories' => \App\ItemCategory::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $item = Item::find($id);

        $item->name = request('name');
        $item->desc = request('desc');
        $item->quantity = request('quantity');
        $item->official_number = request('official_number');
        $item->metadata = ['size' => request('metadata-size')];
        $item->category_id = request('category_id');

        $item->save();
        clog('edit','success',"a modifié un item à l'inventaire",null,"App\Item",$item->id);
        return redirect('/admin/inventory')->with('success','Item sauvegardé avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $item = Item::find(request('id'));

        $item->delete();
        clog('delete','success',"a supprimé un item à l'inventaire",null,"App\Item",$item->id);
    }
}

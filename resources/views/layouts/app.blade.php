<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .fa-beat {
            margin-left:10px;
            animation:fa-beat 2s ease infinite;
                }
                @keyframes fa-beat {
                25% {
                    transform:scale(1);
                }
                50% {
                    transform:scale(1.4);
                }
            }
    </style>
</head>
<body style="background-color: #343a40 !important">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="background-color:rgb(39, 44, 51)">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}" style="color:white">
                    {{ config('squadron.fullname') }}
                </a>
                <div class="float-right" style="color: white;"><a href="https://status.c-cms.cf/" style="color:white">Status 
                @if (getStatus())
                    <i class="fas fa-circle fa-beat" style="color: red;"></i></a></div>
                @else
                    <i class="fas fa-circle" style="color: green;"></i></a></div>
                @endif
            </div>
        </nav>

        <main class="py-4" style="margin-top: 11%">
            @yield('content')
        </main>
    </div>
</body>
</html>

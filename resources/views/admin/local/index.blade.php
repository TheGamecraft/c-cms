@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Inventaire <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <table id="log-data" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID </th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($locals as $local)
                        <tr>
                            <td style="width:5%;">{{$local->id}}</td>  
                            <td>{{$local->name}}</td>
                            <td>{{$local->desc}}</td>
                            <td style="width: 12%;"><a href="/admin/inventory/edit/{{$local->id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier</a><a type="button" class="btn btn-danger" onclick="deleteEvent({{$local->id}});"><i class="fa fa-times-circle" style="color:white;"></i></a></td>
                        </tr>
                    @endforeach
                
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventaire</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Inventaire</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('#log-data').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            });
        } );
    })(jQuery);

     function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/local/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "Le local a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
@endsection

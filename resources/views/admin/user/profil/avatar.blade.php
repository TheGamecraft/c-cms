@extends('layouts.admin.main')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-body text-center">
            @if (session('status'))
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Félicitation</span>
                        Votre avatar a été mis a jour !
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
            Votre Avatar
            <div class="mx-auto d-block">
                <img class="rounded-circle mx-auto d-block" src="/assets/admin/images/avatar/user-{{\Auth::User()->avatar}}.jpg" alt="Card image cap" style="height:12rem">
            </div>
            <hr>
            Cliquer sur un avatar pour mettre a jour votre avatar
            <div class="row">
            @for ($i = 1; $i < 16; $i++)
                <div class="col-md-6 col-lg-4">
                    <a href="/admin/profil/edit/avatar/{{$i}}"><img class="rounded-circle mx-auto d-block" src="/assets/admin/images/avatar/user-{{$i}}.jpg" alt="Card image cap" style="height:12rem"></a>                   
                </div>
            @endfor
            </div>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Profil d'utilisateur</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Utilisateur/Profil/Avatar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
   
@endsection

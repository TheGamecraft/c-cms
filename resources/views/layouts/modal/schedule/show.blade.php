@if($event->hidden == 1)
    <div class="alert alert-danger text-center" role="alert" style="margin-top:-4rem">
        <b>Évenement caché</b>
    </div>
@endif
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">{{$event->name}}</h5>
    <button type="button" class="close" onclick="closeScheduleModal()" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

    @yield('content')

</div>
<div class="modal-footer row">
    <div class="col-sm-8 m-0 text-left">
        @if(\Auth::user()->p('schedule_edit') == 1)
            <a class="btn btn-primary btn-fab btn-fab-mini btn-round" href="/admin/schedule/edit/{{$event->id}}" data-toggle="tooltip" data-placement="bottom" title="Modifier"><i class="material-icons">edit</i></a>
        @endif
{{--        <a class="btn btn-primary btn-fab btn-fab-mini btn-round" href="/admin/schedule/pdf/event/{{$event->id}}" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Imprimer"><i class="material-icons">print</i></a>--}}
        @if(\Auth::user()->p('schedule_delete') == 1)
            <button class="btn btn-danger btn-fab btn-fab-mini btn-round" data-toggle="tooltip" data-placement="bottom" title="Supprimer" onclick="deleteEvent({{$event->id}})"><i class="material-icons">close</i></button>
        @endif
    </div>
    <div class="col-sm-4 m-0 text-right">
        <div class="btn-group">
            <button type="button" class="btn btn-secondary" onclick="closeScheduleModal()">Fermer</button>
        </div>
    </div>
</div>

<nav class="navbar navbar-color-on-scroll fixed-top navbar-expand-lg navbar-transparent" color-on-scroll="100" id="sectionsNav">
	<div class="container">
		<div class="navbar-translate">
			<a class="navbar-brand" href="/">
				{{ App\Config::getData('escadron_name_full')}} </a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="sr-only">Toggle navigation</span>
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="/" class="nav-link">
						<i class="material-icons">home</i> Accueil
					</a>
				</li>
				<li class="dropdown nav-item">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
						<i class="material-icons">airplanemode_active</i>
						@switch(\App\Config::getData('escadron_element'))
							@case('Marine')
							Notre équipage
							@break
							@case('Terre')
							Notre corps de cadet
							@break
							@default
							Notre escadron
						@endswitch
					</a>
					<div class="dropdown-menu dropdown-with-icons">
						<a href="/#news" class="dropdown-item">
							<i class="material-icons mr-2">new_releases</i>Nouvelles
						</a>
						<a href="/#picture" class="dropdown-item">
							<i class="material-icons mr-2">photo_camera</i>Photos
						</a>
						<a href="/#calendar" class="dropdown-item">
							<i class="material-icons mr-2">calendar_today</i>Calendrier
						</a>
						<a href="/#cta" class="dropdown-item">
							<i class="material-icons">location_on</i> Nous rejoindre
						</a>
					</div>
				</li>
				{{-- <li class="dropdown nav-item">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
						<i class="material-icons">people</i>
						Devenir cadet
					</a>
					<div class="dropdown-menu dropdown-with-icons">
						<a href="/p/qui-somme-nous" class="dropdown-item">
							<i class="material-icons mr-2">help</i>Qui somme nous
						</a>
						<a href="/p/nos-activites" class="dropdown-item">
							<i class="material-icons mr-2">landscape</i>Nos activités
						</a>
						<a href="/p/devenir-cadet" class="dropdown-item">
							<i class="material-icons mr-2">task</i> Comment devenir cadet
						</a>
					</div>
				</li> --}}
				@if(\Auth::check())
					<li class="nav-item">
						<a class="nav-link" href="/login">
							<i class="material-icons">dashboard</i> Espace Cadet
						</a>
					</li>
				@else
					<li class="nav-item">
						<a class="nav-link" href="/login">
							<i class="material-icons">lock_open</i> Connexion
						</a>
					</li>
				@endif
				@if(request()->has('editMode'))
					<li class="nav-item">
						<a class="nav-link btn btn-warning" href="/">
							<i class="material-icons">cancel</i> Quitter le mode edition
						</a>
					</li>
				@endif
			</ul>
		</div>
	</div>
</nav>
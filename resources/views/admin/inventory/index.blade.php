@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Inventaire <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <div class="btn-group btn-block">
                @if(\Auth::user()->p('inventory_add') == 1)
                    <a name="add" id="add" class="btn btn-primary btn-block" href="/admin/item/add" role="button">Ajouter un item</a>
                @endif
            </div>
            <hr>
            <table id="log-data" class="table table-striped table-bordered dt-responsive w-100">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nom</th>
                    <th>Categorie</th>
                    <th>Quantité Total</th>
                    <th>Quantité Disponible</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{$item->official_number}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->category()->name}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->available()}}</td>
                            <td>{!! $item->desc !!}</td>
                            <td style="width: 12%;">
                                <div class="btn-group">
                                    @if(\Auth::user()->p('inventory_edit') == 1)
                                    <a href="/admin/item/edit/{{$item->id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier</a>
                                    @endif
                                    @if(\Auth::user()->p('inventory_delete') == 1)
                                        <a type="button" class="btn btn-danger" onclick="deleteEvent({{$item->id}});"><i class="fa fa-times-circle" style="color:white;"></i></a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventaire</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Inventaire</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script src="https://cdn.datatables.net/rowgroup/1.1.0/js/dataTables.rowGroup.min.js"></script>
<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('#log-data').DataTable({
                "order": [[ 2, "asc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "rowGroup": {
                    dataSrc: 2
                }
            });
        } );
    })(jQuery);

     function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/item/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "L'item a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
@endsection

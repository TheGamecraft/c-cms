@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Réservation
                    <a href="#"><i class="fa fa-question-circle float-right" aria-hidden="true"></i></a>
                </h4>
            </div>
            <div class="card-body">
                <div class="alert alert-warning" role="alert">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Information sur l'évènement</h6>
                            <hr class="my-1">
                        </div>
                        <div class="col-md-6 mb-2">
                            {{$event->name}}
                        </div>
                        <div class="col-md-6 mb-2 text-right">
                            {{$event->fulltime}}
                        </div>
                        <div class="col-md-6 mb-2">
                            {{$event->fulldesc}}
                        </div>
                        <div class="col-md-6 text-right">
                            @if(is_numeric($event->user_id))
                                {{$event->user->fullname()}}
                            @else
                                {{$event->user_id}}
                            @endif
                        </div>
                    </div>
                </div>
                <table id="log-data" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Categorie</th>
                        <th>Description</th>
                        <th>Quantité</th>
                        <th width="15%">
                            <a name="add" id="add" class="btn btn-outline-primary" href="/admin/booking/{{$event_type}}/{{$event_id}}/add" role="button">Réserver un item</a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($event->bookings as $booking)
                            <tr>
                                <td>{{$booking->item->id}}</td>
                                <td>{{$booking->item->name}}</td>
                                <td>{{$booking->item->category()->name}}</td>
                                <td>{!! $booking->item->desc !!}</td>
                                <td>{{$booking->amount}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-danger btn-just-icon" onclick="deleteBooking('{{$booking->id}}')"><i class="fas fa-times-circle"></i></button>
                                        <button class="btn btn-primary" onclick="openModal('{{$booking->id}}')"><i class="fas fa-edit"></i> Modifier</button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="https://cdn.datatables.net/rowgroup/1.1.0/js/dataTables.rowGroup.min.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('#log-data').DataTable({
                    "order": [[ 2, "asc" ]],
                    "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                    "rowGroup": {
                        dataSrc: 2
                    }
                });
            } );
        })(jQuery);

        function deleteBooking(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
            }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/booking/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });


                    })(jQuery);

                    swal(
                        'Supprimé!',
                        "L'item a été supprimé",
                        'success'
                    ).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                }
            })
        }

        function openModal(id)
        {
            $.get("/api/booking/modal/edit/item/" + id + "/{{$event_type}}/{{$event_id}}?api_token="+api_token, function (data) {
                $("#modal-content").html(data);
            });
            $('#editModal').modal('toggle')
        }
    </script>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="schedulemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="card card-signup card-plain">
                    <div class="modal-header">
                        <div class="card-header card-header-primary text-center">
                            <h4 class="card-title">Modifier la réservation</h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="/admin/booking/edit">
                            @csrf
                            <div class="card-body" id="modal-content">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>#</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nom de l'item</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Quantité</label>
                                            <input class="form-control" type="number" min="1" required disabled>
                                            <small class="form-text text-muted">Quantité d'item a réserver</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary" disabled>Confimer</button>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Annuler</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

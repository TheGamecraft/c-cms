<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
Du <strong>{{$event->date_begin}}</strong> au <strong>{{$event->date_end}}</strong>
<div>
    {{$event->location}}
    @if ($event->is_mandatory)
        (Activité Obligatoire)
    @else
        (Activité Optionnel)
    @endif
</div>
<div>
    @if (is_int($event->type))
        {{$event->type}}
    @else
        {{\App\ComplementaryActivity::find($event->type)->name}}
    @endif
</div>
<div>
    {!!$event->desc!!}
</div>
@if (!$event->courses->isEmpty())
    <div class="w-100 mt-2">
        <h4>Horaire</h4>
        <table style="width:100%" class="table">
            <tr>
                <th>Niveau</th>
                @for ($p = 1; $p <= \App\Config::getData('admin_periode_nb'); $p++)
                    <th>
                        Période {{$p}} <small>{{\App\Config::all()->where('name','admin_periode_begin')->first()->data[$p]}} à {{\App\Config::all()->where('name','admin_periode_end')->first()->data[$p]}}</small>
                    </th>
                @endfor
            </tr>
            @for ($l = 1; $l <= \App\Config::getData('admin_level_in_schedule_nb'); $l++)
                <tr>
                    <td>
                        Niveau {{$l}}
                    </td>
                    @for ($p = 1; $p <= \App\Config::getData('admin_periode_nb'); $p++)
                        <td>
                            @foreach($event->courses as $course)
                                @if($course->periode == $p && $course->level == $l)
                                    <table class="w-100 mx-5 table-borderless bg-white">
                                        <tr>
                                            <td>{{$course->name}}</td>
                                            <td class="text-right">@if(is_numeric($course->user_id)){{$course->user->fullname()}}@else{{$course->user_id}}@endif</td>
                                        </tr>
                                        <tr>
                                            <td>{{$course->ocom}}</td>
                                            <td class="text-right">{{$course->location}}</td>
                                        </tr>
                                    </table>
                                @endif
                            @endforeach
                        </td>
                    @endfor
                </tr>
            @endfor
        </table>
    </div>
@endif
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
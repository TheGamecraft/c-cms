@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Permission du dossier: {{$dir->name}}</h4>
                <p class="category">/{{$dir->path}}</p>
            </div>
            <div class="card-body">
                <h5>Permission des grades</h5>
                <table class="table table-striped table-responsive w-100">
                    <thead class="table-dark">
                        <tr class="text-center">
                            <td class="text-left" style="width: 25%">Grade</td>
                            <td >Lecture</td>
                            <td>Écriture</td>
                            <td>Gestion</td>
                            <td style="width: 6rem;">
                                <button class="btn btn-primary btn-fab btn-fab-mini btn-round m-0" onclick="addPermission('{{$dir->id}}','rank')">
                                    <i class="material-icons">add</i>
                                </button>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td class="text-left">
                                Utilisateur non authentifié
                            </td>
                            <td>
                                @if(isset($dir->rank_permission[0]))
                                    @if(strpos($dir->rank_permission[0],'r') !== false)
                                        <i class="fas fa-check-circle fa-2x text-success"></i>
                                    @else
                                        <i class="fas fa-times-circle fa-2x text-danger"></i>
                                    @endif
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td>
                                @if(isset($dir->rank_permission[0]))
                                    @if(strpos($dir->rank_permission[0],'w') !== false)
                                        <i class="fas fa-check-circle fa-2x text-success"></i>
                                    @else
                                        <i class="fas fa-times-circle fa-2x text-danger"></i>
                                    @endif
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td>
                                @if(isset($dir->rank_permission[0]))
                                    @if(strpos($dir->rank_permission[0],'p') !== false)
                                        <i class="fas fa-check-circle fa-2x text-success"></i>
                                    @else
                                        <i class="fas fa-times-circle fa-2x text-danger"></i>
                                    @endif
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td class="text-right">
                                <button class="btn btn-primary btn-fab btn-fab-mini btn-round" onclick="editPermission('{{$dir->id}}','rank','0')">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button class="btn btn-danger btn-fab btn-fab-mini btn-round no-cursor" disabled data-toggle="tooltip" data-placement="bottom" title="Vous ne pouvez pas supprimer les permissions des utilisteurs non authentifié">
                                    <i class="material-icons">delete</i>
                                </button>
                            </td>
                        </tr>
                        @foreach($dir->rank_permission as $key => $rank)
                            @if($key != 0)
                                <tr class="text-center">
                                    <td class="text-left">
                                        {{\App\Rank::find($key)->name}}
                                    </td>
                                    <td>
                                        @if(strpos($rank,'r') !== false)
                                            <i class="fas fa-check-circle fa-2x text-success"></i>
                                        @else
                                            <i class="fas fa-times-circle fa-2x text-danger"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if(strpos($rank,'w') !== false)
                                            <i class="fas fa-check-circle fa-2x text-success"></i>
                                        @else
                                            <i class="fas fa-times-circle fa-2x text-danger"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if(strpos($rank,'p') !== false)
                                            <i class="fas fa-check-circle fa-2x text-success"></i>
                                        @else
                                            <i class="fas fa-times-circle fa-2x text-danger"></i>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <button class="btn btn-primary btn-fab btn-fab-mini btn-round" onclick="editPermission('{{$dir->id}}','rank','{{$key}}')">
                                            <i class="material-icons">edit</i>
                                        </button>
                                        <button class="btn btn-danger btn-fab btn-fab-mini btn-round" onclick="deletePermission('{{$dir->id}}','rank','{{$key}}')">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                <h5 class="mt-5">Permission des postes</h5>
                <table class="table table-striped table-responsive w-100">
                    <thead class="table-dark">
                    <tr class="text-center">
                        <td class="text-left" style="width: 25%">Poste</td>
                        <td >Lecture</td>
                        <td>Écriture</td>
                        <td>Gestion</td>
                        <td style="width: 6rem;">
                            <button class="btn btn-primary btn-fab btn-fab-mini btn-round m-0" onclick="addPermission('{{$dir->id}}','job')">
                                <i class="material-icons">add</i>
                            </button>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($dir->job_permission) < 1)
                        <tr>
                            <td colspan="5" class="text-center">
                                Aucune permission de poste
                            </td>
                        </tr>
                    @endif
                    @foreach($dir->job_permission as $key => $rank)
                        <tr class="text-center">
                            <td class="text-left">
                                {{\App\Job::find($key)->name}}
                            </td>
                            <td>
                                @if(strpos($rank,'r') !== false)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td>
                                @if(strpos($rank,'w') !== false)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td>
                                @if(strpos($rank,'p') !== false)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td class="text-right">
                                <button class="btn btn-primary btn-fab btn-fab-mini btn-round" onclick="editPermission('{{$dir->id}}','job','{{$key}}')">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button class="btn btn-danger btn-fab btn-fab-mini btn-round" onclick="deletePermission('{{$dir->id}}','job','{{$key}}')">
                                    <i class="material-icons">delete</i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <h5 class="mt-5">Permission des utilisateurs</h5>
                <table class="table table-striped table-responsive w-100">
                    <thead class="table-dark">
                    <tr class="text-center">
                        <td class="text-left" style="width: 25%">Utilisateurs</td>
                        <td >Lecture</td>
                        <td>Écriture</td>
                        <td>Gestion</td>
                        <td style="width: 6rem;">
                            <button class="btn btn-primary btn-fab btn-fab-mini btn-round m-0" onclick="addPermission('{{$dir->id}}','user')">
                                <i class="material-icons">add</i>
                            </button>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($dir->user_permission) < 1)
                        <tr>
                            <td colspan="5" class="text-center">
                                Aucune permission d'utilisateur
                            </td>
                        </tr>
                    @endif
                    @foreach($dir->user_permission as $key => $rank)
                        <tr class="text-center">
                            <td class="text-left">
                                {{\App\User::find($key)->fullname()}}
                            </td>
                            <td>
                                @if(strpos($rank,'r') !== false)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td>
                                @if(strpos($rank,'w') !== false)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td>
                                @if(strpos($rank,'p') !== false)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                @else
                                    <i class="fas fa-times-circle fa-2x text-danger"></i>
                                @endif
                            </td>
                            <td class="text-right">
                                <button class="btn btn-primary btn-fab btn-fab-mini btn-round" onclick="editPermission('{{$dir->id}}','user','{{$key}}')">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button class="btn btn-danger btn-fab btn-fab-mini btn-round" onclick="deletePermission('{{$dir->id}}','user','{{$key}}')">
                                    <i class="material-icons">delete</i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @csrf
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>

    <div id="permissionModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="permissionModalHtml">
                <div class="modal-header">
                    <h5 class="modal-title">Chargement ...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @loaderDot
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" disabled>Sauvegarder</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="/js/plugins/jquery.ui.position.min.js"></script>
    <script src="/js/plugins/jquery.contextMenu.min.js"></script>
    <script src="/js/plugins/drive-explorer.js"></script>
@endsection

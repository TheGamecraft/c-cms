<?php

namespace App\Http\Middleware;

use Closure;

class AccesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if (\Auth::user()->getAcces(2) == false) {
            abort(401);
        }

        return $next($request);
    }
}

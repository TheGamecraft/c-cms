<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleDriveFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_drive_files', function (Blueprint $table) {
            $table->string('id');
            $table->index('id');
            $table->string('type');
            $table->string('name');
            $table->string('path')->default('');
            $table->string('rank_permission');
            $table->string('job_permission');
            $table->string('user_permission');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_drive_files');
    }
}

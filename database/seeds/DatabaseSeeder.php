<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            JobsTableSeeder::class,
            RanksTableSeeder::class,
            ConfigsTableSeeder::class,
            UsersTableSeeder::class,
            ComplementaryActivitiesSeeder::class,
            EventTypeSeeder::class,
            PublicPageSeeder::class,
        ]);
    }
}

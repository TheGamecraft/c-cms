<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ComplementaryActivity
 *
 * @property int $id
 * @property string $name
 * @property string $public_body
 * @property string $public_slogan
 * @property string $public_header_picture
 * @property string $admin_desc
 * @property string $calendar_color
 * @property string $calendar_icon
 * @property string $begin_time
 * @property string $end_time
 * @property string $location
 * @property int $is_mandatory
 * @property int $is_promoted
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Picture[] $pictures
 * @property-read int|null $pictures_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereAdminDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereBeginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereCalendarColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereCalendarIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereIsMandatory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereIsPromoted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity wherePublicBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity wherePublicHeaderPicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity wherePublicSlogan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ComplementaryActivity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ComplementaryActivity extends Model
{
    public function pictures()
    {
        return $this->morphMany('App\Picture', 'pictureable');
    }
}

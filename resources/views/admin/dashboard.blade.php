@extends('layouts.admin.main')

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-12">
            <div class="col-md-12 p-0">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title"> Mes cours à venir</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                        @if(\Auth::user()->futureCourses()->isEmpty())
                            <div class="col-sm-12 text-center">
                                <h4 class="m-4">Aucun cours à venir</h4>
                            </div>
                        @else
                            @foreach ($userClasse as $course)
                                @if($course->event != null)
                                <div class="col-sm-6">
                                    <div class="card my-3">
                                        <div class="card-body">
                                            <h4>
                                                @if($course->name != null)
                                                    <strong>{{$course->ocom}} - {{$course->name}}</strong>
                                                @else
                                                    <strong>{{$course->desc}}</strong>
                                                @endif
                                                @if($course->lessonPlan)
                                                    @if($course->lessonPlan->approved == 1)
                                                        <span class="float-right"><i class="fas fa-check-circle text-success" data-toggle="tooltip" data-placement="top" title="Plan de cours remis et vérifié"></i></span>
                                                    @else
                                                        <span class="float-right"><i class="fas fa-exclamation-circle text-warning" data-toggle="tooltip" data-placement="top" title="Plan de cours remis mais non vérifié"></i></span>
                                                    @endif
                                                @else
                                                    <span class="float-right"><i class="fas fa-times-circle text-danger" data-toggle="tooltip" data-placement="top" title="Plan de cours non remis"></i></span>
                                                @endif
                                            </h4>
                                            <div class="row">
                                                <p class="col-6 text-left mb-1">{{date('Y-m-d',strtotime($course->event->date_begin))}}</p>
                                                <p class="col-6 text-right mb-1">Période {{$course->periode}}, Niveau {{$course->level}}</p>
                                                <div class="col-12 text-right">
                                                    <hr class="my-1">
                                                    <a href="/admin/course/{{$course->id}}" class="my-1">Détail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        @endif
                        </div>
                        <a href="/admin/profil/courses" type="button" class="btn btn-primary btn-lg btn-block">Afficher tous mes cours</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 p-0">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Activité à venir</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @if(count($futureEvent) == 0)
                                <div class="col-sm-12 text-center">
                                    <h4 class="m-4">Aucune activité à venir</h4>
                                </div>
                            @else
                                @foreach ($futureEvent as $event)
                                    <div class="col-md-12">
                                        <div class="alert cursor" style="background-color: @if($event->calendar_color == null){{ \App\ComplementaryActivity::find($event->type)->calendar_color}} @else {{$event->calendar_color}} @endif" onclick="navigate('schedule?e={{$event->id}}')">
                                            <div class="row text-white">
                                                <div class="col-md-2 text-capitalize m-auto d-none d-md-flex">
                                                    <h3 class="m-0 p-0" style="margin-top: -0.5rem !important;">@if($event->calendar_icon == null) {!! \App\ComplementaryActivity::find($event->type)->calendar_icon !!} @else <i class="{{$event->calendar_icon}}"></i> @endif</h3>
                                                </div>
                                                <div class="col-md">
                                                    {{$event->name}}
                                                </div>
                                                <div class="col-md-4">
                                                    {{$event->date_begin}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <br>
                        <a href="/admin/schedule" type="button" class="btn btn-primary btn-lg btn-block">Afficher plus</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="col-12 p-0">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a>
                            <img class="img" src="/assets/admin/images/avatar/user-{{\Auth::User()->avatar}}.jpg">
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <h5 class="text-sm-center mt-2 mb-1">{{\Auth::User()->fullname()}}</h5>
                            <div class="location text-sm-center"><i class="fas fa-id-card-alt"></i> {{\Auth::User()->rank->name}}</div>
                        </div>
                        <hr>
                        <div class="card-text text-sm-center">
                            <a class="btn btn-block btn-secondary" href="/admin/profil/courses">Mes cours</a>
                            <a class="btn btn-block btn-secondary" href="/admin/profil">Profil</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 p-0">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Nouvelles</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach (\App\News::all()->forPage(1,3) as $msg)
                                <div class="col-12 text-center">
                                    <a href="/admin/message/{{$msg->id}}">
                                        <div style="height:2rem;">{{$msg->title}}</div>
                                        <br>
                                        <div class="msg-body" style="height:12rem;overflow:hidden">
                                            {!!$msg->body!!}
                                        </div>
                                        <br>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <br>
                        <a href="/admin/news" type="button" class="btn btn-primary btn-lg btn-block text-white">Afficher plus</a>
                    </div>
                </div>
            </div>
            @if(\Auth::user()->p('stats_see') == 1)
                <div class="col-12 p-0">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Activité sur le site</h4>
                        </div>
                        <div class="card-body">
                            @foreach (\App\Log::all()->forPage(1,3); as $log)
                                <div class="row mt-3">
                                    <div class="col-lg-12 col-xl-3 mx-3 my-auto">
                                        <span class="badge badge-pill badge-{{$log->typeColor()}} mr-2">{{$log->type}}</span>
                                    </div>
                                    <div class="col">
                                        {{$log->user->fullname()}} - {{$log->event}}
                                    </div>
                                </div>
                            @endforeach
                            <br>
                            <a href="/admin/stats/log" type="button" class="btn btn-primary btn-lg btn-block text-white">Afficher plus</a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('breadcrumb')
    Dashboard
@endsection

@section('scripts')
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script>
@endsection

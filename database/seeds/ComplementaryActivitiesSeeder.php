<?php

use Illuminate\Database\Seeder;

class ComplementaryActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('complementary_activities')->insert([
            [
                'name' => 'Soirée d\'instruction régulière',
                'calendar_color' => 'orange',
                'calendar_icon' => '<i class="fas fa-book"></i>',
                'begin_time' => '18:30',
                'end_time' => '21:45',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Congé',
                'calendar_color' => 'red',
                'calendar_icon' => '<i class="fa fa-times"></i>',
                'begin_time' => '00:01',
                'end_time' => '23:59',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Financement',
                'calendar_color' => 'light-green',
                'calendar_icon' => '<i class="fa fa-usd"></i>',
                'begin_time' => '00:01',
                'end_time' => '23:59',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Bénévolat',
                'calendar_color' => 'green',
                'calendar_icon' => '<i class="fa fa-handshake-o "></i>',
                'begin_time' => '00:01',
                'end_time' => '23:59',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Autre',
                'calendar_color' => 'purple',
                'calendar_icon' => '<i class="fa fa-circle "></i>',
                'begin_time' => '00:01',
                'end_time' => '23:59',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Précidrill',
                'calendar_color' => 'blue',
                'calendar_icon' => '<i class="fa fa-trophy"></i>',
                'begin_time' => '19:00',
                'end_time' => '21:00',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Musique',
                'calendar_color' => 'gold',
                'calendar_icon' => '<i class="fa fa-music"></i>',
                'begin_time' => '19:00',
                'end_time' => '21:00',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Tir de précision',
                'calendar_color' => 'grey',
                'calendar_icon' => '<i class="fa fa-bullseye"></i>',
                'begin_time' => '19:00',
                'end_time' => '21:00',
                'Location' => 'Escadron',
                'public_body' => 'Veuillez modifier le text de description publique par défaut',
                'public_slogan' => 'Veuillez modifier le slogan publique par défaut',
                'public_header_picture' => './assets/img/bg2.jpg',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ]
        ]);
    }
}

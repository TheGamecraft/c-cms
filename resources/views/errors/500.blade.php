@extends('errors.layout')

@section('title', __('Erreur interne'))
@section('code')
    <span>5</span><span>0</span><span>0</span>
@endsection
@section('message', 'Nous sommes désolé, le serveur a rencontré une exception.')
@section('error',$exception->getMessage())
@section('url','https://developer.mozilla.org/fr/docs/Web/HTTP/Status/500')


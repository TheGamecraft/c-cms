<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">{{$event->name}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
        Du <strong>{{$event->date_begin}}</strong> au <strong>{{$event->date_end}}</strong><br>
        <small>{{$event->location}}  </small>
        </div>
        <div class="col-md-6 text-right">
            @if ($event->is_mandatory)
                <span class="badge badge-pill badge-warning">Obligatoire</span>
            @else
                <span class="badge badge-pill badge-info">Optionnel</span>
            @endif
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12">
            {!!$event->desc!!}
        </div>
    </div>     
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
</div>
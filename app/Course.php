<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property int $id
 * @property string $name
 * @property string $ocom
 * @property int $periode
 * @property int $level
 * @property string $location
 * @property string $desc
 * @property string $comment
 * @property string $comment_officer
 * @property int $event_id
 * @property string $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Booking[] $bookings
 * @property-read int|null $bookings_count
 * @property-read \App\Event $event
 * @property-read \App\LessonPlan|null $lessonPlan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCommentOfficer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereOcom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePeriode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUserId($value)
 * @mixin \Eloquent
 */
class Course extends Model
{
    public function bookings()
    {
        return $this->morphMany('App\Booking', 'bookable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function instructor()
    {
        if (\App\User::find($this->user_id)) {
            return \App\User::find($this->user_id)->fullname();
        } else {
            if (is_numeric($this->user_id)) {
                return 'Utilisateur inconnu';
            } else {
                return $this->user_id;
            }
        }
    }

    public function ocom()
    {
        if ($this->ocom != null) {
            $ocom = \App\OCOM::findByOCOM($this->ocom);
            if ($ocom != null) {
                return $ocom;
            }
        }
        return null;
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }

    public function use_course()
    {
        return $this->name == "" && $this->ocom == "";
    }

    public function lessonPlan()
    {
        return $this->hasOne('App\LessonPlan');
    }

    public static function allFuture()
    {
        $courses = Course::all();
        foreach ($courses as $key => $course) {
            if (date('c', strtotime($course->event->date_begin)) <= date('c')) {
                $courses->forget($key);
            }
        }
        return $courses;
    }

    public static function allThisYear()
    {
        $courses = Course::all();
        foreach ($courses as $key => $course) {
            if (date('c', strtotime($course->event->date_begin)) <= date('c', strtotime(\App\Config::getData('instruction_year_begin')))) {
                $courses->forget($key);
            }
            if (date('c', strtotime($course->event->date_begin)) >= date('c', strtotime(\App\Config::getData('instruction_year_end')))) {
                $courses->forget($key);
            }
        }
        return $courses;
    }

    public static function allForUser($user_id)
    {
        $courses = Course::all();
        foreach ($courses as $key => $course) {
            if ($course->user_id != $user_id) {
                $courses->forget($key);
            }
        }
        return $courses;
    }

    public static function allForAuthUser()
    {
        return self::allForUser(\Auth::user()->id);
    }

    public static function allFutureForUser($user_id)
    {
        $courses = Course::all();
        foreach ($courses as $key => $course) {
            if (date('c', strtotime($course->event->date_begin)) <= date('c')) {
                $courses->forget($key);
            }
            if ($course->user_id != $user_id) {
                $courses->forget($key);
            }
        }
        return $courses;
    }

    public static function allFutureForAuthUser()
    {
        return self::allFutureForUser(\Auth::user()->id);
    }

}

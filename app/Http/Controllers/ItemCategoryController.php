<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemCategory;
use Illuminate\Http\Request;

class ItemCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        clogNav('consulte la gestion des catégories d\'inventaire');
        return view('admin.itemcategory.index',['categories' => ItemCategory::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.itemcategory.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $c = new ItemCategory();

        $c->name = \request('name');
        $c->desc = \request('desc');
        if (\request('is_training') == 1)
        {
            $c->is_training = 1;
        }
        else
        {
            $c->is_training = 0;
        }
        if (\request('is_op_appro') == 1)
        {
            $c->is_op_appro = 1;
        }
        else
        {
            $c->is_op_appro = 0;
        }

        $c->save();
        clog('add','success','a ajouté une catégorie a l\'inventaire',null,'App\ItemCategory',$c->id);
        return redirect('/admin/inventory/management/category/')->with('success','Catégorie ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ItemCategory $itemCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.itemcategory.edit',['category' => ItemCategory::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $c = ItemCategory::find($id);

        $c->name = \request('name');
        $c->desc = \request('desc');
        if (\request('is_training') == 1)
        {
            $c->is_training = 1;
        }
        else
        {
            $c->is_training = 0;
        }
        if (\request('is_op_appro') == 1)
        {
            $c->is_op_appro = 1;
        }
        else
        {
            $c->is_op_appro = 0;
        }

        $c->save();
        clog('edit','success','a modifié une catégorie de l\'inventaire',null,'App\ItemCategory',$c->id);
        return redirect('/admin/inventory/management/category/')->with('success','Catégorie modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = ItemCategory::find($id);

        $c->delete();
        clog('delete','success','a supprimé une catégorie de l\'inventaire',null,'App\ItemCategory',$c->id);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckCoursePerm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$perm = 'see')
    {
        $course = \App\Course::findOrFail($request->id);

        if (\Auth::user()->id == $course->user_id)
        {
            if ($perm == 'see' || $perm == 'edit')
            {
                return $next($request);
            }
        }

        if (\Auth::user()->p('course_'.$perm.'_all') == 1)
        {
            return $next($request);
        }
        if (\Auth::user()->p('course_'.$perm) == 1)
        {
            return $next($request);
        }
        if ($perm == 'validate_plan')
        {
            abort(401);
        }
        clog('navigate','danger','L\'utilisateur n\'est pas autorisé à effectuer cette action',\Auth::user()->id);
        return redirect('/admin')->with('error','Vous n\'êtes pas autorisé à effectuer cette action');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function createuser()
    {
        $this->validate(request(), [

            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required'

        ]);

        $user = User::create(request(['name', 'lastname', 'email' , 'password']));

        auth()->login($user);

        return redirect()->home();
    }
}

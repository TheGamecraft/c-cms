@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Ajouter un utilisateur <a href="#"><i class="fa fa-question-circle"
                                                                                 aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <form id="form" action="/admin/user/add" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="firstname">Prénom</label>
                                <input type="text" class="form-control" name="firstname" id="firstname"
                                       aria-describedby="helpId" placeholder="John" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="lastname">Nom de famille</label>
                                <input type="text" class="form-control" name="lastname" id="lastname"
                                       aria-describedby="helpId" placeholder="Doe" required>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-warning" id="email_alert" role="alert" style="display:none;">
                        Les adresses email doivent être identique
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email"
                                       aria-describedby="emailHelp" placeholder="exemple@exvps.ca" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">Confirmer l'addresse email</label>
                                <input type="email" class="form-control" name="emailc" id="emailc"
                                       aria-describedby="emailHelp" placeholder="exemple@exvps.ca"
                                       data-parsley-equalto="#email" required
                                       data-parsley-error-message="Les emails ne sont pas identique">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" name="adresse" id="adresse"
                                       aria-describedby="helpId" placeholder="14 ave Des Rue, Rimouski">
                                <small id="helpId" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="telephone">Telephone</label>
                                <input type="tel"
                                       class="form-control" name="telephone" id="telephone" aria-describedby="helpId"
                                       placeholder="213-546-5401" pattern="([0-9]{3}-[0-9]{3}-[0-9]{4})*"
                                       data-parsley-error-message="Le numéro de téléphone est invalide">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="sexe">Sexe</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="sexe"
                                        id="sexe" required>
                                    <option value="m">Homme</option>
                                    <option value="f">Femme</option>
                                    <option value="a">Autre</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group" style="margin-top: 26px;">
                                <label for="age" style="margin-top: -7px;">Age</label>
                                <input type="number" class="form-control" name="age" id="age" aria-describedby="helpId"
                                       placeholder="Age" min="0" max="100" style="padding-top: 17px;">
                                <small id="helpId" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="rank">Grade</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="rank"
                                        id="rank" required>
                                    @foreach ($RankList as $rank)
                                        <option value="{{$rank->id}}">{{$rank->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="job">Poste</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="job" id="job"
                                        required>
                                    @foreach ($JobsList as $job)
                                        <option value="{{$job->id}}">{{$job->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-warning" id="psw_alert" role="alert" style="display:none;">
                        Les mot de passe doivent être identique
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="psw">Mot de passe</label>
                                <input type="password" class="form-control" name="psw" id="psw"
                                       placeholder="Supermotdepasse" data-parsley-password required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="pswc">Confirmer le mot de passe</label>
                                <input type="password" class="form-control" name="pswc" id="pswc"
                                       placeholder="Supermotdepasse" data-parsley-equalto="#psw"
                                       data-parsley-error-message="Les mot de passe ne sont pas identique" required>
                            </div>
                        </div>
                    </div>
                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $('#form').parsley();
    </script>
@endsection

<input class="d-none" type="checkbox" id="use_course_n{{$niveau}}_p{{$periode}}" name="use_course_n{{$niveau}}_p{{$periode}}" checked>
<div class="row bg-light">
    <div class="col-8 pr-0 m-auto d-flex">
    </div>
    <div class="col-4 text-right">
        <a type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="sr-only">Toggle Dropdown</span>
        </a>
        <div class="dropdown-menu">
            <a id="modeSwitchC{{$niveau}}-{{$periode}}" class="btn-secondary dropdown-item active m-1" onclick="selectCourseMode('course',{{$niveau}},{{$periode}})">Mode "Cours" pour cette période</a>
            <a id="modeSwitchO{{$niveau}}-{{$periode}}" class="btn-secondary dropdown-item m-1" onclick="selectCourseMode('other',{{$niveau}},{{$periode}})">Mode "Autre" pour cette période</a>
            <div class="dropdown-divider"></div>
            <a class="btn-secondary dropdown-item" href="#">Réinitialiser</a>
        </div>
    </div>
</div>
<div class="tab-content text-center">
    <div class="tab-pane active">
        <div class="row pt-2">
            <div class="col-12 d-none" id="descContainer{{$niveau}}-{{$periode}}">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description de la période</label>
                    <textarea class="form-control" name="desc_n{{$niveau}}_p{{$periode}} no-mandatory"  id="exampleFormControlTextarea1" rows="2" placeholder="Description de la période">{{$desc}}</textarea>
                </div>
            </div>
            <div class="col-6 mb-1" id="OCOMContainer{{$niveau}}-{{$periode}}">
                <div class="form-group label-floating">
                    <div class="autocomplete">
                        <input type="text" placeholder="OCOM du cours" id="ocom_n{{$niveau}}_p{{$periode}}" name="ocom_n{{$niveau}}_p{{$periode}}" class="form-control AutoCompleteOCOM no-mandatory" aria-describedby="nameHelp" autocomplete="off" value="{{$ocom}}" required onblur="updateCourseName('{{$niveau}}','{{$periode}}')">
                    </div>
                    <span class="form-control-feedback">
                <i class="material-icons">done</i>
             </span>
                </div>
            </div>
            <div class="col-6" id="nameContainer{{$niveau}}-{{$periode}}">
                <div class="form-group label-floating">
                    <input type="text" placeholder="Nom du cours" id="name_n{{$niveau}}_p{{$periode}}" name="name_n{{$niveau}}_p{{$periode}}" value="{{$name}}" class="form-control no-mandatory" required />
                    <span class="form-control-feedback">
                <i class="material-icons">clear</i>
             </span>
                </div>
            </div>
            <div class="col-6 mb-1">
                <div class="form-group label-floating">
                    <input type="text" placeholder="Lieu du cours" name="location_n{{$niveau}}_p{{$periode}}" value="{{$location}}" class="form-control no-mandatory" required/>
                    <span class="form-control-feedback">
                <i class="material-icons">done</i>
             </span>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group label-floating">
                    <div class="autocomplete">
                        <input type="text" placeholder="Nom de l'instructeur" id="instruc_n{{$niveau}}_p{{$periode}}" name="instruc_n{{$niveau}}_p{{$periode}}" value="{{$instructor}}" class="form-control AutoCompleteUser no-mandatory" aria-describedby="nameHelp" autocomplete="off" required>
                    </div>
                    <span class="form-control-feedback">
                <i class="material-icons">done</i>
             </span>
                </div>
            </div>
        </div>
    </div>
</div>

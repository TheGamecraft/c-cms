@extends('layouts.admin.main')

@section('content')
    <div class="col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-header">
              <strong> Ajouter un message </strong>
            </div>
            <div class="card-body card-block">
                <form action="/admin/message/add" method="POST" enctype="multipart/form-data" class="form-horizontal">

                    {{ csrf_field() }}

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label"> Titre du message</label></div>
                        <div class="col-12 col-md-6"><input id="msg_title" name="msg_title" placeholder="Veuillez indiquer le titre du message" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.add_form_event_name_help')}}</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu du message</label></div>
                        <div class="col-12 col-md-9"><textarea name="msg_body" id="msg_body" rows="9" class="form-control"></textarea>
                        <small class="form-text text-muted">Veuillez écrire le message</small></div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                      </div>
                </form>
            </div>
          </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('calendar.add_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('calendar.add_breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
   <script src="/assets/js/calendar/calendar.js"></script>
   <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=r82pabvd9arn3fjb1e2fsolf2xpixuv4hwfwart4cf1fb7mx"></script>
   <script>
    tinymce.init({
      selector: '#msg_body',
      branding: false,
      menubar: 'edit view format'
    });
    </script>
   <div class="log"></div>
@endsection
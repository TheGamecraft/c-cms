@extends('layouts.admin.main')

@section('content')
    <div class="col-md-8 col-12 mr-auto ml-auto">
        <!--      Wizard container        -->
        <div class="wizard-container">
            <div class="card card-wizard active" data-color="primary">
                <form id="form" action="/admin/setup" method="post" data-parsley-validate novalidate>
                    <div class="card-header text-center">
                        <h3 class="card-title">
                            Bienvenue {{ Auth::user()->fullname() }},
                        </h3>
                        <h5 class="card-description">Avant de pouvoir continer vous devez répondre a quelques
                            questions</h5>
                    </div>
                    <div class="card-body">
                        <h5>Renseignement personnel</h5>
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nom <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="lastname"
                                           value="{{Auth::user()->lastname}}" id="" aria-describedby="helpId" required
                                           placeholder="Doe">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Prenom <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="firstname" id=""
                                           value="{{Auth::user()->firstname}}" aria-describedby="helpId"
                                           placeholder="John" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Adresse</label>
                                    <input type="text" class="form-control" name="address" id=""
                                           value="{{Auth::user()->adress}}" aria-describedby="helpId"
                                           placeholder="16 ave DesRoche, Montréal, QC G0L 1B0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Téléphone</label>
                                    <input type="tel" class="form-control" name="telephone" id=""
                                           value="{{Auth::user()->telephone}}" aria-describedby="helpId"
                                           placeholder="213-546-5401" pattern="([0-9]{3}-[0-9]{3}-[0-9]{4})*"
                                           data-parsley-error-message="Le numéro de téléphone est invalide">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Sexe</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" name="sexe" id="">
                                        <option @if(Auth::user()->sexe == 'm') selected @endif value="m">Homme</option>
                                        <option @if(Auth::user()->sexe == 'f') selected @endif value="f">Femme</option>
                                        <option @if(Auth::user()->sexe == 'a') selected @endif value="a">Autre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5 class="mt-5">Sécurité</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="psw">Nouveau mot de passe <sup class="text-danger">*</sup></label>
                                    <input type="password" class="form-control" name="psw" id="psw"
                                           aria-describedby="helpId" placeholder="" data-parsley-password required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="psw">Confimer le nouveau mot de passe <sup class="text-danger">*</sup></label>
                                    <input type="password" class="form-control" name="psw" id="psw"
                                           aria-describedby="helpId" placeholder="" data-parsley-equalto="#psw" required
                                           data-parsley-error-message="Les mot de passe ne sont pas identique">
                                </div>
                            </div>
                            <div class="col-12">
                                <sup class="text-danger">*</sup> <small>Champ requis</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="submit" class="btn btn-primary btn-block" value="Continuer">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    Dashboard
@endsection

@section('scripts')
    <script>
        $('#form').parsley();
    </script>
@endsection

@extends('layouts.public.main')

@section('content')
    <div class="page-header header-filter clear-filter purple-filter" data-parallax="true" style="background-image: url({{'"'.\App\Config::getData('public_index_img_url').'"'}});">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <div class="brand">
                        <h2>{{ App\Config::getData('text_public_picture_title')}}</h2>
                        <p>{{ App\Config::getData('text_public_picture_desc')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            <div class="section">
                <div class="row">
                    @if ($pictures->isEmpty())
                        <h4 class="text-center">Aucune Photo</h4>
                    @endif
                    @foreach ($pictures as $picture)
                        <div class="col-md-4">
                            <a class="a-without-effect" href="/picture/{{$picture->id}}">
                                <h3><img src="{{$picture->url}}" alt="{{$picture->title}}" width="100%"></h3>
                                <p class="picture-desc-small">{!!$picture->desc!!}</p>
                                <span class="news-small">{{ $picture->created_at }}</span>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="m-3" role="group">
                    {{ $pictures->links() }}
                </div>
                <a href="/" class="btn button primary">Retour</a>
            </div>
        </div>
@endsection

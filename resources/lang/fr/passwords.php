<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => "Le mot de passe doit contenir au moins 6 caractères et être identique à la confirmation.",
    'reset' => "Votre mot de passe a été réinitialisé!",
    'sent' => "Nous vous avons envoyer un lien de récupération à votre adresse email!",
    'token' => "Ce code de récupération est invalide",
    'user' => "Cette adresse email ne correspond à aucun de nos utilisateur!",

];

@servers(['web' => 'deployer@vps188754.vps.ovh.ca'])

@setup
$repository = 'git@gitlab.com:TheGamecraft/c-cms.git';
$release = date('YmdHis');

$releases_dir_697 = '/var/www/c-cms/escadron697/releases';
$app_dir_697 = '/var/www/c-cms/escadron697';
$new_release_dir_697 = $releases_dir_697 .'/'. $release;

$releases_dir_736 = '/var/www/c-cms/escadron736/releases';
$app_dir_736 = '/var/www/c-cms/escadron736';
$new_release_dir_736 = $releases_dir_736 .'/'. $release;

$releases_dir_dev = '/var/www/c-cms/dev/releases';
$app_dir_dev = '/var/www/c-cms/dev';
$new_release_dir_dev = $releases_dir_dev .'/'. $release;

$releases_dir_227 = '/var/www/c-cms/ccmrc227/releases';
$app_dir_227 = '/var/www/c-cms/ccmrc227';
$new_release_dir_227 = $releases_dir_227 .'/'. $release;

$releases_dir_117 = '/var/www/c-cms/ccmrc117/releases';
$app_dir_117 = '/var/www/c-cms/ccmrc117';
$new_release_dir_117 = $releases_dir_117 .'/'. $release;
@endsetup

@story('deploy_697')
clone_repository_697
run_composer_697
update_symlinks_697
@endstory

@story('deploy_736')
clone_repository_736
run_composer_736
update_symlinks_736
@endstory

@story('deploy_dev')
clone_repository_dev
run_composer_dev
update_symlinks_dev
remove_old_release_dev
@endstory

@story('deploy_227')
clone_repository_227
run_composer_227
update_symlinks_227
@endstory

@story('deploy_117')
clone_repository_117
run_composer_117
update_symlinks_117
@endstory

@task('clone_repository_697')
echo 'Cloning repository'
[ -d {{ $releases_dir_697 }} ] || mkdir {{ $releases_dir_697 }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir_697 }}
cd {{ $new_release_dir_697 }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer_697')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir_697 }}
composer install --prefer-dist --no-scripts -q -o --no-dev
npm install
npm run production
@endtask

@task('update_symlinks_697')
echo "Linking storage directory"
rm -rf {{ $new_release_dir_697 }}/storage
ln -nfs {{ $app_dir_697 }}/storage {{ $new_release_dir_697 }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir_697 }}/.env {{ $new_release_dir_697 }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir_697 }} {{ $app_dir_697 }}/current

echo 'Setting permission'
chmod -R 777 {{ $app_dir_697 }}/current/bootstrap/

echo 'Migrate DB'
cd {{ $app_dir_697 }}/current/
yes|php artisan migrate
yes|php artisan db:seed --class=ConfigsTableSeeder
php artisan update

@endtask

@task('clone_repository_736')
echo 'Cloning repository'
[ -d {{ $releases_dir_736 }} ] || mkdir {{ $releases_dir_736 }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir_736 }}
cd {{ $new_release_dir_736 }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer_736')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir_736 }}
composer install --prefer-dist --no-scripts -q -o --no-dev
npm install
npm run production
@endtask

@task('update_symlinks_736')
echo "Linking storage directory"
rm -rf {{ $new_release_dir_736 }}/storage
ln -nfs {{ $app_dir_736 }}/storage {{ $new_release_dir_736 }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir_736 }}/.env {{ $new_release_dir_736 }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir_736 }} {{ $app_dir_736 }}/current

echo 'Setting permission'
chmod -R 777 {{ $app_dir_736 }}/current/bootstrap/

echo 'Migrate DB'
cd {{ $app_dir_736 }}/current/
yes|php artisan migrate
yes|php artisan db:seed --class=ConfigsTableSeeder
php artisan update

@endtask

@task('clone_repository_dev')
echo 'Cloning repository'
[ -d {{ $releases_dir_dev }} ] || mkdir {{ $releases_dir_dev }}
git clone --depth 1 --single-branch -b dev {{ $repository }} {{ $new_release_dir_dev }}
cd {{ $new_release_dir_dev }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer_dev')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir_dev }}
composer install --prefer-dist --no-scripts -q -o --no-dev
npm install
npm run production
@endtask

@task('update_symlinks_dev')
echo "Linking storage directory"
rm -rf {{ $new_release_dir_dev }}/storage
ln -nfs {{ $app_dir_dev }}/storage {{ $new_release_dir_dev }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir_dev }}/.env {{ $new_release_dir_dev }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir_dev }} {{ $app_dir_dev }}/current

echo 'Setting permission'
chmod -R 777 {{ $app_dir_dev }}/current/bootstrap/

echo 'Migrate DB'
cd {{ $app_dir_dev }}/current/
yes|php artisan migrate
yes|php artisan db:seed --class=ConfigsTableSeeder
php artisan update

@endtask

@task('remove_old_release_dev')
cd {{ $releases_dir_dev }}
rm -fr $(ls -t1 | tail -n +3)
@endtask

@task('clone_repository_227')
echo 'Cloning repository'
[ -d {{ $releases_dir_227 }} ] || mkdir {{ $releases_dir_227 }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir_227 }}
cd {{ $new_release_dir_227 }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer_227')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir_227 }}
composer install --prefer-dist --no-scripts -q -o --no-dev
npm install
npm run production
@endtask

@task('update_symlinks_227')
echo "Linking storage directory"
rm -rf {{ $new_release_dir_227 }}/storage
ln -nfs {{ $app_dir_227 }}/storage {{ $new_release_dir_227 }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir_227 }}/.env {{ $new_release_dir_227 }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir_227 }} {{ $app_dir_227 }}/current

echo 'Setting permission'
chmod -R 777 {{ $app_dir_227 }}/current/bootstrap/

echo 'Migrate DB'
cd {{ $app_dir_227 }}/current/
yes|php artisan migrate
yes|php artisan db:seed --class=ConfigsTableSeeder
php artisan update

@endtask

@task('clone_repository_117')
echo 'Cloning repository'
[ -d {{ $releases_dir_117 }} ] || mkdir {{ $releases_dir_117 }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir_117 }}
cd {{ $new_release_dir_117 }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer_117')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir_117 }}
composer install --prefer-dist --no-scripts -q -o --no-dev
npm install
npm run production
@endtask

@task('update_symlinks_117')
echo "Linking storage directory"
rm -rf {{ $new_release_dir_117 }}/storage
ln -nfs {{ $app_dir_117 }}/storage {{ $new_release_dir_117 }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir_117 }}/.env {{ $new_release_dir_117 }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir_117 }} {{ $app_dir_117 }}/current

echo 'Setting permission'
chmod -R 777 {{ $app_dir_117 }}/current/bootstrap/

echo 'Migrate DB'
cd {{ $app_dir_117 }}/current/
yes|php artisan migrate
yes|php artisan db:seed --class=ConfigsTableSeeder
php artisan update

@endtask

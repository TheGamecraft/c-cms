<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;

class GoogleDriveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Storage::extend('google', function($app, $config) {
            $client = new \Google_Client();
            $client->setClientId(\Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_CLIENT_ID')));
            $client->setClientSecret(\Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_CLIENT_SECRET')));
            $client->refreshToken(\Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_REFRESH_TOKEN')));
            $service = new \Google_Service_Drive($client);

            $options = [];
            if(isset($config['teamDriveId'])) {
                $options['teamDriveId'] = $config['teamDriveId'];
            }

            $adapter = new GoogleDriveAdapter($service, \Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_FOLDER_ID')), $options);

            return new \League\Flysystem\Filesystem($adapter);
        });
    }
}

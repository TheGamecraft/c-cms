<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PublicTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUrl()
    {
        $appURL = "dev.c-cms.cf";

        $urls = [
            'login',
            'logout'
        ];

        echo  PHP_EOL;

        foreach ($urls as $url) {
            $response = $this->get($url);
            if((int)$response->status() === 404){
                echo  $appURL . $url . ' (FAILED) return a 404.';
                $this->assertTrue(false);
            } else {
                echo $appURL . $url . ' (SUCCESS)';
                $this->assertTrue(true);
            }
            echo  PHP_EOL;
        }

        foreach ($urls as $url) {
            $response = $this->get($url);
            if((int)$response->status() === 500){
                echo  $appURL . $url . ' (FAILED) return a 500.';
                $this->assertTrue(false);
            } else {
                echo $appURL . $url . ' (SUCCESS)';
                $this->assertTrue(true);
            }
            echo  PHP_EOL;
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('admin_desc');
            $table->string('calendar_color')->default('#FFFFFF');
            $table->string('calendar_icon')->default('fa fa-question-circle');
            $table->string('begin_time')->default('12:00');
            $table->string('end_time')->default('13:00');
            $table->string('location')->default('Escadron');
            $table->boolean('is_mandatory')->default(false);
            $table->boolean('use_weekly_msg')->default(false);
            $table->string('weekly_msg_publication_time')->default('-5day');
            $table->boolean('use_schedule')->default(false);
            $table->text('schedule_model');
            $table->boolean('is_promoted')->default(false);
            $table->boolean('hidden')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_types');
    }
}

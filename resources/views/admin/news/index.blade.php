@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <strong class="card-title">Nouvelles <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <div class="btn-group btn-block">
                    @if(\Auth::user()->p('news_add') == 1)
                        <a name="add" id="add" class="btn btn-outline-primary btn-block" href="/admin/news/add" role="button">Ajouter un nouvelle</a>
                    @endif
                </div>
                <hr>
                <div class="row">
                    @if($news->isEmpty())
                        <div class="col-md-12 text-center">
                            <h4 class="m-4">Aucune nouvelle</h4>
                        </div>
                    @endif
                    @foreach($news as $n)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title news-title">{{$n->title}}</h4>
                                    <p class="category">@if($n->user){{$n->user->fullname()}} -@endif {{$n->created_at}}
                                    <div class="news-tags">
                                        @if($n->tags != [])
                                            @foreach($n->tags as $tag)
                                                @if($tag == "Important")
                                                    <span class="badge badge-pill badge-danger">{{$tag}}</span>
                                                @else
                                                    <span class="badge badge-pill badge-default">{{$tag}}</span>
                                                @endif
                                            @endforeach
                                        @endif
                                        @if($n->files != [""] && isset($n->files))
                                            <span class="badge badge-pill badge-secondary">Fichier joint</span>
                                        @endif
                                    </div>
                                    @if($n->publish == 0) <span class="badge badge-warning float-right">Brouillon</span> @endif</p>
                                </div>
                                <div class="card-body news-body-small">
                                    {!! $n->body !!}
                                </div>
                                <div class="card-footer">
                                    <div class="btn-group">
                                        @if(!isset($n->event_id))
                                            <a href="/news/{{$n->id}}" type="button" class="btn btn-secondary" target="_blank"><i class="fa fa-external-link-alt"></i>&nbsp; Afficher</a>
                                        @else
                                            <a href="/news/{{$n->event_id}}?type=msg" type="button" class="btn btn-secondary" target="_blank"><i class="fa fa-external-link-alt"></i>&nbsp; Afficher</a>
                                        @endif
                                        @if(\Auth::user()->p('news_edit') == 1 && !isset($n->event_id))
                                            <a href="/admin/news/edit/{{$n->id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier</a>
                                        @else
                                            <a href="/admin/schedule/edit/{{$n->event_id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier</a>
                                        @endif
                                        @if(\Auth::user()->p('news_delete') == 1 && !isset($n->event_id))
                                            <a type="button" class="btn btn-danger" onclick="deleteEvent({{$n->id}});"><i class="fa fa-times-circle" style="color:white;"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        <div class="col-12">
                            {{ $news->links() }}
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventaire</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Inventaire</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="https://cdn.datatables.net/rowgroup/1.1.0/js/dataTables.rowGroup.min.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('#log-data').DataTable({
                    "order": [[ 2, "asc" ]],
                    "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                    "rowGroup": {
                        dataSrc: 2
                    }
                });
            } );
        })(jQuery);

        function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
            }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/news/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });


                    })(jQuery);

                    swal(
                        'Supprimé!',
                        "La nouvelle a été supprimé",
                        'success'
                    ).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                }
            })
        }
    </script>
@endsection

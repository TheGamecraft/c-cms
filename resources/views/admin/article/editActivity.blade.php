@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Modification de l'article {{$article->name}} <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <form method="post" action="/admin/article/activity/edit/{{$article->id}}">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Slogan</label>
                                <input class="form-control" type="text" name="public_slogan" value="{{$article->public_slogan}}">
                                <small class="form-text text-muted">Slogan de l'activité</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Url de l'image d'en tête</label>
                                <input class="form-control" type="text" name="public_header_picture" value="{{$article->public_header_picture}}">
                                <small class="form-text text-muted">Url de l'image d'en tête</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Contenu</label>
                                <textarea id="body" name="public_body">{!! $article->public_body !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $('#body').trumbowyg({
            lang: 'fr'
        });
    </script>
@endsection

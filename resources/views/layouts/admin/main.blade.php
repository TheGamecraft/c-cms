<?php

use App\Notifications\Alert;
setlocale(LC_ALL,'fr');

?>
<!doctype html>
<head>

    <!-- Including Head -->
    
        @include('layouts.admin.head')

</head>
<body>
    <div class="wrapper">
    <!-- Including Sidebar -->

            @include('layouts.admin.sidebar')

        <div class="main-panel">

            <!-- Including Header-->
            
                @include('layouts.admin.header')

            <div class="content">
                <div class="content">
                    <div class="container-fluid">

                <!-- Yield Content --> 

                    @include('layouts.admin.alert')

                    @yield('content')
                    </div>
                </div>
            </div>

            <!-- Including Footer -->
            @include('layouts.admin.footer')

        </div>
    </div>
    <!-- Including Javascripts -->
        
        @include('layouts.admin.scripts')

    <!-- Yield Custom Javascipts -->

        @yield('custom_scripts')

</body>
</html>

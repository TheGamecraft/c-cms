<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use phpDocumentor\Reflection\Types\Collection;

/**
 * App\User
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $rank_id
 * @property string $adress
 * @property string $telephone
 * @property string $age
 * @property string $avatar
 * @property string $sexe
 * @property string $job_id
 * @property string $api_token
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Booking[] $bookings
 * @property-read int|null $bookings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Event[] $events
 * @property-read int|null $events_count
 * @property-read \App\Job $job
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\News[] $news
 * @property-read int|null $news_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Rank $rank
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAdress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereJobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSexe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $use_default_psw
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUseDefaultPsw($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rank()
    {
        return $this->belongsTo('App\Rank');
    }

    public function job()
    {
        return $this->belongsTo('App\Job');
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function updateAPI()
    {
        $this->api_token = str_random(60);

        $this->save();
    }

    public function fullname()
    {
        $fullname = $this->lastname.' '.$this->firstname;
        return $fullname;
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function futureCourses()
    {
        return \App\Course::allFutureForUser($this->id);
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->telephone;
    }

    public function getPerm($perm)
    {
        $rank = \App\Rank::find($this->rank);
        $job = \App\Job::find($this->job);

        $rank_perm_value = $rank->$perm;
        $job_perm_value = $job->$perm;
        $user_perm_value = $this->$perm;

        if ($user_perm_value == 1 ) {
            $perm_value = true;
        } else if ($job_perm_value== 1)  {
            $perm_value = true;
        } else if ($rank_perm_value == 1) {
            $perm_value = true;
        } else {
            $perm_value = false;
        }

        return $perm_value;
    }

    public function getAcces($level)
    {
        $rank = \App\Rank::find($this->rank);
        $job = \App\Job::find($this->job);

        $rank_perm_value = $rank->acces_level;
        $job_perm_value = $job->acces_level;
        $user_perm_value = $this->acces_level;

        if ($user_perm_value >= $level ) {
            $perm_value = true;
        } else if ($job_perm_value >= $level)  {
            $perm_value = true;
        } else if ($rank_perm_value >= $level) {
            $perm_value = true;
        } else {
            $perm_value = false;
        }
        return $perm_value;
    }

    public function getClasse()
    {
        $schedules = Schedule::all()->where('type','regular')->sortBy('date');
        $currentUser = \Auth::User();

        $filtered_schedule = collect();
        $user_classes = collect();

        foreach ($schedules as $day) {
            if ($day->date >= date('Y-m-d')) {
                $filtered_schedule->push($day);
            }
        }

        foreach ($filtered_schedule as $schedule) {
            if ($schedule->data['n1_p1_instructor'] == $currentUser->id) {
                $user_classes->push(collect([
                    'date' => $schedule->date,
                    'periode' => '1',
                    'niveau' => '1',
                    'name' => $schedule->data['n1_p1_name'],
                    'ocom' => $schedule->data['n1_p1_ocom'],
                    'local' => $schedule->data['n1_p1_local'],
                    'plan_done' => $schedule->data['n1_p1_plandone'],
                    'material' => $schedule->n1_p1_item,
                ]));
            }

            if ($schedule->data['n1_p2_instructor'] == $currentUser->id) {
                $user_classes->push(collect([
                    'date' => $schedule->date,
                    'periode' => '2',
                    'niveau' => '1',
                    'name' => $schedule->data['n1_p2_name'],
                    'ocom' => $schedule->data['n1_p2_ocom'],
                    'local' => $schedule->data['n1_p2_local'],
                    'plan_done' => $schedule->data['n1_p2_plandone'],
                    'material' => $schedule->n1_p2_item,
                ]));
            }

            if ($schedule->data['n2_p1_instructor'] == $currentUser->id) {
                $user_classes->push(collect([
                    'date' => $schedule->date,
                    'periode' => '1',
                    'niveau' => '2',
                    'name' => $schedule->data['n2_p1_name'],
                    'ocom' => $schedule->data['n2_p1_ocom'],
                    'local' => $schedule->data['n2_p1_local'],
                    'plan_done' => $schedule->data['n2_p1_plandone'],
                    'material' => $schedule->n2_p1_item,
                ]));
            }

            if ($schedule->data['n2_p2_instructor'] == $currentUser->id) {
                $user_classes->push(collect([
                    'date' => $schedule->date,
                    'periode' => '2',
                    'niveau' => '2',
                    'name' => $schedule->data['n2_p2_name'],
                    'ocom' => $schedule->data['n2_p2_ocom'],
                    'local' => $schedule->data['n2_p2_local'],
                    'plan_done' => $schedule->data['n2_p2_plandone'],
                    'material' => $schedule->n2_p2_item,
                ]));
            }

            if ($schedule->data['n3_p1_instructor'] == $currentUser->id) {
                $user_classes->push(collect([
                    'date' => $schedule->date,
                    'periode' => '1',
                    'niveau' => '3',
                    'name' => $schedule->data['n3_p1_name'],
                    'ocom' => $schedule->data['n3_p1_ocom'],
                    'local' => $schedule->data['n3_p1_local'],
                    'plan_done' => $schedule->data['n3_p1_plandone'],
                    'material' => $schedule->n3_p1_item,
                ]));
            }

            if ($schedule->data['n3_p2_instructor'] == $currentUser->id) {
                $user_classes->push(collect([
                    'date' => $schedule->date,
                    'periode' => '2',
                    'niveau' => '3',
                    'name' => $schedule->data['n3_p2_name'],
                    'ocom' => $schedule->data['n3_p2_ocom'],
                    'local' => $schedule->data['n3_p2_local'],
                    'plan_done' => $schedule->data['n3_p2_plandone'],
                    'material' => $schedule->n3_p2_item,
                ]));
            }
        }

        return $user_classes;
    }

    public function countActivity()
    {
        return count(\App\Log::all()->where('user_id',$this->id));
    }

    public function countClasse()
    {
        $nbClasse = 0;

        $schedules = Schedule::all()->where('type','regular')->sortBy('date');

        $filtered_schedule = collect();

        foreach ($schedules as $day) {
            if ($day->date >= date('Y-m-d')) {
                $filtered_schedule->push($day);
            }
        }

        foreach ($filtered_schedule as $schedule) {
            if ($schedule->data['n1_p1_instructor'] == $this->id) {
                $nbClasse = ++$nbClasse;
            }
            if ($schedule->data['n1_p2_instructor'] == $this->id) {
                $nbClasse = ++$nbClasse;
            }
            if ($schedule->data['n2_p1_instructor'] == $this->id) {
                $nbClasse = ++$nbClasse;
            }
            if ($schedule->data['n2_p2_instructor'] == $this->id) {
                $nbClasse = ++$nbClasse;
            }
            if ($schedule->data['n3_p1_instructor'] == $this->id) {
                $nbClasse = ++$nbClasse;
            }
            if ($schedule->data['n3_p2_instructor'] == $this->id) {
                $nbClasse = ++$nbClasse;
            }
        }
        return $nbClasse;
    }

    public function seenMessage($id)
    {
        $this_msg = Message::find($id);

        if(isset($this_msg->data['as_seen']))
        {
            $as_seen = explode("-",$this_msg->data['as_seen']);
            if (array_search(strval($this->id),$as_seen) === false) {
                array_push($as_seen,$this->id);
            }
            $as_seen = array_filter($as_seen);
            $as_seen_str = implode('-',$as_seen);
            $data = [
                'as_seen' => $as_seen_str,
                'parameter' => $this_msg->data['parameter']
            ];
            
            $this_msg->data = $data;

            $this_msg->save();
        }
    }

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public function permission($perm)
    {
        if ($this->job->permission($perm) == 0)
        {
            if ($this->rank->id == 1)
            {
                return 1;
            }
            return $this->rank->permission($perm);
        }
        else
        {
            return $this->job->permission($perm);
        }
    }

    public function p($perm)
    {
        return $this->permission($perm);
    }

    public function getNotificationByDay()
    {
        return \Auth::user()->notifications->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y-m-d');
        });
    }

    public function getNotificationForUI(int $number = 8)
    {
        return \Auth::user()->notifications->take($number)->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y-m-d');
        });
    }
}

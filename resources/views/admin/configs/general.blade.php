@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4>Configuration Générale</h4>
        </div>
        <div class="card-body mt-5">
            <form action="/admin/config/general/edit" method="POST">
                @csrf
                <div class="row ml-2">
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nom complet du CC</label>
                            <input type="text" class="form-control" id="escadron_name_full" name="escadron_name_full" aria-describedby="emailHelp" value="{{\App\Config::getData('escadron_name_full')}}">
                            <small id="emailHelp" class="form-text text-muted">Nom complet du CC.</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nom court du CC</label>
                            <input type="text" class="form-control" name="escadron_name_short" aria-describedby="emailHelp" value="{{\App\Config::getData('escadron_name_short')}}">
                            <small id="emailHelp" class="form-text text-muted">Nom court du CC.</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Numéro du CC</label>
                            <input type="text" class="form-control" name="escadron_number" aria-describedby="emailHelp" value="{{\App\Config::getData('escadron_number')}}">
                            <small id="emailHelp" class="form-text text-muted">Numéro du CC.</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Numéro de téléphone du CC</label>
                            <input type="text" class="form-control" name="escadron_phone" aria-describedby="emailHelp" value="{{\App\Config::getData('escadron_phone')}}">
                            <small id="emailHelp" class="form-text text-muted">Numéro de téléphone du CC.</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Élement du CC</label>
                            <select class="form-control selectpicker" data-style="btn btn-link" name="escadron_element">
                                <option value="Aviation" @if(\App\Config::getData('escadron_element') == 'Aviation') selected @endif>Aviation</option>
                                <option value="Armé" @if(\App\Config::getData('escadron_element') == 'Armé') selected @endif>Armé</option>
                                <option value="Marine" @if(\App\Config::getData('escadron_element') == 'Marine') selected @endif>Marine</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Titre Officiel du CC</label>
                            <select class="form-control selectpicker" data-style="btn btn-link" name="element_title">
                                <option value="Cadet de l'aviation royale du Canada" @if(\App\Config::getData('element_title') == "Cadet de l'aviation royale du Canada") selected @endif>Cadet de l'aviation royale du Canada</option>
                                <option value="Cadets royaux de l’Armée canadienne" @if(\App\Config::getData('element_title') == "Cadets royaux de l’Armée canadienne") selected @endif>Cadets royaux de l’Armée canadienne</option>
                                <option value="Cadets de la Marine royale canadienne" @if(\App\Config::getData('element_title') == "Cadets de la Marine royale canadienne") selected @endif>Cadets de la Marine royale canadienne</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label>Adresse du CC</label>
                            <input type="text" class="form-control" name="escadron_address" aria-describedby="emailHelp" value="{{\App\Config::getData('escadron_address')}}">
                            <small id="emailHelp" class="form-text text-muted">Adresse du CC.</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Lien Google Map du CC</label>
                            <input type="text" class="form-control" name="escadron_direct_googlemap_link" aria-describedby="emailHelp" value="{{\App\Config::getData('escadron_direct_googlemap_link')}}">
                            <small id="emailHelp" class="form-text text-muted">Lien Google Map du CC.</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label>Lien vers la page Facebook du CC</label>
                            <input type="text" class="form-control" name="media_facebook" aria-describedby="emailHelp" value="{{\App\Config::getData('media_facebook')}}">
                            <small id="emailHelp" class="form-text text-muted">Lien vers la page Facebook du CC. Laisser vide si aucun</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Lien vers le compte twitter du CC</label>
                            <input type="text" class="form-control" name="media_twitter" aria-describedby="emailHelp" value="{{\App\Config::getData('media_twitter')}}">
                            <small id="emailHelp" class="form-text text-muted">Lien vers le compte twitter du CC. Laisser vide si aucun</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label>Lien vers le compte instagram du CC</label>
                            <input type="text" class="form-control" name="media_instagram" aria-describedby="emailHelp" value="{{\App\Config::getData('media_instagram')}}">
                            <small id="emailHelp" class="form-text text-muted">Lien vers le compte instagram du CC. Laisser vide si aucun</small>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email du CC</label>
                            <input type="text" class="form-control" name="media_email" aria-describedby="emailHelp" value="{{\App\Config::getData('media_email')}}">
                            <small id="emailHelp" class="form-text text-muted">Email du CC. Laisser vide si aucun</small>
                        </div>
                    </div>
                </div>
                <div class="ml-2 row">
                    <div class="col-md-6 p-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="is_schedule_public" type="checkbox" value="true" @if(\App\Config::getData('is_schedule_public') === "true") checked @endif>
                                L'horaire doit t-il etre disponible publique sur la page d'accueil du site
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 p-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="is_schedule_build" type="checkbox" value="true" @if(\App\Config::getData('is_schedule_build') === "true") checked @endif>
                                L'horaire est t-il complet ? Si cette option est désactivé les utilisateurs ne reseverons pas de notification concernant l'horaire
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12 p-2">
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Configuration Générale</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Configuration/Générale</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script>
    function saveChange(pPerm) {
        (function($) {
		var myswitch = document.getElementById(pPerm);
		$.post('/api/config/general/save?api_token='+api_token, { value: myswitch.checked,perm: pPerm } , function(data) {
            swal({
            title: 'Modification enregistré !',
            type: 'success',
            }).then((result) => {
            if (result.value) {
                location.reload();
            }
            })
          });
	    })(jQuery);
    }
</script>
@endsection

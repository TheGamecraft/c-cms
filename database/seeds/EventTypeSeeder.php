<?php

use Illuminate\Database\Seeder;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_types')->insert([
            [
                'name' => 'Par défault',
                'calendar_color' => '#A4A4A4',
                'calendar_icon' => 'fas fa-book',
                'begin_time' => '18:30',
                'end_time' => '21:45',
                'Location' => 'Escadron',
                'is_mandatory' => '0',
                'use_weekly_msg' => '0',
                'weekly_msg_publication_time' => '-5days',
                'use_schedule' => '0',
                'schedule_model' => '{"periodes":[{"name":"Periode 1","begin_time":"19:00","end_time":"20:10"},{"name":"Pause","begin_time":"20:10","end_time":"20:30"},{"name":"Periode 2","begin_time":"20:30","end_time":"21:20"}],"niveaux":[{"name":"Niveau 1"},{"name":"Niveau 2"},{"name":"Niveau 3"}]}',
                'is_promoted' => '0',
                'admin_desc' => 'Veuillez modifier la description admin par défaut',
            ],
            [
                'name' => 'Soirée d\'instruction régulière',
                'calendar_color' => 'orange',
                'calendar_icon' => 'fas fa-book',
                'begin_time' => '18:30',
                'end_time' => '21:45',
                'Location' => 'Escadron',
                'is_mandatory' => '1',
                'use_weekly_msg' => '1',
                'admin_desc' => '<p><strong>/* Nom de l\'évènement */</strong></p><p>Heure (Cadets):</p><p>Lieu:</p><p>Tenue:</p><p>Matériel:</p><p>/* Commentaire */</p>',
                'weekly_msg_publication_time' => '-5days',
                'use_schedule' => '1',
                'schedule_model' => '{"periodes":[{"name":"Periode 1","begin_time":"19:00","end_time":"20:10"},{"name":"Pause","begin_time":"20:10","end_time":"20:30"},{"name":"Periode 2","begin_time":"20:30","end_time":"21:20"}],"niveaux":[{"name":"Niveau 1"},{"name":"Niveau 2"},{"name":"Niveau 3"}]}',
                'is_promoted' => '0',
            ]
        ]);
    }
}

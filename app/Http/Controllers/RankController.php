<?php

namespace App\Http\Controllers;

use App\Rank;
use Illuminate\Http\Request;

class RankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.configs.ranks.index',['ranks' => Rank::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.configs.ranks.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $r = new Rank();

        $r->name = $request->name;
        $r->desc = $request->desc;
        $r->acces_level = 1;

        $tpermission = [];

        foreach (\App\Permission::all() as $perm)
        {
            $tkey = $perm->ckey;
            $tpermission[$tkey] = $request->$tkey;
        }

        $r->permissions = json_encode($tpermission);
        $r->save();

        clog('add','success','Grade ajouté avec succès');
        return redirect('/admin/config/ranks')->with('success','Grade ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function show($rank)
    {
        return view('admin.configs.ranks.show',['rank' => Rank::find($rank)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function edit(Rank $rank)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rank)
    {
        $r = Rank::find($rank);

        $r->name = $request->name;
        $r->desc = $request->desc;

        $tpermission = [];

        foreach (\App\Permission::all() as $perm)
        {
            $tkey = $perm->ckey;
            $tpermission[$tkey] = $request->$tkey;
        }

        $r->permissions = json_encode($tpermission);
        $r->save();

        clog('edit','success','Grade modifié avec succès');
        return redirect('/admin/config/ranks')->with('success','Grade modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $rank = Rank::find($request->id);
        foreach (\App\User::all() as $user)
        {
            if($user->rank->id == $rank->id)
            {
                $user->rank_id = 1;
                $user->save();
            }
        }
        clog('delete','success','Grade supprimé avec succès');
        $rank->delete();
    }
}

<div class="inner">
    <div class="title text-center">
        <h1>{{ App\Config::getData('text_public_schedule_title')}}@if(request()->has('editMode')) <a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_schedule_title"><i class="fa fa-pencil" aria-hidden="true"></i></a> @endif</h1>
        <p>{{ App\Config::getData('text_public_schedule_desc')}} @if(request()->has('editMode')) <a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_schedule_desc"><i class="fa fa-pencil" aria-hidden="true"></i></a> @endif</p>
    </div>
    <hr class="mx-5">
    <div id="public_calendar"></div>
</div>
@extends('layouts.admin.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Statistique de l'instruction</h4>
                </div>
                <div class="card-body">
                    <div class="content">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>{{$nbCourseUpToThisDay}}</b> cours on été donnée cette année d'instruction par <b>{{$nbInstructorUpToThisDay}}</b> instructeurs différent au cours de <b>{{$nbEventUpToThisDay}}</b> activitées
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Plan de cours remis</h4>
                    <p class="category">Combiens de plan de cours on été remis au courant de cette année d'instruction</p>
                </div>
                <div class="card-body">
                    <div class="content">
                        <div class="row text-center">
                            <div class="chart-container" style="position: relative; width:100%">
                                <canvas id="chart1"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cours de l'année d'instruction</h4>
                    <p class="category">Combiens de cours ont été donnée au courant de cette année d'instruction</p>
                </div>
                <div class="card-body">
                    <div class="content">
                        <div class="row text-center">
                            <div class="chart-container" style="position: relative; width:100%">
                                <canvas id="chart2"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cours total de la DB</h4>
                    <p class="category">Combiens de cours ont été donnée au courant de cette année d'instruction par rapport au total de la DB</p>
                </div>
                <div class="card-body">
                    <div class="content">
                        <div class="row text-center">
                            <div class="chart-container" style="position: relative; width:100%">
                                <canvas id="chart3"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        var ctx1 = $('#chart1');
        var pieChart1 = new Chart(ctx1, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [{{$nbCourseUpToThisDay - ($nbCoursePlanDoneUTDP+$nbCoursePlanDoneAndCheckUTDP)}}, {{$nbCoursePlanDoneUTDP}}, {{$nbCoursePlanDoneAndCheckUTDP}}],
                    backgroundColor: ['#f44336','#ff9800','#4caf50'],
                    hoverBackgroundColor: ['#f66055','#ffad33','#5fb962']
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: ['Non-Remis', 'Remis non validé','Remis et validé']
            },
            options: {}
        });

        var ctx2 = $('#chart2');
        var pieChart2 = new Chart(ctx2, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [{{$nbCourseUpToThisDay}}, {{$nbCourseThisYear - $nbCourseUpToThisDay}}],
                    backgroundColor: ['#4caf50','#ff9800'],
                    hoverBackgroundColor: ['#5fb962','#ffad33']
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: ['Donnée', 'Pas encore donnée']
            },
            options: {}
        });

        var ctx3 = $('#chart3');
        var pieChart3 = new Chart(ctx3, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [{{$nbCourseUpToThisDay}}, {{$nbCourseInDB}}],
                    backgroundColor: ['#4caf50','#ff9800'],
                    hoverBackgroundColor: ['#5fb962','#ffad33']
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: ['Donnée', 'Pas encore donnée']
            },
            options: {}
        });
    </script>
@endsection

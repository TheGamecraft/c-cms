@extends('layouts.admin.main')

@section('content')
    <div class="card">
        <div class="progress progress-bar-top">
            <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
        </div>
        <div class="row ml-3 mr-3 mt-3">
            <div class="col-sm-1 d-inline-flex">
                <button id="backbtn" type="button" onclick="goBack()" class="btn btn-secondary" style="border-radius: 50% !important; width: 3.5rem;height: 3.5rem;margin-bottom: -10px;margin-top: -2px" disabled><i class="fas fa-arrow-left fa-2x" aria-hidden="true" style="margin-left: -0.6rem;"></i></button>
                <button id="refreshbtn" type="button" onclick="refreshFolder()" class="border-0 bg-transparent ml-3 hover-spin cursor active-spin no-outline" style="margin-bottom: -10px;margin-top: -2px;font-size: 1.1rem"><i class="fas fa-sync-alt"></i></button>
            </div>
            <div class="col-md-4 col-sm d-flex justify-content-end offset-md-7 mt-2 mt-sm-0">
                <div class="dropdown mr-md-2 d-none" id="createDropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-plus"></i> Nouveau
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" data-toggle="modal" data-target="#createFolderModal"><i class="fas fa-folder mr-3"></i> Dossier</a>
                    </div>
                </div>
                <div class="dropdown d-none" id="uploadDropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-upload"></i> Téléverser
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" data-toggle="modal" data-target="#uploadFileModal"><i class="fas fa-file mr-3"></i> Fichier</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="loader" class="w-100 h-100" style="background-color: #0000007a;position: absolute; z-index: 5;display: none;border-radius: 6px">
            <div class="d-flex h-100" style="justify-content: center; align-items: center;">
                @loaderDot
            </div>
        </div>
        <div class="card-body mt-0">
            <div class="drive-explorer" style="min-height: 10rem"></div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="createFileModal" id="createFileModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Créer un fichier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/file/create" method="post">
                    @csrf
                    <input class="d-none currentDir" type="text" name="currentDir">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nom du fichier</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="fichier.txt">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Créer</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="createFolderModal" id="createFolderModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Créer un dossier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/folder/create" method="post">
                    @csrf
                    <input class="d-none currentDir" type="text" name="currentDir">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nom du dossier</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Créer</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="uploadFileModal" id="uploadFileModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Téléverser une fichier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/file/upload" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input class="d-none currentDir" type="text" name="currentDir">
                    <div class="modal-body">
                        <div class="form-group bmd-form-group is-filled">
                            <label class="label-control bmd-label-static">Fichier à téléverser (Max 50Mo)</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput" style="display: flex !important;">
                                <div class="form-control" data-trigger="fileinput">
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-append">
                                    <span class="input-group-text fileinput-exists" data-dismiss="fileinput">Supprimer</span>
                                    <span class="input-group-text btn-file">
                                        <span class="fileinput-new">Parcourir</span>
                                        <span class="fileinput-exists">Modifier</span>
                                        <input type="file" name="fichier">
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Téléverser</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    Fichier / Google Drive
@endsection

@section('custom_scripts')
    <script src="/js/plugins/jquery.ui.position.min.js"></script>
    <script src="/js/plugins/jquery.contextMenu.min.js"></script>
    <script src="/js/plugins/drive-explorer.js"></script>
    <script>
        @if(isset($mode))
        init("{{$folder}}","{{$mode}}");
        @else
        init("{{$folder}}");
        @endif
    </script>
@endsection

<?php

namespace App\Http\Controllers;

use App\PublicPage;
use Illuminate\Http\Request;
use PHPUnit\Util\Json;

class PublicPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.public_page.index', ['pages' => PublicPage::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PublicPage  $publicPage
     * @return \Illuminate\Http\Response
     */
    public function show(PublicPage $publicPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PublicPage  $publicPage
     * @return \Illuminate\Http\Response
     */
    public function edit(string $publicPage)
    {
        return view('admin.public_page.edit', ['page' => PublicPage::find($publicPage)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PublicPage  $publicPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $publicPage)
    {
        $page = PublicPage::find($publicPage);

        $page->body = $request->body;
        $page->header = $request->header;
        $page->banner = $request->banner;

        $page->save();

        return redirect('/admin/public-pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PublicPage  $publicPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(PublicPage $publicPage)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PublicPage  $publicPage
     * @return \Illuminate\Http\Response
     */
    public function display(string $publicPage)
    {
        $page = PublicPage::whereName($publicPage)->first();

        if($page == null) {
            abort(404);
        }

        return view('public.publicpage', ['page' => $page]);
    }
}

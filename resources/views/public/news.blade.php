@extends('layouts.public.main')

@section('content')
    <div class="profile-page sidebar-collapse">
        <div class="page-header header-filter" data-parallax="true" style="background-image: url({{'"'.\App\Config::getData('public_index_img_url').'"'}}); transform: translate3d(0px, 0px, 0px);background-position: center"></div>
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto">
                            <div class="profile">
                                <h2 class="text-white" style="margin-top: -8rem">{{ $new->title }}</h2>
                            </div>
                        </div>
                        <div class="col-md-12 mt-5">
                            <div class="row gtr-50 gtr-uniform">
                                @foreach ($new->pictures as $picture)
                                    <div @if ($loop->first)
                                         class="col-12"
                                         @else
                                         class="col-4"
                                            @endif>
                                        <span class="image fit"><a href="/picture/{{$picture->id}}"><img src="{{ $picture->url }}" alt="{{ $picture->title }}"></a></span>
                                    </div>
                                @endforeach
                            </div>
                            <p>
                                {!! $new->body !!}
                            </p>
                            <span class="news-small">@if(\App\User::find($new->user_id)){{ \App\User::find($new->user_id)->fullname()}},@endif {{ $new->created_at }}</span>
                            <div class="news-tags">
                                @if($new->tags != [])
                                    @foreach($new->tags as $tag)
                                        @if($tag == "Important")
                                            <span class="badge badge-pill badge-danger">{{$tag}}</span>
                                        @else
                                            <span class="badge badge-pill badge-default">{{$tag}}</span>
                                        @endif
                                    @endforeach
                                @endif
                                @if($new->files != [""] && isset($new->files))
                                    <span class="badge badge-pill badge-secondary">Fichier joint</span>
                                @endif
                            </div>
                            <div class="col-12 mt-3">
                                @if($new->files != [""] && isset($new->files))
                                    @foreach($new->files as $file)
                                        <a href="{{\App\GoogleDriveFile::getDownloadURL('.Systeme/.Fichier/.MessageDeLaSemaine',$file)}}" class="btn btn-outline-primary btn-round"><div class="d-inline-flex" style="max-width: 10rem;overflow: hidden">{{$file}}</div><i class="fas fa-cloud-download-alt ml-2"></i></a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <a href="/" class="btn button primary">Retour</a>
                </div>
            </div>
        </div>
    </div>
@endsection


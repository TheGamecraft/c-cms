@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Messages au staff<a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <table id="log-data" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $messages = $messages->sortByDesc('created_at');
                        $messages = $messages->values();

                        $nbOfMsg = $messages->count();                       
                        $nbOfRow = ceil($nbOfMsg/3);

                        for ($i=0; $i < $nbOfRow ; $i++) { 
                            echo '<tr>';
                                for ($e=0; $e < 3 ; $e++) { 
                                    if ($e+(3*$i) < $nbOfMsg) {
                                        echo '<td style="width:33%;"><a href="/admin/message/'.$messages[$e+(3*$i)]->id.'"><h3>'.$messages[$e+(3*$i)]->title.'</h3><br><h6>'.\App\User::find($messages[$e+(3*$i)]->user_id)->fullname().' - '.$messages[$e+(3*$i)]->created_at.'</h6><hr><div style="overflow:hidden;height:13.5rem;">'.$messages[$e+(3*$i)]->body.'</div></a><div class="float-right"><a href="/admin/message/edit/'.$messages[$e+(3*$i)]->id.'" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier</a><a type="button" class="btn btn-danger" onclick="deleteEvent('.$messages[$e+(3*$i)]->id.');"><i class="fa fa-times-circle" style="color:white;"></i></a></div></td>';
                                    } else {
                                        echo '<td></td>';
                                    }
                                }
                            echo '</tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('admin/dashboard.page_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('admin/dashboard.breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

<script type="text/javascript">
    (function($) {
            $(document).ready(function() {
                $('#log-data').DataTable({
                    "ordering": false,
                    "order": [],
                    "lengthMenu": [[3, 4, -1], [9, 12, "All"]],
                });
            } );
        })(jQuery);

    function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/message/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "Le message a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
@endsection

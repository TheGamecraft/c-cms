@extends('layouts.admin.main')

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Configuration de l'année d'instruction</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <form method="post">
                        @method('PATCH')
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <h5>Configuration de l'année d'instruction</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="label-control">Date de début de l'année d'instruction</label>
                                    <input name="instruction_year_begin" type="text" class="form-control datetimepicker"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="label-control">Date de fin de l'année d'instruction</label>
                                    <input name="instruction_year_end" type="text" class="form-control datetimepicker" />
                                </div>
                            </div>
                            <div class="col-12">

                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-primary">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Type d'évènement</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <div class="row">
                        @foreach($events_type as $event_type)
                            <div class="col-md-6 col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{$event_type->name}} <span class="float-right"><i class="{{$event_type->calendar_icon}} fa-2x" style="color: {{$event_type->calendar_color}}"></i></span> </h4>
                                    </div>
                                    <div class="card-body">
                                        <p>De <b>{{$event_type->begin_time}}</b> à <b>{{$event_type->end_time}}</b> - <b>{{$event_type->location}}</b></p>
                                        <div class="d-flex m-2">
                                            @if($event_type->is_mandatory)
                                                <i class="fas fa-check-circle fa-2x text-success"></i>
                                                <p class="my-auto ml-3">Obligatoire</p>
                                            @else
                                                <i class="fas fa-times-circle fa-2x text-danger"></i>
                                                <p class="my-auto ml-3">N'est pas obligatoire</p>
                                            @endif
                                        </div>
                                        <div class="d-flex m-2">
                                            @if($event_type->use_weekly_msg)
                                                <i class="fas fa-check-circle fa-2x text-success"></i>
                                                <p class="my-auto ml-3">Utilise les messages de la semaine</p>
                                            @else
                                                <i class="fas fa-times-circle fa-2x text-danger"></i>
                                                <p class="my-auto ml-3">N'utilise pas les messages de la semaine</p>
                                            @endif
                                        </div>
                                        <div class="d-flex m-2">
                                            @if($event_type->use_schedule)
                                                <i class="fas fa-check-circle fa-2x text-success"></i>
                                                <p class="my-auto ml-3">Utilise un horaire</p>
                                            @else
                                                <i class="fas fa-times-circle fa-2x text-danger"></i>
                                                <p class="my-auto ml-3">N'utilise pas un horaire</p>
                                            @endif
                                        </div>
                                        <div style="height: 2.7rem;overflow: hidden">
                                            {!! $event_type->admin_desc !!}
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <a href="/admin/config/instruction/event_type/{{$event_type->id}}" class="btn btn-primary">
                                                    Modifier
                                                </a>
                                            </div>
                                            <div class="col-md-4 text-right">
                                                @if($event_type->id != 1)
                                                    <button class="btn btn-danger btn-just-icon" onclick="deleteEventType({{$event_type->id}})">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                @else
                                                    <button class="btn btn-danger btn-just-icon" disabled>
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-12 text-right">
                            <a class="btn btn-primary" href="/admin/config/instruction/event_type/create">Ajouter un type d'évenement</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        let begin = moment('{{\App\Config::getData('instruction_year_begin')}}');
        let end = moment('{{\App\Config::getData('instruction_year_end')}}');
        $('input[name="instruction_year_begin"]').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            date: new Date(begin)
        });
        $('input[name="instruction_year_end"]').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            date: new Date(end)
        });

        function deleteEventType(id)
        {
            Swal.fire({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
            }).then((result) => {
                if (result.value) {
                    let request = $.ajax({
                        url: "/api/eventType/"+id,
                        method: "DELETE",
                        data: { api_token : api_token },
                        dataType: "html"
                    });
                    request.done(function (msg) {
                        Swal.fire(
                            'Supprimé!',
                            'Le type d\'évenement a été supprimé',
                            'success'
                        ).then(() => {
                            window.location.href = '/admin/config/instruction';
                        });
                    });
                    request.fail(function (jqXHR, textStatus) {
                        Swal.fire({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Le type d\'évenement n\'a pas pus être supprimé',
                            footer: '<small>'+textStatus+'</small>'
                        })
                    })
                }
            });
        }
    </script>
@endsection
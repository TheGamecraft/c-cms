@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Liste des utilisateurs </h4>
        </div>
        <div class="card-body">
            <table id="log-data" class="table table-striped table-no-bordered table-hover dataTable dtr-inline dt-responsive w-100">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Grade</th>
                    <th>Poste</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($Userslist as $item)
                        <tr>
                            <td style="width: 5%;">{{$item->id}}</td>  
                            <td>{{$item->fullname()}}</td>
                            <td>{{$item->rank->name}}</td>
                            <td>{{$item->job->name}}</td>
                            <td class="td-actions text-right" style="width: 12%;">
                                <a class="btn btn-info p-2 text-white" dusk="edit-btn-{{$item->id}}" href="/admin/user/edit/{{$item->id}}"><i class="material-icons">edit</i></a>
                                <a class="btn btn-danger p-2 text-white" dusk="delete-btn-{{$item->id}}" onclick="deleteEvent({{$item->id}});"><i class="material-icons">close</i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <hr>
            <a dusk="add-user-btn" href="/admin/user/add" class="btn btn-primary btn-lg btn-block">Ajouter un utilisateur</a>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    Utilisateur / Liste
@endsection

@section('custom_scripts')

<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('#log-data').DataTable({
                "order": [[ 0, "asc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            });
        } );
    })(jQuery);

    function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/user/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "L'utilisateur a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
@endsection

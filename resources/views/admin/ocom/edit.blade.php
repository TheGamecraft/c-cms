@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{$ocom->ocom.' / '.$ocom->objectif_competence}}</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <form method="post">
                        @method('PATCH')
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="oren">OREN</label>
                                    <input type="text" class="form-control" id="oren" name="oren" placeholder="103" value="{{$ocom->oren}}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="ocom">OCOM</label>
                                    <input type="text" class="form-control" id="ocom" name="ocom" placeholder="M103.06" value="{{$ocom->ocom}}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="objectif_competence">Objectif de compétence</label>
                                    <input type="text" class="form-control" id="objectif_competence" name="objectif_competence" placeholder="Établir un objectif personnel pour l'année d'instruction" value="{{$ocom->objectif_competence}}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nbPeriode">Nombre de période</label>
                                    <input type="number" class="form-control" id="nbPeriode" name="nbPeriode" placeholder="1" value="{{$ocom->nbPeriode}}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="objectif_rendement">Objectif de rendement</label>
                                    <input type="text" class="form-control" id="objectif_rendement" name="objectif_rendement" placeholder="Participer à titre de membre d'une équipe" value="{{$ocom->objectif_rendement}}" required>
                                </div>
                            </div>
                            <div class="col-md-8 text-right">
                                <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                <a class="btn btn-warning" href="{{ url()->previous() }}">Annuler</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <a class="navbar-brand" href="/admin/ocom">Base de données des cours</a> / Modifier /{{$ocom->ocom}}
@endsection

@section('custom_scripts')
@endsection

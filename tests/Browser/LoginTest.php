<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class xLoginTest extends DuskTestCase
{
    public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email','admin@exvps.ca')
                    ->type('password','SuperAdmin')
                    ->click('@login-button')
                    ->assertPathIs('/admin');
        });
    }
}

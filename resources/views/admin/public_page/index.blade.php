@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Pages publiques</h4>
            </div>
            <div class="card-body mt-5">
                <div class="row">
                    @foreach($pages as $page)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-img-top">
                                    <img class="img-responsive w-100" src="{{$page->banner}}">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">{{$page->name}}</h4>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="/admin/public-pages/edit/{{$page->id}}">
                                                    Modifier <i class="fas fa-edit"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12">
                        <a class="btn btn-primary btn-block" href="/admin/picture/add">Ajouter une images</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('custom_scripts')
    {{-- <script>
        function Delete(pID) {
            swal({
                title: 'Êtes vous certain de vouloir supprimer l\'image?',
                text: "Vous ne pourrez pas annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
            }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/picture/delete/'+pID+'?api_token='+api_token, function(data) {
                            console.log('Delete');
                        });


                    })(jQuery);

                    swal(
                        'Supprimé!',
                        "L'image a été supprimé",
                        'success'
                    ).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                }
            })
        }
    </script> --}}
@endsection

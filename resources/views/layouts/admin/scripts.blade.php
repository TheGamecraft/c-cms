<script src="/js/core/jquery.min.js"></script>
<script src="/js/core/popper.min.js"></script>

<!-- Plugin for the Perfect Scrollbar -->
<script src="/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="/js/core/bootstrap-material-design.min.js"></script>
<script src="/js/material-dashboard.js"></script>
<script type="module" src="/js/bootstrap.js"></script>
<script src="/js/app.js"></script>

<!-- Plugin for the momentJs  -->
<script src="/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="/js/plugins/sweetalert2.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="/js/plugins/bootstrap-selectpicker.js" ></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="/js/plugins/jquery.dataTables.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/js/plugins/nouislider.min.js" ></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="/js/plugins/jasny-bootstrap.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- ChartJS -->
<script src="/js/plugins/Chart.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/js/plugins/bootstrap-notify.js"></script>
<!-- Parsley Plugin -->
<script src="/js/plugins/parsley.min.js"></script>
<script src="/js/plugins/parsley-extra.min.js"></script>

<script src="/js/plugins/trumbowyg/trumbowyg.min.js"></script>
<script src="/js/plugins/trumbowyg/langs/fr.js"></script>
<script src="/js/plugins/trumbowyg/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
<script src="/js/plugins/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
<script src="/js/plugins/trumbowyg/plugins/colors/trumbowyg.colors.min.js"></script>
<script src="/js/plugins/trumbowyg/plugins/emoji/trumbowyg.emoji.min.js"></script>
<script src="/js/notify.js"></script>

<script src="/js/plugins/contextLoader.min.js"></script>
<script>
    var api_token = "<?php echo Auth::User()->api_token ?>";
    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
</script>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=r82pabvd9arn3fjb1e2fsolf2xpixuv4hwfwart4cf1fb7mx"></script>

init();

function init() {
    
    var lastid = 99;

    (function($) {
    $( document ).ajaxError(function() {
        $( ".log" ).text( "Triggered ajaxError handler." );
      });
	})(jQuery);
	
	var d = new Date();
	var m = d.getMonth() + 1;
	var y = d.getFullYear();

	generate(m,y);

}

function openCalendar(btnDate)
{
	(function($) {
		var calendarModal = $('.modal-content');
		$.post('/api/calendar/loadDay?api_token='+api_token, { date: btnDate } , function(data) {
			calendarModal.replaceWith(data);
			console.log('Test');
          });

		
	})(jQuery);
}

function calendarOpen(myid) {
		var mydate = document.getElementById(myid).name;
		$(function() {
			var loadingDiv = $('#calendarmodalload');
			$.get('/adminV2/assets/lib/calendar/calendarmodal.php?date='+mydate+'&api_token='+api_token, function(data) {
				loadingDiv.replaceWith(data);
				console.log("Loading day: "+mydate);
			});
		});
}

function calendarEmptyDay(myid) {
	var mydate = document.getElementById(myid).name;
	$(function() {
		var loadingDiv = $('#calendarmodalload');
		$.get('/adminV2/assets/lib/calendar/calendarEmptyDay.php?date='+mydate+'&api_token='+api_token, function(data) {
			loadingDiv.replaceWith(data);
			console.log("Loading empty day: "+mydate);
		});
	});
}

function generate(pmonth,pyear){
	(function($) {
        var mycalendar = $('.calendar');
        
	      $.post('/api/calendar_ecc/generate?api_token='+api_token, { month: pmonth, year: pyear } , function(data) {
            mycalendar.replaceWith(data);
            
            console.log('Calendar Initialised');
          });
    })(jQuery);
}

function switchType()
{		

	(function($) {
		var eventInput = document.getElementById("event_type");
		var eventName = document.getElementById('event_name');
		var isEventMandatory = document.getElementsByName('is_event_mandatory');
		var eventBeginTime = document.getElementById('event_begin_time');
		var eventEndTime = document.getElementById('event_end_time');
		var eventLocation = document.getElementById('event_location');
		var eventDesc = document.getElementById('event_desc');

		var divSpecialSection = document.getElementById('special_section');



		switch (eventInput.value) {
			case "pilotage":
				eventName.value = "Test";
				isEventMandatory[0].checked = false;
				eventBeginTime.value = "09:30";
				eventEndTime.value = "11:30";
				eventLocation.value = "Escadron";
				eventDesc.value = "Cours de pilotage";
				divSpecialSection.style.display = "none";
				break;
			
			case "regular":
				eventName.value = "Soirée d'instruction régulière";
				isEventMandatory[0].checked = true;
				eventBeginTime.value = "18:30";
				eventEndTime.value = "21:45";
				eventLocation.value = "Escadron";
				eventDesc.value = "Soirée d'instruction régulière";

				var n1_p1_plandone = document.getElementById('n1_p1_plandone');
				var n1_p2_plandone = document.getElementById('n1_p2_plandone');
				var n2_p1_plandone = document.getElementById('n2_p1_plandone');
				var n2_p2_plandone = document.getElementById('n2_p2_plandone');
				var n3_p1_plandone = document.getElementById('n3_p1_plandone');
				var n3_p2_plandone = document.getElementById('n3_p2_plandone');

				if (n1_p1_plandone.checked == false) {
					n1_p1_plandone.checked = false;
				}

				if (n1_p2_plandone.checked == false) {
					n1_p2_plandone.checked = false;
				}

				if (n2_p1_plandone.checked == false) {
					n2_p1_plandone.checked = false;
				}

				if (n2_p2_plandone.checked == false) {
					n2_p2_plandone.checked = false;
				}

				if (n3_p1_plandone.checked == false) {
					n1_p1_plandone.checked = false;
				}

				if (n3_p2_plandone.checked == false) {
					n1_p1_plandone.checked = false;
				}

				divSpecialSection.style.display = "block";
				break;
		
			case "drill":
				eventName.value = "Cours de précidrill";
				isEventMandatory[0].checked = false;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "Maison de mon père";
				eventDesc.value = "Cours de précidrill";
				divSpecialSection.style.display = "none";
				break;
	
			case "music":
				eventName.value = "Cours de musique";
				isEventMandatory[0].checked = false;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "";
				eventDesc.value = "Cours de musique";
				divSpecialSection.style.display = "none";
				break;
			
			case "biathlon":
				eventName.value = "Cour de Biathlon";
				isEventMandatory[0].checked = false;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "";
				eventDesc.value = "Cours de biathlon";
				divSpecialSection.style.display = "none";
				break;
			
			case "marksmanship":
				eventName.value = "Tir de précision";
				isEventMandatory[0].checked = false;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "";
				eventDesc.value = "Tir de précision";
				divSpecialSection.style.display = "none";
				break;

			case "founding":
				eventName.value = "";
				isEventMandatory[0].checked = true;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "";
				eventDesc.value = "";
				divSpecialSection.style.display = "none";
				break;

			case "volunteer":
				eventName.value = "";
				isEventMandatory[0].checked = true;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "";
				eventDesc.value = "";
				divSpecialSection.style.display = "none";
				break;
		
			default:
				eventName.value = "";
				isEventMandatory[0].checked = false;
				eventBeginTime.value = "";
				eventEndTime.value = "";
				eventLocation.value = "";
				eventDesc.value = "";
				divSpecialSection.style.display = "none";
				break;
		}
	})(jQuery);
}

function checkBox()
{
	var isEventMandatory = document.getElementsByName('is_event_mandatory');

	console.log(isEventMandatory[0].checked);

	if (isEventMandatory[0].checked) {
		isEventMandatory[0].checked = true
	} else {
		isEventMandatory[0].checked = false
	}
}

function switchToLevelOne() 
{
	var divLevelOne = document.getElementById("grade_1");
	var divLevelTwo = document.getElementById('grade_2');
	var divLevelThree = document.getElementById('grade_3');

	var btnLevelOne = document.getElementById('btn_grade_1');
	var btnLevelTwo = document.getElementById('btn_grade_2');
	var btnLevelThree = document.getElementById('btn_grade_3');

	divLevelOne.style.display = "block";
	divLevelTwo.style.display = "none";
	divLevelThree.style.display = "none";

	btnLevelOne.className = "btn btn-primary btn-lg col-md-4 col-sm-12 active";
	btnLevelTwo.className = "btn btn-primary btn-lg col-md-4 col-sm-12";
	btnLevelThree.className = "btn btn-primary btn-lg col-md-4 col-sm-12";
}

function switchToLevelTwo() 
{
	var divLevelOne = document.getElementById("grade_1");
	var divLevelTwo = document.getElementById('grade_2');
	var divLevelThree = document.getElementById('grade_3');

	var btnLevelOne = document.getElementById('btn_grade_1');
	var btnLevelTwo = document.getElementById('btn_grade_2');
	var btnLevelThree = document.getElementById('btn_grade_3');

	divLevelOne.style.display = "none";
	divLevelTwo.style.display = "block";
	divLevelThree.style.display = "none";

	btnLevelOne.className = "btn btn-primary btn-lg col-md-4 col-sm-12";
	btnLevelTwo.className = "btn btn-primary btn-lg col-md-4 col-sm-12 active";
	btnLevelThree.className = "btn btn-primary btn-lg col-md-4 col-sm-12";
}

function switchToLevelThree() {
	var divLevelOne = document.getElementById("grade_1");
	var divLevelTwo = document.getElementById('grade_2');
	var divLevelThree = document.getElementById('grade_3');

	var btnLevelOne = document.getElementById('btn_grade_1');
	var btnLevelTwo = document.getElementById('btn_grade_2');
	var btnLevelThree = document.getElementById('btn_grade_3');

	divLevelOne.style.display = "none";
	divLevelTwo.style.display = "none";
	divLevelThree.style.display = "block";

	btnLevelOne.className = "btn btn-primary btn-lg col-md-4 col-sm-12";
	btnLevelTwo.className = "btn btn-primary btn-lg col-md-4 col-sm-12";
	btnLevelThree.className = "btn btn-primary btn-lg col-md-4 col-sm-12 active";
}

function switchTypeWithoutOverride()
{
	(function($) {
		var eventInput = document.getElementById("event_type");
		var divSpecialSection = document.getElementById('special_section');



		switch (eventInput.value) {
			case "pilotage":
				divSpecialSection.style.display = "none";
				break;
			
			case "regular":
				divSpecialSection.style.display = "block";

				var n1_p1_plandone = document.getElementById('n1_p1_plandone');
				var n1_p2_plandone = document.getElementById('n1_p2_plandone');
				var n2_p1_plandone = document.getElementById('n2_p1_plandone');
				var n2_p2_plandone = document.getElementById('n2_p2_plandone');
				var n3_p1_plandone = document.getElementById('n3_p1_plandone');
				var n3_p2_plandone = document.getElementById('n3_p2_plandone');

				if (n1_p1_plandone.checked == false) {
					n1_p1_plandone.checked = false;
				}

				if (n1_p2_plandone.checked == false) {
					n1_p2_plandone.checked = false;
				}

				if (n2_p1_plandone.checked == false) {
					n2_p1_plandone.checked = false;
				}

				if (n2_p2_plandone.checked == false) {
					n2_p2_plandone.checked = false;
				}

				if (n3_p1_plandone.checked == false) {
					n1_p1_plandone.checked = false;
				}

				if (n3_p2_plandone.checked == false) {
					n1_p1_plandone.checked = false;
				}
				break;
		
			case "drill":
				divSpecialSection.style.display = "none";
				break;
	
			case "music":
				divSpecialSection.style.display = "none";
				break;
			
			case "biathlon":
				divSpecialSection.style.display = "none";
				break;
			
			case "marksmanship":
				divSpecialSection.style.display = "none";
				break;

			case "founding":
				divSpecialSection.style.display = "none";
				break;

			case "volunteer":
				divSpecialSection.style.display = "none";
				break;
		
			default:
				divSpecialSection.style.display = "none";
				break;
		}
	})(jQuery);
}
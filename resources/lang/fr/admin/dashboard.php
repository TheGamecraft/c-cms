<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Dashboard Language Lines
    |--------------------------------------------------------------------------
    */

    'breadcrumb' => "Tableau de bord",
    'page_title' => "Tableau de bord",
    'cadet_title' => "CADET DE L'AVIATION ROYALE DU CANADA",
    'get_more' => "En apprendre plus !",

];

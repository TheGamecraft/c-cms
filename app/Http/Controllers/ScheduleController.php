<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Schedule;
use function GuzzleHttp\json_encode;
use PDF;

class ScheduleController extends Controller
{
    public function index()
    {
        return view('admin.configs.schedule',['configs' => \App\Config::all(),'events_type' => \App\EventType::all()]);
    }

    public function update(Request $request)
    {
        $instruction_year_begin = \App\Config::find('instruction_year_begin');
        $instruction_year_end = \App\Config::find('instruction_year_end');

        $instruction_year_begin->data = [date('Y-m-d',strtotime($request->instruction_year_begin))];
        $instruction_year_end->data = [date('Y-m-d',strtotime($request->instruction_year_end))];

        $instruction_year_begin->save();
        $instruction_year_end->save();

        return redirect('/admin/config/instruction')->with('success','Modification sauvegarder avec succès !');
    }

    public function apiIndex()
    {
        $start = strtotime(request()->start);
        $end = strtotime(request()->end);
        $allschedules = Schedule::all();

        $allevents = \App\Event::all();
        $events = [];
        $jsonevents = [];
        $schedules = [];

        foreach ($allschedules as $schedule) {
            if(strtotime($schedule->date) >= $start && strtotime($schedule->date) <= $end) {
                array_push($schedules,$schedule);
            }
        }

        foreach ($allevents as $event) {
            if ($event->hidden == 1) {
                if (\Auth::check())
                {
                    if (\Auth::user()->p('instruction_see_hidden_event') == 1)
                    {
                        if(strtotime($event->date_begin) >= $start && strtotime($event->date_begin) <= $end) {
                            array_push($events,$event);
                        }
                        else if(strtotime($event->date_end) >= $start && strtotime($event->date_end) <= $end) {
                            array_push($events,$event);
                        }
                    }
                }
            }
            else
            {
                if(strtotime($event->date_begin) >= $start && strtotime($event->date_begin) <= $end) {
                    array_push($events,$event);
                }
                else if(strtotime($event->date_end) >= $start && strtotime($event->date_end) <= $end) {
                    array_push($events,$event);
                }
            }
        }
        

        foreach ($schedules as $schedule) {
            
            $color = $this->getColor($schedule->type);
            
            $event = [
                'title' => $schedule->data['event_name'],
                'start' => date('c',strtotime($schedule->date.'T'.$schedule->data['event_begin_time'])),
                'end' => date('c',strtotime($schedule->date.'T'.$schedule->data['event_end_time'])),
                'color' => $color,
                'source' => 'schedule',
                'id' => $schedule->id
            ];
            array_push($jsonevents,$event);
        }

        foreach ($events as $event) {

            if($event->calendar_color == null)
            {
                $color = $this->getColor($event->type);
            }
            else
            {
                $color = $event->calendar_color;
            }

            if($event->calendar_icon == null)
            {
                $icon = "";
            }
            else
            {
                $icon = $event->calendar_icon;
            }

            $name = $event->name;

            if ($event->hidden == 1) {
                $color = $this->hex2rgba($color,0.5);
                $icon = 'fas fa-eye-slash';
                $name = 'Caché - '.$name;
            }

            $myevent = [
                'title' => $name,
                'start' => date('c',strtotime($event->date_begin)),
                'end' => date('c',strtotime($event->date_end)),
                'color' => $color,
                'extraParams' => [
                    'db_type' => 'event'],
                'id' => $event->id,
                'icon' => $icon
            ];
            array_push($jsonevents,$myevent);
        }

        return json_encode($jsonevents);
    }

    function hex2rgba($color, $opacity = false) {

        $default = 'rgb(0,0,0)';

        //Return default if no color provided
        if(empty($color))
            return $default;

        //Sanitize $color if "#" is provided
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }


    public function loadModal($id,$db_type)
    {
        if($db_type == "event")
        {
            $event = \App\Event::find($id);
        }
        else
        {
            $event = \App\Schedule::find($id);
        }
        return view('public.modal.schedule',['event' => $event]);
    }

    public function loadModalFull($id,$db_type)
    {
        if($db_type == "event")
        {
            $event = \App\Event::find($id);
        }
        else
        {
            $event = \App\Schedule::find($id);
        }
        return view('admin.schedule.modal.show',['event' => $event]);
    }

    public function getColor($type)
    {
        $activity = \App\ComplementaryActivity::all();
        $color = 'blue';
        switch ($type) {
            case 'regular':
                $color = 'orange';
                break;
            
            case 'pilotage':
               $color = '#58D3F7';
               break;
            
            case 'drill':
                $color = 'blue';
                break;
            
            case 'music':
                $color = 'green';
                break;
            
            case 'biathlon':
                $color = 'red';
                break;
            
            case 'marksmanship':
                $color = 'grey';
                break;
            
            case 'founding':
                $color = '#00FF40';
                break;
            
            case 'volunteer':
                $color = '#DF0174';
                break;
                
            case 'other':
                $color = '#DF0174';
                break;
            
            default:
                if ($activity->find($type)) {
                    $color = $activity->find($type)->calendar_color;
                }
                break;
        };
        return $color;
    }

    public function printtopdf($id)
    {
        $event = \App\Event::find($id);
        //return view('admin.schedule.print.event',['event' => $event]);
        $pdf = PDF::loadView('admin.schedule.print.event',['event' => $event])->setPaper('8.5x11', 'landscape');
		return $pdf->download($event->date_begin.'.pdf');
    }

    public function create($date)
    {
        $date = str_replace('/','-',$date);
        return view('admin.schedule.event.add',['date' => $date]);
    }

    public function loadModalDefautType($type,$date)
    {

        $activity = \App\ComplementaryActivity::find($type);
        $begin_time = $date." ".$activity->begin_time;
        $end_time = $date." ".$activity->end_time;

        $msg_time = Date('c',strtotime($begin_time.'-4day'));

        return view('admin.schedule.modal.add',[
            'activity' => \App\ComplementaryActivity::find($type), 
            'begin_time' => $begin_time,
            'end_time' => $end_time,
            'msg_time' => $msg_time
            ]);
    }

    public function delete($id)
    {
        $event  = \App\Event::find($id);
        if ($event->hidden == 1)
        {
            if (\Auth::user()->p('instruction_see_hidden_event') != 1)
            {
                abort(403);
            }
        }
        foreach ($event->courses as $course)
        {
            $course->delete();
        }
        $event->delete();
    }
}

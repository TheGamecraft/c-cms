<?php

namespace App\Http\Controllers;

use App\ComplementaryActivity;
use Illuminate\Http\Request;

class ComplementaryActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        clogNav('consulte les configurations d\'activité');
        return view('admin.configs.activity',['configs' => \App\Config::all(),'activities' => \App\ComplementaryActivity::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.configs.activity-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $activity = new ComplementaryActivity();

        $activity->name = request('name');
        $activity->admin_desc = request('admin_desc');
        $activity->public_body = 'Veuillez modifier le text de description publique par défaut';
        $activity->calendar_color = request('calendar_color');
        $activity->calendar_icon = request('calendar_icon');
        $activity->begin_time = request('begin_time');
        $activity->end_time = request('end_time');
        $activity->location = request('location');
        $activity->public_slogan = "Veuillez modifier le slogan publique par défaut";
        $activity->public_header_picture = "./assets/img/bg2.jpg";
        $activity->location = request('location');
        $activity->location = request('location');
        if(request('is_mandatory') == 'on')
        {
            $activity->is_mandatory = true;
        }
        else
        {
            $activity->is_mandatory = false;
        }
        if(request('is_promoted') == 'on')
        {
            $activity->is_promoted = true;
        }
        else
        {
            $activity->is_promoted = false;
        }
        
        $activity->save();
        clog('add','success','a ajouté une activité',null,'App\ComplementaryActivity',$activity->id);
        return redirect('/admin/config/activity')->with('success','Activité ajouté avec succes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComplementaryActivity  $complementaryActivity
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('public.activity',['activity' => ComplementaryActivity::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComplementaryActivity  $complementaryActivity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.configs.activity-edit',['activity' => \App\ComplementaryActivity::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComplementaryActivity  $complementaryActivity
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $activity = ComplementaryActivity::find($id);

        $activity->name = request('name');
        $activity->admin_desc = request('admin_desc');
        $activity->calendar_color = request('calendar_color');
        $activity->calendar_icon = request('calendar_icon');
        $activity->begin_time = request('begin_time');
        $activity->end_time = request('end_time');
        $activity->location = request('location');
        if(request('is_mandatory') == 'true')
        {
            $activity->is_mandatory = true;
        }
        else
        {
            $activity->is_mandatory = false;
        }
        if(request('is_promoted') == 'true')
        {
            $activity->is_promoted = true;
        }
        else
        {
            $activity->is_promoted = false;
        }
        
        $activity->save();
        clog('edit','success','a modifié une activité',null,'App\ComplementaryActivity',$activity->id);
        return redirect('/admin/config/activity')->with('success','Modification sauvegarder avec succes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComplementaryActivity  $complementaryActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id = request('id');

        $activity = ComplementaryActivity::find($id);

        clog('delete','success','a supprimé une activité',null,'App\ComplementaryActivity',$id);
        $activity->delete();
    }
}

@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Génération de masse</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="row">
                            @method('PUT')
                            @csrf
                            <div class="col-12">
                                <p>Sélectionner un fichier .cvs contenant l'index des cours a importer.</p>
                            </div>
                            <div class="col-12">
                                <div class="form-group bmd-form-group is-filled">
                                    <label class="label-control bmd-label-static" for="file">Fichier .csv de l'index des cours</label>
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput" style="display: flex !important;">
                                        <div class="form-control" data-trigger="fileinput">
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-append">
                                            <span class="input-group-text fileinput-exists cursor" data-dismiss="fileinput">Supprimer</span>
                                            <span class="input-group-text btn-file">
                                                <span class="fileinput-new cursor">Parcourir</span>
                                                <span class="fileinput-exists cursor">Changer</span>
                                                <input type="file" name="file" accept="text/csv">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Importer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <a class="navbar-brand">Base de données des cours</a> / Génération de masse
@endsection

@section('custom_scripts')
    <script src="/js/ocom.js"></script>
    <script>
        $('.tooltip').tooltip('enable')
    </script>
@endsection

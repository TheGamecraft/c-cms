<div id="accordion" class="col-12" role="tablist">
    <div class="card card-collapse">
        <div class="card-header" role="tab" id="headingOne">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <b>Information générale</b>
                    <i class="material-icons">keyboard_arrow_down</i>
                </a>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Nom de l'événement</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="nameHelp" value="{{$activity->name}}" required>
                        <small id="nameHelp" class="text-muted">Veuillez entrer le nom de l'événement</small>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="form-group">
                        <label class="label-control">Date et Heure de début</label>
                        <input name="begin" type="text" id="datetimepickerbegin" class="form-control datetimepicker" required/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="form-group">
                        <label class="label-control">Date et Heure de fin</label>
                        <input name="end" type="text" id="datetimepickerend" class="form-control datetimepicker" required/>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4">
                    <div class="form-group">
                        <label for="name">Lieux</label>
                        <input type="text" name="location" id="location" class="form-control" placeholder="" aria-describedby="nameHelp" value="{{$activity->location}}" required>
                        <small id="nameHelp" class="text-muted">Veuillez entrer le lieu de l'événement</small>
                    </div>
                </div>
                <div class="col-md-12 mt-4">
                    <div class="form-group">
                        <label for="desc">Description</label>
                        <textarea class="form-control richeditor" name="desc" id="desc" rows="6" required>{{$activity->admin_desc}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-collapse" id="collmessagedelasemaine">
        <div class="card-header" role="tab" id="headingTwo">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <b>Message de la semaine</b>
                    <i class="material-icons">keyboard_arrow_down</i>
                </a>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="label-control">Date et heure de publication des messages de la semaine</label>
                            <input name="date_msg" type="text" id="datetimepickermsg" class="form-control datetimepicker"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="desc">Message de le semaine</label>
                            <textarea class="form-control richeditor" name="msg" id="msg" rows="6">{{\App\Config::getData('default_weekly_msg')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-collapse" id="collschedule">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <b>Horaire</b>
                    <i class="material-icons">keyboard_arrow_down</i>
                </a>
            </h5>
        </div>
        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body" style="overflow: scroll">
                <div id="scheduleEditor" class="m-3" style="width: 90vw">
                    @loaderDot
                </div>
            </div>
        </div>
    </div>
</div>
@if ($activity->id == 99)
    <div class="col-md-12 mt-4 text-center">
        <h4>Horaire d'instruction</h4>
    </div>
    <div class="col-md-12">
        <div id="accordion" role="tablist">
            @for ($i = 1; $i <= \App\Config::getData('admin_level_in_schedule_nb'); $i++)
                <div class="card card-collapse">
                    <div class="card-header" role="tab" id="heading{{$i}}">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                                Horaire Niveau {{$i}}
                                <i class="material-icons">keyboard_arrow_down</i>
                            </a>
                        </h5>
                    </div>

                    <div id="collapse{{$i}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$i}}" data-parent="#accordion">
                        <div class="card-body">

                            @for ($p = 1; $p <= \App\Config::getData('admin_periode_nb'); $p++)
                                <h4 class="mt-3" >Période {{$p}}</h4>
                                <div class="row">
                                    <div class="col-sm-6 my-2">
                                        <div class="form-group">
                                            <label for="name">Nom du cours</label>
                                            <input type="text" name="name_n{{$i}}_p{{$p}}" id="name_n{{$i}}_p{{$p}}" class="form-control" aria-describedby="nameHelp" required @if(env('APP_DEBUG') == true)value="Nom du cours"@endif>
                                            <small id="nameHelp" class="text-muted">Veuillez entrer le nom du cours</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Instructeur</label>
                                            <div class="autocomplete">
                                                <input type="text" name="instruc_n{{$i}}_p{{$p}}" id="instruc_n{{$i}}_p{{$p}}" class="form-control AutoComplete" aria-describedby="nameHelp" autocomplete="off" required>
                                            </div>
                                            <small id="nameHelp" class="text-muted">Veuillez entrer le nom de l'instructeur</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 my-2">
                                        <div class="form-group">
                                            <label for="name">OCOM</label>
                                            <input type="text" name="ocom_n{{$i}}_p{{$p}}" id="ocom_n{{$i}}_p{{$p}}" class="form-control" aria-describedby="nameHelp" required @if(env('APP_DEBUG') == true)value="OCOM"@endif>
                                            <small id="nameHelp" class="text-muted">Veuillez entrer l'OCOM</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 my-2">
                                        <div class="form-group">
                                            <label for="name">Lieux</label>
                                            <input type="text" name="loc_n{{$i}}_p{{$p}}" id="loc_n{{$i}}_p{{$p}}" class="form-control" placeholder="" aria-describedby="nameHelp" required @if(env('APP_DEBUG') == true)value="Lieu"@endif>
                                            <small id="nameHelp" class="text-muted">Veuillez entrer le lieux</small>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endfor
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>

@endif
<script>
    var begin = "<?php echo $begin_time ?>";
    var end = "<?php echo $end_time ?>";
    var msg = "<?php echo $msg_time ?>";
    $('#datetimepickerbegin').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        date: new Date(begin)
    });
    $('#datetimepickerend').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        date: new Date(end)
    });
    $('#datetimepickermsg').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        date: new Date(msg)
    });
    $('.richeditor').trumbowyg({
        lang: 'fr'
    });
</script>
@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Réserver un item
                    <a href="#"><i class="fa fa-question-circle float-right" aria-hidden="true"></i></a>
                </h4>
            </div>
            <div class="card-body">
                <table id="log-data" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Categorie</th>
                        <th>Description</th>
                        <th>Quantité Disponible</th>
                        <th width="15%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->category()->name}}</td>
                            <td>{!! $item->desc !!}</td>
                            <td>{{$item->available($event->date_begin,$event->date_end)}}</td>
                            <td>
                                <button class="btn btn-primary btn-block" onclick='openModal("{{$item->id}}")'>Réserver</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="/admin/booking/{{$event_type}}/{{$event_id}}" class="btn btn-secondary">Retour</a>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="https://cdn.datatables.net/rowgroup/1.1.0/js/dataTables.rowGroup.min.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('#log-data').DataTable({
                    "order": [[ 2, "asc" ]],
                    "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                    "rowGroup": {
                        dataSrc: 2
                    }
                });
            } );
        })(jQuery);

        function openModal(id)
        {
            $.get("/api/booking/modal/item/" + id + "/{{$event_type}}/{{$event_id}}?api_token="+api_token, function (data) {
                $("#modal-content").html(data);
            });
            $('#createModal').modal('toggle')
        }
    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="schedulemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="card card-signup card-plain">
                    <div class="modal-header">
                        <div class="card-header card-header-primary text-center">
                            <h4 class="card-title">Confirmer la réservation</h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="">
                            @csrf
                            <div class="card-body" id="modal-content">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>#</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nom de l'item</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Quantité</label>
                                            <input class="form-control" type="number" min="1" required disabled>
                                            <small class="form-text text-muted">Quantité d'item a réserver</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary" disabled>Confimer</button>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Annuler</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

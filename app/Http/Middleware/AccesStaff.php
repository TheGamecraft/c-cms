<?php

namespace App\Http\Middleware;

use Closure;

class AccesStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->getAcces(1) == false) {
            abord(401);
        }
        return $next($request);
    }
}

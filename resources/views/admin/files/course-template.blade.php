@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Plan de cours vierge et documentation <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i></a></h4>
            </div>
            <div class="card-body">
                <div class="content table-responsive table-full-width">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width:50%;">
                                Nom
                            </th>
                            <th class="text-center">
                                Dernière modification
                            </th>
                            <th class="text-right"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dir as $d)
                            <tr>
                                <td>
                                    {{$d['name']}}
                                </td>
                                <td class="text-center">
                                    {{strftime('%e %b %Y',$d['timestamp'])}}
                                </td>
                                <td class="text-right">
                                    <a class="btn btn-primary btn-fab btn-fab-mini btn-round" target="_blank" href="/file/get?f={{urlencode($d['name'])}}&d={{$d['dirname']}}">
                                        <i class="material-icons">cloud_download</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $(document).ready(function() {
            $('.table').DataTable();
        } );
    </script>
@endsection

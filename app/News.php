<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

/**
 * App\News
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $user_id
 * @property int $publish
 * @property array $tags
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Picture[] $pictures
 * @property-read int|null $pictures_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News wherePublish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereUserId($value)
 * @mixin \Eloquent
 */
class News extends Model
{
    protected $casts = [
        'tags' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pictures()
    {
        return $this->morphMany('App\Picture', 'pictureable');
    }

    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }

    public static function allWithWeeklyMsg()
    {

        $news = \App\News::all();

        foreach (\App\Event::all() as $event)
        {
            if($event->use_weekly_msg == 1)
            {
                if (strtotime($event->weekly_msg_publication_time) <= time())
                {
                    $news->push(self::getWeeklyMsg($event));
                }
            }
        }

        return $news;
    }

    public static function getWeeklyMsg(\App\Event $event)
    {
        $n = new News();

        $n->event_id = $event->id;
        $n->title = 'Message de la semaine du '.date('Y-m-d',strtotime($event->date_begin));
        $n->body = $event->desc;
        $n->user_id = $event->user_id;
        $n->publish = 1;
        $n->created_at = $event->created_at;
        $n->updated_at = $event->updated_at;
        $n->tags = ['message de la semaine','Important'];
        if ($event->weekly_msg_file != null)
        {
            $n->files = $event->weekly_msg_file;
        }
        return $n;
    }
}

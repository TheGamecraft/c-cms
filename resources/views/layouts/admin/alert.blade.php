<div class="col-md-12">
    @php
        $configs = \App\Config::all();
        $scheduleWarning = \App\Schedule::checkForWarning();
        $id_warning = 1;
    @endphp
    <!-- Notification -->
    @if (session('status'))
        <div class="alert alert-info" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('success'))
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                showNotification('success','{{ session('success') }}','top', 'center')
            }, false);
        </script>
    @endif
    @if (session('error'))
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                showNotification('error','{{ session('error') }}','top', 'center')
            }, false);
        </script>
    @endif
    @if (session('danger'))
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                showNotification('error','{{ session('error') }}','top', 'center')
            }, false);
        </script>
    @endif
    @if (session('warning'))
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                showNotification('warning','{{ session('warning') }}','top', 'center')
            }, false);
        </script>
    @endif
    </div>
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('type');
            $table->string('n1_p1_item')->default('');
            $table->string('n1_p2_item')->default('');
            $table->string('n2_p1_item')->default('');
            $table->string('n2_p2_item')->default('');
            $table->string('n3_p1_item')->default('');
            $table->string('n3_p2_item')->default('');
            $table->text('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}

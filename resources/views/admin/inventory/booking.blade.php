@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    @foreach ($schedules as $schedule)
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Réservation de matériel du {{$schedule->date}}</strong>
        </div>
        <div class="card-body">
            <?php
                $remove = [0 => ""];        
                $n1_p1_items = array_diff(explode("-",$schedule->n1_p1_item),$remove);
                $n1_p2_items = array_diff(explode("-",$schedule->n1_p2_item),$remove);
                $n2_p1_items = array_diff(explode("-",$schedule->n2_p1_item),$remove);
                $n2_p2_items = array_diff(explode("-",$schedule->n2_p2_item),$remove);
                $n3_p1_items = array_diff(explode("-",$schedule->n3_p1_item),$remove);
                $n3_p2_items = array_diff(explode("-",$schedule->n3_p2_item),$remove);
            ?>
            <table id="data_{{$schedule->id}}" class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>Niveau</th>
                        <th style="width:45%">1er Période</th>
                        <th style="width:45%">2e Période</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th>1</th>
                    <td>
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <tbody>
                                <tr style="border:none;">
                                    <td style="border:none; width:50%">{{ \App\User::find($schedule->data['n1_p1_instructor'])->fullname()}}</td>
                                    <td style="border:none;width:50%">{{ $schedule->data['n1_p1_ocom'].' - '.$schedule->data['n1_p1_name']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        @if (!count($n1_p1_items) == 0)
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <thead>
                                <tr style="border:none;">
                                    <th style="border:none;">Quantité</th>
                                    <th style="border:none;">Item</th>
                                    <th style="border:none;">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($n1_p1_items as $item)
                                    <tr style="border:none;">
                                        <td style="border:none; width:10%">@php $quantity = explode(":",$item); @endphp {{$quantity[1]}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->name}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->desc}}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        @else
                        <div style="text-align:center;"><span class="badge badge-danger" style="display: initial;">Aucune réservation de matériel</span></div>
                        @endif
                    </td>
                    <td>
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <tbody>
                                <tr style="border:none;">
                                    <td style="border:none; width:50%">{{ \App\User::find($schedule->data['n1_p2_instructor'])->fullname()}}</td>
                                    <td style="border:none;width:50%">{{ $schedule->data['n1_p2_ocom'].' - '.$schedule->data['n1_p2_name']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        @if (!count($n1_p2_items) == 0)
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <thead>
                                <tr style="border:none;">
                                    <th style="border:none;">Quantité</th>
                                    <th style="border:none;">Item</th>
                                    <th style="border:none;">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($n1_p2_items as $item)
                                    <tr style="border:none;">
                                        <td style="border:none; width:10%">@php $quantity = explode(":",$item); @endphp {{$quantity[1]}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->name}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->desc}}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        @else
                        <div style="text-align:center;"><span class="badge badge-danger" style="display: initial;">Aucune réservation de matériel</span></div>
                        @endif
                    </td>
                    </tr>
                    <tr>
                    <th>2</th>
                    <td>
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <tbody>
                                <tr style="border:none;">
                                    <td style="border:none; width:50%">{{ \App\User::find($schedule->data['n2_p1_instructor'])->fullname()}}</td>
                                    <td style="border:none;width:50%">{{ $schedule->data['n2_p1_ocom'].' - '.$schedule->data['n2_p1_name']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        @if (!count($n2_p1_items) == 0)
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <thead>
                                <tr style="border:none;">
                                    <th style="border:none;">Quantité</th>
                                    <th style="border:none;">Item</th>
                                    <th style="border:none;">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($n2_p1_items as $item)
                                    <tr style="border:none;">
                                        <td style="border:none; width:10%">@php $quantity = explode(":",$item); @endphp {{$quantity[1]}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->name}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->desc}}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        @else
                        <div style="text-align:center;"><span class="badge badge-danger" style="display: initial;">Aucune réservation de matériel</span></div>
                        @endif
                    </td>
                    <td>
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <tbody>
                                <tr style="border:none;">
                                    <td style="border:none; width:50%">{{ \App\User::find($schedule->data['n2_p2_instructor'])->fullname()}}</td>
                                    <td style="border:none;width:50%">{{ $schedule->data['n2_p2_ocom'].' - '.$schedule->data['n2_p2_name']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        @if (!count($n2_p2_items) == 0)
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <thead>
                                <tr style="border:none;">
                                    <th style="border:none;">Quantité</th>
                                    <th style="border:none;">Item</th>
                                    <th style="border:none;">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($n2_p2_items as $item)
                                    <tr style="border:none;">
                                        <td style="border:none; width:10%">@php $quantity = explode(":",$item); @endphp {{$quantity[1]}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->name}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->desc}}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        @else
                        <div style="text-align:center;"><span class="badge badge-danger" style="display: initial;">Aucune réservation de matériel</span></div>
                        @endif
                    </td>
                    </tr>
                    <tr>
                    <th>3</th>
                    <td>
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <tbody>
                                <tr style="border:none;">
                                    <td style="border:none; width:50%">{{ \App\User::find($schedule->data['n3_p1_instructor'])->fullname()}}</td>
                                    <td style="border:none;width:50%">{{ $schedule->data['n3_p1_ocom'].' - '.$schedule->data['n3_p1_name']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        @if (!count($n3_p1_items) == 0)
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <thead>
                                <tr style="border:none;">
                                    <th style="border:none;">Quantité</th>
                                    <th style="border:none;">Item</th>
                                    <th style="border:none;">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($n3_p1_items as $item)
                                    <tr style="border:none;">
                                        <td style="border:none; width:10%">@php $quantity = explode(":",$item); @endphp {{$quantity[1]}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->name}}</td>
                                        <td style="border:none;">{{\App\Item::find($item)->desc}}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        @else
                        <div style="text-align:center;"><span class="badge badge-danger" style="display: initial;">Aucune réservation de matériel</span></div>
                        @endif
                    </td>
                    <td>
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <tbody>
                                <tr style="border:none;">
                                    <td style="border:none; width:50%">{{ \App\User::find($schedule->data['n3_p2_instructor'])->fullname()}}</td>
                                    <td style="border:none;width:50%">{{ $schedule->data['n3_p2_ocom'].' - '.$schedule->data['n3_p2_name']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        @if (!count($n3_p2_items) == 0)
                        <table class="table float center" style="border:none; max-width:80%; margin:auto;">
                            <thead>
                                <tr style="border:none;">
                                    <th style="border:none;">Quantité</th>
                                    <th style="border:none;">Item</th>
                                    <th style="border:none;">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($n3_p2_items as $item)
                                        <tr style="border:none;">
                                            <td style="border:none; width:10%">@php $quantity = explode(":",$item); @endphp {{$quantity[1]}}</td>
                                            <td style="border:none;">{{\App\Item::find($item)->name}}</td>
                                            <td style="border:none;">{{\App\Item::find($item)->desc}}</td>
                                        </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        @else
                        <div style="text-align:center;"><span class="badge badge-danger" style="display: initial;">Aucune réservation de matériel</span></div>
                        @endif
                    </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @endforeach
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventaire</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Inventaire</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

@foreach ($schedules as $schedule)
    <script>
        (function($) {
            $(document).ready(function() {
                $('#data_{{$schedule->id}}').DataTable({
                    "order": [[ 0, "asc" ]],
                    "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                });
            } );
            })(jQuery);
    </script>
@endforeach

<script type="text/javascript">
     function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/item/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "L'item a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
@endsection

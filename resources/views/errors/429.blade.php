@extends('errors.layout')

@section('title', __('Trop de requêtes'))
@section('code')
    <span>4</span><span>2</span><span>9</span>
@endsection
@section('message', 'Nous sommes désolé, mais le client a émis trop de requêtes. ')
@section('error',$exception->getMessage())
@section('url','https://developer.mozilla.org/fr/docs/Web/HTTP/Status/429')

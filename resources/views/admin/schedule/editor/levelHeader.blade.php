<div id="levelHeader-{{$level_id}}" class="col border-right border-bottom bg-dark text-white">
    <div class="row">
        <div class="col-9">
            <div class="form-group label-floating">
                <input type="text" placeholder="Niveau" name="level_name_{{$level_id}}" class="form-control text-white" value="{{$level_name}}" />
                <span class="form-control-feedback">
                <i class="material-icons">clear</i>
             </span>
            </div>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-link btn-sm dropdown-toggle dropdown-toggle-split text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="sr-only">Toggle Dropdown</span>
            </a>
            <div class="dropdown-menu">
                <a id="modeSwitchPeriodeC{{$level_id}}" class="btn-secondary dropdown-item m-1" onclick="selectCourseModeNiveau('course',{{$level_id}})">Mode "Cours" pour toutes les périodes</a>
                <a id="modeSwitchPeriodeO{{$level_id}}" class="btn-secondary dropdown-item m-1" onclick="selectCourseModeNiveau('other',{{$level_id}})">Mode "Autre" pour toutes les période</a>
            </div>
        </div>
    </div>
</div>

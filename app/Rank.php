<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Rank
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property string $acces_level
 * @property string $permissions
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereAccesLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Rank extends Model
{
    public function permissions()
    {
        return collect(json_decode($this->permissions,true));
    }

    public function permission($permission)
    {
        foreach ($this->permissions() as $perm => $value)
        {
            if ($permission == $perm)
            {
                return $value;
            }
        }

        return 0;
    }

    public function p($perm)
    {
        return $this->permission($perm);
    }
}

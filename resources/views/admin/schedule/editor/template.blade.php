<div class="row">
    <div class="col-2 p-3 border-right border-bottom bg-dark text-white">
        <b>
            Niveau/Periode
        </b>
    </div>
    @foreach($eventType->schedule_model['niveaux'] as $niveau)
        @include('admin.schedule.editor.levelHeader',['level_id' => $loop->index+1,'level_name' => $niveau['name']])
    @endforeach
    <div class="col-1">
        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="addLevel()">
            <i class="material-icons">add</i>
        </a>
        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="removeLevel()">
            <i class="material-icons">remove</i>
        </a>
    </div>
</div>
@foreach($eventType->schedule_model['periodes'] as $periode)
    @include('admin.schedule.editor.periode',[
    'periode_name' => $periode['name'],
    'periode_begin_time' => $periode['begin_time'],
    'periode_end_time' => $periode['end_time'],
    'periode_id' => $loop->index+1,
    'nbLevel' => count($eventType->schedule_model['niveaux'])
    ])
@endforeach
<div class="row">
    <div class="col-2 p-2">
        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="addPeriode()">
            <i class="material-icons">add</i>
        </a>
        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="removePeriode()">
            <i class="material-icons">remove</i>
        </a>
    </div>
</div>

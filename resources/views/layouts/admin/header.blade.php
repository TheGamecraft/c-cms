<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
      <div class="navbar-wrapper">
        <div class="navbar-minimize">
          <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
            <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
            <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
          </button>
        </div>
        {{ Breadcrumbs::render() }}
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end">
        <!--<form class="navbar-form">
          <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
              <i class="material-icons">search</i>
              <div class="ripple-container"></div>
            </button>
          </div>
        </form>-->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="https://gitlab.com/TheGamecraft/c-cms/wikis/home" data-toggle="tooltip" data-placement="bottom" title="Aide">
              <i class="material-icons">help</i>
              <p class="d-lg-none d-md-block">
                Aide
              </p>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">notifications</i>
                <span id="notification-number" number="{{count(Auth::user()->unreadNotifications)}}" class="notification @if (count(Auth::user()->unreadNotifications) == 0) d-none @endif">{{ count(Auth::user()->unreadNotifications) }}</span>
              <p class="d-lg-none d-md-block">
                Notifications
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-notification" aria-labelledby="navbarDropdownMenuLink">
              <div class="card-header">
                <h6 class="text-center m-0">Notification</h6>
              </div>
              <div class="card-body pt-2" style="max-height: 450px;overflow: auto">
                <div class="row">
                  <div class="col-6">

                  </div>
                  <div class="col-6 text-right">
                    <a class="btn btn-sm btn-outline-info text-info" onclick="markAllNotificationAsRead()">Tous lu</a>
                  </div>
                </div>
                @foreach(Auth::user()->getNotificationForUI() as $key => $notificationGroup)
                  <small class="text">{{$key}}</small>
                  <div class="card mb-3 mt-1" style="width: 18rem;">
                    <ul id="notification-container" class="list-group list-group-flush">
                      @foreach($notificationGroup as $notification)
                        <li id="{{$notification->id}}" class="list-group-item @if($notification->read_at == null)cursor notification-success @endif" style="border-bottom: solid 1px lightgrey" @if($notification->read_at == null) onclick="markNotificationAsRead('{{$notification->id}}')" @endif>
                          <div class="d-flex">
                            <div style="min-width: 2rem">
                              @if(isset($notification->data['icon']))
                              {!! $notification->data['icon'] !!}
                                @endif
                            </div>
                            <div>
                              <b class="mb-1">{{$notification->data['name']}}</b>
                              <p class="text-muted m-0">{{$notification->data['msg']}}</p>
                            </div>
                          </div>
                        </li>
                      @endforeach
                    </ul>
                  </div>
                @endforeach
              </div>
              <div class="card-footer">
                <a href="/admin/profil/notifications" class="btn btn-outline-info text-info">Afficher toutes mes notifications</a>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">person</i>
              <p class="d-lg-none d-md-block">
                Profil
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
              <a class="dropdown-item" href="/admin/profil">Mon Profil</a>
              <a class="dropdown-item" href="/admin/profil/courses">Mes cours</a>
              <a class="dropdown-item" href="/admin/profil/settings">Options</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/logout">Déconnexion</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>

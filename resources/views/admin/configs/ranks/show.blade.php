@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Configuration du grade {{$rank->name}}</h4>
            </div>
            <div class="card-body mt-5">
                <form method="post" action="">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nom</label>
                                <input class="form-control" type="text" name="name" value="{{$rank->name}}" required>
                                <small class="form-text text-muted">Nom du grade</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id="desc" name="desc" required>{!! $rank->desc !!}</textarea>
                                <small class="form-text text-muted">Description du grade</small>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h3>Permissions du Grade</h3>
                            <p>
                                Choisissez quels permissions doivent être associé avec ce grade.
                            </p>
                            <table id="permission-table" class="table table-bordered" style="width:100%">
                                <thead class="d-none">
                                    <th class="d-none">Cat</th>
                                    <th>Nom</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @foreach(\App\Permission::allToArray() as $key => $cat)
                                        @foreach($cat as $r)
                                            <tr>
                                                <td class="d-none">{{$key}}</td>
                                                <td>
                                                    <input class="d-none" type="text" id="{{$r['ckey']}}"
                                                           name="{{$r['ckey']}}" value="{{$rank->p($r['ckey'])}}">
                                                    {{$r['communName']}}<br>
                                                    <small>{{$r['desc']}}</small>
                                                </td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <button type="button" id="{{$r['ckey']}}-close"
                                                                class="btn btn-just-icon @if($rank->p($r['ckey']) != -1) btn-outline-danger @else btn-danger @endif"
                                                                onclick="switchPermissionSwitch('{{$r['ckey']}}','close')">
                                                            <i class="fas fa-times"></i></button>
                                                        <button type="button" id="{{$r['ckey']}}-plus"
                                                                class="btn btn-just-icon @if($rank->p($r['ckey']) != 1) btn-outline-success @else btn-success @endif"
                                                                onclick="switchPermissionSwitch('{{$r['ckey']}}','plus')">
                                                            <i class="fa fa-plus"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                            {{--<div id="accordion" role="tablist">
                                @foreach(\App\Permission::allToArray() as $key => $cat)
                                    <div class="mb-4">
                                        <h3 class="text-capitalize">{{$key}}</h3>
                                        <div class="row">
                                            @foreach($cat as $r)
                                                <div class="col-10">
                                                    <input class="d-none" type="text" id="{{$r['ckey']}}"
                                                           name="{{$r['ckey']}}" value="{{$rank->p($r['ckey'])}}">
                                                    <p>{{$r['communName']}}<br>
                                                        <small>{{$r['desc']}}</small>
                                                    </p>
                                                </div>
                                                <div class="col-2">
                                                    <div class="btn-group">
                                                        <button type="button" id="{{$r['ckey']}}-close"
                                                                class="btn btn-just-icon @if($rank->p($r['ckey']) != -1) btn-outline-danger @else btn-danger @endif"
                                                                onclick="switchPermissionSwitch('{{$r['ckey']}}','close')">
                                                            <i class="fas fa-times"></i></button>
                                                        <button type="button" id="{{$r['ckey']}}-plus"
                                                                class="btn btn-just-icon @if($rank->p($r['ckey']) != 1) btn-outline-success @else btn-success @endif"
                                                                onclick="switchPermissionSwitch('{{$r['ckey']}}','plus')">
                                                            <i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>--}}
                        </div>
                        <div class="col-md-12 mt-5">
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="/js/permissions.js"></script>
    <script>
        $('#desc').trumbowyg({
            lang: 'fr'
        });
        $(document).ready(function() {
            $('#permission-table').DataTable({
                rowGroup: {
                    dataSrc: 0
                },
                pageLength: 100,
                lengthChange: false,
                columnDefs: [
                    { "visible": false, "targets": [0] }
                ]
            });
        } );
    </script>
@endsection
function showNotification(type,msg,from, align){

    switch (type) {
        case 'success':
            var icon = 'check_circle'
            break;

        case 'error':
            var icon = 'error'
            type = 'danger'
            break;
        
        case 'warning':
            var icon = 'warning'
            break;
    
        default:
            var icon = "bug_report"
            break;
    }

    $.notify({
        icon: icon,
        message: msg
  
    },{
        type: type,
        timer: 2500,
        placement: {
            from: from,
            align: align
        }
    });
  }
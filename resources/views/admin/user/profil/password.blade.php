@extends('layouts.admin.main')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Modification du mot de passe <i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Félicitation</span>
                        Votre avatar a été mis a jour !
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
            <form autocomplete="off" action="/admin/profil/edit/password" method="POST">
                @csrf
                <div class="alert alert-warning" id="psw_alert" role="alert" style="display:none;">
                    Les mot de passe doivent être identique
                </div>
                <div class="form-group">
                  <label for="pws">Entrer votre nouveau mot de passe</label>
                  <input type="password" class="form-control" name="psw" id="psw" onkeyup="checkPassword()">
                </div>
                <div class="form-group">
                    <label for="pwsc">Confirmer votre nouveau mot de passe</label>
                    <input type="password" class="form-control" name="pswc" id="pswc" onkeyup="checkPassword()">
                </div>
                <button type="submit" id="submit" class="btn btn-primary" disabled>Enregistrer</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Profil d'utilisateur</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Utilisateur/Profil/Mot de passe</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script src="/assets/admin/assets/js/user.js"></script>
@endsection

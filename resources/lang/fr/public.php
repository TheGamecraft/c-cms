<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Public Language Lines
    |--------------------------------------------------------------------------
    */

    'description' => "LES CADETS DE L'AIR S'ADRESSENT AUX JEUNES DE 12 À 18 ANS QUI DÉSIRENT VIVRE DES EXPÉRIENCES ENRICHISSANTES ET RELEVER DE NOUVEAUX DÉFIS, EN PARTICIPANT À DES ACTIVITÉS STIMULANTES DANS UN CADRE DYNAMIQUE ET CHALEUREUX.",
    'cadet_title' => "CADET DE L'AVIATION ROYALE DU CANADA",
    'get_more' => "En apprendre plus !",

];

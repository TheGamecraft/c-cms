<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Collection;

class StatsController extends Controller
{
    public function instruction()
    {
        $instructorUTTD = collect();

        $coursesTY = \App\Course::allThisYear();
        $nbCoursePlanDoneTY = 0;
        $nbCoursePlanDoneAndCheckTY = 0;
        foreach ($coursesTY as $course)
        {
            if ($course->lessonPlan)
            {
                if ($course->lessonPlan->approved)
                {
                    $nbCoursePlanDoneAndCheckTY++;
                }
                else
                {
                    $nbCoursePlanDoneTY++;
                }
            }
        }

        $coursesUTTD = clone $coursesTY;
        $nbCoursePlanDoneUTDP = 0;
        $nbCoursePlanDoneAndCheckUTDP = 0;
        foreach ($coursesUTTD as $key => $course)
        {
            if (date('c',strtotime($course->event->date_begin)) >= date('c'))
            {
                $coursesUTTD->forget($key);
            }
            else
            {
                $instructorUTTD->push($course->instructor());
                if ($course->lessonPlan)
                {
                    if ($course->lessonPlan->approved)
                    {
                        $nbCoursePlanDoneAndCheckUTDP++;
                    }
                    else
                    {
                        $nbCoursePlanDoneUTDP++;
                    }
                }
            }
        }
        $instructorUTTD = $instructorUTTD->unique();
        $eventTY = \App\Event::allThisYear();
        $eventUTTD = clone $eventTY;
        foreach ($eventUTTD as $key => $event)
        {
            if (date('c',strtotime($event->date_begin)) >= date('c'))
            {
                $eventUTTD->forget($key);
            }
        }

        return view('admin.stats.instruction',[
            'nbCourseThisYear' => count($coursesTY),
            'nbInstructorUpToThisDay' => count($instructorUTTD),
            'nbEventThisYear' => count($eventTY),
            'nbEventUpToThisDay' => count($eventUTTD),
            'nbCourseUpToThisDay' => count($coursesUTTD),
            'nbCoursePlanDoneUTDP' => $nbCoursePlanDoneUTDP,
            'nbCoursePlanDoneTY' => $nbCoursePlanDoneTY,
            'nbCoursePlanDoneAndCheckUTDP' => $nbCoursePlanDoneAndCheckUTDP,
            'nbCoursePlanDoneAndCheckTY' => $nbCoursePlanDoneAndCheckTY,
            'nbCourseInDB' => count(\App\OCOM::all()),
        ]);
    }
}

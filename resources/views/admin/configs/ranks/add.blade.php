@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Ajouter un grade</h4>
            </div>
            <div class="card-body mt-5">
                <form method="post" action="/admin/config/ranks/add">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nom</label>
                                <input class="form-control" type="text" name="name" required>
                                <small class="form-text text-muted">Nom du grade</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id="desc" name="desc" required></textarea>
                                <small class="form-text text-muted">Description du grade</small>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h3>Permissions du Grade</h3>
                            <p>
                                Choisissez quels permissions doivent être associé avec ce grade.
                            </p>
                            <table id="permission-table" class="table table-bordered" style="width:100%">
                                <thead class="d-none">
                                <th class="d-none">Cat</th>
                                <th>Nom</th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach(\App\Permission::allToArray() as $key => $cat)
                                    @foreach($cat as $r)
                                        <tr>
                                            <td class="d-none">{{$key}}</td>
                                            <td>
                                                <input class="d-none" type="text" id="{{$r['ckey']}}" name="{{$r['ckey']}}">
                                                {{$r['communName']}}<br>
                                                <small>{{$r['desc']}}</small>
                                            </td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button type="button" id="{{$r['ckey']}}-close" class="btn btn-just-icon btn-outline-danger" onclick="switchPermissionSwitch('{{$r['ckey']}}','close')"><i class="fas fa-times"></i></button>
                                                <!--<button type="button" id="{{$r['ckey']}}-slash" class="btn btn-just-icon btn-warning" onclick="switchPermissionSwitch('{{$r['ckey']}}','slash')"><i class="fa fa-slash"></i></button>-->
                                                    <button type="button" id="{{$r['ckey']}}-plus" class="btn btn-just-icon btn-outline-success" onclick="switchPermissionSwitch('{{$r['ckey']}}','plus')"><i class="fa fa-plus"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 mt-5">
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="/js/permissions.js"></script>
    <script>
        $('#desc').trumbowyg({
            lang: 'fr'
        });
        $(document).ready(function() {
            $('#permission-table').DataTable({
                rowGroup: {
                    dataSrc: 0
                },
                pageLength: 100,
                lengthChange: false,
                columnDefs: [
                    { "visible": false, "targets": [0] }
                ]
            });
        } );
    </script>
@endsection
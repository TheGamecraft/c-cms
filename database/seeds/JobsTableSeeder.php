<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
            [
                'name' => "Indéterminé",
                'desc' => "Aucun poste",
                'permissions' => \App\Permission::allToString(0)
            ]
        ]);
    }
}

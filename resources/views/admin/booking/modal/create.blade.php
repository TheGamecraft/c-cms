
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <label>#</label>
            <input class="form-control" type="text" disabled value="{{$item->id}}">
        </div>
    </div>
    <div class="col-md-2 d-none">
        <div class="form-group">
            <label>#</label>
            <input class="form-control" name="item_id" readonly type="text" value="{{$item->id}}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Nom de l'item</label>
            <input class="form-control" type="text" disabled value="{{$item->name}}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" disabled>{!! $item->desc !!}</textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>Quantité</label>
            <input class="form-control" name="amount" type="number" min="1" required value="1" max="{{$item->available($event->date_begin,$event->date_end)}}">
            <small class="form-text text-muted">Quantité d'item a réserver</small>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>Commentaire</label>
            <textarea class="form-control" name="comment"></textarea>
            <small class="form-text text-muted">Commantaire pour l'officier d'appro</small>
        </div>
    </div>
    <div class="col-md-6">
        <button type="submit" class="btn btn-primary">Confimer</button>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Annuler</button>
    </div>
</div>

@extends('layouts.admin.main')

@section('content')
    <div class="col-sm-12 col-lg-12 text-right">
        <a class="btn btn-primary btn-round" href="/admin/schedule/table">
            <i class="material-icons">table_view</i> Vue Tableau
        </a>
    </div>
    <div class="col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-body pb-0">
                <div id="fullCalendar"></div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width:1500px !important;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="loader">
                        <img class="loader-bg" src="/images/leaf_of_canada.png"></img>    
                        <div class="loader-spinner"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    Horaire
@endsection

@section('custom_scripts')
    <script src='/assets/fullcalendar/core/main.js'></script>
    <script src='/assets/fullcalendar/core/locales/fr-ca.js'></script>
    <script src='/assets/fullcalendar/daygrid/main.js'></script>
    <script src='/assets/fullcalendar/interaction/main.js'></script>
    <script src='/assets/fullcalendar/list/main.js'></script>
    <script src="/js/calendar.js"></script>
    <div class="log"></div>
   <script>
       initFullCalendar(api_token)
    </script>
    <div class="modal fade" id="schedulemodal" tabindex="-1" role="dialog" aria-labelledby="schedulemodal" aria-hidden="true">
            <div class="modal-dialog w-100 modal-lg mx-2 mx-lg-auto" role="document">
                <div class="modal-content" id="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Chargement</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body h-75">
                        <div class="spinner-border text-muted"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
@endsection

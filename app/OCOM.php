<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function GuzzleHttp\Promise\all;

/**
 * App\OCOM
 *
 * @property int $id
 * @property string $ocom
 * @property string $objectif_competence
 * @property string $nbPeriode
 * @property string $objectif_rendement
 * @property string $oren
 * @property int $complementary
 * @property string $course_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereComplementary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereNbPeriode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereObjectifCompetence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereObjectifRendement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereOcom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereOren($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OCOM whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OCOM extends Model
{
    public function courses()
    {
        $courses_id = explode(',',$this->course_id);
        $courses = collect();
        foreach ($courses_id as $course_id)
        {
            $courses->push(\App\Course::find($course_id));
        }
        return $courses;
    }

    public function saveCourses($courses)
    {
        $courses_id = explode(',',$this->course_id);
        foreach ($courses as $cours)
        {
            if ($cours != null){
                array_push($courses_id,$cours->id);
            }
        }
        $courses_id = array_unique($courses_id);
        $courses_id = array_filter($courses_id);
        $this->course_id = implode(',',$courses_id);
        $this->save();
    }

    public function saveCourse($course)
    {
        $courses_id = explode(',',$this->course_id);
        array_push($courses_id,$course->id);
        $courses_id = array_unique($courses_id);
        $courses_id = array_filter($courses_id);
        $this->course_id = implode(',',$courses_id);
        $this->save();
    }

    public function wasGiven()
    {
        $courses = Course::where('ocom',$this->ocom)->get();
        $result = collect();
        $from = strtotime(\App\Config::getData('instruction_year_begin'));
        $to = strtotime(\App\Config::getData('instruction_year_end'));

        foreach ($courses as $course)
        {
            $event = $course->event;
            if (strtotime($event->date_begin) >= $from)
            {
                if (strtotime($event->date_begin) <= $to)
                {
                    $result->push($course->id);
                }
            }
        }
        return $result;
    }

    public function updateWasGiven()
    {
        $this->course_id = implode(",",$this->wasGiven()->toArray());
        $this->save();
    }

    public static function wasUpdateGivenAll()
    {
        $ocoms = OCOM::all();
        foreach ($ocoms as $o)
        {
            $o->updateWasGiven();
        }
    }

    public static function findByOCOM($ocom)
    {
        return OCOM::where('ocom','=',$ocom)->first();
    }

    public function getDurationInMin()
    {
        return $this->nbPeriode * 30;
    }
}

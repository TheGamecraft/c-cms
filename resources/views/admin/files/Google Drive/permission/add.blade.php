<div class="modal-header">
    <h5 class="modal-title">Modification des permissions</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="/admin/drive/{{$folder->id}}/addpermission/{{$s}}" method="post">
    <input id="csrf" type="hidden" name="_token" value="">
    @method('patch')
    <div class="modal-body">
        <div class="row">
            <div class="col-md-3">
                @if($s == 'rank')
                    <select class="selectpicker" name="id" data-style="btn btn-primary btn-round" title="Grade" required>
                        @foreach($list as $l)
                            <option value="{{$l->id}}">{{$l->name}}</option>
                        @endforeach
                    </select>
                @elseif($s == 'job')
                    <select class="selectpicker" name="id" data-style="btn btn-primary btn-round" title="Poste" required>
                        @foreach($list as $l)
                            <option value="{{$l->id}}">{{$l->name}}</option>
                        @endforeach
                    </select>
                @else
                    <select class="selectpicker" name="id" data-style="btn btn-primary btn-round" title="Utilisateur" required>
                        @foreach($list as $l)
                            <option value="{{$l->id}}">{{$l->fullname()}}</option>
                        @endforeach
                    </select>
                @endif
            </div>
            <div class="col-md-3">
                <div class="togglebutton row">
                    <div class="col-3">
                        <label>
                            <input name="read" type="checkbox">
                            <span class="toggle"></span>
                        </label>
                    </div>
                    <div class="col">
                        <label>Peux consulter les fichiers a l'intérieur du dossier</label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="togglebutton row">
                    <div class="col-3">
                        <label>
                            <input name="write" type="checkbox">
                            <span class="toggle"></span>
                        </label>
                    </div>
                    <div class="col">
                        <label>Peux modifier les fichiers a l'intérieur du dossier</label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="togglebutton row">
                    <div class="col-3">
                        <label>
                            <input name="perm" type="checkbox">
                            <span class="toggle"></span>
                        </label>
                    </div>
                    <div class="col">
                        <label>Peux gérer le dossier</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
    </div>
    <script>
        $('select').selectpicker();
    </script>
</form>
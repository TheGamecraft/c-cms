@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Liste des cours</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <p>Liste des cours dans l'horaire</p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="/admin/course"
                                       class="btn btn-block @if($mode == 'future')btn-primary active @else btn-outline-primary @endif">Cours
                                        future</a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="/admin/course?all=true"
                                       class="btn btn-block @if($mode == 'all')btn-primary active @else btn-outline-primary @endif">Tous
                                        les cours</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="mb-0">Filtrer les niveaux</label>
                            <select id="slevel" class="form-control mt-0">
                                <option value="" selected>Ne pas filtrer</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="mb-0">Filtrer les périodes</label>
                            <select id="speriode" class="form-control">
                                <option value="" selected>Ne pas filtrer</option>
                            </select>
                        </div>
                    </div>
                    <table class="table table-striped table-no-bordered table-hover dataTable dtr-inline" id="table"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>OCOM</th>
                            <th>Nom</th>
                            <th>Instructeur</th>
                            <th>Date</th>
                            <th class="text-center">Niveau</th>
                            <th class="text-center">Période</th>
                            <th style="width: 8rem">Plan de cours</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr class="cursor">
                                <td>{{$course->ocom}}</td>
                                <td>{{$course->name}}</td>
                                <td>{{$course->instructor()}}</td>
                                <td>{{$course->event->date_begin}}</td>
                                <td class="text-center">{{$course->level}}</td>
                                <td class="text-center">{{$course->periode}}</td>
                                <th class="text-center">
                                    @if($course->lessonPlan)
                                        @if($course->lessonPlan->approved == 1)
                                            <i class="fas fa-check-circle text-success fa-2x" data-toggle="tooltip"
                                               data-placement="top" title="Plan de cours remis et vérifié"></i>
                                        @else
                                            <i class="fas fa-exclamation-circle text-warning fa-2x"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Plan de cours remis mais non vérifié"></i>
                                        @endif
                                    @else
                                        <i class="fas fa-times-circle text-danger fa-2x" data-toggle="tooltip"
                                           data-placement="top" title="Plan de cours non remis"></i>
                                    @endif
                                </th>
                                <td>
                                    <a href="/admin/course/{{$course->id}}"
                                       class="btn btn-primary btn-fab btn-fab-mini btn-round">
                                        <i class="material-icons">add</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <a class="navbar-brand">Base de données des cours</a>
@endsection

@section('custom_scripts')
    <script>
        $('.tooltip').tooltip('enable')
        $(document).ready(function () {
            $('#table').DataTable({
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering" : true,
                "order": [[3, "asc"]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        if (this[0] == 4 || this[0] == 5) {
                            var select = '';
                            if (this[0] == 4) {
                                select = $('#slevel');
                            } else if(this[0] == 5) {
                                select = $('#speriode');
                            }
                            var column = this;
                            select.on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection

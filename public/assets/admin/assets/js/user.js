function checkPassword() {
    var psw = document.getElementById("psw");
    var pswc = document.getElementById("pswc");
    var psw_alert = document.getElementById("psw_alert");
    var submit = document.getElementById("submit");

    if (psw.value == pswc.value) {
        psw_alert.style.display = "none";
        submit.removeAttribute("disabled");
    } else {
        psw_alert.style.display = "block";
        submit.setAttribute("disabled", "disabled");
    }
}

function checkEmail() {
    var email = document.getElementById("email");
    var emailc = document.getElementById("emailc");
    var email_alert = document.getElementById("email_alert");
    var submit = document.getElementById("submit");

    if (email.value == emailc.value) {
        email_alert.style.display = "none";
        submit.removeAttribute("disabled");
    } else {
        email_alert.style.display = "block";
        submit.setAttribute("disabled", "disabled");
    }
}
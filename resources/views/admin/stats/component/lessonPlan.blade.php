<div class="col-md-6">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Plan de cours remis</h4>
            <p class="category">Combiens de plan de cours on été remis au courant de cette année d'instruction</p>
        </div>
        <div class="card-body">
            <div class="content">
                <div class="row text-center">
                    <div class="chart-container" style="position: relative; width:100%">
                        <canvas id="chartLessonPlan"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var chartLessonPlan = $('#chartLessonPlan');
    var pieChartLessonPlan = new Chart(chartLessonPlan, {
        type: 'pie',
        data: {
            datasets: [{
                data: [{{$nbCourseUpToThisDay - ($nbCoursePlanDoneUTDP+$nbCoursePlanDoneAndCheckUTDP)}}, {{$nbCoursePlanDoneUTDP}}, {{$nbCoursePlanDoneAndCheckUTDP}}],
                backgroundColor: ['#f44336','#ff9800','#4caf50'],
                hoverBackgroundColor: ['#f66055','#ffad33','#5fb962']
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: ['Non-Remis', 'Remis non validé','Remis et validé']
        },
        options: {}
    });
</script>
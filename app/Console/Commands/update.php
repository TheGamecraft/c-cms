<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update C-CMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting update ...');

        $this->updateDatabase();

        $this->info('Update completed !');

    }

    private function updateDatabase()
    {
        $this->info('Updating database ...');

        // Check event table
        if (!Schema::hasColumn('events','hidden'))
        {
            $this->info('Updating events table ...');
            Schema::table('events', function ($table) {
               $table->boolean('hidden')->default(0);
            });
        }
        else
        {
            $this->info('Events table is up to date ...');
        }

        // Check event_type table
        if (!Schema::hasColumn('event_types','hidden'))
        {
            $this->info('Updating event_types table ...');
            Schema::table('event_types', function ($table) {
                $table->boolean('hidden')->default(0);
            });
        }
        else
        {
            $this->info('Event_types table is up to date ...');
        }

        return 0;
    }
}

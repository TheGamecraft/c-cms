@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Modifier une catégorie <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <form action="/admin/inventory/management/category/edit/{{$category->id}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nom de la catégorie</label>
                                <input name="name" type="text" class="form-control" required value="{{$category->name}}">
                            </div>
                        </div>
                        <div class="col-md-6 pt-3">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="is_training" value="1" @if($category->is_training == 1) checked @endif>
                                    Les items de cette catégorie sont ils disponible pour l'instruction ?
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pt-3">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="is_op_appro" type="checkbox" value="1" @if($category->is_op_appro == 1) checked @endif>
                                    Les items de cette catégorie sont ils réservés à l'officier d'approvisionnement?
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-4">
                            <label for="desc">Description de l'image</label>
                            <div class="form-group">
                                <textarea name="desc" id="desc" class="form-control" required>{!! $category->desc !!}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $('#desc').trumbowyg({
            lang: 'fr'
        });
    </script>
@endsection

<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all();

        $jobs_sorted = $jobs->sortBy('name');

        $jobs = $jobs_sorted->values();

        return view('admin.configs.jobs.index', ['jobs' => $jobs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = new Job;

        $job->name = $request->name;
        $job->desc = $request->desc;

        $tpermission = [];

        foreach (\App\Permission::all() as $perm)
        {
            $tkey = $perm->ckey;
            $tpermission[$tkey] = $request->$tkey;
        }

        $job->permissions = json_encode($tpermission);
        $job->save();

        clog('add','success','Poste ajouté avec succès');
        return redirect('/admin/config/jobs')->with('success','Poste ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = Job::find($id);

        return view('admin.job.show', ['job' => $job]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::find($id);

        return view('admin.job.edit', ['job' => $job]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = Job::find($id);

        $job->name = $request->name;
        $job->desc = $request->desc;

        $tpermission = [];

        foreach (\App\Permission::all() as $perm)
        {
            $tkey = $perm->ckey;
            $tpermission[$tkey] = $request->$tkey;
        }

        $job->permissions = json_encode($tpermission);
        $job->save();

        clog('add','success','Poste modifié avec succès');
        return redirect('/admin/config/jobs')->with('success','Poste modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Request $request)
    {
        $job = Job::find($request->id);
        foreach (\App\User::all() as $user)
        {
            if($user->job->id == $job->id)
            {
                $user->job_id = 1;
                $user->save();
            }
        }
        clog('delete','success','Poste supprimé avec succès');
        $job->delete();
    }
}

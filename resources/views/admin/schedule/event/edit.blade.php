@extends('layouts.admin.main')

@section('content')
    <form action="/admin/schedule/event/edit/{{$event->id}}" method="POST" enctype="multipart/form-data">
        <div class="row">
            @csrf
            <div class="col-9">
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Modifier un événement à l'horaire</h4>
                    </div>
                    <div class="card-body ">
                        <div class="row" id="container">
                            <div id="accordion" class="col-12" role="tablist">
                                <div class="card card-collapse">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <b>Information générale</b>
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Nom de l'événement</label>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="nameHelp" required>
                                                    <small id="nameHelp" class="text-muted">Veuillez entrer le nom de l'événement</small>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-4">
                                                <div class="form-group">
                                                    <label class="label-control">Date et Heure de début</label>
                                                    <input name="begin_time" type="text" id="begin_time" class="form-control datetimepicker" required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-4">
                                                <div class="form-group">
                                                    <label class="label-control">Date et Heure de fin</label>
                                                    <input name="end_time" type="text" id="end_time" class="form-control datetimepicker" required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-4">
                                                <div class="form-group">
                                                    <label for="name">Lieux</label>
                                                    <input type="text" name="location" id="location" class="form-control" placeholder="" aria-describedby="nameHelp" required>
                                                    <small id="nameHelp" class="text-muted">Veuillez entrer le lieu de l'événement</small>
                                                </div>
                                            </div>
                                            <div id="collmessagedelasemaine" class="col-12 d-none">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="label-control">Date et heure de publication des messages de la semaine</label>
                                                            <input name="date_msg" type="text" id="weekly_msg_publication_time" class="form-control datetimepicker"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <button class="btn btn-outline-primary btn-block" disabled type="button" data-toggle="collapse" data-target="#collapseFiles" aria-expanded="false" aria-controls="collapseExample">
                                                                Joindre des fichiers avec les messages de la semaine
                                                            </button>
                                                        <div class="collapse" id="collapseFiles">
                                                            <ul class="list-group">
                                                                @foreach($event->weekly_msg_file as $file)
                                                                    <li id="{{$file}}" class="list-group-item">{{ $file }} <a class="btn btn-primary btn-fab btn-fab-mini btn-round float-right text-white" onclick="removeFile('{{$file}}')"><i class="material-icons">delete</i> </a></li>
                                                                @endforeach
                                                                <input class="d-none" type="text" name="removedfile" id="removedfile" value="">
                                                            </ul>
                                                            <hr>
                                                            <div class="form-group bmd-form-group is-filled">
                                                                <label class="label-control bmd-label-static">Ajouter des fichiers</label>
                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput" style="display: flex !important;">
                                                                    <div class="form-control" data-trigger="fileinput">
                                                                        <span class="fileinput-filename"></span>
                                                                    </div>
                                                                    <span class="input-group-append">
                                                                <span class="input-group-text fileinput-exists cursor" data-dismiss="fileinput">Remove</span>
                                                                <span class="input-group-text btn-file">
                                                                    <span class="fileinput-new cursor">Select file</span>
                                                                    <span class="fileinput-exists cursor">Change</span>
                                                                    <input type="file" name="files[]" multiple>
                                                                </span>
                                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-4">
                                                <label class="mb-0" for="desc">Description</label>
                                                <div class="form-group">
                                                    <textarea class="form-control richeditor" name="admin_desc" id="admin_desc" rows="6" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-collapse d-none" id="collschedule">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <b>Horaire</b>
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body" style="overflow: scroll">
                                            <div id="editor" class="m-3" style="width: 110vw">
                                                @loaderDot
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-5">Sauvegarder</button>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Options</h4>
                    </div>
                    <div class="card-body ">
                        <div class="form-group">
                            <label for="type">Type d'événement</label>
                            <small class="text-muted d-block">Choisir le type d'activité supprimera vos modification actuel</small>
                            <select disabled class="form-control selectpicker" data-style="btn btn-link" name="type" id="type" required>
                                @foreach (\App\EventType::all() as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0" for="type">Activité obligatoire</label>
                            <small class="text-muted d-block">L'activité est-elle obligatoire pour tout les cadets ?</small>
                            <div class="togglebutton">
                                <label>
                                    <input id="is_mandatory" name="is_mandatory" type="checkbox">
                                    <span class="toggle"></span>
                                    L'activité est obligatoire
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="m-0" for="type">Message de la semaine</label>
                            <small class="text-muted d-block">Inclure des messages de la semaine avec l'activité ?</small>
                            <div class="togglebutton">
                                <label>
                                    <input id="use_weekly_msg" type="checkbox" name="use_weekly_msg" onchange="switchUseWeeklyMsg()">
                                    <span class="toggle"></span>
                                    Avec message de la semaine
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="m-0" for="type">Horaire</label>
                            <small class="text-muted d-block">Inclure un horaire avec l'activité ?</small>
                            <div class="togglebutton">
                                <label>
                                    <input type="checkbox" id="use_schedule" name="use_schedule" checked onchange="switchUseSchedule()">
                                    <span class="toggle"></span>
                                    Avec horaire
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="m-0" for="type">Événement caché</label>
                            <small class="text-muted d-block">L'événement doit t-il être caché?</small>
                            <div class="togglebutton">
                                <label>
                                    <input type="checkbox" id="hidden" name="hidden">
                                    <span class="toggle"></span>
                                    Caché
                                </label>
                            </div>
                        </div>
                        <div id="accordion-apparence" role="tablist">
                            <div class="card card-collapse">
                                <div class="card-header" role="tab" id="heading-apparence">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#col-apparence" aria-expanded="false" aria-controls="col-apparence">
                                            Apparence
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </a>
                                    </h5>
                                </div>

                                <div id="col-apparence" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion-apparence">
                                    <div class="card-body">
                                        <div class="form-group iconpicker-container">
                                            <label for="type">Icone</label>
                                            <small class="text-muted d-block">Icone de l'activité</small>
                                            <div class="input-group iconpicker-container">
                                                <input id="calendar_icon" name="calendar_icon" data-placement="bottomRight" class="form-control icp icp-auto iconpicker-element iconpicker-input" value="fas fa-archive" type="text">
                                                <span class="input-group-addon"><i id="calendar_icon_display" class="fas fa-assistive-listening-systems"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="type">Couleur</label>
                                            <small class="text-muted d-block">Couleur de l'activité</small>
                                            <div class="input-group iconpicker-container">
                                                <input class="form-control" type="text" name="calendar_color" id="calendar_color" value="#2196F3" onclick="pickr.show()">
                                                <span class="color-picker"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('custom_scripts')
    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>
    <script src="/js/plugins/fontawesome-icon-picker/fontawesome-iconpicker.js"></script>
    <script>
        $('.icp-auto').iconpicker({ placement: 'left',animation: 'false'});
        const pickr = Pickr.create({
            el: '.color-picker',
            theme: 'monolith', // or 'monolith', or 'nano'

            swatches: [
                'rgba(244, 67, 54, 1)',
                'rgba(233, 30, 99, 1)',
                'rgba(156, 39, 176, 1)',
                'rgba(103, 58, 183, 1)',
                'rgba(63, 81, 181, 1)',
                'rgba(33, 150, 243, 1)',
                'rgba(3, 169, 244, 1)',
                'rgba(0, 188, 212, 1)',
                'rgba(0, 150, 136, 1)',
                'rgba(76, 175, 80, 1)',
                'rgba(139, 195, 74, 1)',
                'rgba(205, 220, 57, 1)',
                'rgba(255, 235, 59, 1)',
                'rgba(255, 193, 7, 1)'
            ],
            comparison: false,
            default: '#2196F3',
            components: {

                // Main components
                preview: true,
                opacity: false,
                hue: true,

                // Input / output Options
                interaction: {
                    hex: true,
                    rgba: false,
                    hsla: false,
                    hsva: false,
                    cmyk: false,
                    input: true,
                    clear: false,
                    save: false
                }
            }
        });
        pickr.on('change', (color,instance) => {
            $('#calendar_color').val(color.toHEXA().toString());
        });
    </script>
    <script src="/js/calendar.js"></script>
    <script src="/js/plugins/schedule/editorv2.js"></script>
    <script src="/js/plugins/autocomplete.js"></script>
    <script>
        $(function () {
            console.log('Document READY loading schedule editor');
            loadEvent('{{$event->date_begin}}',{{$event->id}});
        })
    </script>
@endsection

@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Matériel {{$schedule->data['n'.$niveau.'_p'.$periode.'_name']}} - {{$schedule->data['n'.$niveau.'_p'.$periode.'_ocom']}}<a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3>Matériel Réservé</h3>
                    <br>
                    <p>Cliquer sur la flèche a droite d'un item pour annuler la réservation</p>
                    <hr>
                    <table id="log-data" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Description</th>
                            <th>Quantité</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                @if ($item !== null)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->desc}}</td>
                                        <td>{{$item->quantity}}</td>
                                        <td style="width: 12%;"><form action="/admin/inventory/remove/{{$schedule->id}}/{{$periode}}/{{$niveau}}" method="post">{{ csrf_field() }}<button type="submit" name="remove" value="{{$item->id}}:{{$item->quantity}}" class="btn btn-danger"><i class="fa fa-arrow-right" style="color:white;"></i></button></form></td>
                                    </tr>
                                @endif
                            @endforeach
                        
                        </tbody>
                    </table>
                </div>
                <div class="col-6">
                    <h3>Matériel Disponible</h3>
                    <br>
                    <p>Cliquer sur la flèche a gauche d'un item pour le réserver</p>
                    <hr>
                    <table id="log-data_2" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Nom</th>
                            <th>Description</th>
                            <th>Quantité</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($dispo_item as $item)
                                @if ($item !== null)
                                    <tr>
                                        <td style="width: 12%;"><form action="/admin/inventory/add/{{$schedule->id}}/{{$periode}}/{{$niveau}}" method="post">{{ csrf_field() }}<button type="submit" name="add" class="btn btn-success" value="{{$item->id}}"><i class="fa fa-arrow-left" style="color:white;"></i></button></td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->desc}}</td>
                                        <td><input type="number" class="form-control" name="qt" id="qt" min="1" max="{{$item->quantity}}" value="{{$item->quantity}}"></div></form></td>
                                    </tr>
                                @endif
                            @endforeach
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
            <a href="/admin/calendar" class="btn btn-primary btn-block">Retour a l'horaire</a>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventaire</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Inventaire</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('#log-data').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            });
        } );
    })(jQuery);

    (function($) {
        $(document).ready(function() {
            $('#log-data_2').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            });
        } );
    })(jQuery);

     function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/item/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "L'item a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
@endsection

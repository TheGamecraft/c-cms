@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Ajouter une images</h4>
            </div>
            <div class="card-body mt-5">
                <form action="/admin/article/activity/picture/{{$article->id}}/add" method="post">

                    @csrf

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nom de l'image</label>
                            <input name="title" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>URL de l'image</label>
                            <input name="url" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <label for="desc">Description de l'image</label>
                        <div class="form-group">
                            <textarea name="desc" id="desc" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('custom_scripts')
    <script>
        $('#desc').trumbowyg({
            lang: 'fr'
        });
        function saveChange(pPerm) {
            (function($) {
                var myswitch = document.getElementById(pPerm);
                $.post('/api/config/general/save?api_token='+api_token, { value: myswitch.checked,perm: pPerm } , function(data) {
                    swal({
                        title: 'Modification enregistré !',
                        type: 'success',
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                });
            })(jQuery);
        }
    </script>
@endsection

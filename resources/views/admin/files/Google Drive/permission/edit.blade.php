<div class="modal-header">
    <h5 class="modal-title">Modification des permissions</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="/admin/drive/{{$folder->id}}/permission/{{$s}}/{{$subject->id}}" method="post">
    <input id="csrf" type="hidden" name="_token" value="">
    @method('patch')
    <div class="modal-body">
        <div class="row">
            <div class="col-md-3">
                @switch($s)
                    @case('rank')
                        Grade : {{$subject->name}}
                    @break
                    @case('job')
                        Poste : {{$subject->name}}
                    @break
                    @case('user')
                        Utilisateur : {{$subject->fullname()}}
                    @break
                @endswitch
            </div>
            <div class="col-md-3">
                <div class="togglebutton row">
                    <div class="col-3">
                        <label>
                            <input name="read" type="checkbox" @if(strpos($perm,'r') !== false) checked @endif>
                            <span class="toggle"></span>
                        </label>
                    </div>
                    <div class="col">
                        <label>Peux consulter les fichiers a l'intérieur du dossier</label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="togglebutton row">
                    <div class="col-3">
                        <label>
                            <input name="write" type="checkbox" @if(strpos($perm,'w') !== false) checked @endif>
                            <span class="toggle"></span>
                        </label>
                    </div>
                    <div class="col">
                        <label>Peux modifier les fichiers a l'intérieur du dossier</label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="togglebutton row">
                    <div class="col-3">
                        <label>
                            <input name="perm" type="checkbox" @if(strpos($perm,'p') !== false) checked @endif>
                            <span class="toggle"></span>
                        </label>
                    </div>
                    <div class="col">
                        <label>Peux gérer le dossier</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
    </div>
</form>
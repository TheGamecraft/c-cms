@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Configuration des postes</h4>
            </div>
            <div class="card-body mt-5">
                <div class="row">
                    <div class="col-md-12">
                        <a href="/admin/config/jobs/add" class="btn btn-primary btn-block mb-3">Ajouter un poste</a>
                    </div>
                    @foreach($jobs as $job)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header @if($job->id == 1) card-header-danger @else card-header-primary @endif">
                                    <h4>{{$job->name}}</h4>
                                </div>
                                <div class="card-body">
                                    <div>
                                        {!! $job->desc!!}
                                    </div>
                                    <div class="row text-right">
                                        <div class="col">
                                            @if($job->id == 1)
                                                <button disabled class="btn btn-primary btn-block">Gérer</button>
                                            @else
                                                <a href="/admin/config/jobs/{{$job->id}}" class="btn btn-primary btn-block">Gérer</a>
                                            @endif
                                        </div>
                                        <div class="col">
                                            @if($job->id == 1)
                                                <button disabled class="btn btn-danger btn-just-icon"><i class="fas fa-times"></i></button>
                                            @else
                                                <button class="btn btn-danger btn-just-icon" onclick="deleteJobs({{$job->id}})"><i class="fas fa-times"></i></button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        function deleteJobs(pid){
            swal({
                title: 'Êtes vous certain ?',
                html:
                    'Vous ne pourrez annuler cette action! <br>' +
                    '<strong>Veuillez donner un nouveau poste a tous les utilisateurs posédant présentement le poste que vous voulez supprimer</strong>',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
            }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/config/job/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });


                    })(jQuery);

                    swal(
                        'Supprimé!',
                        "Le poste a été supprimé",
                        'success'
                    ).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                }
            })
        }
    </script>
@endsection

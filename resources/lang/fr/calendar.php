<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Calendar Language Lines
    |--------------------------------------------------------------------------
    |General Translation
    */

    'nothing_today' => "Il n'y a rien a l'horaire pour cette date",
    'add_to_schedule' => "Ajouter une activité a l'horaire",
    'begin_at' => " de ",
    'end_at' => " à ",

    /** Admin Display Page */
    'admin_page_title' => "Horaire",
    'admin_breadcrumb' => "Horaire",

    /** Admin Add Page */
    'add_title' => "Ajouter a l'horaire",
    'add_breadcrumb' => "Horaire/Ajouter",
     /** Event Type */
        'add_form_event_type' => "Type d'évenement",
        'add_form_event_type_pilotage' => "Activité complémentaire - Pilotage",
        'add_form_event_type_dril' => "Activité complémentaire - Précidrill",
        'add_form_event_type_music' => "Activité complémentaire - Musique",
        'add_form_event_type_biathlon' => "Activité complémentaire - Biathlon",
        'add_form_event_type_marksmanship' => "Activité complémentaire - Tir",
        'add_form_event_type_founding' => "Financement",
        'add_form_event_type_volunteer' => "Bénévolat",
        'add_form_event_type_other' => "Autre",
        'add_form_event_type_instruction' => "Soirée d'instruction régulière",
        'add_form_event_type_select' => "Choisir le type d'évenement",
    /** Event Name */
        'add_form_event_name' => "Nom de l'évenement",
        'add_form_event_name_placeholder' => "Nom",
        'add_form_event_name_help' => "Veuillez indiquer le nom de l'événement.",

    /** Event Date */
        'add_form_event_date' => "Date de l'evenement",

    /** Event Time */
        'add_form_event_time_begin' => "Heure de début",
        'add_form_event_time_begin_help' => "Veuillez indiquer l'heure de début.",
        'add_form_event_time_end' => "Heure de fin",
        'add_form_event_time_end_help' => "Veuillez indiquer l'heure de fin.",

    /** Event Location */
        'add_form_event_place' => "Lieux de l'évenement",
        'add_form_event_place_placeholder' => "Ex: Escadron (540 Rue St Germain E, Rimouski, QC G5L 1E9)",
        'add_form_event_place_help' => "Veuillez indiquer le lieux de l'évenement.",

    /** Event Mandatory */
        'add_form_event_mandatory' => "Evenement obligatoire",
        'add_form_event_mandatory_help' => "L'evenement est-il obligatoire pour tous les cadets ?",

    /** Event description */
        'add_form_event_desc' => "Description",
        'add_form_event_desc_placeholder' => "Écriver ici ...",
        'add_form_event_desc_help' => "Veuillez entrer ici une courte description de l'événement ainsi que toutes autres informations utiles",

    /** Regular day event */
        'reg_p1' => "Première période",
        'reg_p2' => "Deuxième période",
        'reg_classe_name' => "Nom du cours",
        'reg_ocom' => "OCOM",
        'reg_plan_done' => "Plan de cours remis?",
        'reg_instructor_name' => "Instructeur",
        'reg_location' => "Local",

    /** 
     * Translation for the pilotage activity
     */
    'pilotage_title' => "Cours de pilotage",
];

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Picture
 *
 * @property int $id
 * @property string $url
 * @property string $title
 * @property string $desc
 * @property int $pictureable_id
 * @property string $pictureable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $pictureable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture wherePictureableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture wherePictureableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereUrl($value)
 * @mixin \Eloquent
 */
class Picture extends Model
{
    public function pictureable()
    {
        return $this->morphTo();
    }

    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}

<!-- Required meta tags -->
<title>{{ App\Config::getData('escadron_name_full')}}</title>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="icon" type="image/png" href="/favicon.png">

<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<script src="https://kit.fontawesome.com/d18efcab73.js"></script>

<!-- Material CSS -->
<link rel="stylesheet" href="/css/material-kit.css" />
<link rel="stylesheet" href="/css/custom.css" />

<!-- Plugin CSS -->
<link href='/assets/fullcalendar/core/main.css' rel='stylesheet' />
<link href='/assets/fullcalendar/daygrid/main.css' rel='stylesheet' />
<link href='/assets/fullcalendar/list/main.css' rel='stylesheet' />
<link href='/css/ck-content.css' rel='stylesheet' />
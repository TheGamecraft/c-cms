<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Config
 *
 * @property int $id
 * @property string $name
 * @property int $state
 * @property array $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Config extends Model
{
    protected $casts = [
        'data' => 'array',
    ];

    public function data() 
    {
        return $this->data[0];
    }

    public static function getData($configName)
    {
        return Config::where('name',$configName)->first()->data();
    }

    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }

    public static function find($name)
    {
        return Config::where('name',$name)->first();
    }
}

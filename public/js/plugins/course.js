$('.richeditor').trumbowyg({
    lang: 'fr',
    btns: [
        ['viewHTML'],
        ['emoji'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['fontfamily'],
        ['fontsize'],
        ['foreColor', 'backColor'],
        ['link'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ]
});

function switchPlanStatus(id)
{
    $.ajax({
        type: 'POST',
        url: '/api/course/'+id+'/plan/validate?api_token='+api_token,
        success: function (data) {
            if (data == '1')
            {
                showNotification('success','Le plan de cours est maintenant vérifié','top', 'center');
                $("[name='isPlanCheck']").prop( "checked", true );

            }
            else
            {
                showNotification('warning','Le plan de cours n\'est maintenant plus vérifié','top', 'center');
                $("[name='isPlanCheck']").prop( "checked", false );

            }
        },
        error:  function () {
            showNotification('error','Erreur impossible de changer le status du plan de cours','top', 'center');
            $("[name='isPlanCheck']").prop( "checked", false );
        }
    })
}
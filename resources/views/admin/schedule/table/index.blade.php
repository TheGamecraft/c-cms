@extends('layouts.admin.main')

@section('content')
    <div class="col-sm-12 col-lg-12 text-right">
        <a class="btn btn-primary btn-round" href="/admin/schedule">
            <i class="material-icons">today</i> Vue Horaire
        </a>
    </div>
    <div class="col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Horaire tableau</h3>
            </div>
            <div class="card-body" style="overflow: scroll;max-height: 79vh">
                <div>
                    <table class="table table-bordered">
                        <thead class="table-dark">
                            <tr>
                                <th></th>
                                @for($i = 1;$i <= \App\Event::getMaxLevels($events); $i++)
                                    <th>
                                        Niveau {{$i}}
                                    </th>
                                @endfor
                            </tr>
                            <tr>
                                <td>
                                    <div class="row" style="width: 29rem">
                                        <div class="col-2 pr-0">
                                            Semaine
                                        </div>
                                        <div class="col-4">
                                           Nom
                                        </div>
                                        <div class="col-3">
                                            Date
                                        </div>
                                        <div class="col-3">
                                            Période
                                        </div>
                                    </div>
                                </td>
                                @for($i = 1;$i <= \App\Event::getMaxLevels($events); $i++)
                                    <td>
                                        <div class="row" style="width: 35rem">
                                            <div class="col-5">
                                                OCOM - Description
                                            </div>
                                            <div class="col-3">
                                                Instructeur
                                            </div>
                                            <div class="col-2">
                                                Salle
                                            </div>
                                            <div class="col-2">
                                                Materiel
                                            </div>
                                        </div>
                                    </td>
                                @endfor
                            </tr>
                        </thead>
                        @foreach($events as $event)
                            <tr class="schedule-table-week">
                                <td>
                                    <div class="row" style="width: 29rem">
                                        <div class="col-2 text-center m-auto">
                                            {{ $loop->iteration }}
                                        </div>
                                        <div class="col-4 m-auto">
                                            {{$event->name}}
                                        </div>
                                        <div class="col-3 m-auto">
                                            {{$event->date_begin}} à {{ $event->date_end }}
                                        </div>
                                        <div class="col-3 m-auto pr-0">
                                            @if($event->use_schedule == 1)
                                                @foreach($event->schedule["periodes"] as $periode)
                                                    <table class="table-borderless">
                                                        <tr>
                                                            <td style="height: 6rem;vertical-align: middle">
                                                                P{{$loop->iteration}} - {{ $periode['begin_time'] }} à {{ $periode['end_time'] }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                @if($event->use_schedule == 1)
                                    @for($n = 1;$n <= $event->nbNiveau();$n++)
                                        <td style="padding: 0px;">
                                            @for($p = 1;$p <= $event->nbPeriode();$p++)
                                                <table class="table-borderless">
                                                    <tr @if($p != $event->nbPeriode())class="border-bottom"@endif>
                                                        <td style="height: 6rem;">
                                                            <div class="row" style="width: 35rem">
                                                                @php($course = $event->course($p,$n))
                                                                <div class="col-5 m-auto">
                                                                    @if(!$course->use_course())
                                                                        <b>{{ $course->ocom }}</b> - {{ $course->name }}
                                                                    @else
                                                                        {{ $course->desc }}
                                                                    @endif
                                                                </div>
                                                                <div class="col-3">
                                                                    {{ $course->instructor() }}
                                                                </div>
                                                                <div class="col-2">
                                                                    {{ $course->location }}
                                                                </div>
                                                                <div class="col-2">
                                                                    NA
                                                                </div>
                                                                @php($course = null)
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            @endfor
                                        </td>
                                    @endfor
                                @else
                                    <td colspan="{{\App\Event::getMaxLevels($events)}}">
                                        {!! $event->desc !!}
                                    </td>
                                @endif

                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
@endsection

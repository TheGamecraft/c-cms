<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('rank_id')->default(1);
            $table->string('adress')->default('');
            $table->string('telephone')->default('');
            $table->string('age');
            $table->string('avatar')->default('1');
            $table->string('sexe');
            $table->string('job_id')->default(6);
            $table->boolean('use_default_psw')->default(true);
            $table->string('api_token', 60)->unique()->default(str_shuffle(str_random(60)));
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

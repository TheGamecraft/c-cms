<div class="row" id="row-{{$periode_id}}">
    <div class="col-2 d-inline border-right border-bottom bg-light">
        <div class="row">
            <div class="col-9">
                <div class="form-group label-floating">
                    <input type="text" placeholder="Période" name="periode_name_{{$periode_id}}" class="form-control" value="{{$periode_name}}" />
                    <span class="form-control-feedback">
                            <i class="material-icons">clear</i>
                        </span>
                </div>
            </div>
            <div class="col-3 text-right">
                <a type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                </a>
                <div class="dropdown-menu">
                    <a id="modeSwitchPeriodeC{{$periode_id}}" class="btn-secondary dropdown-item m-1" onclick="selectCourseModePeriode('course',{{$periode_id}})">Mode "Cours" pour toute la période</a>
                    <a id="modeSwitchPeriodeO{{$periode_id}}" class="btn-secondary dropdown-item m-1" onclick="selectCourseModePeriode('other',{{$periode_id}})">Mode "Autre" pour toute la période</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group label-floating">
                    <input type="time" class="form-control" name="periode_begin_time_{{$periode_id}}" value="{{$periode_begin_time}}" />
                    <span class="form-control-feedback"><i class="material-icons">clear</i></span>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group label-floating">
                    <input type="time" class="form-control" name="periode_end_time_{{$periode_id}}" value="{{$periode_end_time}}" />
                    <span class="form-control-feedback"><i class="material-icons">clear</i></span>
                </div>
            </div>
        </div>
    </div>
    @for($i = 1; $i <= $nbLevel;$i++)
        @include('admin.schedule.editor.level',['periode_id' => $periode_id,'level_id' => $i])
    @endfor
    <div class="col-1">

    </div>
</div>

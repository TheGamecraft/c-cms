<?php

namespace App\Http\Controllers;

use App\Course;
use App\Event;
use App\EventType;
use App\GoogleDriveFile;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store()
    {
        $event = new Event();

        if (\App\GoogleDriveFile::checkConfig())
        {
            if (\request()->hasFile('files'))
            {
                $dir = \App\GoogleDriveFile::findByPath('.Systeme/.Fichier/.MessageDeLaSemaine');
                $files = [];
                foreach (\request()->file('files') as $f)
                {
                    $name = urlencode(pathinfo($f->getClientOriginalName())['filename'].'_'.uniqid().'.'.$f->getClientOriginalExtension());
                    \Storage::cloud()->putFileAs($dir->id,$f,$name);
                    array_push($files,$name);
                }
                $event->weekly_msg_file = $files;
            }
            else
            {
                $event->weekly_msg_file = [''];
            }
        } else {
            $event->weekly_msg_file = [''];
        }

        $event->name = request('name');
        $event->date_begin = request('begin_time');
        $event->date_end = request('end_time');
        $event->type = request('type');
        $event->user_id = \Auth::user()->id;
        $event->location = request('location');

        if(request('is_mandatory') != null){
            $event->is_mandatory = 1;
        } else {
            $event->is_mandatory  = 0;
        }

        $event->desc = \request('admin_desc');

        if(\request("use_weekly_msg"))
        {
            $event->use_weekly_msg = 1;
            $event->weekly_msg_publication_time = \request('date_msg');
            $event->msg = request('admin_desc');
        }
        else
        {
            $event->use_weekly_msg = 0;
            $event->weekly_msg_publication_time = "";
            $event->msg = "";
        }

        if(\request("use_schedule"))
        {
            $event->use_schedule = 1;
        }
        else
        {
            $event->use_schedule = 0;
        }

        if(\request("hidden"))
        {
            $event->hidden = 1;
        }
        else
        {
            $event->hidden = 0;
        }

        $event->calendar_color = \request('calendar_color');
        $event->calendar_icon = \request('calendar_icon');

        $nbLevel = 1;
        $niveaux = [];
        while (\request('level_name_'.$nbLevel))
        {
            array_push($niveaux,['name' => \request('level_name_'.$nbLevel)]);
            $nbLevel++;
        }

        $nbPeriode = 1;
        $periodes = [];
        while (\request('periode_name_'.$nbPeriode))
        {
            array_push($periodes,[
                'name' => \request('periode_name_'.$nbPeriode),
                'begin_time' => \request('periode_begin_time_'.$nbPeriode),
                'end_time' => \request('periode_end_time_'.$nbPeriode)
            ]);
            $nbPeriode++;
        }
        $event->schedule = [
            'periodes' => $periodes,
            'niveaux' => $niveaux,
            'courses' => []
        ];

        $event->save();

        if ($event->use_schedule) {
            for ($l=1; $l < $nbLevel; $l++) {
                for ($p=1; $p < $nbPeriode; $p++) {

                    $course = new \App\Course();

                    $users = \App\User::all();
                    $instructor = request('instruc_n'.$l.'_p'.$p);

                    foreach ($users as $user) {
                        if($user->fullname() == request('instruc_n'.$l.'_p'.$p))
                        {
                            $instructor = $user->id;
                        }
                    }

                    if ($instructor == null) {
                        $course->user_id = '';
                    } else {
                        $course->user_id = $instructor;
                    }
                    if (request('location_n'.$l.'_p'.$p) != null) {
                        $course->location = request('location_n'.$l.'_p'.$p);
                    } else {
                        $course->location = "";
                    }
                    $course->periode = $p;
                    $course->level = $l;

                    $course->event_id = $event->id;

                    if(\request("use_course_n".$l."_p".$p) == 'on')
                    {
                        if (request('name_n'.$l.'_p'.$p) == null) {
                            $course->name = "";
                        } else {
                            $course->name = request('name_n'.$l.'_p'.$p);
                        }
                        if (request('ocom_n'.$l.'_p'.$p) == null) {
                            $course->ocom = "";
                        } else {
                            $course->ocom = request('ocom_n'.$l.'_p'.$p);
                        }
                        $course->desc = "";
                    }
                    else
                    {
                        $course->name = "";
                        $course->ocom = "";
                        if (request('desc_n'.$l.'_p'.$p) == null)
                        {
                            $course->desc = "";
                        }
                        else
                        {
                            $course->desc = request('desc_n'.$l.'_p'.$p);
                        }
                    }
                    $course->comment_officer = "";
                    $course->comment = "";
                    $course->save();

                    $ocom = \App\OCOM::where('ocom','=',request('ocom_n'.$l.'_p'.$p))->first();
                    if ($ocom != null)
                    {
                        $foo = $ocom->courses();
                        $foo->push($course);
                        $ocom->saveCourses($foo);
                    }
                }
            }   
        }
        clog('add','success','a ajouté un évènement',null,'App\Event',$event->id);
        return redirect('/admin/schedule')->with('success','Événement ajouter à l\'horaire');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function edit($id)
    {
        $event = \App\Event::find($id);
        if ($event->hidden == 1)
        {
            if (\Auth::user()->p('instruction_see_hidden_event') != 1)
            {
                return  redirect('/admin/schedule')->with('error','Modification non authorisé');
            }
        }
        return view('admin.schedule.event.edit',['event' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return Response
     */
    public function update($id)
    {
        $event = Event::findOrFail($id);
        if ($event->hidden == 1)
        {
            if (\Auth::user()->p('instruction_see_hidden_event') != 1)
            {
                return  redirect('/admin/schedule')->with('error','Modification non authorisé');
            }
        }

//        if (\App\GoogleDriveFile::checkConfig())
//        {
//            if (\request()->hasFile('files'))
//            {
//                $dir = \App\GoogleDriveFile::findByPath('.Systeme/.Fichier/.MessageDeLaSemaine');
//                $files = [];
//                foreach (\request()->file('files') as $f)
//                {
//                    $name = urlencode(pathinfo($f->getClientOriginalName())['filename'].'_'.uniqid().'.'.$f->getClientOriginalExtension());
//                    \Storage::cloud()->putFileAs($dir->id,$f,$name);
//                    array_push($files,$name);
//                }
//                $event->weekly_msg_file = $files;
//            }
//            else
//            {
//                $event->weekly_msg_file = [''];
//            }
//        } else {
//            $event->weekly_msg_file = [''];
//        }

        $event->name = request('name');
        $event->date_begin = request('begin_time');
        $event->date_end = request('end_time');
        $event->user_id = \Auth::user()->id;
        $event->location = request('location');

        if(request('is_mandatory') != null){
            $event->is_mandatory = 1;
        } else {
            $event->is_mandatory  = 0;
        }

        $event->desc = \request('admin_desc');

        if(\request("use_weekly_msg"))
        {
            $event->use_weekly_msg = 1;
            $event->weekly_msg_publication_time = \request('date_msg');
            $event->msg = request('admin_desc');
        }
        else
        {
            $event->use_weekly_msg = 0;
            $event->weekly_msg_publication_time = "";
            $event->msg = "";
        }

        if(\request("use_schedule"))
        {
            $event->use_schedule = 1;
        }
        else
        {
            $event->use_schedule = 0;
        }

        if(\request("hidden"))
        {
            $event->hidden = 1;
        }
        else
        {
            $event->hidden = 0;
        }

        $event->calendar_color = \request('calendar_color');
        $event->calendar_icon = \request('calendar_icon');

        $nbLevel = 1;
        $niveaux = [];
        while (\request('level_name_'.$nbLevel))
        {
            array_push($niveaux,['name' => \request('level_name_'.$nbLevel)]);
            $nbLevel++;
        }

        $nbPeriode = 1;
        $periodes = [];
        while (\request('periode_name_'.$nbPeriode))
        {
            array_push($periodes,[
                'name' => \request('periode_name_'.$nbPeriode),
                'begin_time' => \request('periode_begin_time_'.$nbPeriode),
                'end_time' => \request('periode_end_time_'.$nbPeriode)
            ]);
            $nbPeriode++;
        }
        $event->schedule = [
            'periodes' => $periodes,
            'niveaux' => $niveaux,
            'courses' => []
        ];

        $event->save();

        if ($event->use_schedule) {
            for ($l=1; $l < $nbLevel; $l++) {
                for ($p=1; $p < $nbPeriode; $p++) {

                    $course = $event->course($p,$l);
                    if ($course == null)
                    {
                        $course = new Course();
                    }

                    $users = \App\User::all();
                    $instructor = request('instruc_n'.$l.'_p'.$p);

                    foreach ($users as $user) {
                        if($user->fullname() == request('instruc_n'.$l.'_p'.$p))
                        {
                            $instructor = $user->id;
                        }
                    }
                    if ($instructor == null) {
                        $course->user_id = "";
                    } else {
                        $course->user_id = $instructor;
                    }
                    if (request('location_n'.$l.'_p'.$p) == null) {
                        $course->location = "";
                    } else {
                        $course->location = request('location_n'.$l.'_p'.$p);
                    }
                    $course->periode = $p;
                    $course->level = $l;

                    $course->event_id = $event->id;

                    if(\request("use_course_n".$l."_p".$p) == 'on')
                    {
                        $course->name = request('name_n'.$l.'_p'.$p);
                        $course->ocom = request('ocom_n'.$l.'_p'.$p);
                        $course->desc = "";
                    }
                    else
                    {
                        $course->name = "";
                        $course->ocom = "";
                        if (request('desc_n'.$l.'_p'.$p) == null)
                        {
                            $course->desc = "";
                        }
                        else
                        {
                            $course->desc = request('desc_n'.$l.'_p'.$p);
                        }
                    }
                    $course->comment_officer = "";
                    $course->comment = "";
                    $course->save();

                    $ocom = \App\OCOM::where('ocom','=',request('ocom_n'.$l.'_p'.$p))->first();
                    if ($ocom != null)
                    {
                        $foo = $ocom->courses();
                        $foo->push($course);
                        $ocom->saveCourses($foo);
                    }
                }
            }
        }
        clog('add','success','a modifier un évènement',null,'App\Event',$event->id);
        return redirect('/admin/schedule')->with('success','Événement modifier à l\'horaire');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return Response
     */
    public function destroy(Event $event)
    {
        //
    }

    public function checkEvent()
    {
        //
    }

    public function toJson($id)
    {
        $event = Event::findOrFail($id);
        $model = $event->schedule;
        $default_value = [];
        if ($event->use_schedule == 1) {
            foreach ($model['periodes'] as $periode_index => $periode)
            {
                $niveau_array = [];
                foreach ($model['niveaux'] as $niveau_index => $niveau)
                {
                    $course = $event->course($periode_index+1,$niveau_index+1);
                    $use_course = "on";
                    if ($course->name == null) {
                        $use_course = "off";
                    }
                    array_push($niveau_array,[
                        'ocom' => $course->ocom,
                        'name' => $course->name,
                        'location' => $course->location,
                        'instructor' => $course->instructor(),
                        'desc' => $course->desc,
                        'use_course' => $use_course
                    ]);
                }
                array_push($default_value,$niveau_array);
            }
            $model['default_value'] = $default_value;
        }
        $event->schedule_model = $model;
        return $event->toArray();
    }
}

@extends('layouts.public.main')

@section('content')
    <div class="page-header header-filter clear-filter purple-filter" data-parallax="true" style="background-image: url({{'"'.\App\Config::getData('public_index_img_url').'"'}});">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <div class="brand">
                        <h2>{{ App\Config::getData('text_public_picture_title')}}</h2>
                        <p>{{ App\Config::getData('text_public_picture_desc')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            <div class="section">
                    <div>
                        <h2 class="title">{{ $picture->title }}</h2>
                        <div><img class="img-responsive w-100" src="{{ $picture->url }}" alt="{{ $picture->title }}"></div>
                        <p>
                            {!! $picture->desc !!}
                        </p>
                        <span class="news-small">{{ $picture->created_at }}</span>
                        <a href="{{ url()->previous() }}" class="btn button primary">Retour</a>
                    </div>
            </div>
        </div>
@endsection
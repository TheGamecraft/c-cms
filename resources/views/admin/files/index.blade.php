@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Fichiers <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <div class="content">
                <p>Les fichiers si dessous sont disponible autant dans l'espace administration que dans l'espace cadet cadre.</p>
                <table class="table table-striped dt-responsive material-datatables" id="table" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Catégorie</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Plan de cours Vierge</td>
                        <td>Plan de cours</td>
                        <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1i1a0sjI8I3nzt4mlcLvznjqYF-12JgfQ">Télécharger</a></td>
                    </tr>
                        <tr>
                            <td>Mini Ordre Operation Projet Soirée Journée Vierge</td>
                            <td>Ordre d'opération</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1sqkeUp-djZDjltitGvjR0efMQgyB_sos">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Mini Ordre Operation Vierge</td>
                            <td>Ordre d'opération</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1RjSSwK9NIVUFbHKlu0hbkK5IeTnAFWq9">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Liste des tenues</td>
                            <td>Tenues</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1JUXaPQhHGJffE7CTnB1BAkqwM8g9t8ef">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Cadet commandant</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1DdI9eOptKarpApsUdO-6gkDFYtD6DHi8">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Cadet commandant adjoint</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1jDuKQzY3Dam0J9mSGDWiv2I1agDYacRm">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Chef entrainement</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=12p8rGhSZloPFurD--RZO9KQVoRmXAEnE">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Instructeur sénior</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=13fNufhR2hYhKgeiHUo0W_V-vF_W8SPkC">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Instructeur</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1Y1gEsNP7mz2SmJPwxi7YUfpxgioJKvR0">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Assistant Instructeur</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1BxKj0J20QZ5hVQ1womwS8GUWvuq-VsJy">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Commandant de section</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1jOmyNFZ2rSOwCFjcoABx6VFcvEMKCf73">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Commandant de la garde</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=18T3rQQ-RN551meOGGPD8Ni2wbpvKNfYz">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Commandant adjoint de section</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1A5NkPhSJ5E-bIPiLRwa7VAOXQrrHIzn6">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Commandant adjoint de garde</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1ncphhTpBm9uhq0isGFNzDs_-TRPrxfKw">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Cadet cadre de la logistique</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1PcmlegtAqmdX2ufGQMubkNxfkrCcSIge">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Cadet cadre de l'administration</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1haEG9jSabp10VtI7EV2OyLctn9-63T8G">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Commandant adjoint de section</td>
                            <td>Énoncé de fonction</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1A5NkPhSJ5E-bIPiLRwa7VAOXQrrHIzn6">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Information NECPC</td>
                            <td>Divers</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1KG0IoPxpqctqqVwCfM0WyKq4y6RDBtUJ">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Évaluation pratique sur la coordination d’un ordre d’opération sur le terrain</td>
                            <td>Divers</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=1AAPB7IdpIw8UGJwIoTNQZ3cg9ODfWrAp">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>RENCONTRE PRÉILIMINAIRE D’INSTRUCTION</td>
                            <td>Divers</td>
                            <td><a class="btn btn-primary btn-block" href="https://drive.google.com/uc?export=download&amp;id=16lT4YzNjGWd2SFmgSbmj1LcPG9cDkkyo">Télécharger</a></td>
                        </tr>
                        <tr>
                            <td>Liste nominal des cadets</td>
                            <td>Divers</td>
                            <td><a class="btn btn-primary btn-block" href="{{\App\Config::getData('cadet_list')}}">Télécharger</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
Fichier / Autres
@endsection

@section('custom_scripts')
    <script>
    $(document).ready(function() {
        $('#table').DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            "ordering" : false,
            "rowGroup": {
                dataSrc: 1
            },
            "columnDefs": [
                { "visible": false, "targets": 1 }
            ]
        });
    } );
</script>
@endsection

@extends('layouts.admin.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="card-title">3.2.5</h3>
                            <p class="category">2019-10-19</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="badge badge-pill badge-success">STABLE</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        <i class="fas fa-exclamation-triangle text-white fa-2x mr-3"></i>Plusieurs fonctionnalité sont <strong>DÉSACTIVÉ</strong> le temps de les moderniser
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                Nouveauté
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout d'un breadcrumb
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de la base de donnée des cours
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout d'une section "Mes cours"
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout d'une section "Mes fichiers"
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout d'une liste des cours pour l'instruction
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de statistique pour l'instruction
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour des pages d'erreurs
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour de l'horaire vers la version 3
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour du système de fichier vers Google Drive
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour du système de permission
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour de la section "Guide pédagogique et Norme de qualification"
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour de la section "Fichier" de l'instruction
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour du calendrier
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Mise à jour de la section nouvelle
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Déplacement des plugins JS sur le serveur
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                Bug
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de multiples bugs
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction d'un bug d'affichage des icones
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de multiples bugs dans la gestion des grades
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de multiples bugs dans la gestion des postes
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="card-title">3.2.4</h3>
                            <p class="category">2019-10-19</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="badge badge-pill badge-success">STABLE</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                Nouveauté
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            La gestion des postes est maintenant possible.
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Refonte du code responsable de la gestion des postes
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de la base de données des utilisateurs
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de message la semaine.
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                Bug
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de multiples bugs</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="card-title">3.2.3</h3>
                            <p class="category">2019-09-13</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="badge badge-pill badge-success">STABLE</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        <i class="fas fa-exclamation-triangle text-white fa-2x mr-3"></i>La gestion des postes est <strong>DÉSACTIVÉ</strong> le temps de moderniser la base de donnée
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                Nouveauté
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            La gestion des grades est maintenant possible.
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Refonte du code responsable de la gestion des grades
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de la base de données des grades
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Suppression de L'ECC au profit d'un nouveau systeme de permission. Le grade et les permissions du grade définissent ce que peux faire un utilisateur sur le site.
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de la possibilité d'imprimer une activité depuis l'horaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de la possibilité de réserver du matériel depuis l'horaire
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                Bug
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de <a href="https://op.exvps.ca/versions/8">4 bugs</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="card-title">3.2.2</h3>
                            <p class="category">2019-09-11</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="badge badge-pill badge-success">STABLE</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                Nouveauté
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Réactivation des réservations
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Refonte du code responsable de la gestion de l'inventaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de la base de données des réservations
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de la possibilité d'imprimer une activité depuis l'horaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de la possibilité de réserver du matériel depuis l'horaire
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                Bug
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de <a href="https://op.exvps.ca/versions/8">4 bugs</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="card-title">3.2.1</h3>
                            <p class="category">2019-09-03</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="badge badge-pill badge-success">STABLE</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        <i class="fas fa-exclamation-triangle text-white fa-2x mr-3"></i>Les réservations sont <strong>DÉSACTIVÉ</strong> le temps de moderniser la base de donnée
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                Nouveauté
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de l'inventaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de la structure de la base de donnée de l'inventaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de catégorie dynamique dans l'inventaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout d'un système de nouvelles
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                           Ajout d'un système d'article pour chaque activité complémentaire
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                Bug
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de <a href="https://op.exvps.ca/versions/8">4 bugs</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="card-title">3.2.0</h3>
                            <p class="category">2019-08-26</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="badge badge-pill badge-success">STABLE</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        <i class="fas fa-exclamation-triangle text-white fa-2x mr-3"></i>L'inventaire et les réservations sont <strong>DÉSACTIVÉ</strong> le temps de moderniser la base de donnée
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                Nouveauté
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de l'affichage de l'horaire avec <a href="https://fullcalendar.io/">fullcalendar</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de la structure de la base de donnée de l'horaire
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Modernisation de l'interface avec Material UI / Dashboard
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout d'une bibliothèque d'image
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Ajout de nouvelle configuration
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Le nombre de période dans l'horaire est maintenant dynamique
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Le nombre de niveau dans l'horaire est maintenant dynamique
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Les activitées sont maintenant dynamique
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            L'horaire est maintenant disponible sur le site publique
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Les photos du calendrier sont disponible au publique depuis la page d'accueil
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                Bug
                            <ul class="list-group list-group-flush ml-3">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="text-success" style="font-size: 1.3rem;width: 1.5rem">
                                            <i class="fas fa-bug"></i>
                                        </div>
                                        <div class="col m-auto text-left">
                                            Correction de <a href="https://op.exvps.ca/projects/c-cms/work_packages?query_props=%7B%22c%22%3A%5B%22id%22%2C%22subject%22%2C%22type%22%2C%22status%22%2C%22assignee%22%5D%2C%22hl%22%3A%22none%22%2C%22hi%22%3Afalse%2C%22g%22%3A%22type%22%2C%22t%22%3A%22id%3Aasc%22%2C%22f%22%3A%5B%7B%22n%22%3A%22status%22%2C%22o%22%3A%22c%22%2C%22v%22%3A%5B%5D%7D%2C%7B%22n%22%3A%22version%22%2C%22o%22%3A%22%3D%22%2C%22v%22%3A%5B%227%22%5D%7D%5D%2C%22pa%22%3A1%2C%22pp%22%3A20%7D">22 bugs</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('admin/update.page_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('admin/update.breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        //
    </script>
@endsection
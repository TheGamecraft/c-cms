<div id="header" class="page-header pricing-page header-filter" style="background-image: url({{'"'.\App\Config::getData('public_index_img_url').'"'}})">
    <div class="container mt-lg-5 pt-md-5">
        <div class="row mt-sm-5 pt-md-5">
            <div class="col-md-6 ml-auto mr-auto text-center">
                <h1 id="test">
                    {{App\Config::getData('escadron_name_full')}}
                </h1>
                <a class="badge badge-primary edit-btn" href="/admin/public/edit/escadron_name_full" style="display: none;">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <h4>
                    {{App\Config::getData('element_title')}}
                    <a class="badge badge-primary edit-btn" href="/admin/public/edit/element_title" style="display: none;">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </h4>
            </div>
        </div>
        <div class="m-5">
            <h4 class="m-md-5">
                {{ App\Config::getData('text_public_banner_cadet_desc') }}
                @if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_banner_cadet_desc"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif
            </h4>
        </div>
    </div>
</div>
@extends('errors.layout')

@section('title', __('Page introuvable'))
@section('code')
    <span>4</span><span>0</span><span>4</span>
@endsection
@section('message', 'Nous sommes désolé, la page demandée ne semble pas exister.')
@section('error',$exception->getMessage())
@section('url','https://developer.mozilla.org/fr/docs/Web/HTTP/Status/404')

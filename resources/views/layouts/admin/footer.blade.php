<footer class="footer">
  <div class="row w-100">
    <div class="col-4">
      <nav class="float-left">
        <ul>
          <li>
            <a href="https://docs.exvps.ca/">
              Documentation
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/TheGamecraft/c-cms/blob/master/LICENSE">
              Licenses
            </a>
          </li>
        </ul>
      </nav>
    </div>
    <div class="col-4 m-auto">
      <small class="text-gray">C-CMS @version('compact')</small>
    </div>
    <div class="col-4">
      <div class="container-fluid">
        <div class="copyright float-right">
          &copy;
          <script>
            document.write(new Date().getFullYear())
          </script>, fait avec <i class="material-icons">favorite</i> par
          <a href="https://gitlab.com/c-cms" target="_blank">L'équipe de C-CMS</a>
        </div>
      </div>
    </div>
  </div>
  </footer>
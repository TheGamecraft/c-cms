<?php

namespace App\Http\Controllers;

use App\Course;
use App\GoogleDriveFile;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mode = 'future';
        if (\request('all'))
        {
            $courses = Course::all();
            $mode = 'all';
        }
        else
        {
            $courses = Course::allFuture();
        }
        foreach ($courses as $course)
        {
            if($course->ocom() != null)
            {
                $course->ocom()->updateWasGiven();
            }
        }
        return view('admin.course.index',['courses' => $courses,'mode' => $mode]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $course = Course::find($id);
        $user = \App\User::find($course->user_id);
        $name = $course->user_id;
        if ($user != null)
        {
            $name = $user->fullname();
        }
        return view('admin.course.show',['course' => $course,'username' => $name,'lessonPlanDir' => \App\GoogleDriveFile::findByPath('.Systeme/.Fichier/.PlanDeCours')->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Course $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    public function updateCommentOfficer(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $course->comment_officer = $request->comment_officer;
        $course->save();
        return redirect()->back()->with('success','Modification enregisté avec succès');
    }

    public function updateComment(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $course->comment = $request->comment;
        $course->save();
        return redirect()->back()->with('success','Modification enregisté avec succès');
    }

    public function updateCommentOfficerPlan(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $course->lessonPlan->comment = $request->comment_officer;
        $course->lessonPlan->save();
        return redirect()->back()->with('success','Modification enregisté avec succès');
    }

    public function updateCommentPlan(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $course->lessonPlan->desc = $request->comment;
        $course->lessonPlan->save();
        return redirect()->back()->with('success','Modification enregisté avec succès');
    }

    public function updateLessonPlan(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $name = $course->ocom.'_'.$course->instructor().'_'.$course->event->date_begin.'_'.'v'.date('c').'.'.\request()->file('file')->getClientOriginalExtension();
        $name = urlencode($name);
        $dirID = \App\GoogleDriveFile::findByPath('.Systeme/.Fichier/.PlanDeCours')->id;

        if ($course->lessonPlan != null)
        {
            $lessonPlan = $course->lessonPlan;
            $contents = collect(\Storage::cloud()->listContents($dirID, false));

            $file = $contents
                ->where('type', '=', 'file')
                ->where('filename', '=', pathinfo($course->lessonPlan->file, PATHINFO_FILENAME))
                ->where('extension', '=', pathinfo($course->lessonPlan->file, PATHINFO_EXTENSION))
                ->first(); // there can be duplicate file names!
            \Storage::cloud()->delete($file['path']);
        }
        else
        {
            $lessonPlan = new \App\LessonPlan();
        }
        \Storage::cloud()->putFileAs($dirID,\request()->file('file'),$name);
        $metadata = \Storage::cloud()->getMetadata($dirID.'/'.$name);

        $lessonPlan->user_id = \Auth::user()->id;
        $lessonPlan->file = $metadata['name'];
        $lessonPlan->course_id = $course->id;
        $lessonPlan->desc = "";
        $lessonPlan->comment = "";
        $lessonPlan->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }

    public function validatePlan($id)
    {
        $course = Course::findOrFail($id);
        if ($course->lessonPlan)
        {
            if ($course->lessonPlan->approved)
            {
                $course->lessonPlan->approved = false;
            }
            else
            {
                $course->lessonPlan->approved = true;
            }
            $course->lessonPlan->save();
            return strval($course->lessonPlan->approved);
        }
        return abort(500);
    }
}

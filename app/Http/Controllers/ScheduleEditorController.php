<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScheduleEditorController extends Controller
{
    public function getCourse(int $id = null)
    {
        return view('admin.schedule.editor.course');
    }

    public function getCourseEmpty(int $niveau,int $periode,$event_type = 1)
    {
        $eventType = \App\EventType::findOrFail($event_type);
        $data = $eventType->getScheduleModelData($niveau,$periode);
        $ocom = $data['ocom'];
        $name = $data['name'];
        $location = $data['location'];
        $instructor = $data['instructor'];
        $desc = $data['desc'];

        return view('admin.schedule.editor.course',[
            'periode' => $periode,
            'niveau' => $niveau,
            'ocom' => $ocom,
            'name' => $name,
            'location' => $location,
            'instructor' => $instructor,
            'desc' => $desc]);
    }

    public function getTemplate(int $id)
    {
        return view('admin.schedule.editor.template',["eventType" => \App\EventType::find($id)]);
    }

    public function getEventTemplate(int $id)
    {
        $eventType = \App\EventType::find($id);

        return json_encode($eventType);
    }

    public function getEmptyPeriode(int $id)
    {
        return view('admin.schedule.editor.periode',[
            'periode_name' => 'Periode '.$id,
            'periode_begin_time' => '00:00',
            'periode_end_time' => '00:00',
            'periode_id' => $id,
            'nbLevel' => request('nblevel')
        ]);
    }

    public function getEmptyLevel(int $id)
    {
        return view('admin.schedule.editor.level',['periode_id' => \request('nbPeriode'),'level_id' => $id]);
    }

    public function getLevelHeader(int $id)
    {
        return view('admin.schedule.editor.levelHeader',['level_id' => $id,'level_name' => 'Niveau '.$id]);
    }
}

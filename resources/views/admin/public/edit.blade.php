@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Modifier la page d'accueil</strong>
        </div>
        <div class="card-body">
            <form action="/admin/public/edit/{{$config->name}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                          <label for="data">Texte</label>
                            <textarea class="form-control" name="data" id="data" rows="5" required>{{$config->data()}}</textarea>
                          <small id="helpdata" class="form-text text-muted">Texte à afficher sur la page d'accueil</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventaire</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Inventaire</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

@endsection

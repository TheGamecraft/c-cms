@extends('errors.layout')

@section('title', __('Service indisponible'))
@section('code')
    <span>5</span><span>0</span><span>3</span>
@endsection
@section('message', 'Nous sommes désolé, mais le service est temporairement indisponible ou en maintenance.')
@section('error',$exception->getMessage())
@section('url','https://developer.mozilla.org/fr/docs/Web/HTTP/Status/503')
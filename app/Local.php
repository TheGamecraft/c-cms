<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Local
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Local whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Local extends Model
{
    //
}

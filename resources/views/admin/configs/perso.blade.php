@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Apparence</h4>
            </div>
            <div class="card-body mt-5">
                <form action="/admin/config/customisation" method="POST">
                    @csrf
                    <div class="row ml-2">
                        <div class="col-md-12 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="escadron_name_full" name="public_index_img_url" aria-describedby="emailHelp" value="{{\App\Config::getData('public_index_img_url')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_banner_cadet_desc" name="text_public_banner_cadet_desc" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_banner_cadet_desc')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_banner_apprendre_plus" name="text_public_banner_apprendre_plus" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_banner_apprendre_plus')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_intro_title" name="text_public_intro_title" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_intro_title')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_intro_desc" name="text_public_intro_desc" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_intro_desc')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_picture_title" name="text_public_picture_title" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_picture_title')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_picture_desc" name="text_public_picture_desc" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_picture_desc')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_news_title" name="text_public_news_title" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_news_title')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_news_desc" name="text_public_news_desc" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_news_desc')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_news_button" name="text_public_news_button" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_news_button')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_schedule_desc" name="text_public_schedule_desc" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_schedule_desc')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_schedule_title" name="text_public_schedule_title" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_schedule_title')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_picture_nb" name="text_public_picture_nb" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_picture_nb')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Photo page publique</label>
                                <input type="text" class="form-control" id="text_public_cta" name="text_public_cta" aria-describedby="emailHelp" value="{{\App\Config::getData('text_public_cta')}}">
                                <small id="emailHelp" class="form-text text-muted">URL vers la photo a afficher dans l'en tête de la page publique.</small>
                            </div>
                        </div>
                        <div class="col-md-12 p-2">
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                            <a href="/?editMode" target="_blank" class="btn btn-outline-secondary">Activer le mode édition</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        function saveChange(pPerm) {
            (function($) {
                var myswitch = document.getElementById(pPerm);
                $.post('/api/config/general/save?api_token='+api_token, { value: myswitch.checked,perm: pPerm } , function(data) {
                    swal({
                        title: 'Modification enregistré !',
                        type: 'success',
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                });
            })(jQuery);
        }
    </script>
@endsection

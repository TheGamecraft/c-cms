@extends('layouts.admin.main')

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Mes notifications</h4>
            </div>
            <div class="card-body">
                <div class="material-datatables">
                    <table id="notification-table" class="table table-striped w-100 notification-table">
                        <thead>
                            <td>Notifications</td>
                        </thead>
                        <tbody>
                        @foreach($notifications as $n)
                            <tr>
                                <td id="notification-{{$n->id}}" @if($n->read_at == null) class="notification-success cursor" onclick="markNotificationAsRead('{{$n->id}}')" @endif>
                                    @if(isset($n->data['title']))
                                        {{$n->data['title']}}
                                    @else
                                        <b>{{$n->data['name']}}</b><br>
                                        {{$n->data['msg']}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $(document).ready(function() {
            $('#notification-table').DataTable();
        } );
    </script>
@endsection

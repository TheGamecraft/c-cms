<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'firstname' => 'visiteur',
                'lastname' => 'Autre',
                'email' => 'visiteur@exvps.ca',
                'password' => bcrypt('f329er8kl2jHJGHdEj12567'),
                'rank_id' => '1',
                'adress' => '',
                'age' => '99',
                'avatar' => '3',
                'sexe' => 'm',
                'job_id' => '1',
                'api_token' => str_shuffle(str_random(60)),
            ],
            [
                'firstname' => 'Administrateur',
                'lastname' => 'Administrateur',
                'email' => 'admin@exvps.ca',
                'password' => bcrypt('SuperAdmin'),
                'rank_id' => '1',
                'adress' => '',
                'age' => '99',
                'avatar' => '3',
                'sexe' => 'm',
                'job_id' => '1',
                'api_token' => str_shuffle(str_random(60)),
            ]
        ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PublicPage
 *
 * @property int $id
 * @property string $name
 * @property string $banner
 * @property string $header
 * @property string $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicPage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PublicPage extends Model
{
    //
}

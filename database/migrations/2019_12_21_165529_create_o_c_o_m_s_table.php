<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOCOMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_c_o_m_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ocom');
            $table->string('objectif_competence');
            $table->string('nbPeriode');
            $table->string('objectif_rendement');
            $table->string('oren');
            $table->boolean('complementary');
            $table->string('course_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_c_o_m_s');
    }
}

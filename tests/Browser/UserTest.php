<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UserTest extends DuskTestCase
{
    public function testUser()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(\App\User::find(2))
                    ->visit('/admin/user')
                    ->assertSee('Autre visiteur')
                    ->assertSee('Administrateur Administrateur')
                    ->click('@add-user-btn')
                    ->assertPathIs('/admin/user/add')
                    ->type('firstname','Test')
                    ->type('lastname','User')
                    ->type('email','test@exvps.ca')
                    ->type('emailc','test@exvps.ca')
                    ->type('adresse','14 ave Des Rue, Rimouski')
                    ->type('telephone','4187544158')
                    ->select('sexe','Femme')
                    ->type('age','55')
                    ->select('rank','SuperAdmin')
                    ->select('job','Indéterminé')
                    ->type('psw','SuperAdmin')
                    ->type('pswc','SuperAdmin')
                    ->click('#submit')
                    ->assertSee('User Test')
                    ->click('@edit-btn-'.\App\User::all()->where('firstname','=','Test')->first()->id)
                    ->type('lastname','test')
                    ->click('#submit')
                    ->assertSee('test Test')
                    ->click('@delete-btn-'.\App\User::all()->where('firstname','=','Test')->first()->id)
                    ->press('Oui')
                    ->press('OK');
        });
    }
}

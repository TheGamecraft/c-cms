@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Configuration du système de fichier</h4>
            </div>
            <div class="card-body mt-5">
                <form action="/admin/config/files" method="POST">
                    @csrf
                    <div class="row ml-2">
                        <h3 class="col-12">Google Drive</h3>
                        <div class="col-md-6">
                            <div class="alert alert-info" role="alert">
                                Vous pouvez suivre un <a class="alert-link" target="_blank" href="https://github.com/ivanvermeyen/laravel-google-drive-demo/blob/master/README/1-getting-your-dlient-id-and-secret.md">tutoriel (Anglais) <i class="fas fa-external-link-square-alt text-white"></i></a> pour trouver c'est information.
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="google-drive-status" class="alert alert-primary" role="alert">
                                <div class="row pb-1">
                                    <div class="col-10" id="google-drive-status-text">
                                        Google Drive est initialisé correctement
                                    </div>
                                    <div class="col-2">
                                        <div id="loaderDot" class="float-right d-none" style="margin-top: -30px;height: 41px">
                                            @loaderDot
                                        </div>
                                        <div class="float-right" id="btn-sync">
                                            <button type="button" class="btn btn-round btn-fab" style="margin-top: -0.55rem" onclick="checkFileSystem()">
                                                <i class="fas fa-sync"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 p-2">
                            <div class="togglebutton">
                                <label>
                                    <input id="is_active" type="checkbox" data-toggle="switch" name="is_active">
                                    <span class="toggle"></span>
                                    Activer Google Drive
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">GOOGLE_DRIVE_CLIENT_ID</label>
                                <input type="text" class="form-control" id="GOOGLE_DRIVE_CLIENT_ID" name="GOOGLE_DRIVE_CLIENT_ID" aria-describedby="emailHelp" value="{{Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_CLIENT_ID')) }}">
                                <small id="emailHelp" class="form-text text-muted">ID de client Google Drive</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">GOOGLE_DRIVE_CLIENT_SECRET</label>
                                <input type="text" class="form-control" id="GOOGLE_DRIVE_CLIENT_SECRET" name="GOOGLE_DRIVE_CLIENT_SECRET" aria-describedby="emailHelp" value="{{Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_CLIENT_SECRET'))}}">
                                <small id="emailHelp" class="form-text text-muted">ID secret du disque Google Drive</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">GOOGLE_DRIVE_REFRESH_TOKEN</label>
                                <input type="text" class="form-control" id="GOOGLE_DRIVE_REFRESH_TOKEN" name="GOOGLE_DRIVE_REFRESH_TOKEN" aria-describedby="emailHelp" value="{{Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_REFRESH_TOKEN'))}}">
                                <small id="emailHelp" class="form-text text-muted">Token de rafraichissement du disque Google Drive</small>
                            </div>
                        </div>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">GOOGLE_DRIVE_FOLDER_ID</label>
                                <input type="text" class="form-control" id="GOOGLE_DRIVE_FOLDER_ID" name="GOOGLE_DRIVE_FOLDER_ID" aria-describedby="emailHelp" value="{{Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_FOLDER_ID'))}}">
                                <small id="emailHelp" class="form-text text-muted">ID du dossier Google Drive, "null" pour la racine</small>
                            </div>
                        </div>
                        <h3 class="col-12">Configuration de la liste nominale</h3>
                        <div class="col-md-6 p-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Liste nominative des cadets</label>
                                <input type="text" class="form-control" id="cadet_list" name="cadet_list" aria-describedby="emailHelp" value="{{\App\Config::getData('cadet_list')}}">
                                <small id="emailHelp" class="form-text text-muted">Url vers la liste nominative des cadets</small>
                            </div>
                        </div>
                        <div class="col-md-12 p-2">
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
Configuration du système de fichier
@endsection

@section('custom_scripts')
    <script>
        if({{\App\Config::getData('is_Google_Drive_enabled')}})
        {
            $('#is_active').prop( "checked", true );
        }
        else
        {
            $('#is_active').prop( "checked", false );
        }
        checkFileSystem();

        function checkFileSystem() {
            $('#loaderDot').removeClass('d-none');
            $('#btn-sync').addClass('d-none');
            let text = $('#google-drive-status-text');
            let div = $('#google-drive-status');
            div.removeClass('alert-primary');
            div.addClass('alert-info');
            text.html("Vérification du système de fichier ...");
            $.get('/api/drive/checkfilestructure',{api_token: api_token}, function (data) {
                if(data.length != 0)
                {
                    div.removeClass('alert-primary');
                    div.removeClass('alert-info');
                    div.removeClass('alert-danger');
                    div.addClass('alert-danger');
                    text.html("");
                    data.forEach(d => {
                        text.append(d);
                       console.log(d);
                    });
                }
                else
                {
                    div.removeClass('alert-primary');
                    div.removeClass('alert-info');
                    div.removeClass('alert-danger');
                    div.addClass('alert-primary');
                    text.html("Google Drive est initialisé correctement");
                }
                $('#loaderDot').addClass('d-none');
                $('#btn-sync').removeClass('d-none');
            })
        }
    </script>
@endsection

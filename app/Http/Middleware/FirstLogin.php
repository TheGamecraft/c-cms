<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class FirstLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('get'))
        {
            if (\Auth::user()->use_default_psw == true)
            {
                return redirect('/admin/setup');
            }
        }
        return $next($request);
    }
}

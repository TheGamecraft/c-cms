<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Log
 *
 * @property int $id
 * @property string $type
 * @property string $result
 * @property string $event
 * @property int $user_id
 * @property string $logable_type
 * @property int $logable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $logable
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereLogableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereLogableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Log whereUserId($value)
 * @mixin \Eloquent
 */
class Log extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function logable()
    {
        return $this->morphTo();
    }

    public function typeColor()
    {
        switch ($this->type)
        {
            case 'add':
                return 'success';
                break;
            case 'edit':
                return 'primary';
                break;
            case 'delete':
                return 'warning';
                break;
            case 'see':
                return 'info';
                break;
            case 'navigate':
                return 'secondary';
                break;
            case 'error':
                return 'danger';
                break;
        }
    }

    public function logableURL()
    {
        if ($this->logable != null)
        {
            switch ($this->logable_type)
            {
                case "App\News":
                    return "/news/".$this->logable->id;
            }
        }
    }
}

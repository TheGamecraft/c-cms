<?php

// Admin
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Error 404
Breadcrumbs::for('errors.404', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Page introuvable');
});

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push('Dashboard', route('admin.dashboard'));
});

// Admin > Setup
Breadcrumbs::for('admin.setup', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Configuration initiale', route('admin.setup'));
});

// Admin > Update
Breadcrumbs::for('admin.update', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Note de mise à jour', route('admin.update'));
});

// Admin > Status
Breadcrumbs::for('admin.status', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Status de C-CMS', route('admin.status'));
});

// Admin > Schedule
Breadcrumbs::for('admin.schedule', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Horaire', route('admin.schedule'));
});

// Admin > Schedule > Table
Breadcrumbs::for('admin.schedule.tableview', function ($trail) {
    $trail->parent('admin.schedule');
    $trail->push('Tableau', route('admin.schedule.tableview'));
});

// Admin > Schedule > Add
Breadcrumbs::for('admin.schedule.add', function ($trail,$date) {
    $trail->parent('admin.schedule');
    $trail->push('Ajouter un évenement', route('admin.schedule.add',$date));
});

// Admin > Schedule > Edit
Breadcrumbs::for('admin.schedule.edit', function ($trail,$id) {
    $trail->parent('admin.schedule');
    $trail->push('Modifier un évenement', route('admin.schedule.edit',$id));
});

// Admin > Statistiques
Breadcrumbs::for('admin.log', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Historique des activitées', route('admin.log'));
});

// Admin > User
Breadcrumbs::for('admin.users', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Utilisateurs', route('admin.users'));
});

// Admin > User > [user]
Breadcrumbs::for('admin.user', function ($trail, $id) {
    $user = \App\User::findOrFail($id);
    $trail->parent('admin.users');
    $trail->push($user->fullname(), route('admin.user',$user));
});

// Admin > User > Add
Breadcrumbs::for('admin.user.add', function ($trail) {
    $trail->parent('admin.users');
    $trail->push('Ajouter un utilisateur', route('admin.user.add'));
});

// Admin > User > [user] > Edit
Breadcrumbs::for('admin.user.edit', function ($trail, $id) {
    $user = \App\User::findOrFail($id);
    $trail->parent('admin.user',$id);
    $trail->push('Modifier', route('admin.user.edit', $user));
});

// Admin > User > [user] > Courses
Breadcrumbs::for('admin.user.courses', function ($trail, $id) {
    $user = \App\User::findOrFail($id);
    $trail->parent('admin.user',$id);
    $trail->push('Liste des cours', route('admin.user.courses', $user));
});

// Admin > Config
Breadcrumbs::for('admin.config.general', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Configurations', route('admin.config.general'));
});

// Admin > Config > Instruction
Breadcrumbs::for('admin.config.schedule', function ($trail) {
    $trail->parent('admin.config.general');
    $trail->push('Instruction', route('admin.config.schedule'));
});

// Admin > Config > Instruction > [EventType]
Breadcrumbs::for('admin.config.schedule.event_type', function ($trail,$id) {
    $eventType = \App\EventType::findOrFail($id);
    $trail->parent('admin.config.schedule');
    $trail->push($eventType->name, route('admin.config.schedule.event_type',$id));
});

// Admin > Config > Activite complementaire
Breadcrumbs::for('admin.config.complementary-activity', function ($trail) {
    $trail->parent('admin.config.general');
    $trail->push('Activitée complémentaire', route('admin.config.complementary-activity'));
});

// Admin > Config > Activite complementaire > Add
Breadcrumbs::for('admin.config.complementary-activity.add', function ($trail) {
    $trail->parent('admin.config.complementary-activity');
    $trail->push('Ajouter', route('admin.config.complementary-activity.add'));
});

// Admin > Config > Activite complementaire > Edit
Breadcrumbs::for('admin.config.complementary-activity.edit', function ($trail, $id) {
    $trail->parent('admin.config.complementary-activity');
    $trail->push('Modifier', route('admin.config.complementary-activity.edit', $id));
});

// Admin > Config > Customisation
Breadcrumbs::for('admin.config.customisation', function ($trail) {
    $trail->parent('admin.config.general');
    $trail->push('Apparence', route('admin.config.customisation'));
});

// Admin > Config > Rank
Breadcrumbs::for('admin.config.rank', function ($trail) {
    $trail->parent('admin.config.general');
    $trail->push('Grade', route('admin.config.rank'));
});

// Admin > Config > Rank > Add
Breadcrumbs::for('admin.config.rank.add', function ($trail) {
    $trail->parent('admin.config.rank');
    $trail->push('Ajouter', route('admin.config.rank.add'));
});

// Admin > Config > Rank > Edit
Breadcrumbs::for('admin.config.rank.edit', function ($trail, $id) {
    $trail->parent('admin.config.rank');
    $trail->push('Modifier', route('admin.config.rank.edit',$id));
});

// Admin > Config > Jobs
Breadcrumbs::for('admin.config.jobs', function ($trail) {
    $trail->parent('admin.config.general');
    $trail->push('Postes', route('admin.config.jobs'));
});

// Admin > Config > Jobs > Add
Breadcrumbs::for('admin.config.jobs.add', function ($trail) {
    $trail->parent('admin.config.jobs');
    $trail->push('Ajouter', route('admin.config.jobs.add'));
});

// Admin > Config > Jobs > Edit
Breadcrumbs::for('admin.config.jobs.edit', function ($trail, $id) {
    $trail->parent('admin.config.jobs');
    $trail->push('Modifier', route('admin.config.jobs.edit', $id));
});

// Admin > Config > Files
Breadcrumbs::for('admin.config.files', function ($trail) {
    $trail->parent('admin.config.general');
    $trail->push('Système de fichier', route('admin.config.files'));
});

// Admin > Picture
Breadcrumbs::for('admin.picture', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Photo', route('admin.picture'));
});

// Admin > Picture > Add
Breadcrumbs::for('admin.picture.add', function ($trail) {
    $trail->parent('admin.picture');
    $trail->push('Ajouter', route('admin.picture.add'));
});

// Admin > Picture > Edit
Breadcrumbs::for('admin.picture.edit', function ($trail, $id) {
    $trail->parent('admin.picture');
    $trail->push('Modifier', route('admin.picture.edit', $id));
});

// Admin > Inventaire
Breadcrumbs::for('admin.inv', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Inventaire', route('admin.inv'));
});

// Admin > Inventaire > Management
Breadcrumbs::for('admin.inv.management', function ($trail) {
    $trail->parent('admin.inv');
    $trail->push('Gestion de l\'inventaire', route('admin.inv.management'));
});

// Admin > Inventaire > Management > Category
Breadcrumbs::for('admin.inv.management.category', function ($trail) {
    $trail->parent('admin.inv.management');
    $trail->push('Catégories', route('admin.inv.management.category'));
});

// Admin > Inventaire > Management > Category > Add
Breadcrumbs::for('admin.inv.management.category.add', function ($trail) {
    $trail->parent('admin.inv.management.category');
    $trail->push('Ajouter', route('admin.inv.management.category.add'));
});

// Admin > Inventaire > Management > Category > Edit
Breadcrumbs::for('admin.inv.management.category.edit', function ($trail, $id) {
    $trail->parent('admin.inv.management.category');
    $trail->push('Modifier', route('admin.inv.management.category.edit', $id));
});

// Admin > News
Breadcrumbs::for('admin.news', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Nouvelles', route('admin.news'));
});

// Admin > News > Add
Breadcrumbs::for('admin.news.add', function ($trail) {
    $trail->parent('admin.news');
    $trail->push('Ajouter', route('admin.news.add'));
});

// Admin > News > Edit
Breadcrumbs::for('admin.news.edit', function ($trail, $id) {
    $trail->parent('admin.news');
    $trail->push('Modifier', route('admin.news.edit', $id));
});

// Admin > Article
Breadcrumbs::for('admin.article', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Articles', route('admin.article'));
});

// Admin > Article > Edit
Breadcrumbs::for('admin.article.edit', function ($trail, $id) {
    $trail->parent('admin.article');
    $trail->push('Modifier', route('admin.article.edit', $id));
});

// Admin > Profil
Breadcrumbs::for('admin.profil', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Profil d\'utilisateur', route('admin.profil'));
});

// Admin > Profil > Avatar
Breadcrumbs::for('admin.profil.avatar', function ($trail) {
    $trail->parent('admin.profil');
    $trail->push('Avatar', route('admin.profil.avatar'));
});

// Admin > Profil > Adress
Breadcrumbs::for('admin.profil.adress', function ($trail) {
    $trail->parent('admin.profil');
    $trail->push('Adresse', route('admin.profil.adress'));
});

// Admin > Profil > Telephone
Breadcrumbs::for('admin.profil.telephone', function ($trail) {
    $trail->parent('admin.profil');
    $trail->push('Téléphone', route('admin.profil.telephone'));
});

// Admin > Profil > Password
Breadcrumbs::for('admin.profil.psw', function ($trail) {
    $trail->parent('admin.profil');
    $trail->push('Password', route('admin.profil.psw'));
});

// Admin > Profil > Adress
Breadcrumbs::for('admin.profil.courses', function ($trail) {
    $trail->parent('admin.profil');
    $trail->push('Mes cours', route('admin.profil.courses'));
});

// Admin > Profil > Notification
Breadcrumbs::for('admin.profil.notifications', function ($trail) {
    $trail->parent('admin.profil');
    $trail->push('Mes notifications', route('admin.profil.notifications'));
});

// Admin > Drive
Breadcrumbs::for('admin.drive', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Explorateur de fichier', route('admin.drive'));
});

// Admin > Drive > Permission
Breadcrumbs::for('admin.drive.permission', function ($trail,$folder) {
    $trail->parent('admin.drive.folder',$folder);
    $trail->push('Permission', route('admin.drive.permission',$folder));
});

// Admin > Drive > [$folder]
Breadcrumbs::for('admin.drive.folder', function ($trail,$folder) {
    $trail->parent('admin.drive');
    $trail->push(\Storage::cloud()->getMetadata($folder)['name'], route('admin.drive',$folder));
});

// Admin > Instruction
Breadcrumbs::for('admin.instruction', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Instruction', route('admin.instruction'));
});

// Admin > Instruction > Guide
Breadcrumbs::for('admin.instruction.guide', function ($trail) {
    $trail->parent('admin.instruction');
    $trail->push('Guide pédagogique et norme de qualification', route('admin.instruction.guide'));
});

// Admin > Instruction > Lesson Plan
Breadcrumbs::for('admin.instruction.course', function ($trail) {
    $trail->parent('admin.instruction');
    $trail->push('Plan de cours et documentation', route('admin.instruction.course'));
});

// Admin > Instruction > Lesson Plan
Breadcrumbs::for('admin.stats.instruction', function ($trail) {
    $trail->parent('admin.instruction');
    $trail->push('Statistique de l\'instruction', route('admin.stats.instruction'));
});

// Admin > Files
Breadcrumbs::for('admin.files', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Mes fichier', route('admin.files'));
});

// Admin > Files > Cadet
Breadcrumbs::for('admin.files.cadet', function ($trail) {
    $trail->parent('admin.files');
    $trail->push('Cadet', route('admin.files.cadet'));
});

// Admin > Files > Staff
Breadcrumbs::for('admin.files.staff', function ($trail) {
    $trail->parent('admin.files');
    $trail->push('Staff', route('admin.files.staff'));
});

// Admin > Files > ETAMAS
Breadcrumbs::for('admin.files.etamas', function ($trail) {
    $trail->parent('admin.files');
    $trail->push('ETAMAS', route('admin.files.etamas'));
});

// Admin > Files > Officier
Breadcrumbs::for('admin.files.officier', function ($trail) {
    $trail->parent('admin.files');
    $trail->push('Officier', route('admin.files.officier'));
});

// Admin > Files > Publique
Breadcrumbs::for('admin.files.publique', function ($trail) {
    $trail->parent('admin.files');
    $trail->push('Publique', route('admin.files.publique'));
});

// Admin > OCOM
Breadcrumbs::for('admin.ocom', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Base de données des cours', route('admin.ocom'));
});

// Admin > OCOM > Generate
Breadcrumbs::for('admin.ocom.generate', function ($trail) {
    $trail->parent('admin.ocom');
    $trail->push('Génération de masse', route('admin.ocom.generate'));
});

// Admin > OCOM > Add
Breadcrumbs::for('admin.ocom.add', function ($trail) {
    $trail->parent('admin.ocom');
    $trail->push('Ajouter', route('admin.ocom.add'));
});

// Admin > OCOM > [ocom]
Breadcrumbs::for('admin.ocom.show', function ($trail, $id) {
    $ocom = \App\OCOM::findOrFail($id);
    $trail->parent('admin.ocom');
    $trail->push($ocom->ocom, route('admin.ocom.show', $ocom));
});

// Admin > OCOM > [ocom] > Edit
Breadcrumbs::for('admin.ocom.edit', function ($trail, $id) {
    $ocom = \App\OCOM::findOrFail($id);
    $trail->parent('admin.ocom.show',$id);
    $trail->push('Modifier', route('admin.ocom.edit', $ocom));
});

// Admin > Course
Breadcrumbs::for('admin.course', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Liste des cours', route('admin.course'));
});

// Admin > Course > [course]
Breadcrumbs::for('admin.course.show', function ($trail, $id) {
    $course = \App\Course::findOrFail($id);
    $trail->parent('admin.course',$id);
    $trail->push($course->ocom.' ('.$course->event->date_begin.')', route('admin.course.show',$course));
});
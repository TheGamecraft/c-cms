@extends('errors.layout')

@section('title', __('Accès refusé'))
@section('code')
<span>4</span><span>0</span><span>3</span>
@endsection
@section('message', 'Nous sommes désolé, le serveur a compris la requête, mais refuse de l\'exécuter.')
@section('error',$exception->getMessage())
@section('url','https://developer.mozilla.org/fr/docs/Web/HTTP/Status/403')

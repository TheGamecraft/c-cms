@extends('layouts.admin.main')

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Ajouter un type d'évenement</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <form method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="label-control">Nom</label>
                                    <input type="text" class="form-control" name="name" required/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="label-control">Emplacement</label>
                                    <input type="text" class="form-control" name="location" required/>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox" checked="" name="is_mandatory">
                                        <span class="toggle"></span>
                                        Obligatoire
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox" name="hidden">
                                        <span class="toggle"></span>
                                        Évenement caché
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="label-control">Heure de début</label>
                                    <input name="begin_time" type="time" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="label-control">Heure de fin</label>
                                    <input name="end_time" type="time" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group iconpicker-container">
                                    <label for="type">Icone</label>
                                    <div class="input-group iconpicker-container">
                                        <input id="calendar_icon" name="calendar_icon" data-placement="bottomRight" class="form-control icp icp-auto iconpicker-element iconpicker-input" type="text" required>
                                        <span class="input-group-addon"><i id="calendar_icon_display" class="fas fa-assistive-listening-systems"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="type">Couleur</label>
                                    <div class="input-group iconpicker-container">
                                        <input class="form-control" type="text" name="calendar_color" id="calendar_color" onclick="pickr.show()" required>
                                        <span class="color-picker"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="mb-0">Description</label>
                                <div class="form-group">
                                    <textarea name="admin_desc" class="form-control richeditor" name="admin_desc" id="admin_desc" rows="6" required>
                                    </textarea>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <label class="mb-0">Message de la semaine</label>
                                <div class="togglebutton float-right">
                                    <label>
                                        <input type="checkbox" name="use_weekly_msg" type="button" data-toggle="collapse" data-target="#collapseMSG" aria-expanded="false" aria-controls="collapseMSG">
                                        <span class="toggle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="collapse w-100" id="collapseMSG">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Heure publication des messages de la semaine</label>
                                        <select class="form-control mt-3" name="weekly_msg_publication_time">
                                            <option value="-1days">-1 Jour</option>
                                            <option value="-2days">-2 Jours</option>
                                            <option value="-3days">-3 Jours</option>
                                            <option value="-4days">-4 Jours</option>
                                            <option value="-5days">-5 Jours</option>
                                            <option value="-6days">-6 Jours</option>
                                            <option value="-7days">-7 Jours</option>
                                            <option value="-2weeks">-2 Semaines</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <label class="mb-0">Horaire</label>
                                <div class="togglebutton float-right">
                                    <label>
                                        <input type="checkbox" name="use_schedule" type="button" data-toggle="collapse" data-target="#collapseSchedule" aria-expanded="false" aria-controls="collapseSchedule">
                                        <span class="toggle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="collapse" id="collapseSchedule">
                                    <div class="card-body" style="overflow: scroll">
                                        <div id="editor" class="m-3" style="width: 110vw">
                                            @loaderDot
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-primary">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>
    <script src="/js/plugins/fontawesome-icon-picker/fontawesome-iconpicker.js"></script>
    <script src="/js/plugins/schedule/editorv2.js"></script>
    <script src="/js/plugins/autocomplete.js"></script>
    <script>
        $('.richeditor').trumbowyg({
            lang: 'fr',
            btns: [
                ['viewHTML'],
                ['emoji'],
                ['undo', 'redo'], // Only supported in Blink browsers
                ['strong', 'em', 'del'],
                ['superscript', 'subscript'],
                ['fontfamily'],
                ['fontsize'],
                ['foreColor', 'backColor'],
                ['link'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen']
            ]
        });

        $('.icp-auto').iconpicker({ placement: 'right',animation: 'false'});
        const pickr = Pickr.create({
            el: '.color-picker',
            theme: 'monolith', // or 'monolith', or 'nano'

            swatches: [
                'rgba(244, 67, 54, 1)',
                'rgba(233, 30, 99, 1)',
                'rgba(156, 39, 176, 1)',
                'rgba(103, 58, 183, 1)',
                'rgba(63, 81, 181, 1)',
                'rgba(33, 150, 243, 1)',
                'rgba(3, 169, 244, 1)',
                'rgba(0, 188, 212, 1)',
                'rgba(0, 150, 136, 1)',
                'rgba(76, 175, 80, 1)',
                'rgba(139, 195, 74, 1)',
                'rgba(205, 220, 57, 1)',
                'rgba(255, 235, 59, 1)',
                'rgba(255, 193, 7, 1)'
            ],
            comparison: false,
            components: {

                // Main components
                preview: true,
                opacity: false,
                hue: true,

                // Input / output Options
                interaction: {
                    hex: true,
                    rgba: false,
                    hsla: false,
                    hsva: false,
                    cmyk: false,
                    input: true,
                    clear: false,
                    save: false
                }
            }
        });
        pickr.on('change', (color,instance) => {
            $('#calendar_color').val(color.toHEXA().toString());
        });
        initEditor(1,'eventType');
    </script>
@endsection
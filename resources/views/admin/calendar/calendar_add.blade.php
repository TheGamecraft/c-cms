@extends('layouts.admin.main')

@section('content')
    <div class="col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-header">
              <strong> {{ trans('calendar.add_title')}} </strong> {{ ucfirst(strftime("%A le %e %B %Y", strtotime($RequestDate)))}}
            </div>
            <div class="card-body card-block">
                <form action="/admin/calendar/add" method="POST" enctype="multipart/form-data" class="form-horizontal">

                    {{ csrf_field() }}

                    <div class="row form-group">
                    <div class="col col-md-3"><label for="select" class=" form-control-label"> {{ trans('calendar.add_form_event_type')}} </label></div>
                    <div class="col-12 col-md-9">
                        <select name="event_type" id="event_type" class="form-control" onchange="switchType()">
                            <option value="null"> {{trans('calendar.add_form_event_type_select')}} </option>
                            @foreach ($ComplementaryActivity as $activity)
                                <option value="{{ $activity->id }}"> {{ $activity->name }} </option>  
                            @endforeach                     
                        </select>
                    </div>
                    </div>
                    <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"> {{ trans('calendar.add_form_event_name') }}</label></div>
                    <div class="col-12 col-md-6"><input id="event_name" name="event_name" placeholder="{{ trans('calendar.add_form_event_name_placeholder')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.add_form_event_name_help')}}</small></div>
                    <div class="col col-md-3">
                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.add_form_event_mandatory")}} </label>
                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="is_event_mandatory" name="is_event_mandatory" class="switch-input" checked="true" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                        <small class="form-text text-muted"> {{trans('calendar.add_form_event_mandatory_help')}}</small>
                    </div>
                    </div>
                    <div class="row form-group">
                    <div class="col col-md-3"><label for="disabled-input" class=" form-control-label"> {{ trans("calendar.add_form_event_date")}} </label></div>
                    <div class="col-12 col-md-9"><input id="event_date" name="event_date" readonly class="form-control" value="{{ $RequestDate}}" type="text"></div>
                    </div>
                    <div class="row form-group">
                    <div class="col col-md-3"><label for="disabled-input" class=" form-control-label"> {{ trans("calendar.add_form_event_time_begin")}} </label></div>
                    <div class="col-12 col-md-4"><input id="event_begin_time" name="event_begin_time" class="form-control" type="time"><small class="form-text text-muted"> {{trans('calendar.add_form_event_time_begin_help')}}</small></div>
                    <div class="col col-md-1"><label for="disabled-input" class=" form-control-label"> {{ trans("calendar.add_form_event_time_end")}} </label></div>
                    <div class="col-12 col-md-4"><input id="event_end_time" name="event_end_time" class="form-control" type="time"><small class="form-text text-muted"> {{trans('calendar.add_form_event_time_end_help')}}</small></div> 
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label"> {{ trans('calendar.add_form_event_place') }}</label></div>
                        <div class="col-12 col-md-9"><input id="event_location" name="event_location" placeholder="{{ trans('calendar.add_form_event_place_placeholder')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.add_form_event_name_help')}}</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">{{ trans('calendar.add_form_event_desc')}}</label></div>
                    <div class="col-12 col-md-9"><textarea name="event_desc" id="event_desc" rows="9" placeholder="{{ trans('calendar.add_form_event_desc_placeholder') }}" class="form-control"></textarea>
                        <small class="form-text text-muted"> {{trans('calendar.add_form_event_desc_help')}}</small></div>
                    </div>

                    <!-- Special section ONLY for regular event type -->
                    <div id="special_section" style="display: none">
                        <div class="row form-group">
                            <button type="button" class="btn btn-primary btn-lg col-md-4 col-sm-12 active" id="btn_grade_1" onclick="switchToLevelOne()">Niveau 1</button>
                            <button type="button" class="btn btn-primary btn-lg col-md-4 col-sm-12" id="btn_grade_2" onclick="switchToLevelTwo()">Niveau 2</button>
                            <button type="button" class="btn btn-primary btn-lg col-md-4 col-sm-12" id="btn_grade_3" onclick="switchToLevelThree()">Niveau 3</button>
                        </div>
                        <div id="grade_1">
                            <!-- First periode -->
                            <div>
                                <h3> {{ trans('calendar.reg_p1') }} </h3>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_classe_name')}} </label></div>
                                    <div class="col-md-5"><input id="n1_p1_name" name="n1_p1_name" placeholder="{{ trans('calendar.reg_classe_name')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small></div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_ocom')}} </label></div>
                                    <div class="col-md-4"><input id="n1_p1_ocom" name="n1_p1_ocom" placeholder="{{ trans('calendar.reg_ocom')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_ocom_help')}}</small></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_instructor_name')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n1_p1_instructor" id="n1_p1_instructor" class="form-control">
                                            @foreach ($Userslist as $user)
                                                <option value=" {{ $user->id }}"> {{ $user->lastname." ".$user->firstname}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_location')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n1_p1_local" id="n1_p1_local" class="form-control">
                                            @foreach ($LocalsList as $local)
                                                <option value=" {{ $local->id }}"> {{ $local->name}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col col-md-2">
                                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.reg_plan_done")}} </label>
                                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="n1_p1_plandone" name="n1_p1_plandone" class="switch-input" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_plan_done_help')}}</small>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: 25px; margin-bottom: 40px;">

                            <!-- Second periode -->
                            <div>
                                <h3> {{ trans('calendar.reg_p2') }} </h3>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_classe_name')}} </label></div>
                                    <div class="col-md-5"><input id="n1_p2_name" name="n1_p2_name" placeholder="{{ trans('calendar.reg_classe_name')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small></div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_ocom')}} </label></div>
                                    <div class="col-md-4"><input id="n1_p2_ocom" name="n1_p2_ocom" placeholder="{{ trans('calendar.reg_ocom')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_ocom_help')}}</small></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_instructor_name')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n1_p2_instructor" id="n1_p2_instructor" class="form-control">
                                            @foreach ($Userslist as $user)
                                                <option value=" {{ $user->id }}"> {{ $user->lastname." ".$user->firstname}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_location')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n1_p2_local" id="n1_p2_local" class="form-control">
                                            @foreach ($LocalsList as $local)
                                                <option value=" {{ $local->id }}"> {{ $local->name}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col col-md-2">
                                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.reg_plan_done")}} </label>
                                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="n1_p2_plandone" name="n1_p2_plandone" class="switch-input" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_plan_done_help')}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="grade_2" style="display: none">
                            <!-- First periode -->
                            <div>
                                <h3> {{ trans('calendar.reg_p1') }} </h3>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_classe_name')}} </label></div>
                                    <div class="col-md-5"><input id="n2_p1_name" name="n2_p1_name" placeholder="{{ trans('calendar.reg_classe_name')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small></div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_ocom')}} </label></div>
                                    <div class="col-md-4"><input id="n2_p1_ocom" name="n2_p1_ocom" placeholder="{{ trans('calendar.reg_ocom')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_ocom_help')}}</small></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_instructor_name')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n2_p1_instructor" id="n2_p1_instructor" class="form-control">
                                            @foreach ($Userslist as $user)
                                                <option value=" {{ $user->id }}"> {{ $user->lastname." ".$user->firstname}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_location')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n2_p1_local" id="n2_p1_local" class="form-control">
                                            @foreach ($LocalsList as $local)
                                                <option value=" {{ $local->id }}"> {{ $local->name}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col col-md-2">
                                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.reg_plan_done")}} </label>
                                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="n2_p1_plandone" name="n2_p1_plandone" class="switch-input" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_plan_done_help')}}</small>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: 25px; margin-bottom: 40px;">

                            <!-- Second periode -->
                            <div>
                                <h3> {{ trans('calendar.reg_p2') }} </h3>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_classe_name')}} </label></div>
                                    <div class="col-md-5"><input id="n2_p2_name" name="n2_p2_name" placeholder="{{ trans('calendar.reg_classe_name')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small></div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_ocom')}} </label></div>
                                    <div class="col-md-4"><input id="n2_p2_ocom" name="n2_p2_ocom" placeholder="{{ trans('calendar.reg_ocom')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_ocom_help')}}</small></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_instructor_name')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n2_p2_instructor" id="n2_p2_instructor" class="form-control">
                                            @foreach ($Userslist as $user)
                                                <option value=" {{ $user->id }}"> {{ $user->lastname." ".$user->firstname}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_location')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n2_p2_local" id="n2_p2_local" class="form-control">
                                            @foreach ($LocalsList as $local)
                                                <option value=" {{ $local->id }}"> {{ $local->name}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col col-md-2">
                                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.reg_plan_done")}} </label>
                                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="n2_p2_plandone" name="n2_p2_plandone" class="switch-input" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_plan_done_help')}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="grade_3" style="display: none">
                            <!-- First periode -->
                            <div>
                                <h3> {{ trans('calendar.reg_p1') }} </h3>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_classe_name')}} </label></div>
                                    <div class="col-md-5"><input id="n3_p1_name" name="n3_p1_name" placeholder="{{ trans('calendar.reg_classe_name')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small></div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_ocom')}} </label></div>
                                    <div class="col-md-4"><input id="n3_p1_ocom" name="n3_p1_ocom" placeholder="{{ trans('calendar.reg_ocom')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_ocom_help')}}</small></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_instructor_name')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n3_p1_instructor" id="n3_p1_instructor" class="form-control">
                                            @foreach ($Userslist as $user)
                                                <option value=" {{ $user->id }}"> {{ $user->lastname." ".$user->firstname}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_location')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n3_p1_local" id="n3_p1_local" class="form-control">
                                            @foreach ($LocalsList as $local)
                                                <option value=" {{ $local->id }}"> {{ $local->name}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col col-md-2">
                                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.reg_plan_done")}} </label>
                                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="n3_p1_plandone" name="n3_p1_plandone" class="switch-input" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_plan_done_help')}}</small>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: 25px; margin-bottom: 40px;">

                            <!-- Second periode -->
                            <div>
                                <h3> {{ trans('calendar.reg_p2') }} </h3>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_classe_name')}} </label></div>
                                    <div class="col-md-5"><input id="n3_p2_name" name="n3_p2_name" placeholder="{{ trans('calendar.reg_classe_name')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small></div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_ocom')}} </label></div>
                                    <div class="col-md-4"><input id="n3_p2_ocom" name="n3_p2_ocom" placeholder="{{ trans('calendar.reg_ocom')}}" class="form-control" type="text"><small class="form-text text-muted"> {{trans('calendar.reg_ocom_help')}}</small></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"><label class=" form-control-label"> {{ trans('calendar.reg_instructor_name')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n3_p2_instructor" id="n3_p2_instructor" class="form-control">
                                            @foreach ($Userslist as $user)
                                                <option value=" {{ $user->id }}"> {{ $user->lastname." ".$user->firstname}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col-md-1"><label for="select" class=" form-control-label"> {{ trans('calendar.reg_location')}} </label></div>
                                    <div class="col-md-3">
                                        <select name="n3_p2_local" id="n3_p2_local" class="form-control">
                                            @foreach ($LocalsList as $local)
                                                <option value=" {{ $local->id }}"> {{ $local->name}} </option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_classe_name_help')}}</small>
                                    </div>
                                    <div class="col col-md-2">
                                        <label for="disabled-input" class=" form-control-label"> {{ trans("calendar.reg_plan_done")}} </label>
                                        <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input id="n3_p2_plandone" name="n3_p2_plandone" class="switch-input" type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                        <small class="form-text text-muted"> {{trans('calendar.reg_plan_done_help')}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                      </div>
                </form>
            </div>
          </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('calendar.add_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('calendar.add_breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
   <script src="/assets/js/calendar/calendar.js"></script>
   <script>
    tinymce.init({
      selector: '#event_desc',
      branding: false,
      menubar: 'edit view format'
    });
    </script>
   <div class="log"></div>
@endsection
<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Boolean;

class ConfigController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        clogNav('consulte les configurations');
        return view ('admin.configs.general',['configs' => Config::all()]);
    }

    public function customisation()
    {
        return view ('admin.configs.perso',['configs' => Config::all()]);
    }

    public function customisationUpdate()
    {
        $configs = [
            'public_index_img_url'
        ];

        foreach ($configs as $config) {
            $c = \App\Config::all()->where('name',$config)->first();
            $c->data = [request($config)];
            $c->save();
        }

        clog('edit','success','a modifié la configuration');

        return redirect('/admin/config/customisation')->with('success','Modification sauvegarder avec succès !');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function show(Config $config)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function edit(Config $config)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $configs = [
            'is_schedule_public',
            'is_schedule_build',
            'escadron_name_full',
            'escadron_name_short',
            'escadron_number',
            'escadron_phone',
            'escadron_element',
            'element_title',
            'escadron_address',
            'escadron_direct_googlemap_link',
            'media_facebook',
            'media_twitter',
            'media_instagram',
            'media_email',

        ];

        foreach ($configs as $config) {
            $c = \App\Config::all()->where('name',$config)->first();
            $c->data = [request($config)];
            $c->save();
        }
        clog('edit','success','a modifié la configuration');
        return redirect('/admin/config')->with('success','Modification sauvegarder avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        //
    }

    public function showfilesConfig()
    {
        $error = [];
        return view('admin.configs.file',['configs' => Config::all(),'error' => $error]);
    }

    public function editfilesConfig()
    {
        $configList = [
            'cadet_list',
        ];

        $configListEncrypt = [
            'GOOGLE_DRIVE_CLIENT_ID',
            'GOOGLE_DRIVE_CLIENT_SECRET',
            'GOOGLE_DRIVE_REFRESH_TOKEN',
            'GOOGLE_DRIVE_FOLDER_ID'
        ];

        if (\request('is_active') == 'on')
        {
            $config = \App\Config::find('is_Google_Drive_enabled');
            $config->data = ['true'];
            $config->save();
        }
        else
        {
            $config = \App\Config::find('is_Google_Drive_enabled');
            $config->data = ['false'];
            $config->save();
        }


        foreach ($configList as $configName)
        {
            $config = \App\Config::find($configName);
            $config->data = [\request($configName)];
            $config->save();
        }

        foreach ($configListEncrypt as $configName)
        {
            $config = \App\Config::find($configName);
            $config->data = [\Crypt::encryptString(\request($configName))];
            $config->save();
        }

        clog('edit','success','a modifié la configuration des fichiers');
        return redirect('/admin/config/files')->with('success','Modification sauvegarder avec succès !');
    }
}

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => "L' :attribute doit être accepter.",
    'active_url'           => "L' :attribute n'est pas une url valide.",
    'after'                => "L' :attribute doit être une date apres :date.",
    'after_or_equal'       => "L' :attribute doit être une date plus grande ou égale à :date.",
    'alpha'                => "L' :attribute doit seulement contenir des lettres.",
    'alpha_dash'           => "L' :attribute peut contenir seulement des nombres, lettres et symboles.",
    'alpha_num'            => "L' :attribute peut seulement contenir des nombres et des lettres.",
    'array'                => "L' :attribute doit être un tableau.",
    'before'               => "L' :attribute doit être une date avant :date.",
    'before_or_equal'      => "L' :attribute doit être une date avant ou égale à :date.",
    'between'              => [
        'numeric' => "L' :attribute doit être entre :min et :max.",
        'file'    => "L' :attribute doit être entre :min et :max kilobytes.",
        'string'  => "L' :attribute doit être entre :min et :max characters.",
        'array'   => "L' :attribute doit avoir entre :min et :max items.",
    ],
    'boolean'              => "L' :attribute champ doit être vrai ou faux.",
    'confirmed'            => "L' :attribute confirmation ne doit pas correspondre.",
    'date'                 => "L' :attribute n'est pas une date valide.",
    'date_format'          => "L' :attribute ne correspond pas au format :format.",
    'different'            => "L' :attribute et :other doivent être différent.",
    'digits'               => "L' :attribute doit être :digits des chiffres.",
    'digits_between'       => "L' :attribute doit être entre :min et :max nombres.",
    'dimensions'           => "L' :attribute a des dimensions d'image invalide.",
    'distinct'             => "L' :attribute champ a une valeur duppliquée.",
    'email'                => "L' :attribute doit être une adresse email valide.",
    'exists'               => "L' :attribute sélectionner est invalide.",
    'file'                 => "L' :attribute doit être un fichier.",
    'filled'               => "L' :attribute champ doit avoir une valeur.",
    'gt'                   => [
        'numeric' => "L' :attribute doit être supérieur que :value.",
        'file'    => "L' :attribute doit être supérieur que :value kilobytes.",
        'string'  => "L' :attribute doit être plus grand que :value lettres.",
        'array'   => "L' :attribute doit avoir plus que :value items.",
    ],
    'gte'                  => [
        'numeric' => "L' :attribute doit être supérieur ou égal à :value.",
        'file'    => "L' :attribute doit être supérieur ou égal à :value kilobytes.",
        'string'  => "L' :attribute doit être supérieur ou égal à :value lettres.",
        'array'   => "L' :attribute doit avoir :value items ou plus.",
    ],
    'image'                => "L' :attribute doit être une image.",
    'in'                   => "L' :attribute sélectionner est invalide.",
    'in_array'             => "L' :attribute champ n'existe pas dans :other.",
    'integer'              => "L' :attribute doit être un nombre entier.",
    'ip'                   => "L' :attribute doit être une adresse ip valide.",
    'ipv4'                 => "L' :attribute doit être une adresse IPv4 valide.",
    'ipv6'                 => "L' :attribute doit être une adresse IPv6 valide.",
    'json'                 => "L' :attribute doit être une chaine JSON valide.",
    'lt'                   => [
        'numeric' => "L' :attribute doit être moins que :value.",
        'file'    => "L' :attribute doit être moins que :value kilobytes.",
        'string'  => "L' :attribute doit être moins que :value lettre.",
        'array'   => "L' :attribute doit avoir moins que :value items.",
    ],
    'lte'                  => [
        'numeric' => "L' :attribute doit être inférieur ou égal à :value.",
        'file'    => "L' :attribute doit être inférieur ou égal à :value kilobytes.",
        'string'  => "L' :attribute doit être inférieur ou égal à :value lettres.",
        'array'   => "L' :attribute ne doit pas avoir plus de :value items.",
    ],
    'max'                  => [
        'numeric' => "L' :attribute ne doit pas être plus grand que :max.",
        'file'    => "L' :attribute ne doit pas être plus grand que :max kilobytes.",
        'string'  => "L' :attribute ne doit pas être plus grand que :max lettres.",
        'array'   => "L' :attribute ne doit pas avoir plus que :max items.",
    ],
    'mimes'                => "L' :attribute doit être un fichier de type: :values.",
    'mimetypes'            => "L' :attribute doit être un fichier de type: :values.",
    'min'                  => [
        'numeric' => "L' :attribute doit être d'au moins :min.",
        'file'    => "L' :attribute doit être d'au moins :min kilobytes.",
        'string'  => "L' :attribute doit être d'au moins :min lettres.",
        'array'   => "L' :attribute doit être d'au moins :min items.",
    ],
    'not_in'               => "L' :attribute sélectionner est invalide.",
    'not_regex'            => "Le format de l' :attribute est invalide.",
    'numeric'              => "L' :attribute doit être un nombre.",
    'present'              => "L' :attribute champ doit être présent.",
    'regex'                => "L' :attribute format est invalide.",
    'required'             => "L' :attribute champ set requis.",
    'required_if'          => "L' :attribute champ est requis quand :other est :value.",
    'required_unless'      => "L' :attribute champ est requis sauf si :other est dans :values.",
    'required_with'        => "L' :attribute champ est requis quand :values sont présentes.",
    'required_with_all'    => "L' :attribute champ est requis quand :values sont présentes",
    'required_without'     => "L' :attribute champ est requis quand :values ne sont pas présentes.",
    'required_without_all' => "L' :attribute champ est requis lorsqu'aucunes :values ne sont présentes.",
    'same'                 => "L' :attribute et :other doivent correspondre.",
    'size'                 => [
        'numeric' => "L' :attribute doit être :size.",
        'file'    => "L' :attribute doit être :size kilobytes.",
        'string'  => "L' :attribute doit être :size lettres.",
        'array'   => "L' :attribute doit contenir :size items.",
    ],
    'string'               => "L' :attribute doit être une chaine.",
    'timezone'             => "L' :attribute doit être une zone valide.",
    'unique'               => "L' :attribute a déjà été prit.",
    'uploaded'             => "L' :attribute n'a pas pu se télécharger.",
    'url'                  => "L' :attribute format est invalide.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => "custom-message",
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Notifications\mail;
use \App\Notifications\sms;
use \App\Notifications\Alert;
use \App\Log;
use \App\Schedule;
use \App\Local;
use \App\User;
use Carbon\Carbon;


class CalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['generate','load','delete']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.calendar.calendar_display');
    }

    public function indexTable()
    {
        $event = \App\Event::allThisYear()->sortBy('date_begin')->filter(function ($value,$key) {
                return $value->hidden != 1;
            });
        return view('admin.schedule.table.index',['events' => $event]);
    }
}

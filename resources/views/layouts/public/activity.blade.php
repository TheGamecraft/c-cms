<div class="row">
    @foreach ($activities as $activity)
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-image" style="height: 18rem;overflow: hidden">
                    <img class="img-fluid" src="{{$activity->public_header_picture}}" alt=""/>
                </div>
                <div class="card-body">
                    <h4 class="card-title">{{$activity->name}}</h4>
                    <div style="height: 4rem;overflow: hidden;">{!! $activity->public_body !!}</div>
                    <a name="activity" id="activity" class="btn btn-primary mt-2" href="/activity/{{$activity->id}}" role="button">Plus d'information</a>
                </div>
            </div>
        </div>        
    @endforeach
</div>
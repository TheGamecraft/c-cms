<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('date_begin');
            $table->string('date_end');
            $table->string('type');
            $table->string('user_id');
            $table->string('location');
            $table->boolean('is_mandatory');
            $table->boolean('use_weekly_msg');
            $table->boolean('use_schedule');
            $table->text('desc');
            $table->text('msg');
            $table->string('weekly_msg_file');
            $table->text('schedule');
            $table->string('calendar_color');
            $table->string('calendar_icon');
            $table->string('weekly_msg_publication_time')->default('-5day');
            $table->boolean('hidden')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

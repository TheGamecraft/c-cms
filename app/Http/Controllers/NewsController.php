<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.allnews',['news' => News::allWithWeeklyMsg()->where('publish','=','1')->sortByDesc('updated_at')->paginate(9)]);
    }

    public function indexAdmin()
    {
        clogNav('a consulté les nouvelles');
        return view('admin.news.index',['news' => News::allWithWeeklyMsg()->sortByDesc('updated_at')->paginate(9)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $n = new News();

        $n->title = $request->title;
        $n->body = $request->body;
        if ($request->publish == "1")
        {
            $n->publish = 1;
        }
        else
        {
            $n->publish = 0;
        }
        $n->user_id = \Auth::user()->id;

        $n->save();
        clog('add','success','a ajouté une nouvelle',null,'App\News',$n->id);
        return redirect('/admin/news')->with('success','Nouvelle ajouté avec succès');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (\request('type') == 'msg')
        {
            return view('public.news', ['new' => \App\News::getWeeklyMsg(\App\Event::find($id))]);
        }
        else
        {
            return view('public.news', ['new' => \App\News::find($id)]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.news.update',['news' => News::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $n = News::find($id);

        $n->title = $request->title;
        $n->body = $request->body;
        if ($request->publish == "1")
        {
            $n->publish = 1;
        }
        else
        {
            $n->publish = 0;
        }
        $n->user_id = \Auth::user()->id;

        $n->save();
        clog('edit','success','a modifié une nouvelle',null,'App\News',$id);
        return redirect('/admin/news')->with('success','Nouvelle modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $news = News::find(request('id'));

        $news->delete();
        clog('delete','success','a supprimé une nouvelle',null,'App\News',$id);
    }
}

let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/bootstrap.js','public/js').sourceMaps()
    .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/theme/material-dashboard/assets/scss/material-dashboard.scss', 'public/css')
   .styles('resources/custom.css','public/css/custom.css');

<div class="title">
    <h1 class="title">
        {{ App\Config::getData('text_public_intro_title')}}@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_intro_title" ><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif
    </h1>
    <p>{{ App\Config::getData('text_public_intro_desc')}}@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_intro_desc" ><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</p>
</div>
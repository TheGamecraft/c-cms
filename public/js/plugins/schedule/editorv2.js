var nbOfLevel = 0;
var nbOfPeriode = 0;
var eventType;
var editorMode = 'schedule';
function initEditor(event_id = 1, mode = 'schedule')
{
    let isLoad = $.Deferred();
    let editor = $('#editor');
    editorMode = mode;
    eventType = null;
    let eventTypePromise = null;
    if (mode == "schedule-edit") {
        eventTypePromise = $.ajax({
            type: 'GET',
            url: '/api/event/'+event_id+'?api_token='+api_token,
            success: function (template) {
                eventType = template;
            },
            error:  function () {
                showNotification('error','Impossible d\'initialiser l\'éditeur d\'horaire ...','top', 'center')
            }
        });
    } else {
        eventTypePromise = $.ajax({
            type: 'GET',
            url: '/api/eventType/'+event_id+'?api_token='+api_token,
            success: function (template) {
                eventType = template;
            },
            error:  function () {
                showNotification('error','Impossible d\'initialiser l\'éditeur d\'horaire ...','top', 'center')
            }
        });
    }

    $.when(eventTypePromise).done(function () {
        nbOfLevel = eventType['schedule_model']['niveaux'].length;
        nbOfPeriode = eventType['schedule_model']['periodes'].length;
        updateWidth();
        editor.html(getTemplate(eventType['schedule_model']));
        $('.richeditor').trumbowyg({
            lang: 'fr',
            btns: [
                ['viewHTML'],
                ['emoji'],
                ['undo', 'redo'], // Only supported in Blink browsers
                ['strong', 'em', 'del'],
                ['superscript', 'subscript'],
                ['fontfamily'],
                ['fontsize'],
                ['foreColor', 'backColor'],
                ['link'],
                ['insertImage'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen']
            ]
        });
        if (mode == 'eventType' || mode == 'schedule-add' || mode == "schedule-edit")
        {
            let scheduleModel = eventType['schedule_model'];
            if (scheduleModel['default_value'])
            {
                $.each(scheduleModel['niveaux'], function (level_id, level) {
                    $.each(scheduleModel['periodes'], function (periode_id, value) {
                        if (scheduleModel['default_value'][level_id,periode_id])
                        {
                            updateLevel(periode_id,level_id,scheduleModel['default_value'][periode_id][level_id]);
                        }
                    });
                });
            }
            if (mode != 'schedule-add')
            {
                $('.scheduleInput').removeAttr('required')
            }
        }
        if (mode == 'schedule-add')
        {
            $('.datetimepicker').datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                }
            });
        }
        initAutoComplete("AutoCompleteUser");
        initAutoCompleteOCOM('AutoCompleteOCOM');
        isLoad.resolve();
    });
    return isLoad.promise();
}

function updateWidth()
{
    $('#editor').css('width',(23*(nbOfLevel+1))+'rem');
}

function getTemplate(scheduleModel)
{
    let headerHTML = '';
    $.each(scheduleModel['niveaux'], function (index, value) {
        headerHTML = headerHTML+getHeader(index,value['name']);
    });
    let periodesHTML = '';
    $.each(scheduleModel['periodes'], function (index, value) {
        periodesHTML = periodesHTML+getPeriode(index,value,scheduleModel['niveaux']);
    });
    return '<div class="row">\n' +
        '    <div class="editor-header col p-3 border-right border-bottom bg-dark text-white">\n' +
        '        <b>\n' +
        '            Niveau/Periode\n' +
        '        </b>\n' +
        '    </div>\n' +
            headerHTML +
        '    <div class="col-1">\n' +
        '        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="addLevel()">\n' +
        '            <i class="material-icons">add</i>\n' +
        '        </a>\n' +
        '        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="removeLevel()">\n' +
        '            <i class="material-icons">remove</i>\n' +
        '        </a>\n' +
        '    </div>\n' +
        '</div>\n'+
        periodesHTML +
        '<div class="row">\n' +
        '    <div class="col p-2">\n' +
        '        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="addPeriode()">\n' +
        '            <i class="material-icons">add</i>\n' +
        '        </a>\n' +
        '        <a class="btn btn-primary btn-fab btn-fab-mini btn-round text-white" onclick="removePeriode()">\n' +
        '            <i class="material-icons">remove</i>\n' +
        '        </a>\n' +
        '    </div>\n' +
        '</div>\n';
}

function getHeader(level_id,level_name)
{
    return '<div id="levelHeader-'+level_id+'" class="col border-right border-bottom bg-dark text-white">\n' +
        '    <div class="row">\n' +
        '        <div class="col-9">\n' +
        '            <div class="form-group label-floating">\n' +
        '                <input type="text" placeholder="Niveau" name="level_name_'+(level_id+1)+'" class="form-control text-white" value="'+level_name+'" />\n' +
        '                <span class="form-control-feedback">\n' +
        '                <i class="material-icons">clear</i>\n' +
        '             </span>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="col-3 text-right">\n' +
        '            <a type="button" class="btn btn-link btn-sm dropdown-toggle dropdown-toggle-split text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
        '                <span class="sr-only">Toggle Dropdown</span>\n' +
        '            </a>\n' +
        '            <div class="dropdown-menu">\n' +
        '                <a id="modeSwitchPeriodeC'+level_id+'" class="btn-secondary dropdown-item m-1" onclick="selectCourseModeNiveau(\'course\','+level_id+')">Mode "Cours" pour toutes les périodes</a>\n' +
        '                <a id="modeSwitchPeriodeO'+level_id+'" class="btn-secondary dropdown-item m-1" onclick="selectCourseModeNiveau(\'other\','+level_id+')">Mode "Autre" pour toutes les période</a>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>'
}

function getPeriode(id,periode,niveaux) {
    let temp = '';

    $.each(niveaux, function (index, value) {
        temp = temp+getCourse(index,id);
    });

    return '<div class="row" id="row-'+id+'">\n' +
        '    <div class="col d-inline border-right border-bottom bg-light">\n' +
        '        <div class="row">\n' +
        '            <div class="col-9">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <input type="text" placeholder="Période" name="periode_name_'+(id+1)+'" class="form-control" value="'+periode['name']+'" />\n' +
        '                    <span class="form-control-feedback">\n' +
        '                            <i class="material-icons">clear</i>\n' +
        '                        </span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-3 text-right">\n' +
        '                <a type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
        '                    <span class="sr-only">Toggle Dropdown</span>\n' +
        '                </a>\n' +
        '                <div class="dropdown-menu">\n' +
        '                    <a id="modeSwitchPeriodeC'+id+'" class="btn-secondary dropdown-item m-1" onclick="selectCourseModePeriode(\'course\','+id+')">Mode "Cours" pour toute la période</a>\n' +
        '                    <a id="modeSwitchPeriodeO'+id+'" class="btn-secondary dropdown-item m-1" onclick="selectCourseModePeriode(\'other\','+id+')">Mode "Autre" pour toute la période</a>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="row">\n' +
        '            <div class="col-6">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <input type="time" class="form-control" name="periode_begin_time_'+(id+1)+'" value="'+periode['begin_time']+'" />\n' +
        '                    <span class="form-control-feedback"><i class="material-icons">clear</i></span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-6">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <input type="time" class="form-control" name="periode_end_time_'+(id+1)+'" value="'+periode['end_time']+'" />\n' +
        '                    <span class="form-control-feedback"><i class="material-icons">clear</i></span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        temp +
        '    <div class="col-1">\n' +
        '\n' +
        '    </div>\n' +
        '</div>'
}

function getCourse(level_id,periode_id)
{
    let isCourse = "";
    if (eventType['schedule_model']['default_value'])
    {
        if(eventType['schedule_model']['default_value'][periode_id][level_id]['use_course'] != null)
        {
            isCourse = "required";
        }
    }
    level_id++;
    periode_id++;
    return '<div id="container-'+level_id+'-'+periode_id+'" niveau="'+level_id+'" periode="'+periode_id+'" class="col m-0 border-bottom border-right scheduleEditor-course">\n' +
       '<input class="d-none" type="checkbox" id="use_course_n'+level_id+'_p'+periode_id+'" name="use_course_n'+level_id+'_p'+periode_id+'" checked>\n' +
        '<div class="row bg-light">\n' +
        '    <div class="col-8 pr-0 m-auto d-flex">\n' +
        '    </div>\n' +
        '    <div class="col-4 text-right">\n' +
        '        <a type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
        '            <span class="sr-only">Toggle Dropdown</span>\n' +
        '        </a>\n' +
        '        <div class="dropdown-menu">\n' +
        '            <a id="modeSwitchC'+level_id+'-'+periode_id+'" class="btn-secondary dropdown-item active m-1" onclick="selectCourseMode(\'course\','+level_id+','+periode_id+')">Mode "Cours" pour cette période</a>\n' +
        '            <a id="modeSwitchO'+level_id+'-'+periode_id+'" class="btn-secondary dropdown-item m-1" onclick="selectCourseMode(\'other\','+level_id+','+periode_id+')">Mode "Autre" pour cette période</a>\n' +
        '            <div class="dropdown-divider"></div>\n' +
        '            <a class="btn-secondary dropdown-item" href="#">Réinitialiser</a>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>\n' +
        '<div class="tab-content text-center">\n' +
        '    <div class="tab-pane active">\n' +
        '        <div class="row pt-2">\n' +
        '            <div class="col-12 d-none" id="descContainer'+level_id+'-'+periode_id+'">\n' +
        '                <div class="form-group">\n' +
        '                    <textarea class="form-control scheduleInput" id="desc_n'+level_id+'_p'+periode_id+'" name="desc_n'+level_id+'_p'+periode_id+'" rows="2" placeholder="Description de la période"></textarea>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-6 mb-1" id="OCOMContainer'+level_id+'-'+periode_id+'">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <div class="autocomplete">\n' +
        '                        <input type="text" placeholder="OCOM du cours" id="ocom_n'+level_id+'_p'+periode_id+'" name="ocom_n'+level_id+'_p'+periode_id+'" class="form-control AutoCompleteOCOM scheduleInput" aria-describedby="nameHelp" autocomplete="off" '+isCourse+' onchange="updateCourseName(\''+level_id+'\',\''+periode_id+'\')">\n' +
        '                    </div>\n' +
        '                    <span class="form-control-feedback">\n' +
        '                <i class="material-icons">done</i>\n' +
        '             </span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-6" id="nameContainer'+level_id+'-'+periode_id+'">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <input type="text" placeholder="Nom du cours" id="name_n'+level_id+'_p'+periode_id+'" name="name_n'+level_id+'_p'+periode_id+'" class="form-control scheduleInput" '+isCourse+' />\n' +
        '                    <span class="form-control-feedback">\n' +
        '                <i class="material-icons">clear</i>\n' +
        '             </span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-6 mb-1">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <input type="text" placeholder="Lieu du cours" id="location_n'+level_id+'_p'+periode_id+'" name="location_n'+level_id+'_p'+periode_id+'" class="form-control scheduleInput" '+isCourse+'/>\n' +
        '                    <span class="form-control-feedback">\n' +
        '                <i class="material-icons">done</i>\n' +
        '             </span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-6">\n' +
        '                <div class="form-group label-floating">\n' +
        '                    <div class="autocomplete">\n' +
        '                        <input type="text" placeholder="Nom de l\'instructeur" id="instruc_n'+level_id+'_p'+periode_id+'" name="instruc_n'+level_id+'_p'+periode_id+'" class="form-control AutoCompleteUser scheduleInput" aria-describedby="nameHelp" autocomplete="off" '+isCourse+'>\n' +
        '                    </div>\n' +
        '                    <span class="form-control-feedback">\n' +
        '                <i class="material-icons">done</i>\n' +
        '             </span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>\n'+
        '</div>';
}

function updateLevel(p,l,level)
{
    p++;
    l++;
    $('#desc_n'+l+'_p'+p).val(level['desc']);
    $('#ocom_n'+l+'_p'+p).val(level['ocom']);
    $('#name_n'+l+'_p'+p).val(level['name']);
    $('#location_n'+l+'_p'+p).val(level['location']);
    $('#instruc_n'+l+'_p'+p).val(level['instructor']);

    if (level['use_course'] != 'on')
    {
        selectCourseMode('autre',l,p);
    }
}

function removePeriode()
{
    $('#row-'+(nbOfPeriode-1)).remove();
    eventType['schedule_model']['periodes'].pop();
    nbOfPeriode--;
}

function removeLevel()
{
    $('#levelHeader-'+(nbOfLevel-1)).remove();
    for (let i = 0; i < nbOfPeriode; i++) {
        $('#container-'+(nbOfLevel)+'-'+(i+1)).remove();
    }
    eventType['schedule_model']['niveaux'].pop();
    updateWidth();
    nbOfLevel--;
}

function addLevel()
{
    $('#levelHeader-'+(nbOfLevel-1)).after(getHeader(nbOfLevel,'Niveau '+(nbOfLevel+1)));

    let scheduleModel = eventType['schedule_model'];
    $.each(scheduleModel['periodes'], function (periode_id, value) {
        console.log($('#container-'+(nbOfLevel)+'-'+periode_id));
        $('#container-'+(nbOfLevel)+'-'+(periode_id+1)).after(getCourse(nbOfLevel,periode_id));
    });

    eventType['schedule_model']['niveaux'].push({name:'Niveau '+(nbOfLevel+1)});
    updateWidth();
    nbOfLevel++;
    initAutoComplete("AutoCompleteUser");
    initAutoCompleteOCOM('AutoCompleteOCOM');
    if (editorMode == 'eventType')
    {
        $('.scheduleInput').removeAttr('required')
    }
}

function addPeriode()
{
    $('#row-'+(nbOfPeriode-1)).after(getPeriode(nbOfPeriode,{
        "name": "Periode "+nbOfPeriode,
        "begin_time": "",
        "end_time": ""
    },eventType['schedule_model']['niveaux']));

    eventType['schedule_model']['periodes'].push({
        "name": "Periode "+nbOfPeriode,
        "begin_time": "",
        "end_time": ""
    });
    nbOfPeriode++;
    initAutoComplete("AutoCompleteUser");
    initAutoCompleteOCOM('AutoCompleteOCOM');
    if (editorMode == 'eventType')
    {
        $('.scheduleInput').removeAttr('required')
    }
}

function selectCourseMode(mode, niveau, periode)
{
    if(mode == "course")
    {
        $('#descContainer'+niveau+'-'+periode).addClass('d-none');
        $('#isDoneContainer'+niveau+'-'+periode).removeClass('d-none');
        $('#isDoneContainer'+niveau+'-'+periode).addClass('d-flex');
        $('#OCOMContainer'+niveau+'-'+periode).removeClass('d-none');
        $('#nameContainer'+niveau+'-'+periode).removeClass('d-none');
        $('#ocom_n'+niveau+'_p'+periode).prop('required',true);
        $('#name_n'+niveau+'_p'+periode).prop('required',true);
        $('#instruc_n'+niveau+'_p'+periode).prop('required',true);
        $('#modeSwitchC'+niveau+'-'+periode).addClass('active');
        $('#modeSwitchO'+niveau+'-'+periode).removeClass('active');
        $('#use_course_n'+niveau+'_p'+periode).prop("checked", true);
    }
    else
    {
        $('#descContainer'+niveau+'-'+periode).removeClass('d-none');
        $('#isDoneContainer'+niveau+'-'+periode).addClass('d-none');
        $('#isDoneContainer'+niveau+'-'+periode).removeClass('d-flex');
        $('#OCOMContainer'+niveau+'-'+periode).addClass('d-none');
        $('#nameContainer'+niveau+'-'+periode).addClass('d-none');
        $('#ocom_n'+niveau+'_p'+periode).removeAttr('required');
        $('#name_n'+niveau+'_p'+periode).removeAttr('required');
        $('#instruc_n'+niveau+'_p'+periode).prop('required',true);
        $('#modeSwitchC'+niveau+'-'+periode).removeClass('active');
        $('#modeSwitchO'+niveau+'-'+periode).addClass('active');
        $('#use_course_n'+niveau+'_p'+periode).prop("checked", false);
    }
}

function selectCourseModePeriode(mode,periode)
{
    for (let i = 0; i < nbOfLevel; i++) {
        selectCourseMode(mode,i+1,periode+1);
    }
}

function selectCourseModeNiveau(mode,niveau)
{
    for (let i = 0; i < nbOfPeriode; i++) {
        selectCourseMode(mode,niveau+1,i+1);
    }
}

function updatePlantext(toggle,id)
{
    if($('#'+toggle).is(":checked"))
    {
        $('#'+id).removeClass('text-warning');
        $('#'+id).addClass('text-success');
        $('#'+id).html('Plan de cours remis')
    }
    else
    {
        $('#'+id).addClass('text-warning');
        $('#'+id).removeClass('text-success');
        $('#'+id).html('Plan de cours non remis')
    }
}

function updateCourseName(niveau, periode) {
    setTimeout(function(){

        let val = $('#ocom_n'+niveau+'_p'+periode).val();
        console.log(val);
        if(val != "")
        {
            $.ajax({
                type: 'GET',
                url: '/api/ocom/'+val+'/name?api_token='+api_token,
                success: function (data) {
                    if(data != null && data != "")
                    {
                        $('#name_n'+niveau+'_p'+periode).val(data);
                    }
                },
                error:  function () {
                    showNotification('error','Erreur impossible de charger l\'objectif de rendement ...','top', 'center')
                }
            })
        }
    }, 250);
}

function loadEventType(date,id = 1)
{
    if ($('#type'))
    {
        id = $('#type').val();
    }
    initEditor(id,'schedule-add').done(function () {

        if (eventType['is_mandatory'] == 1)
        {
            $('#is_mandatory').prop('checked',true);
        }
        else
        {
            $('#is_mandatory').removeAttr('checked');
        }

        if (eventType['use_schedule'] == 1)
        {
            $('#use_schedule').prop('checked',true);
            switchUseSchedule();
        }
        else
        {
            $('#use_schedule').removeAttr('checked');
            switchUseSchedule();
        }

        if (eventType['hidden'] == 1)
        {
            $('#hidden').prop('checked',true);
        }
        else
        {
            $('#hidden').removeAttr('checked');
        }

        if (eventType['use_weekly_msg'] == 1)
        {
            $('#use_weekly_msg').prop('checked',true);
            switchUseWeeklyMsg();
        }
        else
        {
            $('#use_weekly_msg').removeAttr('checked');
            switchUseWeeklyMsg();
        }
        let begin_time = $('#begin_time');
        begin_time.data("DateTimePicker").destroy();
        begin_time.datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            date: new Date(moment(date+" "+eventType['begin_time']))
        });
        let end_time = $('#end_time');
        end_time.data("DateTimePicker").destroy();
        end_time.datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            date: new Date(moment(date+" "+eventType['end_time']))
        });
        let weekly_msg_publication_time = $('#weekly_msg_publication_time');
        weekly_msg_publication_time.data("DateTimePicker").destroy();
        if (eventType['weekly_msg_publication_time'].match(/\d+/) != null || eventType['weekly_msg_publication_time'].match(/\d+/) != null)
        {
            weekly_msg_publication_time.datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                },
                date: new Date(moment(date+" "+eventType['begin_time']).subtract(
                    eventType['weekly_msg_publication_time'].match(/\d+/)[0],
                    eventType['weekly_msg_publication_time'].match(/[a-z]+/)[0]
                ))
            });
        }
        else
        {
            weekly_msg_publication_time.datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                },
                date: new Date(moment(date+" "+eventType['begin_time']).subtract(1,'days'))
            });
        }
        $('#location').val(eventType['location']);
        $('#name').val(eventType['name']);
        $('#admin_desc').trumbowyg('html', eventType['admin_desc']);
        $('#calendar_color').val(eventType['calendar_color']);
        pickr.setColor(eventType['calendar_color']);
        $('#calendar_icon').val(eventType['calendar_icon']);
        $('#calendar_icon_display').removeAttr('class');
        $('#calendar_icon_display').addClass(eventType['calendar_icon']);
    })
}

function loadEvent(date,id)
{
    initEditor(id,'schedule-edit').done(function () {

        if (eventType['is_mandatory'] == 1)
        {
            $('#is_mandatory').prop('checked',true);
        }
        else
        {
            $('#is_mandatory').removeAttr('checked');
        }

        if (eventType['use_schedule'] == 1)
        {
            $('#use_schedule').prop('checked',true);
            switchUseSchedule();
        }
        else
        {
            $('#use_schedule').removeAttr('checked');
            switchUseSchedule();
        }

        if (eventType['hidden'] == 1)
        {
            $('#hidden').prop('checked',true);
        }
        else
        {
            $('#hidden').removeAttr('checked');
        }

        if (eventType['use_weekly_msg'] == 1)
        {
            $('#use_weekly_msg').prop('checked',true);
            switchUseWeeklyMsg();
        }
        else
        {
            $('#use_weekly_msg').removeAttr('checked');
            switchUseWeeklyMsg();
        }
        let begin_time = $('#begin_time');
        begin_time.datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            date: new Date(moment(eventType['date_begin'],"MM/DD/YYYY HH:mm A"))
        });
        let end_time = $('#end_time');
        end_time.datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            date: new Date(moment(eventType['date_end'],"MM/DD/YYYY HH:mm A"))
        });
        let weekly_msg_publication_time = $('#weekly_msg_publication_time');
        if (eventType['use_schedule'])
        {
            weekly_msg_publication_time.datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                },
                date: new Date(moment(eventType['weekly_msg_publication_time'],"MM/DD/YYYY HH:mm A"))
            });
        }
        else
        {
            weekly_msg_publication_time.datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                },
                date: new Date(moment(eventType['date_begin'],"MM/DD/YYYY HH:mm A").subtract(1,'days'))
            });
        }
        $('#location').val(eventType['location']);
        $('#name').val(eventType['name']);
        $('#admin_desc').trumbowyg('html', eventType['desc']);
        $('#calendar_color').val(eventType['calendar_color']);
        pickr.setColor(eventType['calendar_color']);
        $('#calendar_icon').val(eventType['calendar_icon']);
        $('#calendar_icon_display').removeAttr('class');
        $('#calendar_icon_display').addClass(eventType['calendar_icon']);
    })
}

function switchUseSchedule()
{
    if($('#use_schedule').is(":checked"))
    {
        $('#collschedule').removeClass('d-none');
        $('.scheduleInput').removeAttr('disabled')
    }
    else
    {
        $('#collschedule').addClass('d-none');
        $('.scheduleInput').prop('disabled',true)
    }
}

function switchUseWeeklyMsg()
{
    if($('#use_weekly_msg').is(":checked"))
    {
        $('#collmessagedelasemaine').removeClass('d-none');
    }
    else
    {
        $('#collmessagedelasemaine').addClass('d-none');
    }
}

function removeFile(file) {
    document.getElementById("removedfile").setAttribute("value",document.getElementById("removedfile").getAttribute("value")+"|"+file);
    document.getElementById(file).remove();
    console.log(file);
}
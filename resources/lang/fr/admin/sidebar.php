<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Siderbar Language Lines
    |--------------------------------------------------------------------------
    */

    'dashboard' => "Tableau de bord",
    'ecc_title' => "Espace Cadet-Cadre",
    'msg_title' => "Messages",
    'msg_list' => "Liste des messages",
    'msg_add' => "Ajouter un message",
    'msg_edit' => "Modifier un message",
    'msg_delete' => "Supprimer un message",
    'blueprint' => "Plan de cour",
    'blueprint_list' => "Liste des plans de cours",
    'admin_title' => "Administration",
    'public_page_title' => "Page Publique",
    'public_page_see' => "Voir la page publique",
    'public_page_edit' => "Modifier la page publique",
    'public_page_add_img' => "Ajouter une image",
    'public_page_list_article' => "Liste des articles",
    'public_page_add_article' => "Ajouter un article",
    'public_page_edit_article' => "Modifier un article",
    'public_page_delete_article' => "Supprimer un article",
    'calendar_title' => "Horaire",
    'calendar_display' => "Afficher l'horaire",
    'services_title' => "Services",
    'stats_title' => "Statistiques",
    'users_title' => "Utilisateurs",
    'tools_title' => "Outils",
    'update' => "Mise a jour",
    'bug' => "Signaler un bug",
];

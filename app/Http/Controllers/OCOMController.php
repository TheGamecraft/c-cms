<?php

namespace App\Http\Controllers;

use App\OCOM;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use function GuzzleHttp\json_encode;
use function GuzzleHttp\Psr7\str;

class OCOMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        OCOM::wasUpdateGivenAll();
        return view('admin.ocom.index',['ocoms' => OCOM::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ocom.add');
    }

    public function generate()
    {
        $file = fopen(\request()->file('file'),'r');
        $data = fread($file,filesize(\request()->file('file')));
        fclose($file);
        $data = str_replace(["\r","\n"],'',$data);

        // Detection et nettoyage des ORENs
        preg_match_all('/[MX\d][X\d]\d+,[^,]+/',$data,$matches_orens);
        foreach ($matches_orens[0] as $key => $oren) {
            $oren = str_replace('"','',$oren);
            $matches_orens[0][$key] = $oren;
        }
        $orens_pass_2 = collect($matches_orens[0])->unique();
        $orens = [];
        foreach ($orens_pass_2 as $oren)
        {
            $foo = explode(',',$oren);
            $orens[$foo[0]] = $foo[1];
        }

        // Detection et nettoyage des OCOMs
        preg_match_all('/[MC][X\d]\d{2}\.\d{2}\w?,[^,]+,\d/',$data,$matches_ocom);
        foreach ($matches_ocom[0] as $key => $ocom) {
            $ocom = str_replace('"','',$ocom);
            $matches_ocom[0][$key] = $ocom;
        }
        $ocoms_pass_2 = collect($matches_ocom[0])->unique();

        foreach ($ocoms_pass_2 as $ocom_pass_2)
        {
            $foo = explode(',',$ocom_pass_2);

            if (OCOM::findByOCOM($foo[0]) == null)
            {
                preg_match('/[X\d]\d{2}/',$ocom_pass_2,$matches);
                $oren = $matches[0];
                $complementary = false;
                if ($foo[0][0] == "C")
                {
                    $complementary = true;
                }
                $nocom = new OCOM();
                $nocom->ocom = $foo[0];
                $nocom->objectif_competence = $foo[1];
                $nocom->nbPeriode = $foo[2];
                $nocom->objectif_rendement = $orens[$oren];
                $nocom->oren = $oren;
                $nocom->complementary = $complementary;
                $nocom->course_id = '';

                $nocom->save();
            }
        }

        $this->updateOCOMDB();
        return redirect('/admin/ocom')->with('success','Base de donnée des cours générée avec succès!');
    }

    public function g()
    {
        OCOM::truncate();

        $matches = [];
        $orens = [];
        preg_match_all('/[X\d]\d\d,[^,]*/',\request('text'),$matches);
        foreach ($matches[0] as $match)
        {
            $match = str_replace('"',"",$match);
            $match = trim(preg_replace('/\s\s+/', ' ', $match));
            $oren = "";
            preg_match('/[X\d]\d\d/',$match,$oren);
            $value = preg_replace('/[X\d]\d\d,/','',$match);
            if($value != "")
            {
                $orens[$oren[0]] = preg_replace('/[X\d]\d\d,/','',$match);
            }
        }

        preg_match_all('/[MC]\S\d\d.\d\d[A-Z]?,[^,]*,\d/',$text,$matches);

        foreach ($matches[0] as $match)
        {
            $newocom = new OCOM();

            $match = str_replace('"',"",$match);

            $ocom = "";
            preg_match('/[MC]\S\d\d.\d\d[A-Z]?/',$match,$ocom);

            if(!OCOM::where('ocom',$ocom)->first())
            {
                $oren = "";
                preg_match('/[X\d]\d\d/',$ocom[0],$oren);

                $obj_competence = str_replace($ocom[0].",","",$match);
                $obj_competence = trim(preg_replace('/\s\s+/', ' ', $obj_competence));
                $obj_competence = trim($obj_competence,"\\");

                $nbPeriode = preg_replace('/[MC]\S\d\d.\d\d[A-Z]?,[^,]*,/','',$match);
                $newocom->nbPeriode = $nbPeriode;

                $obj_competence = str_replace(",".$nbPeriode,'',$obj_competence);

                $newocom->ocom = $ocom[0];
                $newocom->oren = $oren[0];

                if ($newocom->ocom[0] == 'C' || $newocom->ocom[0] == 'c')
                {
                    $newocom->complementary = true;
                }
                else
                {
                    $newocom->complementary = false;
                }

                $newocom->course_id = '';

                $newocom->objectif_competence = $obj_competence;
                $newocom->objectif_rendement = $orens[$oren[0]];
                $newocom->objectif_rendement = trim($newocom->objectif_rendement,"\\");
                $newocom->save();
            }
        }
        return redirect('/admin/ocom')->with('success','Base de donnée des cours générée avec succès!');
    }

    public function showgenerate()
    {
        return view('admin.ocom.generate');
    }

    public function updateOCOMDB()
    {
        $ocoms = OCOM::all();

        $from = strtotime(\App\Config::getData('instruction_year_begin'));
        $to = strtotime(\App\Config::getData('instruction_year_end'));
        $allEvent = \App\Event::all();
        $events = collect();

        foreach ($allEvent as $e)
        {
            if ($e->use_schedule == 1)
            {
                if (strtotime($e->date_begin) >= $from)
                {
                    if (strtotime($e->date_begin) <= $to)
                    {
                        $events->push($e);
                        foreach ($e->courses as $c)
                        {
                            $r = $ocoms->where('ocom',$c->ocom)->first();
                            if ($r != null)
                            {
                                $r->saveCourse($c);
                            }
                        }
                    }
                }
            }
        }
        foreach ($ocoms as $o)
        {
            $o->save();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ocom = new OCOM();
        $ocom->ocom = $request->ocom;
        $ocom->objectif_competence = $request->objectif_competence;
        $ocom->nbPeriode = $request->nbPeriode;
        $ocom->objectif_rendement = $request->objectif_rendement;
        $ocom->oren = $request->oren;

        if ($ocom->ocom[0] == 'C' || $ocom->ocom[0] == 'c')
        {
            $ocom->complementary = true;
        }
        else
        {
            $ocom->complementary = false;
        }
        $ocom->course_id = "";
        $ocom->save();
        clog('add','success','a ajouter un ocom',\Auth::User()->id);
        return redirect('/admin/ocom/'.$ocom->id);
    }

    /**
     * Display the specified resource.
     *
     * @param $OCOM
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($OCOM)
    {
        return view('admin.ocom.show',['ocom' => OCOM::find($OCOM)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.ocom.edit',['ocom' => OCOM::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request,$id)
    {
        $ocom = OCOM::find($id);
        $ocom->ocom = $request->ocom;
        $ocom->objectif_competence = $request->objectif_competence;
        $ocom->nbPeriode = $request->nbPeriode;
        $ocom->objectif_rendement = $request->objectif_rendement;
        $ocom->oren = $request->oren;

        if ($ocom->ocom[0] == 'C' || $ocom->ocom[0] == 'c')
        {
            $ocom->complementary = true;
        }
        else
        {
            $ocom->complementary = false;
        }

        $ocom->save();
        clog('edit','success','a modifié un ocom',\Auth::User()->id);
        return redirect('/admin/ocom/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param OCOM $oCOM
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OCOM::find($id)->delete();
    }

    public function jsonList()
    {
        $ocoms = OCOM::all();

        $name = [];

        foreach ($ocoms as $ocom) {
            array_push($name, $ocom->ocom);
        }

        return json_encode($name);
    }

    public function getName(string $ocom)
    {
        $foo = OCOM::all()->where('ocom','=',$ocom)->first();
        if($foo != null)
        {
            return $foo->objectif_competence;
        }
        else
        {
            return null;
        }
    }
}

@extends('layouts.public.main')

@section('content')
    <div class="page-header header-filter clear-filter" data-parallax="true" style="background-image: url({{'"'.\App\Config::getData('public_index_img_url').'"'}});">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <div class="brand">
                        <h2>{{ App\Config::getData('text_public_news_title')}}</h2>
                        <p>{{ App\Config::getData('text_public_news_desc')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            <div class="section">
                @if ($news->isEmpty())
                    <h4 class="text-center">Aucune nouvelles</h4>
                @endif
                <div class="row">
                    @foreach ($news as $new)
                        <div class="col-md-4">
                            <h3 class="news-title" >{{ $new->title }}</h3>
                            <div class="news-body-small">
                                {!! $new->body !!}
                            </div>
                            <span class="news-small">@if(\App\User::find($new->user_id)){{ \App\User::find($new->user_id)->fullname()}},@endif {{ $new->created_at }}</span>
                            <div class="news-tags">
                                @if($new->tags != [])
                                    @foreach($new->tags as $tag)
                                        @if($tag == "Important")
                                            <span class="badge badge-pill badge-danger">{{$tag}}</span>
                                        @else
                                            <span class="badge badge-pill badge-default">{{$tag}}</span>
                                        @endif
                                    @endforeach
                                @endif
                                    @if($new->files != [""])
                                        <span class="badge badge-pill badge-secondary">Fichier joint</span>
                                    @endif
                            </div>
                            <a name="news" id="news" class="btn btn-block btn-secondary mt-2" role="button"
                                @if(isset($new->event_id))
                                    href="/news/{{$new->event_id}}?type=msg" role="button">Voir plus!
                                @else
                                    href="/news/{{$new->id}}" role="button">Voir plus!
                                @endif
                                </a>
                        </div>
                    @endforeach
                </div>
                <div class="m-3" role="group">
                    {{ $news->links() }}
                </div>
                <a href="/" class="btn button primary">Retour</a>
        </div>
    </div>
@endsection
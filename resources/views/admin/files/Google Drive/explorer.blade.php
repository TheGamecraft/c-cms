<table class="table table-hover table-responsive dt-responsive material-datatables w-100 d-sm-table" id="table">
    <thead class="thead-dark">
    <tr>
        <td class="text-center explorerType"><strong>#</strong></td>
        <td class="text-left">Nom</td>
        <td class="text-center">Dernière modification</td>
        <td class="td-actions text-right">
            @if($permission['p'])
                <div class="dropdown">
                    <div id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-2x ml-3 text-gray" style="margin-right: .8rem !important;cursor: pointer;margin-top: -10px;margin-bottom: -6px"></i>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/admin/drive/{{$currentDir}}/permission">
                            <i class="fas fa-lock mr-2"></i></i>Permission
                        </a>
                        <a class="dropdown-item text-danger" onclick="deleteFolder('{{ $currentDir }}')">
                            <i class="fas fa-trash-alt mr-2"></i>Supprimer
                        </a>
                    </div>
                </div>
            @else
                <i class="fas fa-lock text-danger mr-2" data-toggle="tooltip" data-placement="left" title="Vous n'avez pas les permissions nécessaires pour modifier ce dossier"></i>
            @endif
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($directories as $directory)
        @php
            $read = \App\GoogleDriveFile::getPermForAuthUser($directory['basename'],'r');
            $manage = \App\GoogleDriveFile::getPermForAuthUser($directory['basename'],'p');
        @endphp
        <tr class="context-menu-one">
            @if($read)
                <td onclick="loadFolder('{{$directory['basename']}}')" style="cursor: pointer;" >
                    <i class="fas fa-folder fa-2x"></i>
                </td>
            @else
                <td>
                    <i class="fas fa-folder fa-2x text-muted"></i>
                </td>
            @endif
            <td @if($read) onclick="loadFolder('{{$directory['basename']}}')" style="cursor: pointer;"  @else class="text-muted" @endif>{{$directory['name']}}</td>
            <td @if($read) class="text-center"  onclick="loadFolder('{{$directory['basename']}}')" style="cursor: pointer;"  @else class="text-center text-muted"  @endif ><span data-toggle="tooltip" data-placement="bottom" title="{{date('r',$directory['timestamp'])}}">{{strftime('%e %b %Y',$directory['timestamp'])}}</span></td>
            <td class="td-actions text-right">
                @if($manage)
                    <div class="dropdown">
                        <div id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-2x ml-3 text-gray" style="margin-right: .8rem !important;cursor: pointer;"></i>
                        </div>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/admin/drive/{{$directory['basename']}}/permission">
                                <i class="fas fa-lock mr-2"></i>Permission
                            </a>
                            <a class="dropdown-item text-danger" onclick="deleteFolder('{{ $directory['basename'] }}')">
                                <i class="fas fa-trash-alt mr-2"></i>Supprimer
                            </a>
                        </div>
                    </div>
                @else
                    @if(!$read)
                        <i class="fas fa-lock text-danger mr-2" data-toggle="tooltip" data-placement="left" title="Vous n'avez pas les permissions nécessaires pour modifier ce dossier"></i>
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
    @foreach($files as $file)
        <tr onclick="showfile('{{$file['basename']}}')" style="cursor: pointer;">
            <td><i class="{{ \App\GoogleDriveFile::icon($file['extension']) }} fa-2x"></i></td>
            <td>{{$file['name']}}</td>
            <td class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="{{date('r',$file['timestamp'])}}">{{strftime('%e %b %Y',$file['timestamp'])}}</span></td>
            <td class="td-actions text-right">
                <i id="fa-{{$file['basename']}}" class="fas fa-angle-down fa-2x pr-2 text-gray"></i>
            </td>
        </tr>
        <tr id="{{$file['basename']}}" class="d-none">
            <td style="border-top: none" colspan="4">
                <div class="row">
                    <div class="col-md-2">
                        Type : {{$file['type']}} / {{$file['extension']}}
                    </div>
                    <div class="col-md-2">
                        Taille : {{GetSizeName($file['size'])}}
                    </div>
                    <div class="col-md-4">
                        Permission : rw
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="/file/get?f={{urlencode($file['name'])}}&d={{$file['dirname']}}" rel="tooltip" class="btn btn-info">
                            <i class="material-icons">cloud_download</i>
                        </a>
                        <button onclick="deleteFile('{{$file['name']}}','{{$file['dirname']}}')" rel="tooltip" class="btn btn-danger text-white">
                            <i class="material-icons">delete</i>
                        </button>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    @if(count($directories) == 0 && count($files) == 0)
        <tr>
            <td colspan="4" class="text-center m-2">
                Le dossier est vide
            </td>
        </tr>
    @endif
    </tbody>
</table>
<small class="float-right mr-4">Dernière mise à jour {{date('r')}}</small>
<script>
    updateHeader('{{$permission['p']}}');
</script>
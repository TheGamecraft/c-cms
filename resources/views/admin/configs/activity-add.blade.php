@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Ajouter une acitivité
        </div>
        <div class="card-body">
                <form method="POST" action="/admin/config/activity/add">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nom de l'activite</label>
                                <input type="text"
                                       class="form-control" name="name" id="name" aria-describedby="helpName" placeholder="Soirée d'instruction réguliere" required>
                                <small id="helpName" class="form-text text-muted">Veuillez entrer le nom de l'activité</small>
                            </div>
                        </div>
                        <div class=" col-md-3">
                            <div class="form-group">
                                <label for="calendar_color">Couleur dans le calendrier</label>
                                <input type="text"
                                       class="form-control" name="calendar_color" id="calendar_color" aria-describedby="helpcalendar_color" placeholder="#000000 ou red" required>
                                <small id="helpcalendar_color" class="form-text text-muted">Nom de la couleur en anglais ou valeur HTML</small>
                            </div>
                        </div>
                        <div class=" col-md-3">
                            <div class="form-group">
                                <label for="calendar_icon">Icone dans le calendrier</label>
                                <input type="text"
                                       class="form-control" name="calendar_icon" id="calendar_icon" aria-describedby="helpcalendar_icon" placeholder='<i class="fa fa-book" aria-hidden="true"></i>' required>
                                <small id="helpcalendar_icon" class="form-text text-muted">Balise complete de l'icon Font-Awesome</small>
                            </div>
                        </div>
                        <div class=" col-md-3">
                            <div class="form-group">
                                <div class="col col-md-8">
                                    <label for="text-input" class=" form-control-label">Obligatoire par défaut</label>
                                    <small class="form-text text-muted">L'activitée est t-elle obligatoire par défaut lors de la création d'activité dans l'horaire</small>
                                </div>
                                <div class=" col-md-4">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;">
                                        <input id="is_mandatory" name="is_mandatory" class="switch-input" type="checkbox">
                                        <span class="switch-label"></span><span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-3">
                            <div class="form-group">
                                <div class="col col-md-8">
                                    <label for="text-input" class=" form-control-label">Promu sur la page d'accueil</label>
                                    <small class="form-text text-muted">L'activitée doit-elle être visible sur la page d'accueil</small>
                                </div>
                                <div class="col col-md-4">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;">
                                        <input id="is_promoted" name="is_promoted" class="switch-input" type="checkbox">
                                        <span class="switch-label"></span><span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-3">
                            <div class="form-group">
                                <label for="begin_time">Heure de début par défaut</label>
                                <input type="time"
                                       class="form-control" name="begin_time" id="begin_time" aria-describedby="helpName" required>
                                <small id="helpName" class="form-text text-muted">Heure de début par défaut lors de la création d'activité dans l'horaire</small>
                            </div>
                        </div>
                        <div class=" col-md-3">
                            <div class="form-group">
                                <label for="end_time">Heure de fin par défaut</label>
                                <input type="time"
                                       class="form-control" name="end_time" id="end_time" aria-describedby="helpName" required>
                                <small id="helpName" class="form-text text-muted">Heure de fin par défaut lors de la création d'activité dans l'horaire</small>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <label for="location">Emplacement par défaut</label>
                                <input type="text"
                                       class="form-control" name="location" id="location" aria-describedby="helpName" placeholder="Escadron" required>
                                <small id="helpName" class="form-text text-muted">Emplacement par défaut de l'activité</small>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <label for="admin_desc">Description par défaut sur le calendrier</label>
                                <textarea type="text"
                                          class="form-control" name="admin_desc" id="admin_desc" aria-describedby="helpName" placeholder='Veuillez modifier la description admin par défaut' required></textarea>
                                <small id="helpName" class="form-text text-muted">Description par défaut lors de la création d'activité dans l'horaire</small>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <a href="/admin/config/activity" class="btn btn-secondary">Annuler</a>
                                <button type="submit" class="btn btn-primary">Sauvegarder</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Configuration Activitées</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Configuration/Activitées/Ajouter</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
@endsection

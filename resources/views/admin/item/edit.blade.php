@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Ajouter un item à l'inventaire</h4>
            </div>
            <div class="card-body mt-5">
                <form action="/admin/item/edit/{{$item->id}}" method="post">

                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Numéro d'article</label>
                                <input name="official_number" type="text" class="form-control" value="{{$item->official_number}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nom de l'article</label>
                                <input name="name" type="text" class="form-control" value="{{$item->name}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Taille de l'article</label>
                                <input name="metadata-size" type="text" class="form-control" value="{{$item->metadata['size']}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Quantité disponible</label>
                                <input name="quantity" type="number" class="form-control" value="{{$item->quantity}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Catégorie</label>
                                <select name="category_id" class="form-control selectpicker" required>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"
                                        @if($item->category_id = $category->id) selected @endif>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-4">
                            <label for="desc">Description de l'image</label>
                            <div class="form-group">
                                <textarea name="desc" id="desc" class="form-control" required>{!! $item->desc !!}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('custom_scripts')
    <script>
        $('#desc').trumbowyg({
            lang: 'fr'
        });
        function saveChange(pPerm) {
            (function($) {
                var myswitch = document.getElementById(pPerm);
                $.post('/api/config/general/save?api_token='+api_token, { value: myswitch.checked,perm: pPerm } , function(data) {
                    swal({
                        title: 'Modification enregistré !',
                        type: 'success',
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                });
            })(jQuery);
        }
    </script>
@endsection

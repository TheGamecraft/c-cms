@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong>Modification d'une activité</strong>
        </div>
        <div class="card-body">
            <form method="POST" action="/admin/config/activity/edit/{{$activity->id}}">
                <div class="row">
                    @csrf
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="name">Nom de l'activite</label>
                            <input type="text"
                            class="form-control" name="name" id="name" aria-describedby="helpName" placeholder="Soirée d'instruction réguliere" value="{{$activity->name}}">
                            <small id="helpName" class="form-text text-muted">Veuillez entrer le nom de l'activité</small>
                        </div>    
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="calendar_color">Couleur dans le calendrier</label>
                            <input type="text"
                            class="form-control" name="calendar_color" id="calendar_color" aria-describedby="helpcalendar_color" placeholder="#000000 ou red" value="{{$activity->calendar_color}}"">
                            <small id="helpcalendar_color" class="form-text text-muted">Nom de la couleur en anglais ou <a href="https://htmlcolorcodes.com/fr/">valeur HTML</a></small>
                        </div>    
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="calendar_icon">Icone dans le calendrier</label>
                            <input type="text"
                            class="form-control" name="calendar_icon" id="calendar_icon" aria-describedby="helpcalendar_icon" placeholder='<i class="fa fa-book" aria-hidden="true"></i>
                            ' value="{{$activity->calendar_icon}}">
                            <small id="helpcalendar_icon" class="form-text text-muted">Balise complete de l'icon <a href="https://fontawesome.com/v4.7.0/"> Font-Awesome</a></small>
                        </div>    
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="location">Emplacement par défaut</label>
                            <input type="text"
                            class="form-control" name="location" id="location" aria-describedby="helpName" placeholder="Soirée d'instruction réguliere" value="{{$activity->location}}">
                            <small id="helpName" class="form-text text-muted">Emplacement par défaut de l'activité</small>
                        </div>    
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="begin_time">Heure de début par défaut</label>
                            <input type="time"
                            class="form-control" name="begin_time" id="begin_time" aria-describedby="helpName" value="{{$activity->begin_time}}">
                            <small id="helpName" class="form-text text-muted">Heure de début par défaut lors de la création d'activité dans l'horaire</small>
                        </div>    
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="end_time">Heure de fin par défaut</label>
                            <input type="time"
                            class="form-control" name="end_time" id="end_time" aria-describedby="helpName" value="{{$activity->end_time}}">
                            <small id="helpName" class="form-text text-muted">Heure de fin par défaut lors de la création d'activité dans l'horaire</small>
                        </div>    
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="is_mandatory" type="checkbox" value="true" @if($activity->is_mandatory == 1) checked @endif>
                                    L'activitée est t-elle obligatoire par défaut lors de la création d'activité dans l'horaire
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="is_promoted" type="checkbox" value="true" @if($activity->is_promoted == 1) checked @endif>
                                    L'activitée doit-elle être visible sur la page d'accueil
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-12 mt-3" >
                        <div class="form-group">
                            <label for="admin_desc">Description par défaut sur le calendrier</label>
                            <textarea type="text"
                            class="form-control" name="admin_desc" id="admin_desc" aria-describedby="helpName" placeholder='Veuillez modifier la description admin par défaut'>{{$activity->admin_desc}}</textarea>
                            <small id="helpName" class="form-text text-muted">Description par défaut lors de la création d'activité dans l'horaire</small>
                        </div>    
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <a href="/admin/config/activity" class="btn btn-secondary">Annuler</a>
                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>    
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Configuration Activitées</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Configuration/Activitées/{{$activity->name}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script>
    function saveChange(pPerm) {
        (function($) {
		var myswitch = document.getElementById(pPerm);
		$.post('/api/config/general/save?api_token='+api_token, { value: myswitch.checked,perm: pPerm } , function(data) {
            swal({
            title: 'Modification enregistré !',
            type: 'success',
            }).then((result) => {
            if (result.value) {
                location.reload();
            }
            })
          });
	    })(jQuery);
    }
</script>
@endsection

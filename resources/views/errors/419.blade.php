@extends('errors.layout')

@section('title', __('Page expiré'))
@section('code')
    <span>4</span><span>1</span><span>9</span>
@endsection
@section('message', 'Nous sommes désolé, la page a expiré.')
@section('error',$exception->getMessage())
@section('url','')

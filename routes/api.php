<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/schedule/events','ScheduleController@apiIndex');
Route::get('/schedule/events/modal/{id}/{db_type}','ScheduleController@loadModal');

Route::middleware('auth:api')->group(function () {

    Route::get('/schedule/events/auth','ScheduleController@apiIndex');
    Route::get('/schedule/events/modal/full/{id}/{db_type}','ScheduleController@loadModalFull')->middleware('perm:schedule_see');
    Route::get('/schedule/events/add/modal/{type}/{date}','ScheduleController@loadModalDefautType')->middleware('perm:schedule_add');
    Route::get('/schedule/editor/init/{id}','ScheduleEditorController@getTemplate')->middleware('perm:schedule_edit');
    Route::get('/schedule/editor/course/{niveau}/{periode}/{event_type?}','ScheduleEditorController@getCourseEmpty')->middleware('perm:schedule_edit');
    Route::get('/schedule/editor/template/{id}','ScheduleEditorController@getEventTemplate')->middleware('perm:schedule_add');
    Route::get('/schedule/editor/emptyperiode/{id}','ScheduleEditorController@getEmptyPeriode')->middleware('perm:schedule_add');
    Route::get('/schedule/editor/emptylevel/{id}','ScheduleEditorController@getEmptyLevel')->middleware('perm:schedule_add');
    Route::get('/schedule/editor/levelHeader/{id}','ScheduleEditorController@getLevelHeader')->middleware('perm:schedule_add');
    Route::post('/schedule/event/delete/{id}','ScheduleController@delete')->middleware('perm:schedule_delete');

    /** Booking */
    Route::get('/booking/modal/item/{id}/{event_type}/{event_id}','BookingController@modalItem')->middleware('perm:booking_see');
    Route::get('/booking/modal/edit/item/{id}/{event_type}/{event_id}','BookingController@modalItemEdit')->middleware('perm:booking_edit');
    Route::post('/booking/delete','BookingController@destroy')->middleware('perm:booking_delete');
    /** Picture */
    Route::post('/picture/delete/{id}','PictureController@destroy')->middleware('perm:picture_delete');

    /** Event Type */
    Route::get('/eventType/{id}','EventTypeController@toJson');
    Route::delete('/eventType/{id}','EventTypeController@destroy');

    /** Event */
    Route::get('/event/{id}','EventController@toJson');

    /** Message Route */
    Route::post('/message/delete', 'MessageController@destroy')->middleware('perm:msg_delete');

    /** News Route */
    Route::post('/news/delete','NewsController@destroy')->middleware('perm:news_delete');

    /** User Route */
    Route::post('/user/delete', 'UserController@destroy')->middleware('perm:user_delete');
    Route::get('/user/list', 'UserController@apiList')->middleware('perm:user_see');

    /** OCOM Route */
    Route::get('/ocom/list', 'OCOMController@jsonList')->middleware('perm:schedule_see');
    Route::get('/ocom/{id}/name','OCOMController@getName')->middleware('perm:schedule_see');

    /** Inventory Route */
    Route::post('/item/delete', 'ItemController@destroy')->middleware('perm:inventory_delete');

    /** Item Category */
    Route::post('/itemcategory/delete/{id}','ItemCategoryController@destroy')->middleware('perm:inventory_edit');

    /** Config Route */
    Route::post('/config/general/save', 'ConfigController@update')->middleware('perm:config_edit');
    Route::post('/config/activity/delete', 'ComplementaryActivityController@destroy')->middleware('perm:config_delete');

    /** Rank Route */
    Route::post('/config/rank/delete','RankController@destroy')->middleware('perm:config_edit');

    /** Job Route */
    Route::post('/config/job/delete','JobController@destroy')->middleware('perm:config_edit');

    /** Course Route */
    Route::post('/course/{id}/plan/validate','CourseController@validatePlan')->middleware('courseperm:validate_plan');;

    /** File Exlorer Route */
    Route::get('/drive/{folder}/permission/{subject}/{id}','GoogleDriveController@editPermissionModal')->middleware('perm:file_see');
    Route::get('/drive/{folder}/addpermission/{subject}','GoogleDriveController@addPermissionModal')->middleware('perm:file_see');
    Route::get('/drive/folders/{folder?}','GoogleDriveController@list')->middleware('fileperm:folder,r');
    Route::get('/drive/path/{folder}','GoogleDriveController@getPath')->middleware('fileperm:folder,r');
    Route::get('/drive/patharray','GoogleDriveController@getPathArray')->middleware('perm:file_see');
    Route::get('/drive/files/{dir}/{file}','GoogleDriveController@showMetadata')->middleware('fileperm:folder,r');
    Route::get('/drive/checkfilestructure','GoogleDriveController@checkFileSystem')->middleware('perm:config_edit');;

    Route::get('/user/perm/{id}', function($id) {
        $user = \App\User::find($id);
        return $user->getPerm("schedule_edit");
    })->middleware('perm:user_see');

    Route::delete('/ocom/{id}','OCOMController@destroy');

    Route::post('/notification/{id}/read','NotificationController@markAsRead');
    Route::get('/notification/allRead','NotificationController@markAllAsRead');

});
Route::get('/admin/ping', function(){
    $users = \App\User::all();
    $schedules = \App\Schedule::all();
    $oups = false;

    foreach ($users as $user) {
        if($user == null){
            $oups = true;
        }
    }
    foreach ($schedules as $schedule) {
        if($schedule == null){
            $oups = true;
        }
    }
    
    if ($oups) {
        return "oups";
    } else {
        return "pong";
    }
});

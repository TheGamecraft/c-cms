@extends('layouts.admin.main')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Modification de mon adresse <i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Félicitation</span>
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
            <form autocomplete="off" action="/admin/profil/edit/adress" method="POST">
                @csrf
                <div class="form-group">
                  <label for="adress">Entrer votre nouvelle adresse</label>
                  <input type="text" class="form-control" name="adress" id="adress" placeholder="17 ave Pico">
                </div>
                <button type="submit" id="submit" class="btn btn-primary">Enregistrer</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Profil d'utilisateur</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Utilisateur/Profil/Adresse</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script src="/assets/admin/assets/js/user.js"></script>
@endsection

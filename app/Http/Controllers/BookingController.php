<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($type, $id)
    {
        switch ($type)
        {
            case 'course':
                $event = \App\Course::find($id);
                $event->fulltime = $event->event->date_begin.', Niveau '.$event->level.', Période '.$event->periode;
                $event->fulldesc = $event->ocom;
                break;
            case 'event':
                $event = \App\Event::find($id);
                $event->fulltime = $event->date_begin;
                $event->fulldesc = $event->desc;
                break;
            default:
                abort(500);
        }

        clogNav('consulte les réservations');
        return view('admin.booking.index',['event' => $event,'event_type' => $type,'event_id' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($type,$id)
    {
        $event = null;
        if($type == 'course')
        {
            $event = \App\Course::find($id)->event;
        }
        else if ($type == 'event')
        {
            $event = \App\Event::find($id);
        }
        return view('admin.booking.create',['items' => \App\Item::training(),'event_type' => $type,'event_id' => $id,'event' => $event]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request,$type,$id)
    {
        $b = new Booking();

        $b->item_id = $request->item_id;
        $b->amount = $request->amount;
        $b->bookable_id = $id;
        $b->user_id = \Auth::user()->id;
        if (!$request->comment == '')
        {
            $b->comment = $request->comment;

        }
        else
        {
            $b->comment = 'Aucun commentaire';
        }
        switch ($type)
        {
            case 'course':
                $b->bookable_type = 'App\Course';
                break;
            case 'event':
                $b->bookable_type = 'App\Event';
            default:
                $b->bookable_type = '';
        }

        $b->save();
        clog('add','success','a ajouté une réservation avec succès',null,'App\Booking',$b->id);
        return redirect('/admin/booking/course/'.$id)->with('success','Réservation ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return Response
     */
    public function update(Request $request, Booking $booking)
    {
        $b = Booking::find($request->booking_id);
        $b->amount = $request->amount;
        $b->comment = $request->comment;

        $b->save();
        return back()->with('success','Modification sauvegardé avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return Response
     */
    public function destroy(Request $request)
    {
        $b = Booking::find($request->id);

        $b->delete();
    }

    public function modalItem($id,$event_type,$event_id)
    {
        $event = null;
        if ($event_type == 'course')
        {
            $event = \App\Course::find($event_id)->event;
        }
        else
        {
            $event = \App\Event::find($event_id);
        }
        return view('admin.booking.modal.create',['item' => \App\Item::find($id),'event' => $event]);
    }

    public function modalItemEdit($id,$event_type,$event_id)
    {
        $event = null;
        if ($event_type == 'course')
        {
            $event = \App\Course::find($event_id)->event;
        }
        else
        {
            $event = \App\Event::find($event_id);
        }
        return view('admin.booking.modal.edit',['booking' => \App\Booking::find($id),'event' => $event]);
    }
}

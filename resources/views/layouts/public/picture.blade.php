<div class="title">
	<h1>{{ App\Config::getData('text_public_picture_title')}}</h1>@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_picture_title"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif
	<p>{{ App\Config::getData('text_public_picture_desc')}}@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_picture_desc"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</p>
</div>
<hr>
@if ($pictures->isEmpty())
	<h4 class="text-center mt-5 mb-5">Aucune Photo</h4>
@else
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		@foreach($pictures as $picture)
		<div class="carousel-item @if($loop->first) active @endif">
			<img class="d-block w-100" src="{{$picture->url}}" alt="First slide">
		</div>
		@endforeach
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<div class="m-5">
	<a href="/pictures" class="btn btn-primary btn-block p-2">Voir toutes les photos</a>
</div>
@endif
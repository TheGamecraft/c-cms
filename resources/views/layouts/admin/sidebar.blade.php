@php
$sidebar = [
    'Nouvelle' => [
        'route' => 'admin.news',
        'icon' => 'new_releases',
        'perm' => 'news_see',
        'child' => null
    ],
    'Horaire' => [
        'route' => 'admin.schedule',
        'icon' => 'calendar_today',
        'perm' => 'schedule_see',
        'child' => null
    ],
    'Instruction' => [
        'route' => null,
        'icon' => 'menu_book',
        'perm' => null,
        'child' => [
            'Guide et NQP' => [
                'route' => 'admin.instruction.guide',
                'icon' => 'fas fa-book',
                'perm' => 'instruction_guide_see',
                'child' => null
            ],
            'BD des cours' => [
                'route' => 'admin.ocom',
                'icon' => 'fas fa-database',
                'perm' => 'instruction_db_ocom_see',
                'child' => null
            ],
            'Liste des cours' => [
                'route' => 'admin.course',
                'icon' => 'fas fa-list',
                'perm' => 'course_see_all',
                'child' => null
            ],
            //'Fichier' => [
            //    'route' => 'admin.instruction.files',
            //    'icon' => 'fas fa-folder',
            //    'perm' => null,
            //    'child' => null
            //],
            'Statistiques' => [
                'route' => 'admin.stats.instruction',
                'icon' => 'fas fa-chart-line',
                'perm' => 'stats_instruction_see',
                'child' => null
            ],
        ]
    ],
    'Administration' => [
        'route' => null,
        'icon' => 'recent_actors',
        'perm' => null,
        'child' => [
            //'Articles' => [
            //    'route' => 'admin.article',
            //   'icon' => 'fas fa-newspaper',
            //    'perm' => 'article_see',
            //    'child' => null
            //],
            //'Images' => [
            //    'route' => 'admin.picture',
            //    'icon' => 'fas fa-images',
            //    'perm' => 'picture_see',
            //    'child' => null
            //],
            'Utilisateurs' => [
                'route' => 'admin.users',
                'icon' => 'fas fa-users',
                'perm' => 'user_see',
                'child' => null
            ],
            //'Liste nominative' => [
            //    'route' => 'admin.users',
            //    'icon' => 'fas fa-address-book',
            //    'perm' => 'cadet_list_see',
            //    'child' => null
            //],
            'Logs' => [
                'route' => 'admin.stats.log',
                'icon' => 'fas fa-stream',
                'perm' => 'stats_see',
                'child' => null
            ],
        ]
    ],
    //'Fichiers' => [
    //    'route' => null,
    //    'icon' => 'folder',
    //    'perm' => 'file_see',
    //    'child' => \App\GoogleDriveFile::getSidebarFile(),
    //],
    'Configuration' => [
        'route' => null,
        'icon' => 'menu_book',
        'perm' => 'config_see',
        'child' => [
            'Général' => [
                'route' => 'admin.config.general',
                'icon' => 'fas fa-cogs',
                'perm' => 'config_edit',
                'child' => null
            ],
            'Instruction' => [
                'route' => 'admin.config.schedule',
                'icon' => 'fas fa-book-open',
                'perm' => 'config_edit_instruction',
                'child' => null
            ],
            'Administration' => [
                'route' => 'admin.config.complementary-activity',
                'icon' => 'fas fa-file-alt',
                'perm' => 'config_edit_administration',
                'child' => null
            ],
            'Grade' => [
                'route' => 'admin.config.rank',
                'icon' => 'fas fa-user-shield',
                'perm' => 'config_edit_rank',
                'child' => null
            ],
            'Poste' => [
                'route' => 'admin.config.jobs',
                'icon' => 'fas fa-user-tag',
                'perm' => 'config_edit_job',
                'child' => null
            ],
            //'Fichier' => [
            //    'route' => 'admin.config.files',
            //    'icon' => 'fas fa-folder',
            //    'perm' => 'config_edit_files',
            //    'child' => null
            //],
            'Apparence' => [
                'route' => 'admin.config.customisation',
                'icon' => 'fas fa-palette',
                'perm' => 'config_edit_customization',
                'child' => null
            ]
        ]
    ],
]
@endphp
<div class="sidebar" data-color="white" data-background-color="black" data-image="/images/sidebar.png">
    <div class="logo">
        <a href="/" class="logo-mini">
            <img class="img-fluid" src="/assets/admin/images/favicon.png" alt="Logo">
        </a>
        <a href="/" class="simple-text logo-normal">
            C-CMS
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="/assets/admin/images/avatar/user-{{\Auth::User()->avatar}}.jpg">
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username collapsed" aria-expanded="false">
              <span>
                {{\Auth::user()->lastname}}
                <b class="caret"></b>
              </span>
                </a>
                <div class="collapse" id="collapseExample" style="">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/profil">
                                <span class="sidebar-mini"> <i class="fas fa-user-circle"></i> </span>
                                <span class="sidebar-normal"> Mon profil </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/profil/courses">
                                <span class="sidebar-mini"> <i class="fas fa-chalkboard"></i> </span>
                                <span class="sidebar-normal"> Mes cours </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/profil/settings">
                                <span class="sidebar-mini"> <i class="fas fa-cog"></i> </span>
                                <span class="sidebar-normal"> Options </span>
                            </a>
                        </li>
                        <li class="nav-item mt-3">
                            <a class="nav-link" href="/logout">
                                <span class="sidebar-mini"> <i class="fas fa-sign-out-alt"></i> </span>
                                <span class="sidebar-normal"> Déconnexion </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item @if(\Request::route()->getName() == 'admin.dashboard') active @endif">
                <a class="nav-link" href="/admin">
                    <i class="material-icons">dashboard</i>
                    <p> Dashboard </p>
                </a>
            </li>
            <hr>
            @foreach($sidebar as $name => $s)
                @if(\Auth::user()->p($s['perm']) == 1 || $s['perm'] == null)
                    @if($s['route'])
                        <li class="nav-item @if(\Request::route()->getName() == $s['route']) active @endif">
                            <a class="nav-link" href="{{route($s['route'])}}">
                                <i class="material-icons">{{$s['icon']}}</i>
                                <p> {{$name}} </p>
                            </a>
                        </li>
                    @else
                        <li class="nav-item " id="link-{{str_replace(' ', '', $name)}}">
                            <a class="nav-link" data-toggle="collapse" href="#{{str_replace(' ', '', $name)}}">
                                <i class="material-icons">{{$s['icon']}}</i>
                                <p> {{$name}}
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="{{str_replace(' ', '', $name)}}">
                                <ul class="nav">
                                    @foreach($s['child'] as $n => $i)
                                        @if(\Auth::user()->p($i['perm']) == 1 || $i['perm'] == null)
                                            <li class="nav-item pl-2 @if(strpos(\Request::route()->getName(),$i['route']) !== false) active @endif" parent="{{str_replace(' ', '', $name)}}">
                                                <a class="nav-link" href="{{route($i['route'])}}">
                                                    <span class="sidebar-mini"> <i class="{{$i['icon']}}"></i> </span>
                                                    <span class="sidebar-normal"> {{$n}} </span>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @endif
                @endif
            @endforeach
            <h6 class="ml-4 mt-5" style="color:white">
                Outils
            </h6>
            <li class="nav-item ">
                <a class="nav-link" href="https://gitlab.com/TheGamecraft/c-cms/issues">
                    <i class="material-icons">bug_report</i>
                    <p> Signaler un bug </p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="/admin/update">
                    <i class="material-icons">feedback</i>
                    <p> Mise a jour </p>
                </a>
            </li>
        </ul>
    </div>
</div>

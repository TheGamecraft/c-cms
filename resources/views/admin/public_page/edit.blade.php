@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Modification de la page {{ $page->name }}</h4>
        </div>
        <div class="card-body">
            <div class="content">
                <form method="post">
                    @method('PATCH')
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                              <label for="banner">Image d'en tête</label>
                              <input type="text" class="form-control" name="banner" id="banner" aria-describedby="bannerHelp" value="{{ $page->banner }}">
                              <small id="bannerHelp" class="form-text text-muted">Url de l'image de l'en tête</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="oren">En tête</label>
                                <textarea class="editorHeader d-none" name="header">{!! $page->header !!}</textarea>      
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="oren">Contenu</label>
                                <textarea class="editorBody d-none" name="body">{!! $page->body !!}</textarea> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">Sauvegarder</button>
                        </div>
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_scripts')
<script src="/js/plugins/ckeditor/ckeditor.js"></script>
{{-- <script src="https://cdn.ckeditor.com/ckeditor5/29.1.0/classic/ckeditor.js"></script> --}}
<script>
    ClassicEditor.create( document.querySelector( '.editorHeader' ), {					
        toolbar: {
            items: [
            'heading',
            '|',
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'alignment',
            'fontSize',
            '|',
            'outdent',
            'indent',
            'undo',
            'redo'
            ]
        },
        language: 'fr',
        licenseKey: '',
    });

    ClassicEditor.create( document.querySelector( '.editorBody' ), {					
        toolbar: {
            items: [
            'heading',
            '|',
            'bold',
            'italic',
            'link',
            'underline',
            'strikethrough',
            'alignment',
            'bulletedList',
            'numberedList',
            'fontSize',
            '|',
            'outdent',
            'indent',
            '|',
            'horizontalLine',
            'imageUpload',
            'blockQuote',
            'insertTable',
            'mediaEmbed',
            'undo',
            'redo'
            ]
        },
        language: 'fr',
        image: {
            toolbar: [
            'imageTextAlternative',
            'imageStyle:inline',
            'imageStyle:block',
            'imageStyle:side'
            ]
        },
        table: {
            contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells'
            ]
        },
        licenseKey: '',
    });
</script>
@endsection

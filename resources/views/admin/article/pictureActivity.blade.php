@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4>Images de l'article {{$article->name}}</h4>
            </div>
            <div class="card-body mt-5">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            Toutes ces images seront disponible dans l'article
                        </p>
                    </div>
                    @if(isset($article->pictures))
                        @if($article->pictures->isEmpty())
                        <h5 class="text-center w-100">Aucune photo</h5>
                        @endif
                    @endif
                    @foreach($article->pictures as $picture)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-img-top">
                                    <img class="img-responsive w-100" src="{{$picture->url}}">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">{{$picture->title}}</h4>
                                    <p>{!!$picture->desc!!}</p>
                                    <div class="btn-group">
                                        <a class="btn btn-primary" href="/admin/picture/edit/{{$picture->id}}"><i class="fas fa-edit"></i></a>
                                        <button class="btn btn-danger" onclick="Delete({{$picture->id}})"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12">
                        <a class="btn btn-primary btn-block" href="/admin/article/activity/picture/{{$article->id}}/add">Ajouter une images</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('custom_scripts')
    <script>
        function Delete(pID) {
            swal({
                title: 'Êtes vous certain de vouloir supprimer l\'image?',
                text: "Vous ne pourrez pas annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
            }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/picture/delete/'+pID+'?api_token='+api_token, function(data) {
                            console.log('Delete');
                        });


                    })(jQuery);

                    swal(
                        'Supprimé!',
                        "L'image a été supprimé",
                        'success'
                    ).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                }
            })
        }
    </script>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use function GuzzleHttp\json_encode;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $UserList = User::all();

        return view('admin.user.index' ,['Userslist' => $UserList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = \App\Job::all();
        $ranks = \App\Rank::all();

        return view('admin.user.add', ['JobsList' => $jobs, 'RankList' => $ranks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $user = new User;

        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->email = request('email');

        if (request('adresse') == null) {
            $user->adress = "";
        } else {
            $user->adress = request('adresse');
        }

        if (request('telephone') == null) {
            $user->telephone = "";
        } else {
            $user->telephone = request('telephone');
        }

        $user->sexe = request('sexe');

        if (request('age') == null) {
            $user->age = "";
        } else {
            $user->age = request('age');
        }

        $user->avatar = rand(1,16);
        $user->rank_id = request('rank');
        $user->job_id = request('job');
        $user->password = bcrypt(request('psw'));
        $user->api_token = str_random(60);

        $user->save();

        return redirect('/admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobs = \App\Job::all();
        $ranks = \App\Rank::all();

        $user = \App\User::find($id);

        return view('admin.user.edit', ['JobsList' => $jobs, 'RankList' => $ranks, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $user = User::find($id);
        
        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->email = request('email');

        if (request('adresse') == null) {
            $user->adress = "";
        } else {
            $user->adress = request('adresse');
        }

        if (request('telephone') == null) {
            $user->telephone = "";
        } else {
            $user->telephone = request('telephone');
        }

        $user->sexe = request('sexe');

        if (request('age') == null) {
            $user->age = "";
        } else {
            $user->age = request('age');
        }

        $user->rank_id = request('rank');
        $user->job_id = request('job');
        if (request('psw') != null) {
            $user->password = bcrypt(request('psw'));
        }
        $user->api_token = str_random(60);

        $user->save();

        return redirect('/admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id = request('id');

        $user = User::find($id);
        
        $user->delete();
    }

    public function notificationmark($id)
    {
        $notification = \Auth::User()->unreadNotifications->where('id',$id)->first();

        $notification->delete();
        return redirect(request('url'));
    }

    public function notificationmarkALL()
    {
        $notifications = \Auth::User()->unreadNotifications;
        foreach($notifications as $notification)
        {
            $notification->delete();
        }
        return redirect()->back();
    }

    public function notificationmarkECC($id)
    {
        $notification = \Auth::User()->unreadNotifications->where('id',$id)->first();

        $notification->delete();
        return redirect('/ecc');
    }

    public function showUserProfil($id = 0)
    {
        if ($id == 0) {
            $id = \Auth::User()->id;
        }
        return view('admin.user.profil',['user' => \App\User::find($id)]);
    }

    public function editUserAvatar($id)
    {
        $user = \Auth::User();

        $user->avatar = $id;

        $user->save();

        return back()->with('success', 'Votre avatar a été mis à jour !');
    }

    public function UserAvatar()
    {
        return view('admin.user.profil.avatar');
    }

    public function UserPassword()
    {
        return view('admin.user.profil.password');
    }

    public function editUserPassword()
    {
        $user = \Auth::User();

        $user->password = bcrypt(request('psw'));

        $user->save();

        return redirect('/admin/profil')->with('success', 'Modification enregistré');
    }
    
    public function UserAdress()
    {
        return view('admin.user.profil.adress');
    }

    public function editUserAdress()
    {
        $user = \Auth::user();

        $user->adress = request('adress');

        $user->save();

        return redirect('/admin/profil')->with('success', 'Modification enregistré');
    }

    public function UserTelephone()
    {
        return view('admin.user.profil.telephone');
    }

    public function editUserTelephone()
    {
        $user = \Auth::user();

        $user->telephone = request('telephone');

        $user->save();

        return redirect('/admin/profil')->with('success', 'Modification enregistré');
    }

    public function apiList()
    {
        $users = \App\User::all();

        $name = [];

        foreach ($users as $user) {
            array_push($name, $user->fullname());
        }

        return json_encode($name);
    }

    public function showCourses($id)
    {
        return view('admin.user.courses',['courses' => User::find($id)->courses]);
    }

    public function showCourse($id,$course_id)
    {
        return view('admin.user.course',['courses' => User::find($id)->courses]);
    }

    public function userNotification()
    {
        return view('admin.user.profil.notifications',['notifications' => \Auth::user()->notifications]);
    }
}

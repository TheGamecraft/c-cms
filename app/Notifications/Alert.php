<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Alert extends Notification
{
    use Queueable;

    protected $fromUser;
    protected $myNotification;
    private $myUrl;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$msg,$url)
    {
        $this->myNotification = $msg;
        $this->fromUser = $user;
        $this->myUrl = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'from' => $this->fromUser,
            'msg' => $this->myNotification,
            'url' => $this->myUrl,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'from' => $this->fromUser,
            'msg' => $this->myNotification,
            'url' => $this->myUrl,
        ]);
    }

    public function broadcastType()
    {
        return 'notification.alert';
    }
}

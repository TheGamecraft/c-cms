<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use \App\Log;
use Illuminate\View\View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        clog('navigate','success','consulte le tableau de bord');

        return view('admin.dashboard',['futureEvent' => \App\Event::future()->take(3),'userClasse' => \Auth::user()->futureCourses()->forPage(1,6)]);
    }

    public function setup()
    {
        if (\Auth::user()->use_default_psw == true)
        {
            return view('admin.user.profil.setup');
        }
        return redirect('/admin');
    }

    public function saveSetup()
    {
        $user = \Auth::user();
        $user->firstname = \request('firstname');
        $user->lastname = \request('lastname');
        $user->password = bcrypt(request('psw'));

        if (\request('address') != null)
        {
            $user->adress = \request('address');
        }
        if (\request('telephone') != null)
        {
            $user->telephone = \request('telephone');
        }
        if (\request('sexe') != null)
        {
            $user->sexe = \request('sexe');
        }
        $user->use_default_psw = false;
        $user->save();
        return redirect('/admin');
    }

    public function update()
    {
        clog('navigate','success','consulte les notes de mise à jours');

        return view('admin.update');
    }

    public function status()
    {
        $alerts = \App\Event::checkForError();
        return view('admin.status.index',['alerts' => $alerts]);
    }

    public function instruction()
    {

    }

}

#!/bin/bash
sudo composer update
sudo chmod -R 777 storage/
sudo chmod -R 777 bootstrap/
php artisan migrate
php artisan db:seed
cp resources/views/default.blade.php resources/views/index.blade.php
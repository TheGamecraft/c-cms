<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ScheduleNotification extends Notification
{
    use Queueable;

    protected String $name;
    protected String $msg;
    protected String $url;
    protected String $icon;

    /**
     * Create a new notification instance.
     *
     * @param String $name
     * @param String $msg
     * @param String $url
     */
    public function __construct(String $name,String $msg,String $url)
    {
        $this->name = $name;
        $this->msg = $msg;
        $this->url = $url;
        $this->icon = '<i class="far fa-calendar"></i>';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'name' => $this->name,
            'msg' => $this->msg,
            'url' => $this->url,
            'icon' => $this->icon
        ];
    }

    public function broadcastType()
    {
        return 'notification.schedule';
    }
}

<?php

use Illuminate\Database\Seeder;

class RanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ranks')->insert([
            [
                'name' => "SuperAdmin",
                'acces_level' => '2',
                'desc' => 'Compte SuperAdmin donne toutes les permissions <strong>ne peux être modifié</strong>',
                'permissions' => \App\Permission::allToString(1)
            ]
        ]);
    }
}

@extends('layouts.admin.main')

@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Modification de mon numéro de téléphone </strong>
            </div>
            <div class="card-body">
                <form autocomplete="off" action="/admin/profil/edit/telephone" method="POST" data-parsley-validate novalidate>
                    @csrf
                    <div class="form-group">
                        <label for="adress">Entrer votre nouveau numero de téléphone</label>
                        <input type="text" class="form-control" name="telephone" id="telephone" placeholder="213-546-5401" pattern="([0-9]{3}-[0-9]{3}-[0-9]{4})*"
                               data-parsley-error-message="Le numéro de téléphone est invalide" required>
                    </div>
                    <button type="submit" id="submit" class="btn btn-primary">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $('#form').parsley();
    </script>
@endsection

@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Gestion de l'inventaire <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Catégorie</h4>
                                <p class="category">Gestion des catégories de l'inventaire</p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>
                                            Les catégories permettent de diviser l'inventaire et de définir les permissions d'accès aux items
                                        </p>
                                    </div>
                                    <div class="col-12">
                                        <div class="btn-group">
                                            <a class="btn btn-primary" href="/admin/inventory/management/category">Gérer les catégories</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

@endsection

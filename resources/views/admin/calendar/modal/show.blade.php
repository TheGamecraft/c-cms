<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="scrollmodalLabel">{{$date}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        @foreach ($schedules as $schedule)
        <div>
            <div class="row">
                <div class="col-7">
                    <p>{{$schedule->data['event_name']}} de {{$schedule->data['event_begin_time']}} à {{ $schedule->data['event_end_time']}}</p>
                </div>
                <div class="col-3">
                    @if ($schedule->data['is_event_mandatory'])
                        <div class="btn btn-block btn-warning m-l-10 m-b-10 float-right">Activité obligatoire</div>
                    @else
                        <div class="btn btn-block btn-primary m-l-10 m-b-10 float-right">L'activité n'est pas obligatoire</div>                        
                    @endif
                </div>
                <div class="col-1">
                    <a href="/admin/calendar/edit/{{$schedule->id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i> Modifier</a>
                </div>
                <div class="col-1">
                    <a type="button" class="btn btn-danger" onclick="deleteEvent({{$schedule->id}});">
                        <i class="fa fa-times-circle" style="color:white;"></i>
                    </a>
                </div>
            </div>
            <p>{!!$schedule->data['event_desc']!!}</p>
            @if ($schedule->type == 'regular' || $schedule->type == 1)
                <table class="table table-responsive">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-center" style="width: 25px;">Niveau</th>
                            @for ($i = 0; $i < \App\Config::getData('admin_periode_nb'); $i++)
                            <th>Période {{$i + 1}}</th>    
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @for ($niv = 1; $niv <= \App\Config::getData('admin_level_in_schedule_nb'); $niv++)
                            <tr>
                                <td class="text-center">{{ $niv }}</td>
                                @for ($periode = 1; $periode <= \App\Config::getData('admin_periode_nb'); $periode++)
                                    <td>
                                        <div class="row m-2">
                                            <div class="col-md-6 m-0 p-3">
                                                @if ($schedule->data['n'.$niv.'_p'.$periode.'_instructor'])
                                                    @if (\App\User::find($schedule->data['n'.$niv.'_p'.$periode.'_instructor']))
                                                        {{\App\User::find($schedule->data['n'.$niv.'_p'.$periode.'_instructor'])->fullname()}}
                                                    @else
                                                        <div class="text-warning font-weight-bold"><i class="fa fa-exclamation-triangle"></i> Instructeur Inconnu</div>
                                                    @endif
                                                @else
                                                    <div class="text-danger font-weight-bold"><i class="fa fa-exclamation-circle"></i> Champs Manquant dans la base de donnee</div>
                                                @endif
                                            </div>
                                            <div class="col-md-6 m-0 p-3 text-md-right">
                                                @if ($schedule->data['n'.$niv.'_p'.$periode.'_plandone'] )
                                                    <span class="badge badge-success">Remis</span>
                                                @else
                                                    <span class="badge badge-danger">Non Remis</span>
                                                @endif
                                            </div>
                                            <div class="col-md-6 m-0 p-3">
                                                @if ($schedule->data['n'.$niv.'_p'.$periode.'_ocom'])
                                                    {{$schedule->data['n'.$niv.'_p'.$periode.'_ocom']}} - 
                                                @endif
                                                @if ($schedule->data['n'.$niv.'_p'.$periode.'_name'])
                                                    {{$schedule->data['n'.$niv.'_p'.$periode.'_name']}} 
                                                @endif
                                            </div>
                                            <div class="col-md-6 m-0 p-3 text-md-right">
                                                @if ($schedule->data['n'.$niv.'_p'.$periode.'_local'])
                                                    @if (\App\Local::find($schedule->data['n'.$niv.'_p'.$periode.'_local']))
                                                        {{\App\Local::find($schedule->data['n'.$niv.'_p'.$periode.'_local'])->name}}
                                                    @else
                                                        <div class="text-warning font-weight-bold"><i class="fa fa-exclamation-triangle"></i> Local Inconnu</div>
                                                    @endif
                                                @else
                                                    <div class="text-danger font-weight-bold"><i class="fa fa-exclamation-circle"></i> Champs Manquant dans la base de donnee</div>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                @endfor
                            </tr>
                        @endfor
                    </tbody>
                    </table>            
            @endif
            </div> 
        @endforeach
        @if ($schedules->isEmpty())
           <h5 class="text-center">Aucune activité à l'horaire</h5>
        @endif
    </div>
    <div class="modal-footer">
        <a href="/admin/calendar/add/2019_05_03" type="button" class="btn btn-primary btn-lg btn-block">Ajouter une activité a l'horaire</a>
    </div>
</div>
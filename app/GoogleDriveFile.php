<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\FileNotFoundException;
use mysql_xdevapi\Exception;

/**
 * App\GoogleDriveFile
 *
 * @property string $id
 * @property string $type
 * @property string $name
 * @property string $path
 * @property array $rank_permission
 * @property array $job_permission
 * @property array $user_permission
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereJobPermission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereRankPermission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GoogleDriveFile whereUserPermission($value)
 * @mixin \Eloquent
 */
class GoogleDriveFile extends Model
{
    protected $primaryKey = 'id'; // or null

    public $incrementing = false;

    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';

    protected $casts = [
        'rank_permission' => 'array',
        'job_permission' => 'array',
        'user_permission' => 'array',
    ];

    public static function icon($extension)
    {
        $icon = "fas fa-file";
        switch ($extension)
        {
            case 'pdf':
                $icon = 'fas fa-file-pdf text-pdf';
                break;
            case 'php':
                $icon = 'fas fa-file-code text-code';
                break;
            case 'pptx':
                $icon = 'fas fa-file-powerpoint text-powerpoint';
                break;
            case 'docx':
            case 'odt':
                $icon = 'fas fa-file-word text-word';
                break;
            case 'ods':
            case 'xlsx':
                $icon = 'fas fa-file-excel text-excel';
                break;
            case 'png':
            case 'PNG':
            case 'jpg':
            case 'jpeg':
                $icon = 'fas fa-file-image text-image';
                break;
        }
        return $icon;
    }

    public static function checkConfig()
    {
        $configNull = (\Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_CLIENT_ID')) != "" && \Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_CLIENT_SECRET')) != "" && \Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_REFRESH_TOKEN')) != "" && \Crypt::decryptString(\App\Config::getData('GOOGLE_DRIVE_FOLDER_ID')) != "");
        $configOk = true;

        if ($configNull)
        {
            try {
                \Storage::cloud()->listContents("/", false);
            }
            catch (\Exception $e)
            {
                $configOk = false;
            }
        }

        return $configNull && $configOk;
    }

    public static function findByName($name)
    {
        return GoogleDriveFile::where('name','=',$name)->get()->first();
    }

    public static function findByPath($path)
    {
        return GoogleDriveFile::where('path','=',$path)->get()->first();
    }

    public static function findByPathInDrive($path,$current_directory = '/')
    {
        if ($path != "")
        {
            $exploded_path = explode('/',$path);
            if ($exploded_path[0] == "")
            {
                array_splice($exploded_path,0,1);
            }
            $contents = collect(Storage::cloud()->listContents($current_directory, false));

            $dir = $contents->where('type', '=', 'dir')
                ->where('name', '=', $exploded_path[0])
                ->first();

            if ( ! $dir)
            {
                return false;
            }
            else
            {
                array_splice($exploded_path,0,1);
                $newPath = implode('/',$exploded_path);
                if ($newPath == "")
                {
                    return $dir['basename'];
                }
                else
                {
                    return GoogleDriveFile::findByPathInDrive($newPath,$dir['basename']);
                }
            }
        }
        return true;
    }

    public static function createByPathInDrive($path,$current_directory = '/')
    {
        if ($path != "")
        {
            $exploded_path = explode('/',$path);
            $size = count($exploded_path);
            if ($size > 1)
            {
                $parent = self::findByName($exploded_path[$size-2]);
                \Storage::cloud()->createDir($parent->id.'/'.$exploded_path[$size-1]);
            }
            else
            {
                \Storage::cloud()->createDir('/'.$exploded_path[$size-1]);
            }
            return self::findByPathInDrive($path);
        }
        return false;
    }

    public function setPermission($subject, $value)
    {
        $explodedSubject = explode('.',$subject);
        $subject = $explodedSubject[0];
        $id = $explodedSubject[1];

        $permission = null;
        if ($subject == 'rank')
        {
            $permission = $this->rank_permission;
        }
        elseif ($subject == 'job')
        {
            $permission = $this->job_permission;
        }
        elseif ($subject == 'user')
        {
            $permission = $this->user_permission;
        }

         [$id] = $value;

        if ($subject == 'rank')
        {
            $this->rank_permission = $permission;
        }
        elseif ($subject == 'job')
        {
            $this->job_permission = $permission;
        }
        elseif ($subject == 'user')
        {
            $this->user_permission = $permission;
        }
        $this->save();
    }

    public function getAllPermission($subject)
    {
        $permission = null;
        if ($subject == 'rank')
        {
            $permission = $this->rank_permission;
        }
        elseif ($subject == 'job')
        {
            $permission = $this->job_permission;
        }
        elseif ($subject == 'user')
        {
            $permission = $this->user_permission;
        }

        return $permission;
    }

    public function getPermission($subject)
    {
        $explodedSubject = explode('.',$subject);
        $subject = $explodedSubject[0];
        $id = $explodedSubject[1];

        if (isset($this->getAllPermission($subject)[$id]))
        {
            return $this->getAllPermission($subject)[$id];
        }
        else
        {
            return '';
        }
    }

    public function canUser($user, $permission = 'r')
    {
        if (strpos($this->getPermission('rank.0'),$permission) !== false)
        {
            return true;
        }
        if (strpos($this->getPermission('user.'.$user->id),$permission) === false)
        {
            if (strpos($this->getPermission('job.'.$user->job->id),$permission) === false)
            {
                if (strpos($this->getPermission('rank.'.$user->rank->id),$permission) === false)
                {
                    return false;
                }

            }
        }
        return true;
    }

    public function canAuthUser($perm = 'r')
    {
        return $this->canUser(\Auth::user(),$perm);
    }

    public static function getPermForUser($folder,$user,$perm = 'r')
    {
        if ($folder == '' || $folder == "root")
        {
            if ($user->p('file_manage') == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        $dir = \App\GoogleDriveFile::find($folder);
        if ($dir == null)
        {
            $path = GoogleDriveFile::getPathFolder($folder);
            $exploderPath = explode('/',$path);
            for ($i = count($exploderPath)-1; $i >= 0; $i--)
            {
                $dir = \App\GoogleDriveFile::find($exploderPath[$i]);
                if ($dir != null)
                {
                    $metaData = \Storage::cloud()->getMetadata($folder);
                    $googleDriveFile = new GoogleDriveFile();
                    $googleDriveFile->id = $folder;
                    $googleDriveFile->type = 'directory';
                    $googleDriveFile->name = $metaData['name'];
                    $googleDriveFile->path = self::recreatePath($folder);
                    $googleDriveFile->rank_permission = $dir->rank_permission;
                    $googleDriveFile->job_permission = $dir->job_permission;
                    $googleDriveFile->user_permission = $dir->user_permission;
                    $googleDriveFile->save();
                    return $dir->canUser($user,$perm);
                }
            }
            return false;
        }
        else
        {
            return $dir->canUser($user,$perm);
        }
    }

    public static function getPermForAuthUser($folder,$perm = 'r')
    {
        return self::getPermForUser($folder,\Auth::user(),$perm);
    }

    public static function getPathFolder($folder)
    {
        $allDir = \Storage::cloud()->allDirectories();
        foreach ($allDir as $dir)
        {
            $exploderDir = explode('/',$dir);
            if ($exploderDir[count($exploderDir)-1] == $folder)
            {
                return $dir;
            }
        }
    }

    public static function recreatePath($folder)
    {
        $path = [];
        $name = [];
        $directories = collect(json_decode(self::getPathArray(),true));
        foreach ($directories as $dir)
        {
            $path[$dir['basename']] = $dir['dirname'];
            $name[$dir['basename']] = $dir['name'];
        }
        $realPath = $name[$folder];
        $foo = $folder;
        while ($foo != "")
        {
            $bar = explode('/',$path[$foo]);
            $foo = $bar[count($bar)-1];
            if ($foo != "")
            {
                $realPath = $name[$foo].'/'.$realPath;
            }
        }
        return $realPath;
    }

    public static function getPathArray()
    {
        $contents = collect(Storage::cloud()->listContents('/', true));
        return json_encode($contents->where('type', '=', 'dir'));
    }

    public static function getSidebarFile()
    {
        if (!\App\GoogleDriveFile::checkConfig())
        {
            return [];
        }
        $user = \Auth::user();
        $sidebarArray = [];
        $dirList = \App\GoogleDriveFile::all();
        $temp = $dirList->where('path','=','.Privé/.Cadet')->first();
        if ($temp != null)
        {
            if ($dirList->where('path','=','.Privé/.Cadet')->first()->canAuthUser())
            {
                $sidebarArray['Cadet'] = [
                    'route' => 'admin.files.cadet',
                    'icon' => 'fas fa-folder-open',
                    'perm' => null,
                    'child' => null
                ];
            }
        }

        $temp = $dirList->where('path','=','.Privé/.Staff')->first();
        if ($temp != null)
        {
            if ($dirList->where('path','=','.Privé/.Staff')->first()->canAuthUser())
            {
                $sidebarArray['Staff'] = [
                    'route' => 'admin.files.staff',
                    'icon' => 'fas fa-folder-open',
                    'perm' => null,
                    'child' => null
                ];
            }
        }

        $temp = $dirList->where('path','=','.Privé/.ETAMAS')->first();
        if ($temp != null)
        {
            if ($dirList->where('path','=','.Privé/.ETAMAS')->first()->canAuthUser())
            {
                $sidebarArray['ETAMAS'] = [
                    'route' => 'admin.files.etamas',
                    'icon' => 'fas fa-folder-open',
                    'perm' => null,
                    'child' => null
                ];
            }
        }

        $temp = $dirList->where('path','=','.Privé/.Officier')->first();
        if ($temp != null)
        {
            if ($dirList->where('path','=','.Privé/.Officier')->first()->canAuthUser())
            {
                $sidebarArray['Officier'] = [
                    'route' => 'admin.files.officier',
                    'icon' => 'fas fa-folder-open',
                    'perm' => null,
                    'child' => null
                ];
            }
        }
        $sidebarArray['Publique'] = [
            'route' => 'admin.files.publique',
            'icon' => 'fas fa-folder-open',
            'perm' => null,
            'child' => null
        ];
        if (\Auth::user()->p('drive_see') == 1)
        {
            $sidebarArray['Google Drive'] = [
                'route' => 'admin.drive',
                'icon' => 'fab fa-google-drive',
                'perm' => null,
                'child' => null
            ];
        }
        return $sidebarArray;
    }

    public static function getDownloadURL($path,$file)
    {
        $dir = urlencode(\App\GoogleDriveFile::findByPath($path)->id);
        return '/file/get?d='.$dir.'&f='.urlencode($file);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckPerm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permission)
    {
        if (\Auth::user()->p($permission) != 1)
        {
            clog('navigate','danger','L\'utilisateur n\'est pas autorisé à effectuer cette action',\Auth::user()->id);
            return redirect('/admin')->with('error','Vous n\'êtes pas autorisé à effectuer cette action');
        }
        return $next($request);
    }
}

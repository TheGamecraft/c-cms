@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Liste des cours</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <p>Liste des cours dans la base de données</p>
                        </div>
                        <div class="col-md-4 justify-content-end pr-4 d-flex">
                            <a href="ocom/add" data-placement="left" title="Ajouter un cour">
                                <i class="fas fa-plus-circle fa-3x"></i>
                            </a>
                            <div class="dropdown my-auto mx-3">
                                <a class="dropdown-toggle" href="javascript:;" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="/admin/ocom/generate">Génération de masse</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-no-bordered table-hover dataTable dtr-inline" id="table" style="width:100%">
                        <thead>
                        <tr>
                            <th style="width: 4.5rem">Status</th>
                            <th style="width: 4.5rem">OREN</th>
                            <th style="width: 4.5rem">OCOM</th>
                            <th style="width: 7rem">Nb Periode</th>
                            <th>Objectif de rendement</th>
                            <th>Objectif de compétence</th>
                            <th style="width: 6rem"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ocoms as $ocom)
                        <tr class="cursor">
                            <td class="text-center" onclick="navigate({{$ocom->id}})">
                                @if($ocom->course_id != "")
                                    <div data-toggle="tooltip" data-placement="right" title="Le cours a été donné cette année">
                                        <p class="d-none">1</p>
                                        <i class="fas fa-check-circle fa-2x text-success"></i>
                                    </div>
                                    @else
                                    <div data-toggle="tooltip" data-placement="right" title="Le cours n'a pas été donné cette année">
                                        <p class="d-none">0</p>
                                        <i class="fas fa-times-circle fa-2x text-warning"></i>
                                    </div>
                                @endif
                            </td>
                            <td onclick="navigate({{$ocom->id}})">{{$ocom->oren}}</td>
                            <td onclick="navigate({{$ocom->id}})">{{$ocom->ocom}}</td>
                            <td onclick="navigate({{$ocom->id}})">{{$ocom->nbPeriode}}</td>
                            <td onclick="navigate({{$ocom->id}})">{{$ocom->objectif_rendement}}</td>
                            <td onclick="navigate({{$ocom->id}})">{{$ocom->objectif_competence}}</td>
                            <td>
                                <a href="/admin/ocom/{{$ocom->id}}/edit" class="btn btn-primary btn-fab btn-fab-mini btn-round">
                                    <i class="material-icons">edit</i>
                                </a>
                                <button class="btn btn-danger btn-fab btn-fab-mini btn-round" onclick="deleteOCOM({{$ocom->id}})">
                                    <i class="material-icons">delete</i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <a class="navbar-brand">Base de données des cours</a>
@endsection

@section('custom_scripts')
    <script src="/js/ocom.js"></script>
    <script>
        $('.tooltip').tooltip('enable')
        $(document).ready(function() {
            $('#table').DataTable({
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering" : true,
                "order": [[1, "asc"]]
            });
        } );
    </script>
@endsection

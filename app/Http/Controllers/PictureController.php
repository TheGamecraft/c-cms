<?php

namespace App\Http\Controllers;

use App\Picture;
use Illuminate\Http\Request;

class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.allpicture',['pictures' => \App\Picture::paginate(\App\Config::getData('text_public_picture_nb'))]);
    }

    public function indexAdmin()
    {
        clogNav('consulte les images');
        return view('admin.picture.index',['pictures' => \App\Picture::paginate(\App\Config::getData('text_public_picture_nb'))]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.picture.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $pic = new Picture();

        $pic->title = request('title');
        $pic->url = request('url');
        $pic->desc = request('desc');
        $pic->pictureable_id = 0;
        $pic->pictureable_type = "";

        $pic->save();
        clog('add','success','a ajouté une image',null,'App\Picture',$pic->id);
        return redirect('/admin/picture')->with('success','Image ajoutée avec succès');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        clog('see','success','a consulté une image',null,'App\Picture',$id);
        return view('public.picture',['picture' => \App\Picture::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.picture.edit',['picture' => Picture::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $pic = Picture::find($id);

        $pic->title = request('title');
        $pic->url = request('url');
        $pic->desc = request('desc');

        $pic->save();
        clog('edit','success','a modifié une image',null,'App\Picture',$id);
        if ($pic->pictureable_type == "App\ComplementaryActivity")
        {
            return redirect('admin/article/activity/picture/'.$pic->pictureable->id)->with('success','Image sauvegarder avec succès');
        }
        else
        {
            return redirect('/admin/picture')->with('success','Image sauvegarder avec succès');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pic = Picture::find($id);

        $pic->delete();
        clog('delete','success','a supprimé une image',null,'App\Picture',$id);
    }
}

@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Articles <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($activity as $a)
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$a->name}}</h4>
                                    <p class="category">{{$a->updated_at}}
                                        @if($a->is_promoted == 1) <span class="badge badge-success float-right">Promu sur la page d'accueil</span> @endif</p>
                                </div>
                                <div class="card-body news-body-small">
                                    {!! $a->public_body !!}
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                @if(\Auth::user()->p('article_edit') == 1)
                                                    <a href="/admin/article/activity/edit/{{$a->id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier l'article</a>
                                                @endif
                                                @if(\Auth::user()->p('article_edit') == 1 && \Auth::user()->p('picture_add'))
                                                    <a href="/admin/article/activity/picture/{{$a->id}}" type="button" class="btn btn-secondary"><i class="fas fa-image"></i>&nbsp; Gérer les photos</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <a href="/activity/{{$a->id}}" target="_blank" type="button" class="btn btn-outline-secondary"><i class="fas fa-image"></i>&nbsp; Voir l'article</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

@endsection

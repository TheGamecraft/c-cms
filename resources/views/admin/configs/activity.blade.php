@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Configuration Générale
        </div>
        <div class="card-body">
            <div class="row">
                @foreach ($activities as $activity)
                    <div class="col-lg-12 col-xl-6 p-2">
                        <div class="row m-auto">
                            <div class="col">
                                <label for="text-input" class=" form-control-label">{{$activity->name}}</label>
                                <small class="form-text text-muted">{{$activity->admin_desc}}</small>
                            </div>
                            <div class="col text-right">
                                <div class="btn-group">
                                    <a href="/admin/config/activity/edit/{{$activity->id}}" class="btn btn-primary"><i class="fa fa-cog" aria-hidden="true"></i> Modifier</a>
                                    <button class="btn btn-danger" onclick="delActivity({{$activity->id}})" ><i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Supprimer"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 p-2">
                    <a class="btn btn-primary btn-block" href="/admin/config/activity/add">Ajouter une activité</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Configuration Générale</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Configuration/Générale</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script>
    function delActivity(pid) {
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/config/activity/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "L'activité a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
            }
</script>
@endsection

<div class="title">
    <h1>{{ App\Config::getData('text_public_news_title')}}@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_news_title" ><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</h1>
    <p>{{ App\Config::getData('text_public_news_desc')}} @if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_news_desc" ><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</p>
</div>
<hr class="mx-5">
@if ($news->isEmpty())
    <h4 class="text-center">Aucune nouvelles</h4>
@endif
<div class="row">
    @foreach ($news as $new)
        <div class="col-md-4">
            <h3 class="news-title mb-4">{{ $new->title }}</h3>
            <div class="news-body-small"> {!! $new->body !!}</div>
            <span class="news-small">@if(\App\User::find($new->user_id)){{ \App\User::find($new->user_id)->fullname()}},@endif {{$new->created_at}}</span>
            <div class="news-tags">
                @if($new->tags != [])
                    @foreach($new->tags as $tag)
                        @if($tag == "Important")
                            <span class="badge badge-pill badge-danger">{{$tag}}</span>
                        @else
                            <span class="badge badge-pill badge-default">{{$tag}}</span>
                        @endif
                    @endforeach
                @endif
                    @if($new->files != [""] && isset($new->files))
                        <span class="badge badge-pill badge-secondary">Fichier joint</span>
                    @endif
            </div>
            <a name="news" id="news" class="btn btn-block btn-secondary mt-2"
               @if(isset($new->event_id))
               href="/news/{{$new->event_id}}?type=msg" role="button">Voir plus!
                @else
                    href="/news/{{$new->id}}" role="button">Voir plus!
                @endif
            </a>
        </div>
    @endforeach
</div>
<div class="align-center m-5">
    <a name="news-all" id="news-all" class="btn btn-primary btn-block p-2 text-uppercase" href="/news" role="button">{{ App\Config::getData('text_public_news_button')}}</a>@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_news_button"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif
</div>
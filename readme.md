<p align="center"><img src="https://escadron697.ca/assets/admin/images/CCMSLOGO.png" width="400px"></p>

<p align="center">
Version 2.5.3
</p>

## A propos de C-CMS

C-CMS est un système de gestion de contenu créer pour amener les taches quotidiennes des staffs et officier d'un escadron de cadet dans l'ère numérique. Il est entre autre :

- Simple d'installation et d'utilisation.
- Développer sur un Framework solide et a jour.

C-CMS vous permet de :

- Construire un horaire interactif pour les parents, cadets et officiers.
- Gérer un espace publique, un espace pour les staffs et un espace officer.
- Publier des nouvelles et messages sur votre site.
- Et bien plus

## Licence

C-CMS est développer avec Laravel sous la [Licence MIT](https://opensource.org/licenses/MIT).

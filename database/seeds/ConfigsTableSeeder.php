<?php

use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            [
                'name' => 'is_schedule_public',
                'state' => 0,
                'data' => '["true"]'
            ],
            [
                'name' => 'is_schedule_build',
                'state' => 0,
                'data' => '["false"]'
            ],
            [
                'name' => 'text_public_banner_cadet_desc',
                'state' => 0,
                'data' => '["Les cadets de l\'air s\'adressent aux jeunes de 12 à 18 ans qui désirent vivre des expériences enrichissantes et relever de nouveaux défis, en participant à des activités stimulantes dans un cadre dynamique et chaleureux."]'
            ],
            [
                'name' => 'text_public_banner_apprendre_plus',
                'state' => 0,
                'data' => '["En apprendre plus!"]'
            ],
            [
                'name' => 'text_public_intro_title',
                'state' => 0,
                'data' => '["L\'escadron c\'est ..."]'
            ],
            [
                'name' => 'text_public_intro_desc',
                'state' => 0,
                'data' => '["De nombreuses activités hebdomadaires dont : les soirées du vendredi, les soirées de musique (fanfare), l\'entrainement au tir, une équipe de biathlon, des cours de pilotage et plus encore !"]'
            ],
            [
                'name' => 'text_public_picture_title',
                'state' => 0,
                'data' => '["Photos"]'
            ],
            [
                'name' => 'text_public_picture_desc',
                'state' => 0,
                'data' => '["Voici quelques photos de nos activités, même s\'il est mieux d\'y participer réellement!"]'
            ],
            [
                'name' => 'text_public_news_title',
                'state' => 0,
                'data' => '["Nouvelles"]'
            ],
            [
                'name' => 'text_public_news_desc',
                'state' => 0,
                'data' => '["Retrouver ici les dernieres nouvelles de l\'escadron"]'
            ],
            [
                'name' => 'text_public_news_button',
                'state' => 0,
                'data' => '["Voir toutes les nouvelles!"]'
            ],
            [
                'name' => 'text_public_schedule_desc',
                'state' => 0,
                'data' => '["Voici les activitées à venir !"]'
            ],
            [
                'name' => 'text_public_schedule_title',
                'state' => 0,
                'data' => '["Calendrier"]'
            ],
            [
                'name' => 'text_public_picture_nb',
                'state' => 0,
                'data' => '["6"]'
            ],
            [
                'name' => 'text_public_cta',
                'state' => 0,
                'data' => '["Êtes-vous prêt à en faire partie ?"]'
            ],
            [
                'name' => 'media_facebook',
                'state' => 0,
                'data' => '["https://www.facebook.com/"]'
            ],
            [
                'name' => 'media_twitter',
                'state' => 0,
                'data' => '["https://twitter.com"]'
            ],
            [
                'name' => 'media_instagram',
                'state' => 0,
                'data' => '["https://www.instagram.com"]'
            ],
            [
                'name' => 'media_email',
                'state' => 0,
                'data' => '["exemple@email.com"]'
            ],
            [
                'name' => 'escadron_address',
                'state' => 0,
                'data' => '["000 Rue Exemple, Québec, Canada"]'
            ],
            [
                'name' => 'escadron_name_full',
                'state' => 0,
                'data' => '["Escadron 000 Exemple"]'
            ],
            [
                'name' => 'escadron_name_short',
                'state' => 0,
                'data' => '["Escadron 000"]'
            ],
            [
                'name' => 'escadron_number',
                'state' => 0,
                'data' => '["000"]'
            ],
            [
                'name' => 'escadron_element',
                'state' => 0,
                'data' => '["Aviation"]'
            ],
            [
                'name' => 'element_title',
                'state' => 0,
                'data' => '["Cadet de l\'aviation royale du Canada"]'
            ],
            [
                'name' => 'escadron_direct_googlemap_link',
                'state' => 0,
                'data' => '["https://goo.gl/maps/iLwGZLwWXujwoAg59"]'
            ],
            [
                'name' => 'escadron_phone',
                'state' => 0,
                'data' => '["(418) 722-7712"]'
            ],
            [
                'name' => 'nb_activity_public',
                'state' => 0,
                'data' => '["3"]'
            ],
            [
                'name' => 'file_GP_Niv_1',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1tZhDqCvMor9p6lXxYek7Q0Xc8c2o5pG7"]'
            ],
            [
                'name' => 'file_GQ_Niv_1',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1MK6Lgr_qgP8vwBIiTyIveQu9p2rh1mXj"]'
            ],
            [
                'name' => 'file_GP_Niv_2',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1oLTavI1AQsXMdhZ4QqEkbecfV4j1LKDx"]'
            ],
            [
                'name' => 'file_GQ_Niv_2',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1YzIqyVEfCiGVEI_hKB-ZHt0pAgh-QRxe"]'
            ],
            [
                'name' => 'file_GP_Niv_3',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1FrczHmiGCeONlHCuuxHNx-BZ-qEfEBK8"]'
            ],
            [
                'name' => 'file_GQ_Niv_3',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1S-NFjqlixzC9GNZSqZ1_PqBDFcm-LS1t"]'
            ],
            [
                'name' => 'file_GP_Niv_4',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1EeibjlytdzEpRdzs-eg0pGL8TBv_ZCsu]'
            ],
            [
                'name' => 'file_GQ_Niv_4',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1VmVL4wb6llIc09HkWfzL8YOQYo3ygx86"]'
            ],
            [
                'name' => 'file_empty_lesson_plan',
                'state' => 0,
                'data' => '["https://drive.google.com/uc?export=download&id=1i1a0sjI8I3nzt4mlcLvznjqYF-12JgfQ"]'
            ],
            [
                'name' => 'admin_periode_begin',
                'state' => 0,
                'data' => '["1" => "19:10","2" => "20:30"]'
            ],
            [
                'name' => 'admin_periode_end',
                'state' => 0,
                'data' => '["1" => "20:10","2" => "21:20"]'
            ],
            [
                'name' => 'admin_periode_nb',
                'state' => 0,
                'data' => '["2"]'
            ],
            [
                'name' => 'admin_level_in_schedule_nb',
                'state' => 0,
                'data' => '["3"]'
            ],
            [
                'name' => 'public_index_img_url',
                'state' => 0,
                'data' => '["./assets/img/bg2.jpg"]'
            ],
            [
                'name' => 'default_weekly_msg',
                'state' => 0,
                'data' => "[\"<strong>/* Nom de l'évènement */</strong><br>Heure (Cadets):<br>Lieu:<br>Tenue:<br>Matériel:<br>/* Commentaire */\"]"
            ],
            [
                'name' => 'cadet_list',
                'state' => 0,
                'data' => '["#"]'
            ],
            [
                'name' => 'GOOGLE_DRIVE_CLIENT_ID',
                'state' => 0,
                'data' => '["'.\Crypt::encryptString('').'"]'
            ],
            [
                'name' => 'GOOGLE_DRIVE_CLIENT_SECRET',
                'state' => 0,
                'data' => '["'.\Crypt::encryptString('').'"]'
            ],
            [
                'name' => 'GOOGLE_DRIVE_REFRESH_TOKEN',
                'state' => 0,
                'data' => '["'.\Crypt::encryptString('').'"]'
            ],
            [
                'name' => 'GOOGLE_DRIVE_FOLDER_ID',
                'state' => 0,
                'data' => '["'.\Crypt::encryptString('null').'"]'
            ],
            [
                'name' => 'is_Google_Drive_enabled',
                'state' => 0,
                'data' => '["false"]'
            ],
            [
                'name' => 'instruction_year_begin',
                'state' => 0,
                'data' => '["2019-08-1"]'
            ],
            [
                'name' => 'instruction_year_end',
                'state' => 0,
                'data' => '["2020-05-31"]'
            ]
        ];

        $actualConfigs = \App\Config::all();

        $configToAdd = [];

        foreach ($configs as $config) {
            $found = false;
            foreach ($actualConfigs as $actualConfig) {
                if ($actualConfig->name == $config['name']) {
                    $found = true;
                }
            }

            if (!$found) {
                array_push($configToAdd, $config);
            }
        }

        if ($configToAdd != []) {
            DB::table('configs')->insert($configToAdd);
        }
    }
}

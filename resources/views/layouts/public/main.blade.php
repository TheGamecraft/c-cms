<!DOCTYPE HTML>
<!--
	Spectral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>

    <!-- Include Head -->
    @include('layouts.public.head')

</head>

<body>

<!-- Include Header -->
@include('layouts.public.header')

<div class="wrapper wrapper-full-page">

    @yield('content')


    <footer class="footer">
        <div class="container">

            <!-- Include Footer -->
            @include('layouts.public.footer')

        </div>
    </footer>
</div>

<!-- Scripts -->
<script src="https://polyfill.io/v3/polyfill.min.js?features=Intl.~locale.en"></script>
<script src="/js/core/jquery.min.js"></script>
<script src="/js/core/popper.min.js"></script>
<script src="/js/core/bootstrap-material-design.min.js"></script>
<script src="/js/material-dashboard.js"></script>
<script src="/js/material-kit.min.js"></script>
<script src='/assets/fullcalendar/core/main.js'></script>
<script src='/assets/fullcalendar/core/locales/fr-ca.js'></script>
<script src='/assets/fullcalendar/daygrid/main.js'></script>
<script src='/assets/fullcalendar/interaction/main.js'></script>
<script src='/assets/fullcalendar/list/main.js'></script>
<script>
    function toggleEdit() {
        if ($('#edit-switch').prop("checked")) {
            $('.edit-btn').css({"display": "inline-block"});
        } else {
            $('.edit-btn').css({"display": "none"});
        }
    }
</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var calendarEl = document.getElementById('public_calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['dayGrid','interaction','list'],
            locale: 'fr',
            header: {
                left: 'title',
                center: 'dayGridMonth,listWeek',
                right: 'prev,next'
            },
            events: '/api/schedule/events',
            eventRender: function(event, element) {
                if (event.event.extendedProps.icon && event.view.type == 'dayGridMonth')
                {
                    let i = document.createElement('i');
                    i.className = event.event.extendedProps.icon+' mx-1';
                    event.el.querySelector('.fc-content').prepend(i);
                }
                else if(event.event.extendedProps.icon && event.view.type == 'listWeek')
                {
                    let i = document.createElement('i');
                    i.className = event.event.extendedProps.icon+' mr-1';
                    event.el.querySelector('.fc-list-item-title').prepend(i);
                }
            },
            eventClick: function (info) {
                console.log(info.event.id)
                $.get("/api/schedule/events/modal/" + info.event.id + "/" + info.event.extendedProps.extraParams.db_type, function (data) {
                    $("#modal-content").html(data);
                });
                $('#schedulemodal').modal('toggle')
            }
        });
        calendar.render();
    });

</script>
<div class="modal fade" id="schedulemodal" tabindex="-1" role="dialog" aria-labelledby="schedulemodal"
     aria-hidden="true">
    <div class="modal-dialog w-100 modal-lg mx-2 mx-lg-auto" role="document">
        <div class="modal-content" id="modal-content"></div>
    </div>
</div>
</body>

</html>
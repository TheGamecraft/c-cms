@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Ajouter une nouvelle <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <form method="post" action="/admin/news/add">
                    @csrf

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Titre</label>
                                <input class="form-control" type="text" name="title" required>
                                <small class="form-text text-muted">Titre de la nouvelle à publier</small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" name="publish" value="1">
                                        La nouvelle doit-elle être immédiatement publié ?
                                        <span class="form-check-sign"><span class="check"></span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Contenu</label>
                                <textarea id="body" name="body" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script>
        $('#body').trumbowyg({
            lang: 'fr'
        });
    </script>
@endsection

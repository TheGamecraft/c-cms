<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;

/**
 * App\Schedule
 *
 * @property int $id
 * @property string $date
 * @property string $type
 * @property string $n1_p1_item
 * @property string $n1_p2_item
 * @property string $n2_p1_item
 * @property string $n2_p2_item
 * @property string $n3_p1_item
 * @property string $n3_p2_item
 * @property array $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereN1P1Item($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereN1P2Item($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereN2P1Item($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereN2P2Item($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereN3P1Item($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereN3P2Item($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Schedule extends Model
{
    protected $casts = [
        'data' => 'array',
    ];

    public function getInventory($periode)
    {
        $dispo_id = [];
        $dispo_qt = [];
        $used_id = [];
        $used_qt = [];
        $dispo_item = collect();

        $string_periode = 'p'.$periode;

        $inventory = Item::all();

        foreach ($inventory as $item) {
            array_push($dispo_id, $item->id);
            array_push($dispo_qt, $item->quantity);
        }

        $n1 = 'n1_'.$string_periode.'_item';
        $items_array = explode("-",$this->$n1);
        if ($items_array[0] == "") {
            unset($items_array[0]);
        }
        $items_array = array_values($items_array);
        if ($items_array != "") {
            foreach ($items_array as $item_array) {
                $items_for = explode(":",$item_array);
                array_push($used_id,$items_for[0]);
                array_push($used_qt,$items_for[1]);
            }
        }

        $n2 = 'n2_'.$string_periode.'_item';
        $items_array = explode("-",$this->$n2);
        if ($items_array[0] == "") {
            unset($items_array[0]);
        }
        $items_array = array_values($items_array);

        if ($items_array != "") {
            foreach ($items_array as $item_array) {
                $items_for = explode(":",$item_array);
                array_push($used_id,$items_for[0]);
                array_push($used_qt,$items_for[1]);
            }
        }

        $n3 = 'n3_'.$string_periode.'_item';
        $items_array = explode("-",$this->$n3);
        if ($items_array[0] == "") {
            unset($items_array[0]);
        }
        $items_array = array_values($items_array);
        if ($items_array != "") {
            foreach ($items_array as $item_array) {
                $items_for = explode(":",$item_array);
                array_push($used_id,$items_for[0]);
                array_push($used_qt,$items_for[1]);
            }
        }

        for ($i=0; $i < count($used_id); $i++) { 
            for ($e=0; $e < count($dispo_id); $e++) {
                if (isset($dispo_id[0])) {
                    if ($used_id[$i] == $dispo_id[$e]) {
                        $dispo_qt[$e] = $dispo_qt[$e] - $used_qt[$i];
                        if ($dispo_qt[$e] < 1) {
                            unset($dispo_id[$e]);
                            unset($dispo_qt[$e]);
                        }
                    }
                }
            }
        }
        $dispo_id = array_values($dispo_id);
        $dispo_qt = array_values($dispo_qt);
        for ($i=0; $i < count($dispo_id); $i++) { 
            $this_item = Item::find($dispo_id[$i]);
            $this_item->quantity = $dispo_qt[$i];
            $dispo_item->push($this_item);   
        }

        return $dispo_item;
    }

    public function getUserToNotify($priority = 0)
    {
        $users = \App\User::all();

        if($priority == 0)
        {
            $userToNotify = collect();
            foreach ($users as $user) {
                if ($user->getPerm('schedule_notify')) {
                    $userToNotify->push($user);
                }
            }

            return $userToNotify;
        } else if ($priority == 1)
        {
            return $users;
        }
    }

    public static function checkForWarning()
    {
        $schedule = Schedule::all();
        $warning = collect();
        $today = date('U');

        foreach ($schedule as $activity) {
            
            if ($activity->type == "regular") {

                $time = date('U',strtotime($activity->date));
                if ($time >= $today) {
                    for ($niv=1; $niv <= 3; $niv++) { 
                        for ($pe=1; $pe <= 2; $pe++) {
    
                            /** Check name */
                            if ($activity->data['n'.$niv."_p".$pe."_name"] == "") {
                                $warning->push(['warning' => 'Il doit y avoir un nom pour le cours', 'niveau' => $niv, 'periode' => $pe,'date' => $activity->date]);
                            }
    
                            /** Check OCOM */
                            if ($activity->data['n'.$niv."_p".$pe."_ocom"] == "") {
                                $warning->push(['warning' => "Il doit y avoir un OCOM pour le cours", 'niveau' => $niv, 'periode' => $pe,'date' => $activity->date]);
                            } else {
                                $regex = '/[MC]['.$niv.']\d\d.\d\d/';
                                if (preg_match($regex,trim($activity->data['n'.$niv."_p".$pe."_ocom"])) == 0 && $activity->data['n'.$niv."_p".$pe."_ocom"] != "S.O") {
                                    $warning->push(['warning' => "L'OCOM du cours de semble pas être valide", 'niveau' => $niv, 'periode' => $activity->data['n'.$niv."_p".$pe."_ocom"],'date' => $activity->date]);
                                }
                            }                    
    
                            /** Check Instructor */
                            if ($activity->data['n'.$niv."_p".$pe."_instructor"] == "") {
                                $warning->push(['warning' => "Il doit y avoir un instructeur pour le cours", 'niveau' => $niv, 'periode' => $pe,'date' => $activity->date]);
                            }
    
                            /** Check local */
                            if ($activity->data['n'.$niv."_p".$pe."_local"] == "") {
                                $warning->push(['warning' => "Il doit y avoir un local pour le cours", 'niveau' => $niv, 'periode' => $pe,'date' => $activity->date]);
                            }
                        }
                    }
                }
            }
        }

        return $warning;
    }
}

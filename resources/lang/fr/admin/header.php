<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Siderbar Language Lines
    |--------------------------------------------------------------------------
    */

    'logout' => "Déconnexion",
    'settings' => "Options",
    'notification' => "Notifications",
    'profil' => "Mon profil",
    'search_bar' => "Recherche ..."
];

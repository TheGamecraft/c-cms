<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.index',[
            'news' => \App\News::allWithWeeklyMsg()->where('publish','=','1')->sortByDesc('created_at')->take(3),
            'activities' => \App\ComplementaryActivity::all()->where('is_promoted','1'),
            'pictures' => \App\Picture::all()->sortByDesc('created_at')->take(\App\Config::getData('nb_activity_public'))
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($config)
    {
        return view('admin.public.edit',['config' => \App\Config::where('name',$config)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $config)
    {
        $config = \App\Config::where('name',$config)->first();

        $config->data = [request('data')];

        $config->save();

        return redirect('/?editMode');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

@extends('layouts.public.main')

@section('content')
    <!-- Header -->
    
            <!-- Banner -->
            <div id="banner">
    
                <!-- Include Banner -->
                @include('layouts.public.banner')
    
            </div>

            <div class="main main-raised">
                <div class="w-100 text-center">
                    <a href="#activity" class="btn btn-primary btn-lg" style="margin-top:-7rem;">{{ App\Config::getData('text_public_banner_apprendre_plus') }}</a>
                    @if(request()->has('editMode'))
                    <a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_banner_apprendre_plus">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    @endif
                </div>
                <div class="container">
                    <!-- Introduction -->
                    <div class="section text-center" id="activity">
            
                        <!-- Include Introduction -->
                        @include('layouts.public.introduction')
                        
                        <!-- Include Activity -->
                        @include('layouts.public.activity')
            
                    </div>

                    <!-- News -->
                    <div class="section text-center" id="news">
                        @include('layouts.public.news')
                    </div>
        
                    <!-- Calendar -->
                    @if (\App\Config::getData('is_schedule_public') === "true")
                        <div class="section" id="calendar">
                            @include('layouts.public.calendar')
                        </div>    
                    @endif
            
                    <!-- Photos -->
                    <div class="section text-center" id="picture">
            
                        <!-- Include Picture -->
                        @include('layouts.public.picture')
            
                    </div>
            
                    <!-- CTA -->
                    <div class="section text-center" id="cta">
                        <h2>{{ App\Config::getData('text_public_cta')}}@if(request()->has('editMode'))<a class="badge badge-primary edit-btn" href="/admin/public/edit/text_public_cta" style="display: none;"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif</h2>
                        <a href="{{ App\Config::getData('escadron_direct_googlemap_link')}}" class="btn btn-big btn-primary word-wrap">{{ App\Config::getData('escadron_address')}} - {{ App\Config::getData('escadron_phone')}}</a>
                    </div>
                </div>  
            </div>
    
@endsection
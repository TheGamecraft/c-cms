@extends('layouts.admin.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            @if($alerts[0] == [])
                <div class="alert alert-success" role="alert">
                    <div class="row">
                        <div class="col-10">
                            Aucun avertissement
                        </div>
                        <div class="col text-right m-0">
                            <i class="fas fa-check text-white"></i>
                        </div>
                    </div>
                </div>
            @endif
            @foreach($alerts[0] as $alert)
                <div class="alert alert-warning" role="alert">
                    <div class="row">
                        <div class="col-10">
                            {{$alert}}
                        </div>
                        <div class="col text-right m-0">
                            <i class="fas fa-warning text-white"></i>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-6">
            @if($alerts[0] == [])
                <div class="alert alert-success" role="alert">
                    <div class="row">
                        <div class="col-10">
                            Aucune erreur
                        </div>
                        <div class="col text-right m-0">
                            <i class="fas fa-check text-white"></i>
                        </div>
                    </div>
                </div>
            @endif
            @foreach($alerts[1] as $alert)
                <div class="alert alert-danger" role="alert">
                    <div class="row">
                        <div class="col-10">
                            Des problèmes ont été détecté avec l'horaire. Impossible de les régler de façon automatique.
                        </div>
                        <div class="col text-right m-0 m-auto">
                            <i class="fas fa-times text-white"></i>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Regular header</h4>
                    <p class="category">Category subtitle</p>
                </div>
                <div class="card-body">
                    The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona...
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('admin/dashboard.page_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('admin/dashboard.breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection

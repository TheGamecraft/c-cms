@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Messages au staff<a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
            @foreach ($jobs as $job)
                <li class="nav-item nav-stacked">
                    <a class="nav-link" id="{{$job->name}}-tab" data-toggle="tab" href="#{{$job->name}}" role="tab" aria-controls="{{$job->name}}" aria-selected="false">{{$job->name}}</a>
                </li>
            @endforeach
            </ul>
                <div class="tab-content pl-3 p-1" id="myTabContent">
                    @foreach ($jobs as $job)
                        <div class="tab-pane fade" id="{{$job->name}}" role="tabpanel" aria-labelledby="{{$job->name}}-tab">
                            <div class="row form-group">
                                <div class="col-12">{{ $job->desc }}
                                <small class="form-text text-muted">Description du poste</small></div>
                            </div>
                            <hr>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Niveau d'accès</label></div>
                                <div class="col col-md-3">
                                    <select name="{{$job->id}}_event_type" name="{{$job->id}}_event_type" class="form-control">
                                        @switch($job->acces_level)
                                            @case(1)
                                                <option value="1">Staff</option>
                                                @break
                                            @case(2)
                                                <option value="2">Officer</option>
                                                @break
                                            @default
                                                <option value="0">Cadet</option>    
                                        @endswitch
                                        <option value="0">Cadet</option>
                                        <option value="1">Staff</option>
                                        <option value="2">Officer</option>                  
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Horaire détaillé</label><small class="form-text text-muted">|| {{$job->schedule_see}} || L'utilisateur avec ce poste peut t'il consulter les détails de l'horaire</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_schedule_see" name="{{$job->id}}_schedule_see" class="switch-input" @if($job->schedule_see == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Modification horaire</label><small class="form-text text-muted">|| {{$job->schedule_edit}} || L'utilisateur avec ce poste peut t'il ajouter, modifier et supprimer des événements a l'horaire</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_schedule_edit" name="{{$job->id}}_schedule_edit" class="switch-input" @if($job->schedule_edit == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Notifiation horaire</label><small class="form-text text-muted">|| {{$job->schedule_notify}} || L'utilisateur avec ce poste doit t-il recevoir des notifiation pour tous les changements a l'horaire.</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_schedule_notify" name="{{$job->id}}_schedule_notify" class="switch-input" @if($job->schedule_notify == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Consultation messages</label><small class="form-text text-muted">|| {{$job->message_see}} || L'utilisateur avec ce poste peut t'il consulter les messages</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_message_see" name="{{$job->id}}_message_see" class="switch-input" @if($job->message_see == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Modification messages</label><small class="form-text text-muted"> L'utilisateur avec ce poste peut t'il ajouter, modifier et supprimer des messages</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_message_edit" name="{{$job->id}}_message_edit" class="switch-input" @if($job->message_edit == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Notifiation messages</label><small class="form-text text-muted"> L'utilisateur avec ce poste doit t-il recevoir des notifiation pour tous les changements en lien avec les messages.</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_message_notify" name="{{$job->id}}_message_notify" class="switch-input" @if($job->message_notify == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Modification article</label><small class="form-text text-muted"> L'utilisateur avec ce poste peut t'il ajouter, modifier et supprimer des articles</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_paper_edit" name="{{$job->id}}_paper_edit" class="switch-input" @if($job->paper_edit == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Publication articles</label><small class="form-text text-muted"> L'utilisateur avec ce poste peux t-il autoriser la publication ou appouver des changements a un articles</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_paper_publish" name="{{$job->id}}_paper_publish" class="switch-input" @if($job->paper_publish == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Notifiation articles</label><small class="form-text text-muted"> L'utilisateur avec ce poste doit t-il recevoir des notifiation pour tous les changements en lien avec les articles.</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_paper_notify" name="{{$job->id}}_paper_notify" class="switch-input" @if($job->paper_notify == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Consultation inventaire</label><small class="form-text text-muted"> L'utilisateur avec ce poste peut t'il consulter l'inventaire</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_inventory_see" name="{{$job->id}}_inventory_see" class="switch-input" @if($job->inventory_see == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Modification inventaire</label><small class="form-text text-muted"> L'utilisateur avec ce poste peut t'il ajouter, modifier et supprimer l'inventaire</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_inventory_edit" name="{{$job->id}}_inventory_edit" class="switch-input" @if($job->inventory_edit == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Notifiation inventaire</label><small class="form-text text-muted"> L'utilisateur avec ce poste doit t-il recevoir des notifiation pour tous les changements en lien avec les articles.</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_inventory_notify" name="{{$job->id}}_inventory_notify" class="switch-input" @if($job->inventory_notify == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Modification utilisateur</label><small class="form-text text-muted"> L'utilisateur avec ce poste peut t'il ajouter, modifier et supprimer des utilisateurs</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_user_edit" name="{{$job->id}}_user_edit" class="switch-input" @if($job->user_edit == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Notifiation utilisateur</label><small class="form-text text-muted"> L'utilisateur avec ce poste doit t-il recevoir des notifiation pour tous les changements en lien avec les utilisateurs.</small></div>
                                <div class="col col-md-3" style="margin: auto;">
                                    <label for="disabled-input" class=" form-control-label"></label>
                                    <label class="switch switch-3d switch-primary mr-3" style="margin-left: 3rem;"><input name="{{$job->id}}_user_notify" name="{{$job->id}}_user_notify" class="switch-input" @if($job->user_notify == "1") checked="true" @endif type="checkbox"><span class="switch-label"></span><span class="switch-handle"></span></label>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                    <div class="card-footer">
                        
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('admin/dashboard.page_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('admin/dashboard.breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')


<script type="text/javascript">
    (function($) {
            $(document).ready(function() {
                $('#log-data').DataTable({
                    "ordering": false,
                    "order": [],
                    "lengthMenu": [[3, 4, -1], [9, 12, "All"]],
                });
            } );
        })(jQuery);

    function deleteEvent(pid){
            swal({
                title: 'Êtes vous certain ?',
                text: "Vous ne pourrez annuler cette action",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non'
                }).then((result) => {
                if (result.value) {

                    (function($) {
                        $.post('/api/job/delete?api_token='+api_token, { id: pid } , function(data) {
                            console.log('Delete');
                        });

                        
                    })(jQuery);

                    swal(
                    'Supprimé!',
                    "Le message a été supprimé",
                    'success'
                    ).then((result) => {
                    if (result.value) {
                        location.reload();
                        }
                    })
                }
                })
       }
</script>
   <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=r82pabvd9arn3fjb1e2fsolf2xpixuv4hwfwart4cf1fb7mx"></script>
   @foreach ($jobs as $job)
       <script>
        tinymce.init({
        selector: '#desc_'+<?php echo $job->id?>,
        branding: false,
        menubar: 'edit view format'
        });
        </script>
   @endforeach
   
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationController extends Controller
{
    public function markAsRead($id)
    {
        $n = DatabaseNotification::find($id);
        $n->read_at = date('Y-m-d h:i:s');
        $n->save();
    }

    public function markAllAsRead()
    {
        $notifications = \Auth::user()->unreadNotifications;
        foreach ($notifications as $n)
        {
            $n->read_at = date('Y-m-d h:i:s');
            $n->save();
        }
        return $notifications;
    }
}

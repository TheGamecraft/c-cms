@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Gestion de l'inventaire <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a></strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group">
                            <a class="btn btn-primary" href="/admin/inventory/management/category/add">Ajouter une catégorie</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table id="log-data" class="table table-striped table-bordered dt-responsive w-100">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Nom</th>
                                <th>Description</th>
                                <th>Disponible pour l'instruction</th>
                                <th>Réservé a l'officer d'approvisionnement</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td class="text-center" style="width: 3.5rem;">{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{!! $category->desc !!}</td>
                                    <td class="text-center" style="width: 10rem;">
                                        @if($category->is_training == 1)
                                            <i class="fas fa-check-square text-success fa-2x"></i>
                                        @else
                                            <i class="far fa-square text-warning fa-2x"></i>
                                        @endif
                                    </td>
                                    <td class="text-center" style="width: 10rem;"#>
                                        @if($category->is_op_appro == 1)
                                            <i class="fas fa-check-square text-success fa-2x"></i>
                                        @else
                                            <i class="far fa-square text-warning fa-2x"></i>
                                        @endif
                                    </td>
                                    <td style="width: 12%;">
                                        <div class="btn-group">
                                            <a href="/admin/inventory/management/category/edit/{{$category->id}}" type="button" class="btn btn-secondary"><i class="fa fa-cog"></i>&nbsp; Modifier</a>
                                            <a type="button" class="btn btn-danger" onclick="Delete({{$category->id}});"><i class="fa fa-times-circle" style="color:white;"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
<script>
    (function($) {
        $(document).ready(function() {
            $('#log-data').DataTable({
                "order": [[ 2, "asc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            });
        } );
    })(jQuery);

    function Delete(pID) {
        swal({
            title: 'Êtes vous certain de vouloir supprimer la catégorie?',
            text: "Vous ne pourrez pas annuler cette action",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui',
            cancelButtonText: 'Non'
        }).then((result) => {
            if (result.value) {

                (function($) {
                    $.post('/api/itemcategory/delete/'+pID+'?api_token='+api_token, function(data) {
                        console.log('Delete');
                    });


                })(jQuery);

                swal(
                    'Supprimé!',
                    "La catégorie a été supprimé",
                    'success'
                ).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
            }
        })
    }
</script>
</script>
@endsection

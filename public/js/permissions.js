function switchPermissionSwitch(rank,valeur)
{
    switch (valeur) {
        case 'close':
            $('#'+rank).val('-1');
            $('#'+rank+'-close').addClass('btn-danger');
            $('#'+rank+'-close').removeClass('btn-outline-danger');

            $('#'+rank+'-slash').removeClass('btn-warning');
            $('#'+rank+'-slash').addClass('btn-outline-warning');

            $('#'+rank+'-plus').addClass('btn-outline-success');
            $('#'+rank+'-plus').removeClass('btn-success');
            break;

        case 'slash':
            $('#'+rank).val('0');
            $('#'+rank+'-close').addClass('btn-outline-danger');
            $('#'+rank+'-close').removeClass('btn-danger');

            $('#'+rank+'-slash').addClass('btn-warning');
            $('#'+rank+'-slash').removeClass('btn-outline-warning');

            $('#'+rank+'-plus').addClass('btn-outline-success');
            $('#'+rank+'-plus').removeClass('btn-success');
            break;

        case 'plus':
            $('#'+rank).val('1');
            $('#'+rank+'-close').addClass('btn-outline-danger');
            $('#'+rank+'-close').removeClass('btn-danger');

            $('#'+rank+'-slash').removeClass('btn-warning');
            $('#'+rank+'-slash').addClass('btn-outline-warning');

            $('#'+rank+'-plus').addClass('btn-success');
            $('#'+rank+'-plus').removeClass('btn-outline-success');
            break;
    }
}

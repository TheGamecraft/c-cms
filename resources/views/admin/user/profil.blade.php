@extends('layouts.admin.main')

@section('content')
<div class="row">
    <div @if ($user->id == \Auth::User()->id) class="col-md-12 col-lg-7 col-xl-8" @else class="col-sm-12" @endif>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mx-auto d-block">
                            <img class="rounded-circle mx-auto d-block" src="/assets/admin/images/avatar/user-{{\Auth::User()->avatar}}.jpg" alt="Card image cap" style="height:12rem">
                            <h5 class="text-sm-center mt-2 mb-1">{{\Auth::User()->fullname()}}</h5>
                            <div class="location text-sm-center"><i class="fas fa-id-card-alt"></i> {{\Auth::User()->rank->name}}</div>
                        </div>
                    </div>
                    <div class="col-sm-6 text-center">
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <p>{{$user->job->name}}</p>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <p>
                                Genre<br>
                                @if ($user->sexe == "m")
                                    <i class="fa fa-mars fa-2x" aria-hidden="true" style="color:#0080FF"></i>
                                @elseif($user->sexe == "f")
                                    <i class="fa fa-venus fa-2x" aria-hidden="true" style="color:#FE2EF7"></i>
                                @else
                                    <i class="fa fa-genderless fa-2x" aria-hidden="true"></i>
                                @endif
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                Age<br>
                                @if ($user->age != "Inconnu")
                                {{$user->age}} ans
                                @else
                                Inconnu
                                @endif
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>Téléphone<br>
                                    {{ $user->telephone }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>Adresse<br>
                                {{$user->adress}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-10 offset-md-1 mt-3">
                        <p class="text-center">Dernières activitées de l'utilisateur</p>
                        <table class="table table-striped">
                            <thead class="thead-default">
                            <tr>
                                <th>Horodatage</th>
                                <th>Résultat</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($user->logs()->take(6)->get() as $log)
                                <tr>
                                    <td scope="row">{{$log->created_at}}</td>
                                    <td><span class="badge badge-pill badge-{{$log->result}} text-uppercase">{{$log->result}}</span></td>
                                    <td>{{$log->event}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($user->id == \Auth::User()->id)
        <div class="col-md-12 col-lg-5 col-xl-4">
            <div class="card">
                <div class="card-body">
                    <a name="changepsw" id="changepsw" class="btn btn-warning btn-block" href="/admin/profil/password" role="button">Modifier mon mot de passe</a>
                    <a class="btn btn-secondary btn-block" href="/admin/profil/avatar" role="button">Modifier ma photo de profil</a>
                    <a class="btn btn-secondary btn-block" href="/admin/profil/telephone" role="button">Modifier mon numéro de téléphone</a>
                    <a class="btn btn-secondary btn-block" href="/admin/profil/adress" role="button">Modifier mon adresse</a>
                    <button disabled class="btn btn-primary btn-block" href="/admin/profil/preference" role="button">Modifier mes préférences</button>
                    <a class="btn btn-danger btn-block" href="/logout" role="button">Déconnexion</a>
                </div>
            </div>
        </div>
    @endif
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Profil d'utilisateur</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Utilisateur/Profil</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
   
@endsection

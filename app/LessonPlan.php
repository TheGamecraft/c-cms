<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\LessonPlan
 *
 * @property int $id
 * @property int $user_id
 * @property string $file
 * @property int|null $course_id
 * @property string|null $desc
 * @property string|null $comment
 * @property int $approved
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Course|null $course
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LessonPlan whereUserId($value)
 * @mixin \Eloquent
 */
class LessonPlan extends Model
{
    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}

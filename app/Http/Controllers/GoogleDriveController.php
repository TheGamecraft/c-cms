<?php

namespace App\Http\Controllers;

use App\GoogleDriveFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Psy\Util\Str;
use Symfony\Component\Console\Input\Input;

class GoogleDriveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $dir
     * @param bool $recursive
     * @return \Illuminate\Http\Response
     */
    public function index($folder = '')
    {
        return view('admin.files.Google Drive.index',['folder' => $folder]);
    }

    public function indexFolder($folder = '')
    {
        return view('admin.files.Google Drive.index',['folder' => $folder,'mode' => 'folder']);
    }

    /**
     * Show the form for creating a new file.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createFile()
    {
        Storage::cloud()->put(\request('currentDir'.'/'.\request('name')), '');
        return back()->with('success','Fichier créer avec succès');
    }

    public function createFolder()
    {
        Storage::cloud()->makeDirectory(\request('currentDir').'/'.\request('name'));
        return back()->with('success','Dossier créer avec succès');
    }

    public function uploadFile()
    {
        Storage::cloud()->putFileAs(\request('currentDir'),\request()->file('fichier'),\request()->file('fichier')->getClientOriginalName());
        return back()->with('success','Fichier téléversé avec succès');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public function store(Request $request)
    {
        $path = Storage::cloud()->makeDirectory('Test Dir');
        dd($path);
    }

    /**
     * Display the specified resource.
     *
     * @param $filename
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function show()
    {
        $filename = \request('file');
        $contents = collect(Storage::cloud()->listContents('/1nEe35-GvLX598RketTI-UoaOxIMNxfka', true));

        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!

        //return $file; // array with file info

        $rawData = Storage::cloud()->get($file['path']);

        return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename='$filename'");
    }

    public function showMetadata($dir,$file)
    {
        if ($dir == 'root')
        {
            $dir = '/';
        }
        $contents = collect(Storage::cloud()->listContents($dir, true));

        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($file, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($file, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!

        return $file;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPathArray()
    {
        $contents = collect(Storage::cloud()->listContents('/', true));
        return json_encode($contents->where('type', '=', 'dir'));
    }

    public function getPath($folder)
    {
        $contents = collect(Storage::cloud()->listContents('/', true));
        $dir = collect($contents->where('type', '=', 'dir'));
        foreach ($dir as $d)
        {
            if($d['basename'] == $folder)
            {
                return $d['dirname'];
            }
        }
    }

    public function getFile()
    {
        if(\request('f'))
        {
            $dir = '/';
            if (\request('d') && urldecode(\request('d')) != '')
            {
                $dir = \request('d');
            }
            $filename = urldecode(\request('f'));

            $recursive = false; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $file = $contents
                ->where('type', '=', 'file')
                ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                ->first(); // there can be duplicate file names!
            if ($file == null)
            {
                $filename = \request('f');
                $file = $contents
                    ->where('type', '=', 'file')
                    ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                    ->first(); // there can be duplicate file names!
            }

            $rawData = Storage::cloud()->get($file['path']);
            $filename = urlencode($filename);
            return response($rawData, 200)
                ->header('Content-Type', $file['mimetype'])
                ->header('Content-Disposition', "attachment; filename=$filename");
        }
        else
        {
            abort(404);
        }
    }

    public function deleteFile()
    {
        if(\request('f'))
        {
            $dir = '/';
            if (\request('d') && urldecode(\request('d')) != '')
            {
                $dir = \request('d');
            }
            $filename = urldecode(\request('f'));

            $recursive = false; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $file = $contents
                ->where('type', '=', 'file')
                ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
                ->first(); // there can be duplicate file names!

            Storage::cloud()->delete($file['path']);

            return back()->with('success','Fichier supprimé avec succès');
        }
        else
        {
            abort(404);
        }
    }

    public function deleteDir()
    {
        $directoryID = \request('d');

        // Now find that directory and use its ID (path) to delete it
        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $directory = $contents
            ->where('type', '=', 'dir')
            ->where('basename', '=', $directoryID)
            ->first();

        Storage::cloud()->deleteDirectory($directory['path']);

        return back()->with('success','Dossier supprimé avec succès');
    }

    public function list($folder = 'root')
    {
        $recursive = false; // Get subdirectories also?
        $perm = ['r' => GoogleDriveFile::getPermForAuthUser($folder,'r'),'w' => GoogleDriveFile::getPermForAuthUser($folder,'w'),'p' => GoogleDriveFile::getPermForAuthUser($folder,'p')];
        if ($folder == 'root')
        {
            $contents = collect(Storage::cloud()->listContents('/', $recursive));
        }
        else
        {
            $contents = collect(Storage::cloud()->listContents($folder, $recursive));
        }

        return view('admin.files.Google Drive.explorer',[
            'directories' => $contents->where('type', '=', 'dir')->sortByDesc('name'),
            'files' => $contents->where('type', '=', 'file'),
            'currentDir' => $folder,
            'permission' => $perm]);
    }

    public function checkFileSystem()
    {
        $error = [];
        if(\App\Config::getData('is_Google_Drive_enabled') == "true")
        {
            if (GoogleDriveFile::checkConfig())
            {
                $structure = $this->getFileStructure();
                $this->checkStructure($structure,'/','/',$error);
            }
            else
            {
                $error = ['Il y a un probleme avec vos configurations'];
            }
        }
        else
        {
            $error = ['Google Drive n\'est pas actif'];
        }
        return $error;
    }

    public function checkStructure()
    {
        $structure = $this->getFileStructure();

        foreach ($structure as $directory => $value)
        {
            $basename = GoogleDriveFile::findByPathInDrive($directory);
            if ($basename == false)
            {
                $basename = GoogleDriveFile::createByPathInDrive($directory);
            }

            $googleDriveFile = GoogleDriveFile::findByPath($directory);
            if ($googleDriveFile == null)
            {
                $googleDriveFile = new GoogleDriveFile();
                $googleDriveFile->id = $basename;
                $googleDriveFile->type = 'directory';
                $googleDriveFile->rank_permission = $value['rank'];
                $googleDriveFile->job_permission = [];
                $googleDriveFile->user_permission = [];
                $googleDriveFile->path = $directory;
                $name = explode('/',$directory);
                $googleDriveFile->name = $name[count($name)-1];
                $googleDriveFile->save();
            }
            else
            {
                if ($googleDriveFile->id != $basename)
                {
                    $googleDriveFile->id = $basename;
                    $googleDriveFile->save();
                }
            }
        }
    }

    public function editPermission($folder)
    {
        $f = GoogleDriveFile::find($folder);
        if ($f == null)
        {
            $metadata = \Storage::cloud()->getMetadata($folder);
            $f = new GoogleDriveFile();
            $f->id = $folder;
            $f->type = 'directory';
            $f->name = $metadata['name'];
            $f->rank_permission = [1 => 'rwp'];
            $f->job_permission = [];
            $f->user_permission = [];
            $f->path = $this->recreatePath($folder);
            $f->save();
        }
        return view('admin.files.Google Drive.permission',['dir' => $f]);
    }

    public function getFileStructure()
    {
        return collect([
            '.Systeme' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Systeme/.Fichier' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Systeme/.Fichier/.MessageDeLaSemaine' => [
                'rank' => [1 => 'rwp',0 => 'r'],
                'job' => [],
                'user' => []
            ],
            '.Systeme/.Fichier/.PlanDeCours' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé/.Cadet' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé/.ETAMAS' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé/.Officier' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé/.Staff' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé/.Staff/.Guide' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Privé/.Staff/.Instruction' => [
                'rank' => [1 => 'rwp'],
                'job' => [],
                'user' => []
            ],
            '.Publique' => [
                'rank' => [1 => 'rwp',0 => 'r'],
                'job' => [],
                'user' => []
            ],
            '.Publique/.Fichier' => [
                'rank' => [1 => 'rwp',0 => 'r'],
                'job' => [],
                'user' => []
            ],
            '.Publique/.Image' => [
                'rank' => [1 => 'rwp',0 => 'r'],
                'job' => [],
                'user' => []
            ],
        ]);
    }

    public function listLockDirectory($d)
    {
        $contents = collect(\Storage::cloud()->listContents($d, false));
        $dir = $contents->where('type', '=', 'dir');
        $dir = $dir->where('filename','=','');

        return $dir;
    }

    public function recreatePath($folder)
    {
        $path = [];
        $name = [];
        $directories = collect(json_decode($this->getPathArray(),true));
        foreach ($directories as $dir)
        {
            $path[$dir['basename']] = $dir['dirname'];
            $name[$dir['basename']] = $dir['name'];
        }
        $realPath = $name[$folder];
        $foo = $folder;
        while ($foo != "")
        {
            $bar = explode('/',$path[$foo]);
            $foo = $bar[count($bar)-1];
            if ($foo != "")
            {
                $realPath = $name[$foo].'/'.$realPath;
            }
        }
        return $realPath;
    }

    public function editPermissionModal($folder,$subject,$id)
    {
        $dir = GoogleDriveFile::find($folder);
        $foo = null;
        $perm = null;
        if ($subject == 'rank')
        {
            if ($id == 0)
            {
                $foo = new \App\Rank();
                $foo->name = "Utilisateur non authentifié";
                $foo->id = 0;
                if (isset($dir->rank_permission[$id]))
                {
                    $perm = $dir->rank_permission[$id];
                }
                else
                {
                    $perm = "";
                }
            }
            else
            {
                $foo = \App\Rank::find($id);
                $perm = $dir->rank_permission[$id];
            }
        }
        elseif ($subject == 'job')
        {
            $foo = \App\Job::find($id);
            $perm = $dir->job_permission[$id];
        }
        else
        {
            $foo = \App\User::find($id);
            $perm = $dir->user_permission[$id];
        }
        return view('admin.files.Google Drive.permission.edit',['folder' => $dir,'subject' => $foo,'perm' => $perm,'s' => $subject]);
    }

    public function addPermissionModal($folder,$subject)
    {
        $dir = GoogleDriveFile::find($folder);
        $list = null;
        if ($subject == 'rank')
        {
            $list = \App\Rank::all();
        }
        elseif ($subject == 'job')
        {
            $list = \App\Job::all();
        }
        else
        {
            $list = \App\User::all();
        }
        return view('admin.files.Google Drive.permission.add',['folder' => $dir,'list' => $list,'s' => $subject]);
    }

    public function patchPermission(Request $request,$folder,$subject,$id)
    {
        $f = GoogleDriveFile::find($folder);
        $permstring = '';
        if (isset($request->read))
        {
            if ($request->read == 'on')
            {
                $permstring = $permstring.'r';
            }
        }
        if (isset($request->write))
        {
            if ($request->write == 'on')
            {
                $permstring = $permstring.'w';
            }
        }
        if (isset($request->perm))
        {
            if ($request->perm == 'on')
            {
                $permstring = $permstring.'p';
            }
        }
        if ($subject == 'rank')
        {
            $temp = $f->rank_permission;
            $temp[$id] = $permstring;
            $f->rank_permission = $temp;
        }
        if ($subject == 'job')
        {
            $temp = $f->job_permission;
            $temp[$id] = $permstring;
            $f->job_permission = $temp;
        }
        if ($subject == 'user')
        {
            $temp = $f->user_permission;
            $temp[$id] = $permstring;
            $f->user_permission = $temp;
        }
        $f->save();
        return redirect()->back()->with('success','Modification enregistré avec succès');
    }

    public function addPermission(Request $request,$folder,$subject)
    {
        $f = GoogleDriveFile::find($folder);
        $permstring = '';
        if (isset($request->read))
        {
            if ($request->read == 'on')
            {
                $permstring = $permstring.'r';
            }
        }
        if (isset($request->write))
        {
            if ($request->write == 'on')
            {
                $permstring = $permstring.'w';
            }
        }
        if (isset($request->perm))
        {
            if ($request->perm == 'on')
            {
                $permstring = $permstring.'p';
            }
        }
        if ($subject == 'rank')
        {
            $temp = $f->rank_permission;
            $temp[$request->id] = $permstring;
            $f->rank_permission = $temp;
        }
        if ($subject == 'job')
        {
            $temp = $f->job_permission;
            $temp[$request->id] = $permstring;
            $f->job_permission = $temp;
        }
        if ($subject == 'user')
        {
            $temp = $f->user_permission;
            $temp[$request->id] = $permstring;
            $f->user_permission = $temp;
        }
        $f->save();
        return redirect()->back()->with('success','Modification enregistré avec succès');
    }

    public function deletePermission($folder,$subject,$id)
    {
        $f = GoogleDriveFile::find($folder);
        if ($subject == 'rank')
        {
            $temp = $f->rank_permission;
            unset($temp[$id]);
            $f->rank_permission = $temp;
        }
        if ($subject == 'job')
        {
            $temp = $f->job_permission;
            unset($temp[$id]);
            $f->job_permission = $temp;
        }
        if ($subject == 'user')
        {
            $temp = $f->user_permission;
            unset($temp[$id]);
            $f->user_permission = $temp;
        }
        $f->save();
        return redirect()->back()->with('success','Modification enregistré avec succès');
    }

}

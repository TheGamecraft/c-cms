<?php
    $lang = str_replace('_', '-', app()->getLocale());
    setlocale( LC_ALL, $lang.'_'.strtoupper($lang).'.utf8','fra'); 
?>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<title>{{ ($breadcrumb = Breadcrumbs::current()) ? $breadcrumb->title : 'Espace Administration' }} - C-CMS</title>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="user-id" content="{{ Auth::user()->id }}">

<!-- Favicon, Icon and Font -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<script src="/js/plugins/fontawesome/js/all.min.js"></script>
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

<!-- Material Dashboard CSS -->
<link href="/css/material-dashboard.css" rel="stylesheet">

<!-- Plugin CSS -->
<link href='/assets/fullcalendar/core/main.css' rel='stylesheet' />
<link href='/assets/fullcalendar/daygrid/main.css' rel='stylesheet' />
<link rel="stylesheet" href="/js/plugins/trumbowyg/ui/trumbowyg.min.css">
<link rel="stylesheet" href="/js/plugins/trumbowyg/plugins/colors/ui/trumbowyg.colors.min.css">
<link rel="stylesheet" href="/js/plugins/trumbowyg/plugins/emoji/ui/trumbowyg.emoji.min.css">
<link rel="stylesheet" href="/css/contextLoader.min.css">
<link rel="stylesheet" href="/css/Chart.min.css">

<link rel="stylesheet" href="/js/plugins/fontawesome-icon-picker/fontawesome-iconpicker.css">
<link rel="stylesheet" href="/css/monolith.min.css"/>
<link rel="stylesheet" href="/css/contextLoader.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="/css/custom.css">

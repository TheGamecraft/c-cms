<?php

use Illuminate\Database\Seeder;

class PublicPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('public_pages')->insert([
            [
                'name' => 'qui-somme-nous',
                'banner' => 'https://via.placeholder.com/1920x1080',
                'header' => '<h1>Qui somme nous</h1>',
                'body' => 'Incididunt elit tempor ea exercitation consequat qui reprehenderit ipsum. Ipsum occaecat nisi qui id eu nostrud proident incididunt minim et consequat excepteur. Proident dolor irure ullamco duis cillum duis cupidatat deserunt. Enim exercitation laboris excepteur proident anim amet ex ex deserunt in irure excepteur ullamco. Ad officia velit esse aliquip laborum aute aliqua nostrud qui labore eiusmod quis irure velit. Consequat in nulla consectetur ullamco elit eu eu sunt qui cupidatat eu tempor. Enim tempor do ipsum officia et qui commodo eiusmod adipisicing occaecat laborum.Proident dolor id eu excepteur adipisicing. Pariatur est fugiat esse in dolor fugiat elit amet nisi voluptate magna do nostrud voluptate. Reprehenderit cupidatat consectetur fugiat sit.Culpa proident Lorem veniam culpa ipsum duis ea est labore consectetur sunt. Non duis voluptate magna aliquip aliqua ullamco irure esse incididunt mollit velit esse deserunt ex. Dolor commodo anim pariatur pariatur labore culpa occaecat. Dolore laboris sunt laborum velit Lorem magna. Minim sit et veniam non ullamco aliquip consectetur et ut velit. Ipsum ea eu laboris incididunt exercitation laboris ipsum nulla sunt. Pariatur commodo fugiat irure incididunt in esse nisi ut. Sunt amet do sunt velit velit nostrud eu nisi. Anim in elit veniam consequat sunt tempor consectetur amet labore consectetur occaecat. Ad aute nulla ut veniam.',
            ],
            [
                'name' => 'nos-activites',
                'banner' => 'https://via.placeholder.com/1920x1080',
                'header' => '<h1>Nos activités</h1>',
                'body' => 'Incididunt elit tempor ea exercitation consequat qui reprehenderit ipsum. Ipsum occaecat nisi qui id eu nostrud proident incididunt minim et consequat excepteur. Proident dolor irure ullamco duis cillum duis cupidatat deserunt. Enim exercitation laboris excepteur proident anim amet ex ex deserunt in irure excepteur ullamco. Ad officia velit esse aliquip laborum aute aliqua nostrud qui labore eiusmod quis irure velit. Consequat in nulla consectetur ullamco elit eu eu sunt qui cupidatat eu tempor. Enim tempor do ipsum officia et qui commodo eiusmod adipisicing occaecat laborum.Proident dolor id eu excepteur adipisicing. Pariatur est fugiat esse in dolor fugiat elit amet nisi voluptate magna do nostrud voluptate. Reprehenderit cupidatat consectetur fugiat sit.Culpa proident Lorem veniam culpa ipsum duis ea est labore consectetur sunt. Non duis voluptate magna aliquip aliqua ullamco irure esse incididunt mollit velit esse deserunt ex. Dolor commodo anim pariatur pariatur labore culpa occaecat. Dolore laboris sunt laborum velit Lorem magna. Minim sit et veniam non ullamco aliquip consectetur et ut velit. Ipsum ea eu laboris incididunt exercitation laboris ipsum nulla sunt. Pariatur commodo fugiat irure incididunt in esse nisi ut. Sunt amet do sunt velit velit nostrud eu nisi. Anim in elit veniam consequat sunt tempor consectetur amet labore consectetur occaecat. Ad aute nulla ut veniam.',
            ],
            [
                'name' => 'devenir-cadet',
                'banner' => 'https://via.placeholder.com/1920x1080',
                'header' => '<h1>Devenir cadet</h1>',
                'body' => 'Incididunt elit tempor ea exercitation consequat qui reprehenderit ipsum. Ipsum occaecat nisi qui id eu nostrud proident incididunt minim et consequat excepteur. Proident dolor irure ullamco duis cillum duis cupidatat deserunt. Enim exercitation laboris excepteur proident anim amet ex ex deserunt in irure excepteur ullamco. Ad officia velit esse aliquip laborum aute aliqua nostrud qui labore eiusmod quis irure velit. Consequat in nulla consectetur ullamco elit eu eu sunt qui cupidatat eu tempor. Enim tempor do ipsum officia et qui commodo eiusmod adipisicing occaecat laborum.Proident dolor id eu excepteur adipisicing. Pariatur est fugiat esse in dolor fugiat elit amet nisi voluptate magna do nostrud voluptate. Reprehenderit cupidatat consectetur fugiat sit.Culpa proident Lorem veniam culpa ipsum duis ea est labore consectetur sunt. Non duis voluptate magna aliquip aliqua ullamco irure esse incididunt mollit velit esse deserunt ex. Dolor commodo anim pariatur pariatur labore culpa occaecat. Dolore laboris sunt laborum velit Lorem magna. Minim sit et veniam non ullamco aliquip consectetur et ut velit. Ipsum ea eu laboris incididunt exercitation laboris ipsum nulla sunt. Pariatur commodo fugiat irure incididunt in esse nisi ut. Sunt amet do sunt velit velit nostrud eu nisi. Anim in elit veniam consequat sunt tempor consectetur amet labore consectetur occaecat. Ad aute nulla ut veniam.',
            ],
        ]);
    }
}

@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Modifier un utilisateur <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <form action="/admin/user/edit/{{$user->id}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="firstname">Prénom</label>
                            <input type="text" class="form-control" name="firstname" id="firstname"
                                   aria-describedby="helpId" placeholder="John" required value="{{$user->firstname}}">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="lastname">Nom de famille</label>
                            <input type="text" class="form-control" name="lastname" id="lastname"
                                   aria-describedby="helpId" placeholder="Doe" required value="{{$user->lastname}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                   aria-describedby="emailHelp" placeholder="exemple@exvps.ca" value="{{$user->email}}" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">Confirmer l'addresse email</label>
                            <input type="email" class="form-control" name="emailc" id="emailc"
                                   aria-describedby="emailHelp" placeholder="exemple@exvps.ca"
                                   data-parsley-equalto="#email" required
                                   data-parsley-error-message="Les emails ne sont pas identique" value="{{$user->email}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="adresse">Adresse</label>
                            <input type="text" class="form-control" name="adresse" id="adresse"
                                   aria-describedby="helpId" placeholder="14 ave Des Rue, Rimouski" value="{{$user->adress}}">
                            <small id="helpId" class="form-text text-muted"></small>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="telephone">Telephone</label>
                            <input type="tel"
                                   class="form-control" name="telephone" id="telephone" aria-describedby="helpId"
                                   placeholder="213-546-5401" pattern="([0-9]{3}-[0-9]{3}-[0-9]{4})*"
                                   data-parsley-error-message="Le numéro de téléphone est invalide" value="{{$user->telephone}}">
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sexe">Sexe</label>
                            <select class="form-control selectpicker" data-style="btn btn-link" name="sexe"
                                    id="sexe" required>
                                <option value="m" @if($user->sexe == 'm') selected @endif >Homme</option>
                                <option value="f" @if($user->sexe == 'm') selected @endif >Femme</option>
                                <option value="a" @if($user->sexe == 'm') selected @endif >Autre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group" style="margin-top: 26px;">
                            <label for="age" style="margin-top: -7px;">Age</label>
                            <input type="number" class="form-control" name="age" id="age" aria-describedby="helpId"
                                   placeholder="Age" min="0" max="100" style="padding-top: 17px;">
                            <small id="helpId" class="form-text text-muted"></small>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="rank">Grade</label>
                            <select class="form-control selectpicker" data-style="btn btn-link" name="rank"
                                    id="rank" required>
                                @foreach ($RankList as $rank)
                                    <option value="{{$rank->id}}" @if($user->rank_id == $rank->id) selected @endif >{{$rank->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="job">Poste</label>
                            <select class="form-control selectpicker" data-style="btn btn-link" name="job" id="job"
                                    required>
                                @foreach ($JobsList as $job)
                                    <option value="{{$job->id}}" @if($user->job_id == $job->id) selected @endif>{{$job->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="psw">Mot de passe</label>
                            <input type="password" class="form-control" name="psw" id="psw"
                                   placeholder="Supermotdepasse" data-parsley-password>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="pswc">Confirmer le mot de passe</label>
                            <input type="password" class="form-control" name="pswc" id="pswc"
                                   placeholder="Supermotdepasse" data-parsley-equalto="#psw"
                                   data-parsley-error-message="Les mot de passe ne sont pas identique">
                        </div>
                    </div>
                </div>
                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_scripts')
    <script>
        $('#form').parsley();
    </script>
@endsection

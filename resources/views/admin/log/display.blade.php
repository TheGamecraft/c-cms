@extends('layouts.admin.main')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Activitées des utilisateurs <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </a></strong>
        </div>
        <div class="card-body">
            <div class="material-datatables ">
                <table id="log-data" class="table table-striped dt-responsive w-100 table-no-bordered table-hover dataTable dtr-inline">
                    <thead>
                    <tr>
                        <th>Horodatage</th>
                        <th>Type</th>
                        <th>Résultat</th>
                        <th>Utilisateur</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($log as $item)
                            <tr>
                                <td>{{$item->created_at}}</td>
                                <th><span class="badge badge-pill badge-{{$item->typeColor()}}">{{$item->type}}</span></th>
                                <th><span class="badge badge-pill badge-{{$item->result}} text-uppercase">{{$item->result}}</span></th>
                                @if($item->user_id == 0)
                                    <td>Utilisateur non authentifié</td>
                                @elseif(\App\User::find($item->user_id))
                                    <td>{{\App\User::find($item->user_id)->fullname()}}</td>
                                @else
                                    <td>Utilisateur inconnu</td>
                                @endif
                                <td><a href="">{{$item->event}}</a></td>
                            </tr>
                        @endforeach
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>{{ trans('admin/dashboard.page_title')}}</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">{{ trans('admin/dashboard.breadcrumb')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')

<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('#log-data').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            });
        } );
    })(jQuery);
</script>
@endsection

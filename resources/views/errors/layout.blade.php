<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>@yield('title') - C-CMS</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
    <script src="/js/plugins/fontawesome/js/all.min.js"></script>

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            padding: 0;
            margin: 0;
        }

        #notfound {
            position: relative;
            height: 100vh;
        }

        #notfound .notfound {
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .notfound {
            max-width: 520px;
            width: 100%;
            line-height: 1.4;
            text-align: center;
        }

        .notfound .notfound-404 {
            position: relative;
            height: 240px;
        }

        .notfound .notfound-404 h1 {
            font-family: 'Montserrat', sans-serif;
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            font-size: 252px;
            font-weight: 900;
            margin: 0px;
            color: #262626;
            text-transform: uppercase;
            letter-spacing: -40px;
            margin-left: -20px;
        }

        .notfound .notfound-404 h1>span {
            text-shadow: -8px 0px 0px #fff;
        }

        .notfound .notfound-404 h3 {
            font-family: 'Cabin', sans-serif;
            position: relative;
            font-size: 16px;
            font-weight: 700;
            text-transform: uppercase;
            color: #262626;
            margin: 0px;
            letter-spacing: 3px;
            padding-left: 6px;
        }

        .notfound h2{
            font-family: 'Cabin', sans-serif;
            font-size: 20px;
            font-weight: 400;
            text-transform: uppercase;
            color: #000;
            margin-top: 0px;
            margin-bottom: 25px;
        }

        h3 {
            font-family: 'Cabin', sans-serif;
            font-size: 15px;
            font-weight: 400;
            text-transform: uppercase;
            color: #262626;
            margin-top: 0px;
            margin-bottom: 25px;
        }

        .help {
            float: right;
            margin-right: 8%;
            color: #262626;
        }

        .animate {
            animation: blinker 1.75s linear infinite;
        }

        img {
            height: 50px;
            margin: 8px 10px 8px 15px;
        }

        @media only screen and (max-width: 767px) {
            .notfound .notfound-404 {
                height: 200px;
            }
            .notfound .notfound-404 h1 {
                font-size: 200px;
            }
            .help {
                margin-right: 15%;
            }
            img {
                height: 40px;
            }
        }

        @media only screen and (max-width: 480px) {
            .notfound .notfound-404 {
                height: 162px;
            }
            .notfound .notfound-404 h1 {
                font-size: 162px;
                height: 150px;
                line-height: 162px;
            }
            .notfound h2 {
                font-size: 16px;
            }

            img {
                height: 30px;
            }
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }

    </style>

</head>

<body>

<div id="notfound">
    <img src="/images/C-CMS_G.png">
    <div class="notfound">
        <div class="notfound-404">
            <h3>Oops! @yield('title')</h3>
            <h1>@yield('code')</h1>
            <a target="_blank" class="animate" href="@yield('url')">
                <i class="fas fa-question-circle help"></i>
            </a>
        </div>
        <h2>@yield('message')</h2>
        <h3>@yield('error')</h3>
    </div>
</div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>

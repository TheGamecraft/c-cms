@extends('layouts.admin')

@section('content')
    
    <div>
        Creation d'un utilisateur

        <form action="/register" method="post" class="form-horizontal">

            {{ csrf_field() }}

            <div class="row form-group">
                <div class="col col-md-6">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                        <input type="text" id="input1-group1" name="input1-group1" placeholder="Prénom" class="form-control">
                    </div>
                </div>
                <div class="col col-md-6">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                        <input type="text" id="input1-group1" name="input1-group1" placeholder="Nom de famille" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-12">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                        <input type="text" id="input1-group1" name="input1-group1" placeholder="Adresse Email" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-12">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                        <input type="password" id="input1-group1" name="input1-group1" placeholder="Mot de passe" class="form-control">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
            </button>
        </form>
</div>

@endsection

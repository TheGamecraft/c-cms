<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Event
 *
 * @property int $id
 * @property string $name
 * @property string $date_begin
 * @property string $date_end
 * @property string $type
 * @property string $user_id
 * @property string $location
 * @property int $is_mandatory
 * @property int $use_weekly_msg
 * @property int $use_schedule
 * @property string $desc
 * @property array $msg
 * @property array $weekly_msg_file
 * @property array $schedule
 * @property string $calendar_color
 * @property string $calendar_icon
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Booking[] $bookings
 * @property-read int|null $bookings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCalendarColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCalendarIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDateBegin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereIsMandatory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereUseSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereUseWeeklyMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereWeeklyMsgFile($value)
 * @mixin \Eloquent
 * @property string $weekly_msg_publication_time
 * @property int $hidden
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereWeeklyMsgPublicationTime($value)
 */
class Event extends Model
{
    protected $casts = [
        'schedule' => 'array',
        'msg' => 'array',
        'weekly_msg_file' => 'array'
    ];

    public function bookings()
    {
        return $this->morphMany('App\Booking', 'bookable');
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function type()
    {
        return \App\EventType::find($this->type);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function course($p,$l)
    {
        $courses = $this->courses;

        foreach ($courses as $c)
        {
            if ($c->periode == $p && $c->level == $l)
            {
                return $c;
            }
        }
        $error = new \App\Course();
        $error->name = "Cours manquant dans la BD";
        $error->ocom = "ERREUR";
        $error->periode = $p;
        $error->level = $l;
        $error->location = "";
        $error->desc = "";
        $error->comment = "Le cours est manquant dans la base de données";
        $error->comment_officer = "Le cours est manquant dans la base de données";
        $error->event_id = $this->id;
        $error->user_id = 1;
        return $error;
    }

    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }

    public static function future()
    {
        $events = collect();

        foreach (Event::all() as $event)
        {
            if (date('U',strtotime($event->date_begin)) >= time())
            {
                $events->push($event);
            }
        }
        return $events;
    }

    public static function allThisYear()
    {
        $events = Event::all();
        foreach ($events as $key => $event)
        {
            if (date('c',strtotime($event->date_begin)) <= date('c',strtotime(\App\Config::getData('instruction_year_begin'))))
            {
                $events->forget($key);
            }
            if (date('c',strtotime($event->date_begin)) >= date('c',strtotime(\App\Config::getData('instruction_year_end'))))
            {
                $events->forget($key);
            }
        }
        return $events;
    }

    public function nbPeriode()
    {
        return (count($this->schedule["periodes"]));
    }

    public function nbNiveau()
    {
        return count($this->schedule["niveaux"]);
    }

    static function getMaxLevels($events)
    {
        $maxlevel = 0;
        foreach ($events as $e)
        {
            if ($e->nbNiveau() > $maxlevel)
            {
                $maxlevel = $e->nbNiveau();
            }
        }
        return $maxlevel;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Collection;
use function foo\func;

/**
 * App\Item
 *
 * @property int $id
 * @property int $category_id
 * @property int $quantity
 * @property string $name
 * @property string $official_number
 * @property string $desc
 * @property array $metadata
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Booking[] $bookings
 * @property-read int|null $bookings_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereMetadata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereOfficialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Item extends Model
{
    public static function explodeItems($items)
    {
        $array_items = explode("-",$items);
        $col_items = collect();

        foreach ($array_items as $item) {
            $col_items->push(Item::find($item));
        }
        $col_items->forget(0);
        return $col_items;
    }

    protected $casts = [
        'metadata' => 'array',
    ];

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function category()
    {
        if (\App\ItemCategory::find($this->category_id) == null)
        {
            $this->category_id = -1;
            $this->save();
            $c = new \App\ItemCategory();
            $c->name = "[Categorie Supprimé]";
            $c->is_training = 0;
            $c->is_op_appro = 0;

            return $c;
        }
        return \App\ItemCategory::find($this->category_id);
    }

    public function available($begin = null,$end = null)
    {
        if ($begin == null && $end == null)
        {
            return $this->quantity - $this->booked();
        }
        else
        {
            return $this->quantity - $this->booked($begin,$end);
        }
    }

    public function booked($begin_time = null,$end_time = null)
    {
        $nbBooked = 0;
        foreach ($this->bookings as $b)
        {
            if ($begin_time != null && $end_time != null)
            {
                $b_begin_time = null;
                $b_end_time = null;
                if ($b->bookable_type == 'App\Course')
                {
                    $b_begin_time = $b->bookable->event->date_begin;
                    $b_end_time = $b->bookable->event->date_end;
                }
                else
                {
                    $b_begin_time = $b->bookable->date_begin;
                    $b_end_time = $b->bookable->date_end;
                }
                if (date('U',strtotime($b_begin_time)) <= date('U',strtotime($begin_time)) && date('U',strtotime($b_end_time)) <= date('U',strtotime($end_time)) && date('U',strtotime($b_end_time)) >= date('U',strtotime($begin_time)))
                {
                    $nbBooked = $nbBooked + $b->amount;
                }
                elseif (date('U',strtotime($b_begin_time)) <= date('U',strtotime($begin_time)) && date('U',strtotime($b_end_time)) >= date('U',strtotime($end_time)))
                {
                    $nbBooked = $nbBooked + $b->amount;
                }
                elseif (date('U',strtotime($b_begin_time)) >= date('U',strtotime($begin_time)) && date('U',strtotime($b_begin_time)) <= date('U',strtotime($end_time)) && date('U',strtotime($b_end_time)) >= date('U',strtotime($end_time)))
                {
                    $nbBooked = $nbBooked + $b->amount;
                }
                elseif (date('U',strtotime($b_begin_time)) >= date('U',strtotime($begin_time)) && date('U',strtotime($b_begin_time)) <= date('U',strtotime($end_time)))
                {
                    $nbBooked = $nbBooked + $b->amount;
                }
            }
            else
            {
                $nbBooked = $nbBooked + $b->amount;
            }
        }

        return $nbBooked;
    }

    public static function training()
    {
        $training = collect();

        $training = \App\Item::all()->filter(function($value,$key){
           return $value->category()->is_training == 1 && $value->category()->is_op_appro != 1;
        });

        return $training;
    }
}

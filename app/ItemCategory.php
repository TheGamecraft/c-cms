<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ItemCategory
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property int $is_training
 * @property int $is_op_appro
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Item[] $items
 * @property-read int|null $items_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read int|null $logs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereIsOpAppro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereIsTraining($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ItemCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ItemCategory extends Model
{
    public function items()
    {
        return $this->hasMany('App\Item','category_id');
    }

    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Schedule;

class ECCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = \App\Message::all()->forPage(1,4);
        return view('ecc.dashboard',['userClasse' => \Auth::User()->getClasse()->forPage(1,8), 'messages' => $messages,'AlluserClasse' => \Auth::User()->getClasse()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        return view('ecc.update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function messages($page = 1)
    {
        if ($page < 1) {
            $page = 1;
        }
        $messages = \App\Message::all()->forPage($page,6);
        return view('ecc.messages',['messages' => $messages,'page'=>$page]);
    }

    public function message($id)
    {
        $messages = \App\Message::find($id);
        return view('ecc.message',['message' => $messages]);
    }

    public function guide()
    {
        return view('ecc.guide');
    }

    public function calendar()
    {
        return view('ecc.calendar');
    }

    public function generateCalendar()
    {
        $lang = str_replace('_', '-', app()->getLocale());
        setlocale(LC_ALL, $lang.'_'.strtoupper($lang).'.utf8','fra'); 

        $month = request('month');
        $year = request('year');

        $nextMonth = $month + 1;
        $nextYear = $year;

        if ($nextMonth > 12) {
            $nextMonth = 1;
            $nextYear = $nextYear + 1;
        }

        $prevMonth = $month - 1;
        $prevYear = $year;

        if ($prevMonth < 1) {
            $prevMonth = 12;
            $prevYear = $prevYear - 1;
        }

        $calendar = array();

        $dayinmonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $firstdaymonth = date("w", strtotime("01-".$month."-".$year));

        $addingday = 0;

        for ($i=$firstdaymonth ; $addingday < $dayinmonth ; $i++) {
            $addingday = $addingday + 1;
            $calendar[$i] = $addingday;
        }

        echo '<div class="table calendar">';
        echo '<div class="thead-dark">';
        echo '<div class="row calendar-head"><div class="col-sm-2"><a class="btn" onclick="generate('.$prevMonth.','.$prevYear.')"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></div><div class="col-sm-8">'.ucfirst(strftime("%B %Y", strtotime("01-".$month."-".$year))).'</div><div class="col-sm-2"><a class="btn" onclick="generate('.$nextMonth.','.$nextYear.')"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div></div>';
        echo '<div class="row calendar-head"><div style="width:14%;">Dimanche</div><div style="width:14%;">Lundi</div><div style="width:14%;">Mardi</div><div style="width:14%;">Mercredi</div><div style="width:14%;">Jeudi</div><div style="width:14%;">Vendredi</div><div style="width:14%;">Samedi</div></div>';
        echo '</div>';
        echo '<div class="calendar-body">';
        for ($i=0; $i < 6 ; $i++)
        {
            echo '<div class="calendar-row">';
            for ($a=0; $a < 7 ; $a++)
            {
                if (isset($calendar[(($i*7) + $a)]))
                {
                    /** Date info */
                    $today = date("Y-m-d", strtotime($year."-".$month."-".$calendar[(($i*7) + $a)]));
                    $activityToday = Schedule::where('date','=',$today)->get();

                    /** If nothing today */
                    if ($activityToday->isEmpty()) {
                    echo '<a href="/ecc/calendar/'.$today.'" class="calendar-container calendar-empty" name="'.$today.'" id="calendar_'.$calendar[(($i*7) + $a)].'"><div class="calendar-date">'.date("j", strtotime($today)).'</div></a>';
                    } else {
                        echo '<a href="/ecc/calendar/'.$today.'" class="calendar-container" name="'.$today.'" id="calendar_'.$calendar[(($i*7) + $a)].'">';
                        $text = "";
                        echo '<div class="calendar-date">'.date("j", strtotime($today)).'</div>';
                        foreach ($activityToday as $activity) {
                            echo '<div class="calendar-text" style="width:90%;">';
                            switch ($activity->type) {
                                case 'regular':
                                    echo '<div class="row" style="color:orange;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-book fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>";
                                    break;
                                
                                case 'pilotage':
                                    echo '<div class="row" style="color:#58D3F7;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-plane fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>";
                                    break;
                                
                                case 'drill':
                                    echo '<div class="row" style="color:blue;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-trophy fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>";                                                
                                    break;
                                
                                case 'music':
                                    echo '<div class="row" style="color:green;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-music fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>"; 
                                    break;
                                
                                case 'biathlon':
                                    echo '<div class="row" style="color:red;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-snowflake-o fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>"; 
                                    break;
                                
                                case 'marksmanship':
                                    echo '<div class="row" style="color:grey;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-star fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>"; 
                                    break;
                                
                                case 'founding':
                                    echo '<div class="row" style="color:#00FF40;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>"; 
                                    break;
                                
                                case 'volunteer':
                                    echo '<div class="row" style="color:#DF0174;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-handshake-o fa-stack-1x fa-inverse"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>"; 
                                    break;
                                
                                default:
                                    echo '<div class="row" style="color:#0B615E;"><span class="fa-stack fa-lg col-md-2"><i class="fa fa-circle fa-stack-2x"></i></span><div class="col-md-10 calendar_event_name">'.ucfirst($activity->data['event_name'])."</div></div>"; 
                                    break;
                            }
                        echo '</div>';
                        }
                    echo '</a>';
                    }
                } else {
                    echo '<div class="calendar-container¸calendar-empty" style="border:none !important; width:14%;"></div>';
                }
            }
            echo '</div>';
        }

    }

    public function calendar_date($date)
    {
        $schedules = \App\Schedule::all()->where('date',$date);

        return view('ecc.calendar_date',['schedules' => $schedules]);
    }

    public function booking($id,$niveau,$periode)
    {
        $schedule = Schedule::find($id);

        $periode_item = 'n'.$niveau.'_p'.$periode.'_item';
        $periode_instr = 'n'.$niveau.'_p'.$periode.'_instructor';

        if(\Auth::User()->id != $schedule->data[$periode_instr])
        {
            abort(401);
        }

        $items = collect();
        if (isset($schedule->$periode_item)) {
            $items_array = explode("-",$schedule->$periode_item);

            foreach ($items_array as $item_array) {
                if ($item_array != "") {
                    $item_array_ex = explode(":",$item_array);
                    $this_item = \App\Item::find($item_array_ex[0]);
                    $this_item->quantity = $item_array_ex[1];
                    $items->push($this_item);
                }
            }
        }

        return view('ecc.booking',['schedule' => $schedule, 'periode' => $periode, 'niveau' => $niveau, 'items' => $items, 'dispo_item' => $schedule->getInventory($periode)]);
    }

    public function booking_add($id,$periode,$niveau)
    {
        $schedule = Schedule::find($id);
        $id_to_modify = "passet";
        $qt_to_add = 0;
        $periode_item = 'n'.$niveau.'_p'.$periode.'_item';

        if (isset($schedule->$periode_item)) {
            $array_items = explode("-",$schedule->$periode_item);
            for ($i=0; $i < count($array_items); $i++) { 
                $array_item_id = explode(":",$array_items[$i]);
                
                if ($array_item_id[0] == request('add')) {
                    $id_to_modify = $i;
                    $qt_to_add = $array_item_id[1];
                }
            }
            if ($id_to_modify === "passet") {
                array_push($array_items,request('add').":".request('qt'));
            } else {
                $toadd = $qt_to_add+request('qt');
                $array_items[$id_to_modify] = request('add').":".$toadd;
            }
        } else {
            $array_items = [];
            array_push($array_items,request('add').":".request('qt'));
        }

        $final_items = implode("-",$array_items);

        $schedule->$periode_item = $final_items;

        $schedule->save();

        return redirect('/ecc/inventory/'.$id.'/'.$niveau.'/'.$periode);
    }

    public function booking_remove($id,$periode,$niveau)
    {
        $schedule = Schedule::find($id);
        $remove = [request('remove')];
        $periode_item = 'n'.$niveau.'_p'.$periode.'_item';

        if (isset($schedule->$periode_item)) {
            $array_items = explode("-",$schedule->$periode_item);
            $array_items = array_diff($array_items,$remove);
        } else {
            $array_items = [];
            $array_items = array_diff($array_items,$remove);
        }

        $final_items = implode("-",$array_items);
        $schedule->$periode_item = $final_items;
        $schedule->save();
        
        return redirect('/ecc/inventory/'.$id.'/'.$niveau.'/'.$periode);
    }

    public function list()
    {
        return view('ecc.list');
    }

    public function files()
    {
        return view('ecc.files');
    }

    public function settings()
    {
        return view('ecc.settings');
    }

    public function UserPassword()
    {
        return view('ecc.settings.password');
    }

    public function UserAvatar() 
    {
        return view('ecc.settings.avatar');
    }

    public function UserAdress()
    {
        return view('ecc.settings.adress');
    }
}

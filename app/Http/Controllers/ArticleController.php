<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        clogNav('consulte les articles');
        return view('admin.article.index',['activity' => \App\ComplementaryActivity::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editActivity($id)
    {
        return view('admin.article.editActivity',['article' => \App\ComplementaryActivity::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateActivity(Request $request, $id)
    {
        $a = \App\ComplementaryActivity::find($id);

        $a->public_slogan = $request->public_slogan;
        $a->public_body = $request->public_body;
        $a->public_header_picture = $request->public_header_picture;

        $a->save();
        clog('edit','success','a ajouté un article');
        return redirect('/admin/article')->with('success','Article modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pictureActivity($id)
    {
        return view('admin.article.pictureActivity',['article' => \App\ComplementaryActivity::find($id)]);
    }

    public function pictureActivityCreate($id)
    {
        return view('admin.article.picture.add',['article' => \App\ComplementaryActivity::find($id)]);
    }

    public function pictureActivityStore(Request $request, $id)
    {
        $p = new \App\Picture();

        $p->url = $request->url;
        $p->title = $request->title;
        $p->desc = $request->desc;
        $p->pictureable_id = $id;
        $p->pictureable_type = "App\ComplementaryActivity";

        $p->save();
        clog('add','success','a ajouté une photo avec succès');
        return redirect('/admin/article/activity/picture/'.$id)->with('success','Photo ajoutéé avec succès');
    }
}

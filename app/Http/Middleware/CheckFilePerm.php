<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use \App\GoogleDriveFile;
use \App\Config;

class CheckFilePerm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$type,$permission = 'r')
    {
        if (GoogleDriveFile::checkConfig() && Config::getData('is_Google_Drive_enabled') == 'true')
        {
            // Get folder
            if (isset($request->folder))
            {
                $folder = $request->folder;
            }
            else
            {
                $folder = $request->d;
            }
            $dir = GoogleDriveFile::find($folder);

            // Check if $folder is root directory or $dir is null
            if ($folder == "" || $dir == null)
            {
                if (\Auth::check())
                {
                    if ($permission == 'r')
                    {
                        return $next($request);
                    }
                    else
                    {
                        if (\Auth::user()->p('file_manage') === 1)
                        {
                            return $next($request);
                        }
                    }
                }
                abort(401,'Pas connecter');
            }

            // Check for specific folder permission
            if ($dir != null)
            {
                if (\Auth::check())
                {
                    if (!$dir->canAuthUser($permission))
                    {
                        clog('navigate','danger','Vous n\'avez pas la permission d\'accéder',\Auth::user()->id);
                        return redirect('/admin')->with('error','Vous n\'avez pas la permission d\'accéder');
                    }
                }
                else
                {
                    if (strpos($dir->getPermission('rank.0'),$permission) === false)
                    {
                        clog('navigate','danger','Un utilisateur non authentifié tente d\'accéder a un dossier privé','0');
                        abort(401,'Vous n\'avez pas la permission d\'accéder');
                    }
                }
                return $next($request);
            }
        }
        else
        {
            clog('navigate','danger','Google Drive n\'est pas activé ou les identifiants sont incorrect',\Auth::user()->id);
            return redirect('/admin')->with('error','Google Drive n\'est pas activé ou les identifiants sont incorrect');
        }
        return abort(500,'Wow... Aucune idée comment ce qui viens d\'arriver');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplementaryActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complementary_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('public_body');
            $table->text('public_slogan');
            $table->text('public_header_picture');
            $table->text('admin_desc');
            $table->string('calendar_color')->default('blue');
            $table->string('calendar_icon')->default('<i class="fa fa-question-circle"></i>');
            $table->string('begin_time')->default('12:00');
            $table->string('end_time')->default('13:00');
            $table->string('location')->default('Escadron');
            $table->boolean('is_mandatory')->default(false);
            $table->boolean('is_promoted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complementary_activities');
    }
}

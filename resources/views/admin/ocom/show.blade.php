@extends('layouts.admin.main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{$ocom->ocom.' / '.$ocom->objectif_competence}}</h4>
            </div>
            <div class="card-body">
                <div class="content">
                    <div class="row">
                        <div class="col-md-1 text-center align-middle p-3">
                            @if($ocom->course_id != "")
                                <div data-toggle="tooltip" data-placement="right" title="Le cours a été donné cette année">
                                    <p class="d-none">1</p>
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                </div>
                            @else
                                <div data-toggle="tooltip" data-placement="right" title="Le cours n'a pas été donné cette année">
                                    <p class="d-none">0</p>
                                    <i class="fas fa-times-circle fa-2x text-warning"></i>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-2">
                            <label>OREN</label>
                            <p>{{$ocom->oren}}</p>
                        </div>
                        <div class="col-md-2">
                            <label>OCOM</label>
                            <p>{{$ocom->ocom}}</p>
                        </div>
                        <div class="col-md-4">
                            <label>Objectif de compétence</label>
                            <p>{{$ocom->objectif_competence}}</p>
                        </div>
                        <div class="col-md-3">
                            <label>Nombre de période</label>
                            <p>{{$ocom->nbPeriode}}</p>
                        </div>
                        <div class="col-md-4">
                            <label>Objectif de rendement</label>
                            <p>{{$ocom->objectif_rendement}}</p>
                        </div>
                        <div class="col-md-8 text-right">
                            <a href="/admin/ocom/{{$ocom->id}}/edit" class="btn btn-warning">Modifier</a>
                            <button type="button" class="btn btn-danger" onclick="deleteOCOM('{{$ocom->id}}')">Supprimer</button>
                        </div>
                        <div class="col-12">
                            <label>Index des périodes</label>
                            @if($ocom->course_id == "")
                                <p class="text-center text-muted">Le cours n'a jamais été donnée</p>
                            @else
                                <table id="table" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>Instructeur</th>
                                        <th>Niveau</th>
                                        <th style="width: 8rem">Plan de cours</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ocom->courses() as $course)
                                        <tr>
                                            <th> <a href="/admin/course/{{$course->id}}">{{$course->id}}</a> </th>
                                            <td>{{$course->event->date_begin}}</td>
                                            <td>{{$course->instructor()}}</td>
                                            <td>{{$course->level}}</td>
                                            <td class="text-center">
                                                @if($course->lessonPlan)
                                                    <a href="/file/get?d={{urlencode(\App\GoogleDriveFile::findByPath('.Systeme/.Fichier/.PlanDeCours')->id)}}&f={{urlencode($course->lessonPlan->file)}}">
                                                        @if($course->lessonPlan->approved == 1)
                                                            <i class="fas fa-check-circle text-success fa-2x" data-toggle="tooltip"
                                                               data-placement="top" title="Plan de cours remis et vérifié"></i>
                                                        @else
                                                            <i class="fas fa-exclamation-circle text-warning fa-2x"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="Plan de cours remis mais non vérifié"></i>
                                                        @endif
                                                    </a>
                                                @else
                                                    <i class="fas fa-times-circle text-danger fa-2x" data-toggle="tooltip"
                                                       data-placement="top" title="Plan de cours non remis"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <a class="navbar-brand" href="/admin/ocom">Base de données des cours</a> / {{$ocom->ocom}}
@endsection

@section('custom_scripts')
    <script src="/js/ocom.js"></script>
    <script>
        $('.tooltip').tooltip('enable')
        $(document).ready(function() {
            $('#table').DataTable({
                "ordering" : true,
                "order": [[1, "asc"]]
            });
        } );
    </script>
@endsection

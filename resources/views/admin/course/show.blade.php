@extends('layouts.admin.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{$course->name}} ({{$course->event->date_begin}})</h4>
                </div>
                <div class="card-body">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nom</label>
                                <p>{{$course->name}}</p>
                            </div>
                            <div class="col-md-3">
                                <label>Instructeur</label>
                                <p>{{$username}}</p>
                            </div>
                            <div class="col-md-1">
                                <label>OCOM</label>
                                <p>
                                    @if(\App\OCOM::findByOCOM($course->ocom) != null)
                                        <a href="/admin/ocom/{{\App\OCOM::findByOCOM($course->ocom)->id}}">{{$course->ocom}}</a>
                                    @else
                                        {{$course->ocom}}
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-2">
                                <label>Date</label>
                                <p>{{$course->event->date_begin}}</p>
                            </div>
                            <div class="col-md-1 col-sm-6 text-center">
                                <label>Période</label>
                                <p>{{$course->periode}}</p>
                            </div>
                            <div class="col-sm-1 col-xs-6  text-center">
                                <label>Niveau</label>
                                <p>{{$course->level}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Commentaire de l'instructeur</label>
                                <hr class="m-0">
                                @if($course->comment == null)
                                    <p class="text-center">Aucun commentaire</p>
                                @else
                                    <div>{!! $course->comment !!}</div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label>Commentaire de l'officier</label>
                                <hr class="m-0">
                                @if($course->comment_officer == null)
                                    <p class="text-center">Aucun commentaire</p>
                                @else
                                    <div>{!! $course->comment_officer !!}</div>
                                @endif
                            </div>
                            <div class="col-6">
                                <button class="btn btn-outline-primary btn-block mt-4 @if(\Auth::user()->id != $course->user_id) d-none @endif" data-toggle="modal" data-target="#editComment" @if(\Auth::user()->id != $course->user_id) disabled @endif>Modifier le commentaire</button>
                            </div>
                            <div class="col-6">
                                @if(\Auth::user()->p('course_comment_officer') == 1)
                                    <button class="btn btn-outline-primary btn-block mt-4" data-toggle="modal" data-target="#editCommentOfficer">Modifier le commentaire</button>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-5">
                                <label>Plan de cours</label>
                                @if($course->lessonPlan == null)
                                    <div class="alert alert-warning" role="alert">
                                        Aucun plan de cours remis
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Fichier</label>
                                            <p>{{$course->lessonPlan->file}}</p>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Remise original</label>
                                            <p>{{$course->lessonPlan->created_at}}</p>

                                        </div>
                                        <div class="col-md-2">
                                            <label>Dernière modification</label>
                                            <p>{{$course->lessonPlan->updated_at}}</p>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <label>Vérifié</label>
                                            @if(\Auth::user()->p('course_validate_plan') == 1)
                                                <div class="togglebutton">
                                                    <label>
                                                        <input type="checkbox" name="isPlanCheck" onchange="switchPlanStatus({{$course->id}})" @if($course->lessonPlan->approved == 1) checked @endif >
                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                            @else
                                                @if($course->lessonPlan->approved == 1)
                                                    <p><i class="fas fa-check-circle fa-2x text-success"></i></p>
                                                @else
                                                    <p><i class="fas fa-times-circle fa-2x text-danger"></i></p>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <label>Télécharger</label>
                                            <p>
                                                <a target="_blank" href="/file/get?d={{urlencode($lessonPlanDir)}}&f={{urlencode($course->lessonPlan->file)}}" class="btn btn-primary btn-fab btn-fab-mini btn-round m-0">
                                                    <i class="material-icons">cloud_download</i>
                                                </a>
                                            </p>
                                        </div>
                                        <div class="col-12">
                                            <div id="accordion1" role="tablist">
                                                <div class="card card-collapse">
                                                    <div class="card-header pt-0" role="tab" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <a data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                Commentaire
                                                                <i class="material-icons">keyboard_arrow_down</i>
                                                            </a>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion1">
                                                        <div class="card-body px-2">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Commentaire de l'instructeur</label>
                                                                    @if($course->lessonPlan->desc == null)
                                                                        <p class="text-center">Aucun commentaire</p>
                                                                    @else
                                                                        <div>{!! $course->lessonPlan->desc !!}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Commentaire de l'officier</label>
                                                                    @if($course->lessonPlan->comment == null)
                                                                        <p class="text-center">Aucun commentaire</p>
                                                                    @else
                                                                        <div>{!! $course->lessonPlan->comment !!}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="col-6">
                                                                    <button class="btn btn-outline-primary btn-block mt-4 @if(\Auth::user()->id != $course->user_id) d-none @endif" data-toggle="modal" data-target="#editCommentPlan">Modifier le commentaire</button>
                                                                </div>
                                                                <div class="col-6">
                                                                    @if(\Auth::user()->p('course_comment_plan_officer') == 1)
                                                                        <button class="btn btn-outline-primary btn-block mt-4" data-toggle="modal" data-target="#editCommentOfficerPlan">Modifier le commentaire</button>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <button class="btn btn-outline-primary btn-block mt-4 @if(\Auth::user()->id != $course->user_id) d-none @endif" data-toggle="modal" data-target="#editLessonPlan">Remettre un plan de cours</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editComment" tabindex="-1" role="dialog" aria-labelledby="editCommentLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="/admin/course/{{$course->id}}/comment" method="post">
                    @csrf
                    @method('patch')
                    <div class="modal-header">
                        <h5 class="modal-title" id="editCommentLabel">Modifier les commentaires de l'instructeur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                        <textarea name="comment" class="form-control richeditor" name="admin_desc" id="admin_desc" rows="6" required>
                            {!! $course->comment !!}
                        </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editCommentOfficer" tabindex="-1" role="dialog" aria-labelledby="editCommentOfficerLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="/admin/course/{{$course->id}}/commentOfficer" method="post">
                    @csrf
                    @method('patch')
                    <div class="modal-header">
                        <h5 class="modal-title" id="editCommentOfficerLabel">Modifier les commentaires de l'officier</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                        <textarea name="comment_officer" class="form-control richeditor" name="admin_desc" id="admin_desc" rows="6" required>
                            {!! $course->comment_officer !!}
                        </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editCommentPlan" tabindex="-1" role="dialog" aria-labelledby="editCommentLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="/admin/course/{{$course->id}}/plan/comment" method="post">
                    @csrf
                    @method('patch')
                    <div class="modal-header">
                        <h5 class="modal-title" id="editCommentLabel">Modifier les commentaires de l'instructeur pour le plan de cours</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                        <textarea name="comment" class="form-control richeditor" name="admin_desc" id="admin_desc" rows="6" required>
                            @if($course->lessonPlan)
                                {!! $course->lessonPlan->desc !!}
                            @endif
                        </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editCommentOfficerPlan" tabindex="-1" role="dialog" aria-labelledby="editCommentOfficerLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="/admin/course/{{$course->id}}/plan/commentOfficer" method="post">
                    @csrf
                    @method('patch')
                    <div class="modal-header">
                        <h5 class="modal-title" id="editCommentOfficerLabel">Modifier les commentaires de l'officier pour le plan de cours</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                        <textarea name="comment_officer" class="form-control richeditor" name="admin_desc" id="admin_desc" rows="6" required>
                            @if($course->lessonPlan)
                                {!! $course->lessonPlan->comment !!}
                            @endif
                        </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editLessonPlan" tabindex="-1" role="dialog" aria-labelledby="editLessonPlanLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/admin/course/{{$course->id}}/lessonPlan" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="editLessonPlanLabel">Remettre un plan de cours</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group bmd-form-group is-filled">
                            <label class="label-control bmd-label-static">Choisir le plan de cours</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput" style="display: flex !important;">
                                <div class="form-control" data-trigger="fileinput">
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-append"><span class="input-group-text fileinput-exists cursor" data-dismiss="fileinput">Remove</span><span class="input-group-text btn-file"><span class="fileinput-new cursor">Select file</span><span class="fileinput-exists cursor">Change</span><input type="file" name="file" required></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Remettre</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="/js/plugins/course.js"></script>
@endsection


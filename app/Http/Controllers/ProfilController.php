<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function courses()
    {
        $mode = 'future';
        if (\request('all'))
        {
            $courses = \App\Course::allForAuthUser();
            $mode = 'all';
        }
        else
        {
            $courses = \App\Course::allFutureForAuthUser();
        }
        return view('admin.user.profil.courses',['courses' => $courses,'mode' => $mode]);
    }
}

@extends('errors.layout')

@section('title', __('Non autorisé'))
@section('code')
    <span>4</span><span>0</span><span>1</span>
@endsection
@section('message', 'Nous sommes désolé, vous n\'avez pas l\'autorisation d\'être ici.')
@section('error',$exception->getMessage())
@section('url','https://developer.mozilla.org/fr/docs/Web/HTTP/Status/401')

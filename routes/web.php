<?php

use \App\Notifications\sms;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Basic Auth Route */
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/ocom/create', 'OCOMController@updateOCOMDB');


/** Public Route */
Route::get('/', 'PublicController@index');
Route::get('/news', 'NewsController@index');
Route::get('/news/{id}', 'NewsController@show');
Route::get('/activity', 'ComplementaryActivityController@index');
Route::get('/activity/{id}', 'ComplementaryActivityController@show');
Route::get('/picture/{id}', 'PictureController@show');
Route::get('/pictures', 'PictureController@index');

Route::get('/p/{name}', 'PublicPageController@display');

Route::get('/file/get', 'GoogleDriveController@getFile')->middleware('fileperm:file,r');

/** Setup */
Route::get('/admin/setup', 'AdminController@setup')->middleware('auth')->name('admin.setup');
Route::post('/admin/setup', 'AdminController@saveSetup')->middleware('auth');

Route::middleware(['auth', 'firstlogin'])->name('admin.')->group(function () {

    /* Espace Administration Route */

    /** Dashboard & General */
    Route::get('/admin', 'AdminController@index')->name('dashboard');
    Route::get('/admin/update', 'AdminController@update')->name('update');
    Route::get('/admin/status', 'AdminController@status')->name('status');

    /* PublicPage **/
    Route::get('/admin/public-pages', 'PublicPageController@index')->name('public-page');
    Route::get('/admin/public-pages/create', 'PublicPageController@create')->name('public-page.create');
    Route::get('/admin/public-pages/edit/{id}', 'PublicPageController@edit')->name('public-page.edit');
    Route::patch('/admin/public-pages/edit/{id}', 'PublicPageController@update');
    Route::get('/admin/public-pages/{id}', 'PublicPageController@show')->name('public-page.show');

    /** Schedule */
    Route::get('/admin/schedule', 'CalendarController@index')->middleware('perm:schedule_see')->name('schedule');
    Route::get('/admin/schedule/table', 'CalendarController@indexTable')->middleware('perm:schedule_see')->name('schedule.tableview');
    Route::get('/admin/schedule/pdf/event/{id}', 'ScheduleController@printtopdf')->middleware('perm:schedule_see')->name('schedule.pdf');
    Route::get('/admin/schedule/add/{date}', 'ScheduleController@create')->middleware('perm:schedule_add')->name('schedule.add');
    Route::get('/admin/schedule/edit/{id}', 'EventController@edit')->middleware('perm:schedule_edit')->name('schedule.edit');
    Route::post('/admin/schedule/event/add', 'EventController@Store')->middleware('perm:schedule_add');
    Route::post('/admin/schedule/event/edit/{id}', 'EventController@update')->middleware('perm:schedule_edit');

    /** Statistique */
    Route::get('/admin/stats/log', 'LogController@index')->middleware('perm:stats_see')->name('stats.log');
    Route::get('/admin/stats/instruction', 'StatsController@instruction')->middleware('perm:stats_instruction_see')->name('stats.instruction');

    /** Message */
    Route::get('/admin/message', 'MessageController@index')->middleware('perm:msg_see')->name('message');
    Route::get('/admin/message/add', 'MessageController@create')->middleware('perm:msg_add')->name('message.add');
    Route::post('/admin/message/add', 'MessageController@store')->middleware('perm:msg_add');
    Route::get('/admin/message/{id}', ['uses' => 'MessageController@show'])->middleware('perm:msg_see')->name('message.show');

    /** User */
    Route::get('/admin/user', 'UserController@index')->middleware('perm:user_see')->name('users');
    Route::get('/admin/user/add', 'UserController@create')->middleware('perm:user_add')->name('user.add');
    Route::post('/admin/user/add', 'UserController@store')->middleware('perm:user_add');
    Route::get('/admin/user/{id}', 'UserController@show')->middleware('perm:user_see')->name('user');
    Route::get('/admin/user/{id}/course', 'UserController@showCourses')->name('user.courses');
    Route::get('/admin/user/edit/{id}', 'UserController@edit')->middleware('perm:user_edit')->name('user.edit');
    Route::post('/admin/user/edit/{id}', 'UserController@update')->middleware('perm:user_edit');

    /** Config */
    Route::get('/admin/config/instruction', 'ScheduleController@index')->middleware('perm:config_edit_instruction')->name('config.schedule');
    Route::get('/admin/config/instruction/event_type/create', 'EventTypeController@create')->middleware('perm:config_edit_instruction')->name('config.schedule.event_type.create');
    Route::post('/admin/config/instruction/event_type/create', 'EventTypeController@store')->middleware('perm:config_edit_instruction');
    Route::get('/admin/config/instruction/event_type/{id}', 'EventTypeController@show')->middleware('perm:config_edit_instruction')->name('config.schedule.event_type');
    Route::patch('/admin/config/instruction/event_type/{id}', 'EventTypeController@update')->middleware('perm:config_edit_instruction');
    Route::patch('/admin/config/instruction', 'ScheduleController@update')->middleware('perm:config_edit_instruction');
    Route::get('/admin/config/activity', 'ComplementaryActivityController@index')->middleware('perm:config_edit_administration')->name('config.complementary-activity');
    Route::get('/admin/config/activity/add', 'ComplementaryActivityController@create')->middleware('perm:config_edit_administration')->name('config.complementary-activity.add');
    Route::post('/admin/config/activity/add', 'ComplementaryActivityController@store')->middleware('perm:config_edit_administration');
    Route::get('/admin/config/activity/edit/{id}', 'ComplementaryActivityController@edit')->middleware('perm:config_edit_administration')->name('config.complementary-activity.edit');
    Route::post('/admin/config/activity/edit/{id}', 'ComplementaryActivityController@update')->middleware('perm:config_edit_administration');
    Route::post('/admin/config/general/edit', 'ConfigController@update')->middleware('perm:config_edit');
    Route::get('/admin/config/', 'ConfigController@index')->middleware('perm:config_edit')->name('config.general');
    Route::get('/admin/config/customisation', 'ConfigController@customisation')->middleware('perm:config_edit_customization')->name('config.customisation');
    Route::post('/admin/config/customisation', 'ConfigController@customisationUpdate')->middleware('perm:config_edit_customization');

    Route::get('/admin/config/ranks', 'RankController@index')->middleware('perm:config_edit_rank')->name('config.rank');
    Route::get('/admin/config/ranks/add', 'RankController@create')->middleware('perm:config_edit_rank')->name('config.rank.add');
    Route::post('/admin/config/ranks/add', 'RankController@store')->middleware('perm:config_edit_rank');
    Route::get('/admin/config/ranks/{id}', 'RankController@show')->middleware('perm:config_edit_rank')->name('config.rank.edit');
    Route::post('/admin/config/ranks/{id}', 'RankController@update')->middleware('perm:config_edit_rank');
    Route::get('/admin/config/jobs', 'JobController@index')->middleware('perm:config_edit_job')->name('config.jobs');
    Route::get('/admin/config/jobs/add', 'JobController@create')->middleware('perm:config_edit_job')->name('config.jobs.add');
    Route::post('/admin/config/jobs/add', 'JobController@store')->middleware('perm:config_edit_job');
    Route::get('/admin/config/jobs/{id}', 'JobController@edit')->middleware('perm:config_edit_job')->name('config.jobs.edit');
    Route::post('/admin/config/jobs/{id}', 'JobController@update')->middleware('perm:config_edit_job');

    Route::get('/admin/config/files', 'ConfigController@showfilesConfig')->middleware('perm:config_edit')->name('config.files');
    Route::post('/admin/config/files', 'ConfigController@editfilesConfig')->middleware('perm:config_edit');

    /** Public page */
    Route::get('/admin/public/edit/{config}', 'PublicController@edit')->middleware('perm:config_edit_customization');
    Route::post('/admin/public/edit/{config}', 'PublicController@update')->middleware('perm:config_edit_customization');

    /** Picture */
    Route::get('/admin/picture', 'PictureController@indexAdmin')->middleware('perm:picture_see')->name('picture');
    Route::get('/admin/picture/add', 'PictureController@create')->middleware('perm:picture_add')->name('picture.add');
    Route::post('/admin/picture/add', 'PictureController@store')->middleware('perm:picture_add');
    Route::get('/admin/picture/edit/{id}', 'PictureController@edit')->middleware('perm:picture_edit')->name('picture.edit');
    Route::post('/admin/picture/edit/{id}', 'PictureController@update')->middleware('perm:picture_edit');

    /** Inventory */
    Route::get('/admin/inventory', 'InventoryController@index')->middleware('perm:inventory_see')->name('inv');
    Route::get('/admin/inventory/management', 'InventoryController@management')->middleware('perm:inventory_edit')->name('inv.management');

    /** Item Category */
    Route::get('/admin/inventory/management/category', 'ItemCategoryController@index')->middleware('perm:inventory_edit')->name('inv.management.category');
    Route::get('/admin/inventory/management/category/add', 'ItemCategoryController@create')->middleware('perm:inventory_add')->name('inv.management.category.add');
    Route::post('/admin/inventory/management/category/add', 'ItemCategoryController@store')->middleware('perm:inventory_add');
    Route::get('/admin/inventory/management/category/edit/{id}', 'ItemCategoryController@edit')->middleware('perm:inventory_edit')->name('inv.management.category.edit');
    Route::post('/admin/inventory/management/category/edit/{id}', 'ItemCategoryController@update')->middleware('perm:inventory_edit');

    /** News */
    Route::get('/admin/news', 'NewsController@indexAdmin')->middleware('perm:news_see')->name('news');
    Route::get('/admin/news/add', 'NewsController@create')->middleware('perm:news_add')->name('news.add');
    Route::post('/admin/news/add', 'NewsController@store')->middleware('perm:news_add');
    Route::get('/admin/news/edit/{id}', 'NewsController@edit')->middleware('perm:news_edit')->name('news.edit');
    Route::post('/admin/news/edit/{id}', 'NewsController@update')->middleware('perm:news_edit');

    /** Articles */
    Route::get('/admin/article', 'ArticleController@index')->middleware('perm:article_see')->name('article');
    Route::get('/admin/article/activity/edit/{id}', 'ArticleController@editActivity')->middleware('perm:article_edit')->name('article.edit');
    Route::post('/admin/article/activity/edit/{id}', 'ArticleController@updateActivity')->middleware('perm:article_edit');
    Route::get('/admin/article/activity/picture/{id}', 'ArticleController@pictureActivity')->middleware('perm:article_edit');
    Route::get('/admin/article/activity/picture/{id}/add', 'ArticleController@pictureActivityCreate')->middleware('perm:article_edit');
    Route::post('/admin/article/activity/picture/{id}/add', 'ArticleController@pictureActivityStore')->middleware('perm:article_edit');

    /** Booking */
    Route::get('/admin/booking', 'BookingController@index')->middleware('perm:booking_see');
    Route::get('/admin/booking/{id}', 'BookingController@show')->middleware('perm:booking_see');
    Route::get('/admin/booking/{type}/{id}', 'BookingController@index')->middleware('perm:booking_see');
    Route::get('/admin/booking/{type}/{id}/add', 'BookingController@create')->middleware('perm:booking_add');
    Route::post('/admin/booking/{type}/{id}/add', 'BookingController@store')->middleware('perm:booking_add');
    Route::get('/admin/inventory/booking', 'InventoryController@booking')->middleware('perm:inventory_see');
    Route::post('/admin/booking/edit', 'BookingController@update')->middleware('perm:booking_see')->middleware('perm:booking_edit');

    /** Item */
    Route::get('/admin/item/add', 'ItemController@create')->middleware('perm:inventory_add');
    Route::get('/admin/item/edit/{id}', 'ItemController@edit')->middleware('perm:inventory_edit');
    Route::post('/admin/item/add', 'ItemController@store')->middleware('perm:inventory_add');
    Route::post('/admin/item/edit/{id}', 'ItemController@update')->middleware('perm:inventory_edit');

    /** Notification */
    Route::get('/admin/notication/mark', 'UserController@notificationmarkALL');
    Route::post('/admin/notication/mark/{id}', 'UserController@notificationmark');

    /** Profil */
    Route::get('/admin/profil/courses', 'ProfilController@courses')->name('profil.courses');
    Route::get('/admin/profil/avatar', 'UserController@UserAvatar')->name('profil.avatar');
    Route::get('/admin/profil/password', 'UserController@UserPassword')->name('profil.psw');
    Route::get('/admin/profil/adress', 'UserController@UserAdress')->name('profil.adress');
    Route::get('/admin/profil/telephone', 'UserController@UserTelephone')->name('profil.telephone');
    Route::get('/admin/profil/notifications', 'UserController@userNotification')->name('profil.notifications');
    Route::post('/admin/profil/edit/adress', 'UserController@editUserAdress');
    Route::post('/admin/profil/edit/telephone', 'UserController@editUserTelephone');
    Route::post('/admin/profil/edit/password', 'UserController@editUserPassword');
    Route::get('/admin/profil/edit/avatar/{id}', 'UserController@editUserAvatar');
    Route::get('/admin/profil/{id?}', 'UserController@showUserProfil')->name('profil');

    /** Instruction */
    Route::get('/admin/instruction', 'AdminController@instruction')->name('instruction');
    Route::get('/admin/instruction/guide', 'FilesController@guide')->middleware('perm:instruction_guide_see')->name('instruction.guide');
    Route::get('/admin/instruction/files', 'FilesController@instruction')->middleware('perm:file_see')->name('instruction.files');

    /** Courses */
    Route::get('/admin/course', 'CourseController@index')->name('course');
    Route::get('/admin/course/{id}', 'CourseController@show')->name('course.show')->middleware('courseperm:see');
    Route::patch('/admin/course/{id}/commentOfficer', 'CourseController@updateCommentOfficer')->middleware('courseperm:comment_officer');
    Route::patch('/admin/course/{id}/comment', 'CourseController@updateComment')->middleware('courseperm:edit');
    Route::patch('/admin/course/{id}/plan/commentOfficer', 'CourseController@updateCommentOfficerPlan')->middleware('courseperm:comment_plan_officer');
    Route::patch('/admin/course/{id}/plan/comment', 'CourseController@updateCommentPlan')->middleware('courseperm:edit');
    Route::post('/admin/course/{id}/lessonPlan', 'CourseController@updateLessonPlan')->middleware('courseperm:edit');

    /** Files */
    Route::post('/file/create', 'GoogleDriveController@createFile')->middleware('fileperm:folder,w');
    Route::post('/file/upload', 'GoogleDriveController@uploadFile')->middleware('fileperm:folder,w');
    Route::post('/folder/create', 'GoogleDriveController@createFolder')->middleware('fileperm:folder,w');
    Route::get('/file/delete', 'GoogleDriveController@deleteFile')->middleware('perm:file_delete')->middleware('fileperm:folder,w');
    Route::get('/folder/delete', 'GoogleDriveController@deleteDir')->middleware('perm:file_delete')->middleware('fileperm:folder,w');
    Route::get('/admin/files', 'FilesController@index')->middleware('perm:file_see')->name('files');
    Route::get('/admin/files/cadet', 'FilesController@cadet')->middleware('perm:file_see')->name('files.cadet')->middleware('fileperm:folder,r');
    Route::get('/admin/files/staff', 'FilesController@staff')->middleware('perm:file_see')->name('files.staff')->middleware('fileperm:folder,r');
    Route::get('/admin/files/etamas', 'FilesController@etamas')->middleware('perm:file_see')->name('files.etamas')->middleware('fileperm:folder,r');
    Route::get('/admin/files/officier', 'FilesController@officier')->middleware('perm:file_see')->name('files.officier')->middleware('fileperm:folder,r');
    Route::get('/admin/files/publique', 'FilesController@publique')->middleware('perm:file_see')->name('files.publique')->middleware('fileperm:folder,r');

    Route::get('/admin/drive/{folder?}', 'GoogleDriveController@index')->middleware('fileperm:folder,r', 'perm:drive_see')->name('drive');
    Route::get('/admin/folder/{folder?}', 'GoogleDriveController@index')->middleware('fileperm:folder,r')->name('drive');
    Route::get('/admin/drive/{folder}/permission', 'GoogleDriveController@editPermission')->middleware('fileperm:folder,p')->name('drive.permission');
    Route::patch('/admin/drive/{folder}/permission/{subject}/{id}', 'GoogleDriveController@patchPermission')->middleware('perm:file_see', 'fileperm:folder,p');
    Route::get('/admin/drive/{folder}/deletepermission/{subject}/{id}', 'GoogleDriveController@deletePermission')->middleware('perm:file_see', 'fileperm:folder,p');
    Route::patch('/admin/drive/{folder}/addpermission/{subject}', 'GoogleDriveController@addPermission')->middleware('perm:file_see', 'fileperm:folder,p');


    /** OCOM */
    Route::get('/admin/ocom', 'OCOMController@index')->name('ocom')->middleware('perm:instruction_db_ocom_see');
    Route::get('/admin/ocom/generate', 'OCOMController@showgenerate')->name('ocom.generate')->middleware('perm:instruction_db_ocom_edit');
    Route::put('/admin/ocom/generate', 'OCOMController@generate')->middleware('perm:instruction_db_ocom_edit');
    Route::get('/admin/ocom/add', 'OCOMController@create')->name('ocom.add')->middleware('perm:instruction_db_ocom_add');
    Route::put('/admin/ocom/add', 'OCOMController@store')->middleware('perm:instruction_db_ocom_add');
    Route::get('/admin/ocom/{id}/edit', 'OCOMController@edit')->name('ocom.edit')->middleware('perm:instruction_db_ocom_edit');
    Route::patch('/admin/ocom/{id}/edit', 'OCOMController@update')->middleware('perm:instruction_db_ocom_edit');
    Route::get('/admin/ocom/{id}', 'OCOMController@show')->name('ocom.show')->middleware('perm:instruction_db_ocom_see');
});



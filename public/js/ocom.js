function deleteOCOM(id)
{
    Swal.fire({
        title: 'Êtes vous certain ?',
        text: "Vous ne pourrez annuler cette action",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui',
        cancelButtonText: 'Non'
    }).then((result) => {
        if (result.value) {
            let request = $.ajax({
                url: "/api/ocom/"+id,
                method: "DELETE",
                data: { api_token : api_token },
                dataType: "html"
            });
            request.done(function (msg) {
                Swal.fire(
                    'Supprimé!',
                    'Le fichier a été supprimé',
                    'success'
                ).then(() => {
                    window.location.href = '/admin/ocom';
                });
            });
            request.fail(function (jqXHR, textStatus) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Le cours n\'a pas pus être supprimé',
                    footer: '<small>'+textStatus+'</small>'
                })
            })
        }
    });
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * App\Permission
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission query()
 * @mixin \Eloquent
 */
class Permission extends Model
{
    const PERMISSIONS = [
        'Nouvelle' => [
            'news_see' => [
                'ckey' => 'news_see',
                'communName' => 'Voir les nouvelles',
                'desc' => 'L\'utilisateur peut-il consulter les nouvelles',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'news_add' => [
                'ckey' => 'news_add',
                'communName' => 'Ajouter une nouvelles',
                'desc' => 'L\'utilisateur peut-il ajouter une nouvelle',
                'icon' => 'fa-plus',
                'valeur' => 0
            ],
            'news_edit' => [
                'ckey' => 'news_edit',
                'communName' => 'Modifier une nouvelles',
                'desc' => 'L\'utilisateur peut-il modifier une nouvelle',
                'icon' => 'fa-edit',
                'valeur' => 0
            ],
            'news_delete' => [
                'ckey' => 'news_delete',
                'communName' => 'Supprimer une nouvelles',
                'desc' => 'L\'utilisateur peut-il supprimer une nouvelle',
                'icon' => 'fa-close',
                'valeur' => 0
            ]
        ],
        'Inventaire' => [
            'inventory_see' => [
                'ckey' => 'inventory_see',
                'communName' => 'Voir l\'inventaire',
                'desc' => 'L\'utilisateur peut-il consulter l\'inventaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'inventory_add' => [
                'ckey' => 'inventory_add',
                'communName' => 'Ajouter item a l\'inventaire',
                'desc' => 'L\'utilisateur peut-il ajouter un item a l\'inventaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'inventory_edit' => [
                'ckey' => 'inventory_edit',
                'communName' => 'Modifier un item de l\'inventaire',
                'desc' => 'L\'utilisateur peut-il modifier un item de l\'inventaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'inventory_delete' => [
                'ckey' => 'inventory_delete',
                'communName' => 'Supprimer un item de l\'inventaire',
                'desc' => 'L\'utilisateur peut-il supprimer un item de l\'inventaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ],
        'Utilisateur' => [
            'user_see' => [
                'ckey' => 'user_see',
                'communName' => 'Voir la liste des utilisateurs',
                'desc' => 'L\'utilisateur peut-il consulter la liste des utilisateurs',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'user_add' => [
                'ckey' => 'user_add',
                'communName' => 'Ajouter un utilisateur',
                'desc' => 'L\'utilisateur peut-il ajouter un autre utilisateur',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'user_edit' => [
                'ckey' => 'user_edit',
                'communName' => 'Modifier un utilisateur',
                'desc' => 'L\'utilisateur peut-il modifier un autre utilisateur',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'user_delete' => [
                'ckey' => 'user_delete',
                'communName' => 'Supprimer un utilisateur',
                'desc' => 'L\'utilisateur peut-il supprimer un autre utilisateur',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ],
        'Configuration' => [
            'config_see' => [
                'ckey' => 'config_see',
                'communName' => 'Voir les configurations',
                'desc' => 'L\'utilisateur peut-il consulter les configurations',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'config_edit' => [
                'ckey' => 'config_edit',
                'communName' => 'Modifier les configurations générales',
                'desc' => 'L\'utilisateur peut-il modifier les configurations générales',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'config_edit_instruction' => [
                'ckey' => 'config_edit_instruction',
                'communName' => 'Modifier les configurations de l\'instruction',
                'desc' => 'L\'utilisateur peut-il modifier les configurations de l\'instruction',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'config_edit_administration' => [
                'ckey' => 'config_edit_administration',
                'communName' => 'Modifier les configurations de l\'administration',
                'desc' => 'L\'utilisateur peut-il modifier les configurations de l\'administration',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'config_edit_rank' => [
                'ckey' => 'config_edit_rank',
                'communName' => 'Modifier les configurations des grades',
                'desc' => 'L\'utilisateur peut-il modifier les configurations des grades',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'config_edit_job' => [
                'ckey' => 'config_edit_job',
                'communName' => 'Modifier les configurations des postes',
                'desc' => 'L\'utilisateur peut-il modifier les configurations des postes',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'config_edit_customization' => [
                'ckey' => 'config_edit_customization',
                'communName' => 'Modifier les configurations de l\'apparence du site',
                'desc' => 'L\'utilisateur peut-il modifier les configurations de l\'apparence du site',
                'icon' => 'fa-eye',
                'valeur' => 0
            ]
        ],
        'Statistique' => [
            'stats_see' => [
                'ckey' => 'stats_see',
                'communName' => 'Voir les statistiques',
                'desc' => 'L\'utilisateur peut-il consulter les statistiques',
                'icon' => 'fa-eye',
                'valeur' => 0
            ]
        ],
        'Instruction' => [
            'instruction_guide_see' => [
                'ckey' => 'instruction_guide_see',
                'communName' => 'Voir les guides pédagogiques et normes de qualifications',
                'desc' => 'L\'utilisateur peut-il consulter les guides pédagogiques et normes de qualifications',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'instruction_db_ocom_see' => [
                'ckey' => 'instruction_db_ocom_see',
                'communName' => 'Voir la base de donnée de cours',
                'desc' => 'L\'utilisateur peut-il consulter la base de donnée des cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'instruction_db_ocom_add' => [
                'ckey' => 'instruction_db_ocom_add',
                'communName' => 'Ajouter à la base de donnée de cours',
                'desc' => 'L\'utilisateur peut-il ajouter à la base de donnée des cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'instruction_db_ocom_edit' => [
                'ckey' => 'instruction_db_ocom_edit',
                'communName' => 'Modifier la base de donnée de cours',
                'desc' => 'L\'utilisateur peut-il modifier la base de donnée des cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'instruction_db_ocom_delete' => [
                'ckey' => 'instruction_db_ocom_delete',
                'communName' => 'Supprimer de la base de donnée de cours',
                'desc' => 'L\'utilisateur peut-il supprimer de la base de donnée des cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'course_see_all' => [
                'ckey' => 'course_see_all',
                'communName' => 'Voir les cours de tous les utilisateurs',
                'desc' => 'L\'utilisateur peut-il voir les cours de tous les utilisateurs',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'course_comment_officer' => [
                'ckey' => 'course_comment_officer',
                'communName' => 'Ajouter un commentaire sur n\'importe quel cours',
                'desc' => 'L\'utilisateur peut-il ajouter un commentaire sur n\'importe quel cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'course_validate_plan' => [
                'ckey' => 'course_validate_plan',
                'communName' => 'Valider n\'importe quel plan de cours',
                'desc' => 'L\'utilisateur peut-il valider n\'importe quel plan de cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'course_comment_plan_officer' => [
                'ckey' => 'course_comment_plan_officer',
                'communName' => 'Ajouter un commentaire sur n\'importe quel plan de cours',
                'desc' => 'L\'utilisateur peut-il ajouter un commentaire sur n\'importe quel plan de cours',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'stats_instruction_see' => [
                'ckey' => 'stats_instruction_see',
                'communName' => 'Voir les statistiques de l\'instruction',
                'desc' => 'L\'utilisateur peut-il voir les statistiques de l\'instruction',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'instruction_see_hidden_event' => [
                'ckey' => 'instruction_see_hidden_event',
                'communName' => 'Voir les évenements cachés',
                'desc' => 'L\'utilisateur peut-il voir les évenements cachés',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ],
        'Administration' => [
            'cadet_list_see' => [
                'ckey' => 'cadet_list_see',
                'communName' => 'Voir la liste nominative',
                'desc' => 'L\'utilisateur peut-il voir la liste nominative',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ],
        'Horaire' => [
            'schedule_see' => [
                'ckey' => 'schedule_see',
                'communName' => 'Voir l\'horaire',
                'desc' => 'L\'utilisateur peut-il consulter l\'horaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'schedule_add' => [
                'ckey' => 'schedule_add',
                'communName' => 'Ajouter un évènement à l\'horaire',
                'desc' => 'L\'utilisateur peut-il ajouter un évènement à l\'horaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'schedule_edit' => [
                'ckey' => 'schedule_edit',
                'communName' => 'Modifier un évènement à l\'horaire',
                'desc' => 'L\'utilisateur peut-il modifier un évènement à l\'horaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'schedule_delete' => [
                'ckey' => 'schedule_delete',
                'communName' => 'Supprimer un évènement à l\'horaire',
                'desc' => 'L\'utilisateur peut-il supprimer un évènement à l\'horaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ]
        ],
        'Articles' => [
            'article_see' => [
                'ckey' => 'article_see',
                'communName' => 'Voir les articles',
                'desc' => 'L\'utilisateur peut-il consulter les articles',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'article_add' => [
                'ckey' => 'article_add',
                'communName' => 'Ajouter un article',
                'desc' => 'L\'utilisateur peut-il ajouter un article',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'article_edit' => [
                'ckey' => 'article_edit',
                'communName' => 'Modifier un article',
                'desc' => 'L\'utilisateur peut-il modifier un évènement à l\'horaire',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'article_delete' => [
                'ckey' => 'article_delete',
                'communName' => 'Supprimer un article',
                'desc' => 'L\'utilisateur peut-il supprimer un article',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ],
        'Photos & Images' => [
            'picture_see' => [
                'ckey' => 'picture_see',
                'communName' => 'Voir les images',
                'desc' => 'L\'utilisateur peut-il consulter les images',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'picture_add' => [
                'ckey' => 'picture_add',
                'communName' => 'Ajouter une image',
                'desc' => 'L\'utilisateur peut-il ajouter une image',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'picture_edit' => [
                'ckey' => 'picture_edit',
                'communName' => 'Modifier une image',
                'desc' => 'L\'utilisateur peut-il modifier une image',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'picture_delete' => [
                'ckey' => 'picture_delete',
                'communName' => 'Supprimer une image',
                'desc' => 'L\'utilisateur peut-il supprimer une image',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ],
        'Fichiers' => [
            'file_see' => [
                'ckey' => 'file_see',
                'communName' => 'Voir les fichiers publiques',
                'desc' => 'L\'utilisateur peut-il consulter les fichiers publiques',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
            'drive_see' => [
                'ckey' => 'drive_see',
                'communName' => 'Voir l\'explorateur de fichier',
                'desc' => 'L\'utilisateur peut-il consulter l\'explorateur de fichier',
                'icon' => 'fa-eye',
                'valeur' => 0
            ],
        ]
    ];

    public static function all($value = null,$columns = null)
    {

        $permissions = collect();
        foreach (self::PERMISSIONS as $key => $cat)
        {
            foreach ($cat as $c)
            {
                $p = new Permission();
                $p->ckey = $c['ckey'];
                $p->communName = $c['communName'];
                $p->desc = $c['desc'];
                $p->icon = $c['icon'];
                if ($value == null)
                {
                    $p->value = $c['valeur'];
                }
                else
                {
                    $p->value = $value;
                }
                $permissions->push($p);
            }
        }

        return $permissions;
    }

    public static function allToArray()
    {
        return self::PERMISSIONS;
    }

    public static function allToJSON()
    {
        return self::all()->toJson();
    }

    public static function allToString($value = null)
    {
        $perm = [];
        foreach (Permission::all($value) as $p)
        {
            $perm[$p->ckey] = $p->value;
        }

        return \GuzzleHttp\json_encode($perm);
    }
}

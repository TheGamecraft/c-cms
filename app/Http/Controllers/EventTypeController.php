<?php

namespace App\Http\Controllers;

use App\EventType;
use Illuminate\Http\Request;

class EventTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventType = new EventType();
        $eventType->name = $request->name;
        $eventType->location = $request->location;
        $eventType->begin_time = $request->begin_time;
        $eventType->end_time = $request->end_time;
        $eventType->calendar_icon = $request->calendar_icon;
        $eventType->calendar_color = $request->calendar_color;
        $eventType->admin_desc = $request->admin_desc;

        if ($request->use_weekly_msg == 'on')
        {
            $eventType->use_weekly_msg = 1;
            $eventType->weekly_msg_publication_time = $request->weekly_msg_publication_time;
        }
        else
        {
            $eventType->use_weekly_msg = 0;
            $eventType->weekly_msg_publication_time = '';
        }
        if ($request->use_schedule == 'on')
        {
            $eventType->use_schedule = 1;
        }
        else
        {
            $eventType->use_schedule = 0;
        }
        if(\request("hidden"))
        {
            $eventType->hidden = 1;
        }
        else
        {
            $eventType->hidden = 0;
        }
        if ($request->is_mandatory == 'on')
        {
            $eventType->is_mandatory = 1;
        }
        else
        {
            $eventType->is_mandatory = 0;
        }

        $nbOfLevel = 1;
        while (\request('level_name_'.$nbOfLevel))
        {
            $nbOfLevel++;
        }
        $nbOfLevel = $nbOfLevel-1;

        $nbOfPeriode = 1;
        while (\request('periode_name_'.$nbOfPeriode))
        {
            $nbOfPeriode++;
        }
        $nbOfPeriode = $nbOfPeriode -1;

        $model = [];
        for ($x = 1; $x <= $nbOfPeriode; $x++) {
            $model['periodes'][$x-1] = [
                'name' => \Request('periode_name_'.$x),
                'begin_time' => \Request('periode_begin_time_'.$x),
                'end_time' => \Request('periode_end_time_'.$x)
            ];
        }
        for ($i = 1; $i <= $nbOfLevel; $i++) {
            $model['niveaux'][$i-1] = [
                'name' => \Request('level_name_'.$i)
            ];

            for ($x = 1; $x <= $nbOfPeriode; $x++) {
                $model['default_value'][$x-1][$i-1] = [
                    'ocom' => \Request('ocom_n'.$i.'_p'.$x),
                    'name' => \Request('name_n'.$i.'_p'.$x),
                    'location' => \Request('location_n'.$i.'_p'.$x),
                    'instructor' => \Request('instruc_n'.$i.'_p'.$x),
                    'desc' => \Request('desc_n'.$i.'_p'.$x),
                    'use_course' => \Request('use_course_n'.$i.'_p'.$x),
                ];
            }
        }
        $eventType->schedule_model = $model;
        $eventType->save();
        return redirect('/admin/config/instruction')->with('success','Type d\'évenement ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event_type = EventType::findOrFail($id);
        return view('admin.event_type.show',['event_type' => $event_type]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function edit(EventType $eventType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventType = EventType::findOrFail($id);

        $eventType->name = $request->name;
        $eventType->location = $request->location;
        $eventType->begin_time = $request->begin_time;
        $eventType->end_time = $request->end_time;
        $eventType->calendar_icon = $request->calendar_icon;
        $eventType->calendar_color = $request->calendar_color;
        $eventType->admin_desc = $request->admin_desc;

        if ($request->use_weekly_msg == 'on')
        {
            $eventType->use_weekly_msg = 1;
            $eventType->weekly_msg_publication_time = $request->weekly_msg_publication_time;
        }
        else
        {
            $eventType->use_weekly_msg = 0;
            $eventType->weekly_msg_publication_time = '';
        }
        if ($request->use_schedule == 'on')
        {
            $eventType->use_schedule = 1;
        }
        else
        {
            $eventType->use_schedule = 0;
        }
        if(\request("hidden"))
        {
            $eventType->hidden = 1;
        }
        else
        {
            $eventType->hidden = 0;
        }
        if ($request->is_mandatory == 'on')
        {
            $eventType->is_mandatory = 1;
        }
        else
        {
            $eventType->is_mandatory = 0;
        }

        $nbOfLevel = 1;
        while (\request('level_name_'.$nbOfLevel))
        {
            $nbOfLevel++;
        }
        $nbOfLevel = $nbOfLevel-1;

        $nbOfPeriode = 1;
        while (\request('periode_name_'.$nbOfPeriode))
        {
            $nbOfPeriode++;
        }
        $nbOfPeriode = $nbOfPeriode -1;

        $model = [];
        for ($x = 1; $x <= $nbOfPeriode; $x++) {
            $model['periodes'][$x-1] = [
                'name' => \Request('periode_name_'.$x),
                'begin_time' => \Request('periode_begin_time_'.$x),
                'end_time' => \Request('periode_end_time_'.$x)
            ];
        }
        for ($i = 1; $i <= $nbOfLevel; $i++) {
            $model['niveaux'][$i-1] = [
                'name' => \Request('level_name_'.$i)
            ];

            for ($x = 1; $x <= $nbOfPeriode; $x++) {
                $model['default_value'][$x-1][$i-1] = [
                    'ocom' => \Request('ocom_n'.$i.'_p'.$x),
                    'name' => \Request('name_n'.$i.'_p'.$x),
                    'location' => \Request('location_n'.$i.'_p'.$x),
                    'instructor' => \Request('instruc_n'.$i.'_p'.$x),
                    'desc' => \Request('desc_n'.$i.'_p'.$x),
                    'use_course' => \Request('use_course_n'.$i.'_p'.$x),
                ];
            }
        }
        $eventType->schedule_model = $model;
        $eventType->save();
        return redirect()->back()->with('success','Modification sauvegardé avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $e = EventType::findOrFail($id);
        $e->delete();
    }

    public function toJson($id)
    {
        return EventType::findOrFail($id)->toArray();
    }
}
